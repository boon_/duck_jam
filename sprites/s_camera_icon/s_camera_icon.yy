{
    "id": "1bd886e6-1d1e-4e99-8950-0bd4227a4068",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_camera_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7325a6be-d271-45bd-a387-3328f254177f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bd886e6-1d1e-4e99-8950-0bd4227a4068",
            "compositeImage": {
                "id": "ef668845-71f3-41a8-8318-6fa887351b42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7325a6be-d271-45bd-a387-3328f254177f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6552ceac-c5ec-4164-8064-10e6b2696147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7325a6be-d271-45bd-a387-3328f254177f",
                    "LayerId": "9156e2c2-9284-421c-880d-c4faf3761dfd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9156e2c2-9284-421c-880d-c4faf3761dfd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bd886e6-1d1e-4e99-8950-0bd4227a4068",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}