{
    "id": "923091b8-414f-472a-b007-ce01b604d4f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae90fcc8-4609-4616-bbee-fdb1e4acc37d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "923091b8-414f-472a-b007-ce01b604d4f3",
            "compositeImage": {
                "id": "e8a57587-f9a4-498a-857f-c25f9ad81893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae90fcc8-4609-4616-bbee-fdb1e4acc37d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ae075d2-204b-4383-acf5-f7413a9169ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae90fcc8-4609-4616-bbee-fdb1e4acc37d",
                    "LayerId": "73e6da66-ec31-40b7-a02f-256a0ed8e2c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "73e6da66-ec31-40b7-a02f-256a0ed8e2c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "923091b8-414f-472a-b007-ce01b604d4f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 5,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 47,
    "xorig": 46,
    "yorig": 13
}