{
    "id": "b6a098ee-6103-4a7a-b16d-0b295f60a9db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_drumstick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbbfb5ad-2cff-485b-9c47-148ee0a98600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6a098ee-6103-4a7a-b16d-0b295f60a9db",
            "compositeImage": {
                "id": "58967762-8614-4ddc-8e89-9afe604aff09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbbfb5ad-2cff-485b-9c47-148ee0a98600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2103ef68-a464-41b4-827f-a721a7793747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbbfb5ad-2cff-485b-9c47-148ee0a98600",
                    "LayerId": "9fea6597-b70d-48ba-b1c5-8b25eb524779"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9fea6597-b70d-48ba-b1c5-8b25eb524779",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6a098ee-6103-4a7a-b16d-0b295f60a9db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 20,
    "yorig": 7
}