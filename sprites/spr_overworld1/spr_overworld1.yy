{
    "id": "2eceee8c-74c1-4398-8dc6-9d4428e46826",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_overworld1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 146,
    "bbox_left": 89,
    "bbox_right": 242,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56c74234-2374-477f-a284-1a431c44f983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eceee8c-74c1-4398-8dc6-9d4428e46826",
            "compositeImage": {
                "id": "d43185e9-5bc2-4520-b89f-40c30c903145",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c74234-2374-477f-a284-1a431c44f983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5431b37-82b5-47a3-9d5c-fe12b34ce9b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c74234-2374-477f-a284-1a431c44f983",
                    "LayerId": "dd3b917b-ec05-492c-8d28-f86f15dd9e8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "dd3b917b-ec05-492c-8d28-f86f15dd9e8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2eceee8c-74c1-4398-8dc6-9d4428e46826",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}