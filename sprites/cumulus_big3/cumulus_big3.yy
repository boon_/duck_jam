{
    "id": "1abff63a-edd1-4001-b387-7529419dd6f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cumulus_big3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 124,
    "bbox_left": 4,
    "bbox_right": 225,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8313384-f84b-45be-acfd-1c93a2f29333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abff63a-edd1-4001-b387-7529419dd6f6",
            "compositeImage": {
                "id": "df1d7a9f-8c6d-4443-b666-3975afe692f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8313384-f84b-45be-acfd-1c93a2f29333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dcc945a-03f5-4f5e-97d9-cbf6459ff6fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8313384-f84b-45be-acfd-1c93a2f29333",
                    "LayerId": "bd2ce7a4-b8f6-44f6-83a6-d9ab4697e018"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bd2ce7a4-b8f6-44f6-83a6-d9ab4697e018",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1abff63a-edd1-4001-b387-7529419dd6f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 234,
    "xorig": 0,
    "yorig": 0
}