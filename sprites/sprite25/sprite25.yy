{
    "id": "2de2eab6-07f4-4ce8-8526-a3a5333ca2c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite25",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a3772f7-5c7a-48cb-a105-6353c2a928bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2de2eab6-07f4-4ce8-8526-a3a5333ca2c8",
            "compositeImage": {
                "id": "463668bc-635c-46df-9022-0f9338fb4dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a3772f7-5c7a-48cb-a105-6353c2a928bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f965ee-2daf-489a-bbaf-3f1f7844a0ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a3772f7-5c7a-48cb-a105-6353c2a928bf",
                    "LayerId": "d30e45f2-644d-464f-906d-e88aee0876bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d30e45f2-644d-464f-906d-e88aee0876bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2de2eab6-07f4-4ce8-8526-a3a5333ca2c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}