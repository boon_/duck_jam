{
    "id": "b16ffeeb-5dc7-4d79-9bc3-fe999669ad81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cumulus_small2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 73,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8420be36-0e41-4dcc-8f37-d429aea9b03e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b16ffeeb-5dc7-4d79-9bc3-fe999669ad81",
            "compositeImage": {
                "id": "0ea53c01-0471-451e-a693-12a61cec88ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8420be36-0e41-4dcc-8f37-d429aea9b03e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdf679e7-8ae8-4298-92b3-4cb6b5f1fefa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8420be36-0e41-4dcc-8f37-d429aea9b03e",
                    "LayerId": "6feda0b5-c2f0-4b31-a848-6f346f137663"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "6feda0b5-c2f0-4b31-a848-6f346f137663",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b16ffeeb-5dc7-4d79-9bc3-fe999669ad81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 76,
    "xorig": 0,
    "yorig": 0
}