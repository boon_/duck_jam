{
    "id": "51cf5c62-26b0-4376-aa5b-4a7ec21a3d42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d291ed1f-2a8c-41e6-806e-253ca6dd0ff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51cf5c62-26b0-4376-aa5b-4a7ec21a3d42",
            "compositeImage": {
                "id": "d7581c03-91b2-40ca-8129-6f3c27f20154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d291ed1f-2a8c-41e6-806e-253ca6dd0ff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60df604a-3699-4166-8ae2-db6dd5b6c5f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d291ed1f-2a8c-41e6-806e-253ca6dd0ff6",
                    "LayerId": "0a3d68b9-0b0c-47cf-a99a-f9558544b831"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0a3d68b9-0b0c-47cf-a99a-f9558544b831",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51cf5c62-26b0-4376-aa5b-4a7ec21a3d42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}