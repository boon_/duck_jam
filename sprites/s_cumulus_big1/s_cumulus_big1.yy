{
    "id": "746f4fc7-4476-4a11-92cc-01bdfd822e09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_cumulus_big1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 147,
    "bbox_left": 9,
    "bbox_right": 161,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1267f421-4b93-4bcf-b285-e89aa0dcd28f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "746f4fc7-4476-4a11-92cc-01bdfd822e09",
            "compositeImage": {
                "id": "f4c85a8a-10e2-4c34-9b34-2dacc3c2ac92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1267f421-4b93-4bcf-b285-e89aa0dcd28f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2287d054-6a3e-40a6-816c-40adac8ae770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1267f421-4b93-4bcf-b285-e89aa0dcd28f",
                    "LayerId": "37003669-479c-4af3-8ee2-a1754188379c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 157,
    "layers": [
        {
            "id": "37003669-479c-4af3-8ee2-a1754188379c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "746f4fc7-4476-4a11-92cc-01bdfd822e09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 174,
    "xorig": 0,
    "yorig": 0
}