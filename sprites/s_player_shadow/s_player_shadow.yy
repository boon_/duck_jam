{
    "id": "97b3283b-c79e-4185-9bc6-4b20e7baf09d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8261889c-3c74-40f3-aa77-2a40b1668942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97b3283b-c79e-4185-9bc6-4b20e7baf09d",
            "compositeImage": {
                "id": "75aa3947-7099-49da-956f-de561c2f068f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8261889c-3c74-40f3-aa77-2a40b1668942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f49b0f24-1656-42dd-bf86-cf5fe7b2afc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8261889c-3c74-40f3-aa77-2a40b1668942",
                    "LayerId": "d91e3554-3913-42e9-a41b-ce7e529d4d76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "d91e3554-3913-42e9-a41b-ce7e529d4d76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97b3283b-c79e-4185-9bc6-4b20e7baf09d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 3
}