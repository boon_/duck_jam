{
    "id": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_explorer_goose_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 13,
    "bbox_right": 26,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ebab17f-ca8d-42cd-8dbc-a2289d898312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
            "compositeImage": {
                "id": "d9666a8c-5c63-4d32-95b6-c46fd36dff4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ebab17f-ca8d-42cd-8dbc-a2289d898312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "329e22f8-6164-4e5c-b704-7c6ed9705b57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ebab17f-ca8d-42cd-8dbc-a2289d898312",
                    "LayerId": "f2482bb9-890b-483a-a3c5-f192d2201ce9"
                }
            ]
        },
        {
            "id": "4fbdd384-be3e-49a8-80af-888f65a42622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
            "compositeImage": {
                "id": "ead4c1e5-b6f5-40f7-8c91-543ab094cae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fbdd384-be3e-49a8-80af-888f65a42622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb8ae9a-2b25-49a8-b083-ffa356f88522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fbdd384-be3e-49a8-80af-888f65a42622",
                    "LayerId": "f2482bb9-890b-483a-a3c5-f192d2201ce9"
                }
            ]
        },
        {
            "id": "76e26fac-5d6d-4bb4-8b36-ad78c52665df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
            "compositeImage": {
                "id": "dda90e8e-45b0-4a73-b15c-6b18f44d871a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e26fac-5d6d-4bb4-8b36-ad78c52665df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a68444c-ef73-4d32-aa95-cf87669a06a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e26fac-5d6d-4bb4-8b36-ad78c52665df",
                    "LayerId": "f2482bb9-890b-483a-a3c5-f192d2201ce9"
                }
            ]
        },
        {
            "id": "d6db2a1a-fe2b-456b-9366-55c1089a8c70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
            "compositeImage": {
                "id": "5c263257-e40f-431a-abd6-624e3e7bdc65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6db2a1a-fe2b-456b-9366-55c1089a8c70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29d86f06-4de5-4c8a-9f2d-5e09b7eed033",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6db2a1a-fe2b-456b-9366-55c1089a8c70",
                    "LayerId": "f2482bb9-890b-483a-a3c5-f192d2201ce9"
                }
            ]
        },
        {
            "id": "960eba4e-589a-4358-87e4-82b614afbf5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
            "compositeImage": {
                "id": "20c95288-64f9-44ba-b2f5-1254411d6e49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960eba4e-589a-4358-87e4-82b614afbf5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "338d259e-fc94-4bce-9e71-05f11204fcb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960eba4e-589a-4358-87e4-82b614afbf5f",
                    "LayerId": "f2482bb9-890b-483a-a3c5-f192d2201ce9"
                }
            ]
        },
        {
            "id": "13abf368-9cef-4292-8914-43142446337d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
            "compositeImage": {
                "id": "09ac27c7-1351-4472-aac8-bbcfaf1882af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13abf368-9cef-4292-8914-43142446337d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b4dbaf6-1e50-4973-ba89-7a1c5e6612da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13abf368-9cef-4292-8914-43142446337d",
                    "LayerId": "f2482bb9-890b-483a-a3c5-f192d2201ce9"
                }
            ]
        },
        {
            "id": "d01b3071-ee98-47f0-a93e-2ece74e8a63b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
            "compositeImage": {
                "id": "0010de44-f588-4799-a126-351b9e7f0b38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d01b3071-ee98-47f0-a93e-2ece74e8a63b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23777981-2bc7-42d2-9186-5eef35ce2510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01b3071-ee98-47f0-a93e-2ece74e8a63b",
                    "LayerId": "f2482bb9-890b-483a-a3c5-f192d2201ce9"
                }
            ]
        },
        {
            "id": "c9d292eb-2dfb-4754-830e-532f5bb1932f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
            "compositeImage": {
                "id": "bd6ffb99-7816-472c-80ec-14c020b93577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9d292eb-2dfb-4754-830e-532f5bb1932f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cc04f9e-8cb8-4a9b-a5e3-ac1d3ea41d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9d292eb-2dfb-4754-830e-532f5bb1932f",
                    "LayerId": "f2482bb9-890b-483a-a3c5-f192d2201ce9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "f2482bb9-890b-483a-a3c5-f192d2201ce9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 64
}