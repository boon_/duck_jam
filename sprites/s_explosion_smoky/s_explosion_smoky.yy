{
    "id": "a9e4cdbe-39d7-4b72-aeea-73e0502695d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_explosion_smoky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 45,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80feb837-8a98-46ec-8590-d50fd3cb45c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e4cdbe-39d7-4b72-aeea-73e0502695d6",
            "compositeImage": {
                "id": "d80ebd18-5ffe-4c29-b84c-64fcabbb5bfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80feb837-8a98-46ec-8590-d50fd3cb45c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5798e06b-af52-4b4d-a061-2c61429060b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80feb837-8a98-46ec-8590-d50fd3cb45c9",
                    "LayerId": "4029ee2a-96a6-4eb9-bda3-9835eaa1fe37"
                }
            ]
        },
        {
            "id": "2901a3f2-0239-41f5-ba97-8aae44de67b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e4cdbe-39d7-4b72-aeea-73e0502695d6",
            "compositeImage": {
                "id": "4ce0bad1-c183-4adb-8fe5-3c40d1379b42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2901a3f2-0239-41f5-ba97-8aae44de67b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c61587b-badd-4dbb-b509-698d609b5daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2901a3f2-0239-41f5-ba97-8aae44de67b4",
                    "LayerId": "4029ee2a-96a6-4eb9-bda3-9835eaa1fe37"
                }
            ]
        },
        {
            "id": "3ee4a4ea-8e26-4a61-8ce0-5ddcad45e95b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e4cdbe-39d7-4b72-aeea-73e0502695d6",
            "compositeImage": {
                "id": "74e142d4-53ab-42d3-b9d7-66b8dc4ffd99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ee4a4ea-8e26-4a61-8ce0-5ddcad45e95b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80be41ed-9089-4cc8-a073-80ec23880d49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee4a4ea-8e26-4a61-8ce0-5ddcad45e95b",
                    "LayerId": "4029ee2a-96a6-4eb9-bda3-9835eaa1fe37"
                }
            ]
        },
        {
            "id": "fc6703ad-410d-4a29-8cf6-a20fb4bead9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e4cdbe-39d7-4b72-aeea-73e0502695d6",
            "compositeImage": {
                "id": "3091d6db-d788-42d4-bbf8-474f429bc7a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc6703ad-410d-4a29-8cf6-a20fb4bead9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "191cd3b5-f9a9-4518-8233-ec668b469009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc6703ad-410d-4a29-8cf6-a20fb4bead9c",
                    "LayerId": "4029ee2a-96a6-4eb9-bda3-9835eaa1fe37"
                }
            ]
        },
        {
            "id": "e405008d-5d13-4c7b-82b3-e3455c9daf71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e4cdbe-39d7-4b72-aeea-73e0502695d6",
            "compositeImage": {
                "id": "15fa670e-5f0d-40f7-bc7c-516e16b5d6c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e405008d-5d13-4c7b-82b3-e3455c9daf71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a8410c-b853-42d4-a495-0964d87aba99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e405008d-5d13-4c7b-82b3-e3455c9daf71",
                    "LayerId": "4029ee2a-96a6-4eb9-bda3-9835eaa1fe37"
                }
            ]
        },
        {
            "id": "d8efaf6f-b470-4868-b481-54d5d2461b27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e4cdbe-39d7-4b72-aeea-73e0502695d6",
            "compositeImage": {
                "id": "a524c1e3-5ec8-4d9b-8a6d-c73a704d4779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8efaf6f-b470-4868-b481-54d5d2461b27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ca406a7-ba3c-46cc-b8fe-343cc66a972a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8efaf6f-b470-4868-b481-54d5d2461b27",
                    "LayerId": "4029ee2a-96a6-4eb9-bda3-9835eaa1fe37"
                }
            ]
        },
        {
            "id": "1307d3e1-ac5d-4e4d-8716-ee67a0bedc1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e4cdbe-39d7-4b72-aeea-73e0502695d6",
            "compositeImage": {
                "id": "d927cd82-781f-4a08-bc0d-caa2859ab47a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1307d3e1-ac5d-4e4d-8716-ee67a0bedc1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3108ee12-4457-4677-8e83-8d9d9f104db8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1307d3e1-ac5d-4e4d-8716-ee67a0bedc1c",
                    "LayerId": "4029ee2a-96a6-4eb9-bda3-9835eaa1fe37"
                }
            ]
        },
        {
            "id": "01658006-440c-48e9-948d-45817a694619",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e4cdbe-39d7-4b72-aeea-73e0502695d6",
            "compositeImage": {
                "id": "b42e9fd4-d7ad-4b8c-aca3-c22a4a6f45f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01658006-440c-48e9-948d-45817a694619",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5bfa35-6e83-4e67-a4db-922004715fc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01658006-440c-48e9-948d-45817a694619",
                    "LayerId": "4029ee2a-96a6-4eb9-bda3-9835eaa1fe37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4029ee2a-96a6-4eb9-bda3-9835eaa1fe37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9e4cdbe-39d7-4b72-aeea-73e0502695d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}