{
    "id": "2a1d58c8-2f8c-4f19-9138-808efcce45cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spelunky_shop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d39ff52f-d40e-42b2-a19f-9ea173e47dfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a1d58c8-2f8c-4f19-9138-808efcce45cc",
            "compositeImage": {
                "id": "e4fb7db6-f1ec-4a45-a97e-929acd63ccad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d39ff52f-d40e-42b2-a19f-9ea173e47dfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dca91115-c9ff-44c7-a721-393d2b5cc2a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d39ff52f-d40e-42b2-a19f-9ea173e47dfa",
                    "LayerId": "2d2abbfb-8b16-4958-87f5-2693b0a65c50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "2d2abbfb-8b16-4958-87f5-2693b0a65c50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a1d58c8-2f8c-4f19-9138-808efcce45cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}