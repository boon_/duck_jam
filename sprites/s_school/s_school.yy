{
    "id": "622609a1-1c0b-450e-9703-d54fd08632d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_school",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf324c45-dacb-49c7-8272-55b624a7c472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622609a1-1c0b-450e-9703-d54fd08632d1",
            "compositeImage": {
                "id": "99915abc-0060-4eed-a2ed-9ad14a1c7e9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf324c45-dacb-49c7-8272-55b624a7c472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "371c9f68-5686-4883-b2aa-6c12e6ed8aeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf324c45-dacb-49c7-8272-55b624a7c472",
                    "LayerId": "f4a2f36f-0880-4798-8878-efc2841fef3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "f4a2f36f-0880-4798-8878-efc2841fef3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "622609a1-1c0b-450e-9703-d54fd08632d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}