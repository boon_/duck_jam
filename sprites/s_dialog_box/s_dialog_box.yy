{
    "id": "1fc2c55c-eca3-4680-931f-e222f66b8ecd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dialog_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d199705-b6bd-46ae-bb7e-88bcecd67655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fc2c55c-eca3-4680-931f-e222f66b8ecd",
            "compositeImage": {
                "id": "23341a35-68e6-4305-887c-6e3423d0dccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d199705-b6bd-46ae-bb7e-88bcecd67655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "307d1c5d-d714-4785-944e-84e684c7f171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d199705-b6bd-46ae-bb7e-88bcecd67655",
                    "LayerId": "6e77b1d3-0e3a-43c9-88ad-c62e1c016c5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "6e77b1d3-0e3a-43c9-88ad-c62e1c016c5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fc2c55c-eca3-4680-931f-e222f66b8ecd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 0,
    "yorig": 0
}