{
    "id": "2b1213bf-44d5-40c9-8179-24ea1c7f0517",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_audio_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73cc9e97-7a48-44f6-bf96-75053888adab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b1213bf-44d5-40c9-8179-24ea1c7f0517",
            "compositeImage": {
                "id": "4b83ce40-1107-4a84-874e-3b44554a2cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73cc9e97-7a48-44f6-bf96-75053888adab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62606280-cadc-452a-850c-7f0618516688",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73cc9e97-7a48-44f6-bf96-75053888adab",
                    "LayerId": "55f10bb6-020f-4937-936b-3e0eccde793e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "55f10bb6-020f-4937-936b-3e0eccde793e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b1213bf-44d5-40c9-8179-24ea1c7f0517",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}