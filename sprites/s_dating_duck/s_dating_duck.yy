{
    "id": "ecbf762f-2a68-48bd-af1e-7152e445e680",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dating_duck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 513,
    "bbox_left": 148,
    "bbox_right": 512,
    "bbox_top": 63,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7453369f-9099-4ac8-ad95-8b4c54e841b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecbf762f-2a68-48bd-af1e-7152e445e680",
            "compositeImage": {
                "id": "59d490f1-c697-4fcd-9efa-0d2b9a228375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7453369f-9099-4ac8-ad95-8b4c54e841b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39725e97-883c-4443-bb5e-e1763275b2d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7453369f-9099-4ac8-ad95-8b4c54e841b4",
                    "LayerId": "23aab189-f6ab-41bb-a48b-c0e78446f45b"
                }
            ]
        },
        {
            "id": "8a287114-6764-43fc-9f1b-e2e76d356abd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecbf762f-2a68-48bd-af1e-7152e445e680",
            "compositeImage": {
                "id": "fa8ccd70-410c-4051-bf5e-f1713b7162d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a287114-6764-43fc-9f1b-e2e76d356abd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d95b21-5394-42ad-87bf-b7dcc940f454",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a287114-6764-43fc-9f1b-e2e76d356abd",
                    "LayerId": "23aab189-f6ab-41bb-a48b-c0e78446f45b"
                }
            ]
        },
        {
            "id": "91efc376-4524-4cbd-b7e5-74beac39867e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecbf762f-2a68-48bd-af1e-7152e445e680",
            "compositeImage": {
                "id": "47d5fb15-3bcf-4644-8f90-2850035f3cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91efc376-4524-4cbd-b7e5-74beac39867e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd95b52c-aab4-47a8-a16c-11aeda8222f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91efc376-4524-4cbd-b7e5-74beac39867e",
                    "LayerId": "23aab189-f6ab-41bb-a48b-c0e78446f45b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 514,
    "layers": [
        {
            "id": "23aab189-f6ab-41bb-a48b-c0e78446f45b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecbf762f-2a68-48bd-af1e-7152e445e680",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 576,
    "xorig": 288,
    "yorig": 513
}