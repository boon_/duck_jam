{
    "id": "5ecf4525-e695-457b-ab98-df23590d9118",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spotlight_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8cc6959-e0d7-43f7-a91a-bd50c06c537a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ecf4525-e695-457b-ab98-df23590d9118",
            "compositeImage": {
                "id": "482732fc-abbe-4cf3-8771-c964f1c3b878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8cc6959-e0d7-43f7-a91a-bd50c06c537a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d949541f-c7b7-4b12-a9c7-30a242d2533b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8cc6959-e0d7-43f7-a91a-bd50c06c537a",
                    "LayerId": "ab22919a-f203-465d-8770-b49cd6577fa7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "ab22919a-f203-465d-8770-b49cd6577fa7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ecf4525-e695-457b-ab98-df23590d9118",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 261,
    "yorig": 129
}