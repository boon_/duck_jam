{
    "id": "09318fd0-85f0-4e0d-965e-430ef723799a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 205,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "832ea444-3d58-4d68-a3f5-de1882ae165b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09318fd0-85f0-4e0d-965e-430ef723799a",
            "compositeImage": {
                "id": "ecc0dce9-8d80-43ca-a4a8-4e0f1598bc10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "832ea444-3d58-4d68-a3f5-de1882ae165b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6666f4e4-2dfa-4bc6-9cd3-99721d406fae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "832ea444-3d58-4d68-a3f5-de1882ae165b",
                    "LayerId": "af22bcf2-0793-43e0-a616-01cc32129f08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "af22bcf2-0793-43e0-a616-01cc32129f08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09318fd0-85f0-4e0d-965e-430ef723799a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 206,
    "xorig": 0,
    "yorig": 0
}