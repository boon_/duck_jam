{
    "id": "6480ba9b-5742-42e2-9890-66f861094553",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cumulus_small1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69801582-113e-4641-8862-7e157b878be5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6480ba9b-5742-42e2-9890-66f861094553",
            "compositeImage": {
                "id": "d7fe3045-33ce-4e78-bd55-4fa44441008c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69801582-113e-4641-8862-7e157b878be5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "854e046e-2fac-463a-85c6-004ed89b1527",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69801582-113e-4641-8862-7e157b878be5",
                    "LayerId": "106e2c16-df88-481d-ad15-8e9472589035"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "106e2c16-df88-481d-ad15-8e9472589035",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6480ba9b-5742-42e2-9890-66f861094553",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 0,
    "yorig": 0
}