{
    "id": "8c17f5c2-1c3e-4fef-9224-4342570099a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cumulus_tiny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 2,
    "bbox_right": 117,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b439342-3118-4d1c-85b9-e031c785306f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c17f5c2-1c3e-4fef-9224-4342570099a0",
            "compositeImage": {
                "id": "5d8a49e0-79d6-4450-92df-0c83aedb3ec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b439342-3118-4d1c-85b9-e031c785306f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c2e7a7e-153e-4ce1-affa-05e99999d2f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b439342-3118-4d1c-85b9-e031c785306f",
                    "LayerId": "becdbbbb-2a78-48a6-91eb-515c9537cb15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "becdbbbb-2a78-48a6-91eb-515c9537cb15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c17f5c2-1c3e-4fef-9224-4342570099a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}