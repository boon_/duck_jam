{
    "id": "0b67c53d-e68a-4081-b1c4-dbdb6e179141",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_overworld",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "656a533f-9375-4c7c-b9af-49a0045c1d2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b67c53d-e68a-4081-b1c4-dbdb6e179141",
            "compositeImage": {
                "id": "d506bca5-8555-420e-af53-64c4dc36cc7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "656a533f-9375-4c7c-b9af-49a0045c1d2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28706e38-0827-40bb-897e-7c9677c0aa0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "656a533f-9375-4c7c-b9af-49a0045c1d2d",
                    "LayerId": "12a34cc9-b77f-4578-9012-f6c23342e71c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "12a34cc9-b77f-4578-9012-f6c23342e71c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b67c53d-e68a-4081-b1c4-dbdb6e179141",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}