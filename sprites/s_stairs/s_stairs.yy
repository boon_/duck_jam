{
    "id": "e759611c-cfbf-4e2d-b5d9-a451a93971fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_stairs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22b0dd8f-27c7-435b-9bc9-b8a35599c3d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e759611c-cfbf-4e2d-b5d9-a451a93971fb",
            "compositeImage": {
                "id": "d30588b2-7b37-42c4-ab61-9e97c0432064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22b0dd8f-27c7-435b-9bc9-b8a35599c3d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea3cc9ac-bbc8-4471-8b34-6d62ef2ce31d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22b0dd8f-27c7-435b-9bc9-b8a35599c3d4",
                    "LayerId": "0f481feb-488f-4fb6-aa65-538aa70825e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "0f481feb-488f-4fb6-aa65-538aa70825e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e759611c-cfbf-4e2d-b5d9-a451a93971fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}