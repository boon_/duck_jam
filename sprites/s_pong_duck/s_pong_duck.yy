{
    "id": "30179223-68f0-4e3c-8af9-8331ecab51cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pong_duck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bd033a7-2465-44c5-9a49-ffe0d96570f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30179223-68f0-4e3c-8af9-8331ecab51cd",
            "compositeImage": {
                "id": "a1c49039-43b9-4506-866f-55ff284ad150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd033a7-2465-44c5-9a49-ffe0d96570f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef5f4416-daa5-4314-89f7-a05324b74508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd033a7-2465-44c5-9a49-ffe0d96570f1",
                    "LayerId": "b8f2d8de-a373-4b07-bffe-df0d3f9d59f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b8f2d8de-a373-4b07-bffe-df0d3f9d59f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30179223-68f0-4e3c-8af9-8331ecab51cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 12,
    "yorig": 18
}