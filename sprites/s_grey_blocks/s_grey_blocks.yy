{
    "id": "054c1157-0b3f-4cd3-add1-ff24f6adda9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_grey_blocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2739629-8618-4f4b-a941-913f98fab889",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "054c1157-0b3f-4cd3-add1-ff24f6adda9a",
            "compositeImage": {
                "id": "33e0b97f-795b-4c28-891e-f027b872d758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2739629-8618-4f4b-a941-913f98fab889",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "361d143c-1e19-4df6-829f-01e8c5b8c11b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2739629-8618-4f4b-a941-913f98fab889",
                    "LayerId": "6c7e1656-0b7a-4fa8-a59d-de7c1f0772b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6c7e1656-0b7a-4fa8-a59d-de7c1f0772b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "054c1157-0b3f-4cd3-add1-ff24f6adda9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}