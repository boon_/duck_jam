{
    "id": "e405f91a-3f93-40ab-b56b-9ebd67842f16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spotlight_stage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b020a999-b295-4eb4-8fcb-89854d659e30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e405f91a-3f93-40ab-b56b-9ebd67842f16",
            "compositeImage": {
                "id": "60298af1-a45b-4691-aa0e-ef5d449e0fb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b020a999-b295-4eb4-8fcb-89854d659e30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "156be6d9-4bdf-419f-9e92-259c3489edd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b020a999-b295-4eb4-8fcb-89854d659e30",
                    "LayerId": "6f087044-d59e-484c-8f51-54f90eb220ae"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 224,
    "layers": [
        {
            "id": "6f087044-d59e-484c-8f51-54f90eb220ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e405f91a-3f93-40ab-b56b-9ebd67842f16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}