{
    "id": "720bcfdc-35a0-4758-8c44-36dd735a513d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bread",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8126f6d3-5887-44cb-a116-b5aaebe8ea14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "720bcfdc-35a0-4758-8c44-36dd735a513d",
            "compositeImage": {
                "id": "50b3736a-eaac-48bd-9aa1-e5599926f7df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8126f6d3-5887-44cb-a116-b5aaebe8ea14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "496143dc-dc61-492e-ab1f-380a2ac810d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8126f6d3-5887-44cb-a116-b5aaebe8ea14",
                    "LayerId": "a2b3659a-6c1d-4fb3-b182-a7924708ef6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "a2b3659a-6c1d-4fb3-b182-a7924708ef6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "720bcfdc-35a0-4758-8c44-36dd735a513d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 7,
    "yorig": 7
}