{
    "id": "c05184e6-d363-4499-825f-3f54a4e1dd6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_overworld11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 219,
    "bbox_left": 18,
    "bbox_right": 248,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82e61be2-10d3-400d-bcfe-3c17d409ac34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c05184e6-d363-4499-825f-3f54a4e1dd6b",
            "compositeImage": {
                "id": "6ca63e93-4647-491b-84d0-8d5f771627ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e61be2-10d3-400d-bcfe-3c17d409ac34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18bc16fe-5cff-4557-acae-e97ad4254a0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e61be2-10d3-400d-bcfe-3c17d409ac34",
                    "LayerId": "e1ccb42d-5ac4-4dbd-82f0-68fa76aecc41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "e1ccb42d-5ac4-4dbd-82f0-68fa76aecc41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c05184e6-d363-4499-825f-3f54a4e1dd6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}