{
    "id": "f604d5ae-9649-4f61-a05d-b3947fb515b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ground_blocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 16,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e43e3ea0-bbc9-4963-a2dd-71b967f6d056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f604d5ae-9649-4f61-a05d-b3947fb515b0",
            "compositeImage": {
                "id": "04c50739-3ff6-45c6-b212-ab38ea26e697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e43e3ea0-bbc9-4963-a2dd-71b967f6d056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bef6e23-0903-4504-9949-c925355f1d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e43e3ea0-bbc9-4963-a2dd-71b967f6d056",
                    "LayerId": "76e90f37-3c0a-48b8-8087-74e35b14a47f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "76e90f37-3c0a-48b8-8087-74e35b14a47f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f604d5ae-9649-4f61-a05d-b3947fb515b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}