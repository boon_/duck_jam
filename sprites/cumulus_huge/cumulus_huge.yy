{
    "id": "93fac55b-c59a-493b-9f0b-45ea85249010",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cumulus_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 207,
    "bbox_left": 5,
    "bbox_right": 507,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36f2fda2-373f-4d06-a2fb-970e5b1ca4e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93fac55b-c59a-493b-9f0b-45ea85249010",
            "compositeImage": {
                "id": "262e832e-e22b-44d1-879f-c16b6bfe4b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36f2fda2-373f-4d06-a2fb-970e5b1ca4e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "976a2b6f-e22d-4a1c-b920-9976b942cd2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36f2fda2-373f-4d06-a2fb-970e5b1ca4e7",
                    "LayerId": "bc783f2b-61b5-4079-b18a-dbadc082f9c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 211,
    "layers": [
        {
            "id": "bc783f2b-61b5-4079-b18a-dbadc082f9c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93fac55b-c59a-493b-9f0b-45ea85249010",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}