{
    "id": "0e9b1cac-b62a-489a-b8ba-d18fdc96720f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_photo_realistic_duck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 11,
    "bbox_right": 36,
    "bbox_top": 34,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6e53f38-5521-404a-96d5-eae68b79e4cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9b1cac-b62a-489a-b8ba-d18fdc96720f",
            "compositeImage": {
                "id": "6b581666-1cb1-4a3b-ba6a-6d4b8b35d3d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6e53f38-5521-404a-96d5-eae68b79e4cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14db5794-bde3-4f44-9aff-7e7c601241e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6e53f38-5521-404a-96d5-eae68b79e4cb",
                    "LayerId": "fff26f59-fd9f-4e83-908f-558a108e9ad3"
                }
            ]
        },
        {
            "id": "fba691d7-668b-4500-b9ea-34d43e54b33e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9b1cac-b62a-489a-b8ba-d18fdc96720f",
            "compositeImage": {
                "id": "e0178c36-b93b-4f5e-b1d3-5905119ba8a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba691d7-668b-4500-b9ea-34d43e54b33e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0acd2b92-2cbc-4e40-9df9-0a5cd8f04fff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba691d7-668b-4500-b9ea-34d43e54b33e",
                    "LayerId": "fff26f59-fd9f-4e83-908f-558a108e9ad3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 73,
    "layers": [
        {
            "id": "fff26f59-fd9f-4e83-908f-558a108e9ad3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e9b1cac-b62a-489a-b8ba-d18fdc96720f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 0,
    "yorig": 0
}