{
    "id": "83a6150f-e535-4458-ac6d-58d1f18b142a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cumulus_big2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 7,
    "bbox_right": 181,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48b7ad23-b4e9-417a-bcdd-dd0178107491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83a6150f-e535-4458-ac6d-58d1f18b142a",
            "compositeImage": {
                "id": "8600af39-b442-4639-becb-368bebb0f2eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48b7ad23-b4e9-417a-bcdd-dd0178107491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b5207ef-aa5e-4d3b-8254-f19dd619ea4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b7ad23-b4e9-417a-bcdd-dd0178107491",
                    "LayerId": "e0573f9c-470e-4c71-99db-7e420991b573"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 118,
    "layers": [
        {
            "id": "e0573f9c-470e-4c71-99db-7e420991b573",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83a6150f-e535-4458-ac6d-58d1f18b142a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 190,
    "xorig": 0,
    "yorig": 0
}