{
    "id": "ce5ab77e-9d95-4a6e-b2e2-06adc1f4ca4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cumulus_small3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01cf73c7-1f94-4cc7-afb9-856b5da2bd72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce5ab77e-9d95-4a6e-b2e2-06adc1f4ca4d",
            "compositeImage": {
                "id": "2e5f9c9b-6ab3-432b-abb4-f8f549b22edd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01cf73c7-1f94-4cc7-afb9-856b5da2bd72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1c1ceba-9c72-41a4-91bb-be64426eeb9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01cf73c7-1f94-4cc7-afb9-856b5da2bd72",
                    "LayerId": "afd57569-1402-4d2e-a7a0-5e8f968c8715"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "afd57569-1402-4d2e-a7a0-5e8f968c8715",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce5ab77e-9d95-4a6e-b2e2-06adc1f4ca4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}