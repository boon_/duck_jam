{
    "id": "4e54673f-c158-45d7-96f3-6639464dead8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_white_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9704a3d4-e057-43d0-a8ea-f84aa995e460",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e54673f-c158-45d7-96f3-6639464dead8",
            "compositeImage": {
                "id": "08c59b69-e9d7-4f06-b400-f140b7747640",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9704a3d4-e057-43d0-a8ea-f84aa995e460",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e4f36f0-826a-4054-9514-56fac44af0fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9704a3d4-e057-43d0-a8ea-f84aa995e460",
                    "LayerId": "a8ed130a-ab73-410b-958f-00078bdfd5c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a8ed130a-ab73-410b-958f-00078bdfd5c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e54673f-c158-45d7-96f3-6639464dead8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}