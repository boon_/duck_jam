{
    "id": "c12f51eb-ca55-4049-849b-f6cbdf28e251",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_game_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f616ef3-8c25-4dd3-a480-01540e6a1cd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c12f51eb-ca55-4049-849b-f6cbdf28e251",
            "compositeImage": {
                "id": "d6e7d18d-bfb6-4b93-b41f-3972ac50cabe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f616ef3-8c25-4dd3-a480-01540e6a1cd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95797d63-6857-4bf8-8773-4900dc7fe101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f616ef3-8c25-4dd3-a480-01540e6a1cd2",
                    "LayerId": "26f7f204-0fbc-482a-9d75-a072b2ce0424"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "26f7f204-0fbc-482a-9d75-a072b2ce0424",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c12f51eb-ca55-4049-849b-f6cbdf28e251",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}