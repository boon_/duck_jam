{
    "id": "05e64841-9f30-44fe-b3a1-eb341c9c2128",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_input_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22a42863-f238-4e62-b66b-4259310aacb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05e64841-9f30-44fe-b3a1-eb341c9c2128",
            "compositeImage": {
                "id": "b593f5e6-ce01-44a0-b35e-b89135478603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a42863-f238-4e62-b66b-4259310aacb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78669256-493d-458e-af14-c21fabbaad09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a42863-f238-4e62-b66b-4259310aacb7",
                    "LayerId": "236b5193-f70d-4609-b554-608483e4604b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "236b5193-f70d-4609-b554-608483e4604b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05e64841-9f30-44fe-b3a1-eb341c9c2128",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}