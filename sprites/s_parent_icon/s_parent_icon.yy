{
    "id": "146a9ba2-c00d-42ef-bf3f-ee34de36c496",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_parent_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c488db0d-7e8d-4986-a255-249853e3ad97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "146a9ba2-c00d-42ef-bf3f-ee34de36c496",
            "compositeImage": {
                "id": "981a3f7b-ed6f-48c9-bf7a-26a55a37a91e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c488db0d-7e8d-4986-a255-249853e3ad97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97a0aa16-d7bc-4a8a-8dae-0de480c2f52c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c488db0d-7e8d-4986-a255-249853e3ad97",
                    "LayerId": "e1d19a05-09cb-41d2-812d-8cfebf1ec374"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e1d19a05-09cb-41d2-812d-8cfebf1ec374",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "146a9ba2-c00d-42ef-bf3f-ee34de36c496",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}