{
    "id": "d592c7dd-1f92-4153-ae15-a6d26b04f4f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pipe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 131,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e28b4bb-cd6e-446e-9090-5b1780770f5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d592c7dd-1f92-4153-ae15-a6d26b04f4f5",
            "compositeImage": {
                "id": "a8d231fc-32f4-42d2-a3d7-9a4064009203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e28b4bb-cd6e-446e-9090-5b1780770f5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b705272c-07ce-40d5-82f8-ced4b64c89a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e28b4bb-cd6e-446e-9090-5b1780770f5d",
                    "LayerId": "0f00e282-b273-4b79-880e-b708b8ad9da0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 132,
    "layers": [
        {
            "id": "0f00e282-b273-4b79-880e-b708b8ad9da0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d592c7dd-1f92-4153-ae15-a6d26b04f4f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 47,
    "xorig": 23,
    "yorig": 66
}