{
    "id": "2929a5fe-a57d-4510-a84e-df6c5901488d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spotlight_duck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 22,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "881d3e00-2b2f-489f-8bb7-01d2773631b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2929a5fe-a57d-4510-a84e-df6c5901488d",
            "compositeImage": {
                "id": "6bf01f35-20c1-4f0e-b4d8-7438506dd41e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "881d3e00-2b2f-489f-8bb7-01d2773631b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8920d4a6-bfa6-4540-84e1-7f94b7d6441e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "881d3e00-2b2f-489f-8bb7-01d2773631b6",
                    "LayerId": "d832c494-c2f5-47e8-9356-dc34930dee50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d832c494-c2f5-47e8-9356-dc34930dee50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2929a5fe-a57d-4510-a84e-df6c5901488d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}