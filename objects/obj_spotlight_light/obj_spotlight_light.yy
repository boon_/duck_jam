{
    "id": "2565abc4-179c-4bba-9b2e-59464f75386d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spotlight_light",
    "eventList": [
        {
            "id": "dd8cebe3-57df-4eb7-85c2-06fd11fccbb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2565abc4-179c-4bba-9b2e-59464f75386d"
        },
        {
            "id": "ec7428a3-16f6-4416-873c-2f2489772b92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2565abc4-179c-4bba-9b2e-59464f75386d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5ecf4525-e695-457b-ab98-df23590d9118",
    "visible": true
}