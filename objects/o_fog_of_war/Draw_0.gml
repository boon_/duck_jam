
if( !surface_exists(surf) ){
    surf = surface_create(room_width,room_height);
}


var lx = o_explorer.x; //the light position, based on the mouse location
var ly = o_explorer.y; //the y position based on the mouse
var rad = 250; //light radious
var tile_size = 16; //the size of the tile
var tilemap = layer_tilemap_get_id("WALLS");//find the walls
var startx = floor((lx-rad)/tile_size);
var endx = floor((lx+rad)/tile_size);
var starty = floor((ly-rad)/tile_size);
var endy = floor((ly+rad)/tile_size);
//draw_set_color(c_yellow);
//draw_rectangle(startx*tile_size,starty*tile_size, endx*tile_size,endy*tile_size,true);

//
surface_set_target(surf);
//draw_surface_ext(surf,0,0,1,1,0,image_blend,0.5);
draw_clear_alpha(0,0);
//
//
vertex_begin(VBuffer, VertexFormat);
for(var yy=starty;yy<=endy;yy++)
{
	for(var xx=startx;xx<endx;xx++)
	{
		var tile = tilemap_get(tilemap,xx,yy);
		if (tile!=0){
			
			//get corners of the
			var px1 = xx*tile_size; //top left
			var py1 = yy*tile_size; 
			var px2 = px1+tile_size; //bottom right
			var py2 = py1+tile_size; 
			
			if !SignTest( px1, py1, px2, py1, lx, ly){
			ProjectShadow(VBuffer, px1, py1, px2, py1, lx, ly);
			}
			if !SignTest( px2, py1, px2, py2, lx, ly){
			ProjectShadow(VBuffer, px2, py1, px2, py2, lx, ly);
			}
			if !SignTest( px2, py2, px1, py2, lx, ly){
			ProjectShadow(VBuffer, px2, py2, px1, py2, lx, ly);
			}
			
			if !SignTest( px1, py2, px1, py1, lx, ly){
			ProjectShadow(VBuffer, px1, py2, px1, py1, lx, ly);
			}
		}
	}
}

vertex_end(VBuffer);
vertex_submit(VBuffer,pr_trianglelist,-1);
surface_reset_target();

shader_set(d_fog_of_war);
shader_set_uniform_f(LightPosRadius, lx,ly,rad,0.7);
draw_surface(surf,0,0);
shader_reset();
draw_surface_ext(surf,0,0,1,1,0,image_blend,0.1);
