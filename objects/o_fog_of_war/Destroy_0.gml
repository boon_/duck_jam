/// @description cleans up surfaces
if (surface_exists(surf)) {
	surface_free(surf);	
}
