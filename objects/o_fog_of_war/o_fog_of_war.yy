{
    "id": "a3634be9-e2b8-4490-88d1-8a253cefbc5c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_fog_of_war",
    "eventList": [
        {
            "id": "ecb85c73-69ad-4840-a19f-0462146f4154",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a3634be9-e2b8-4490-88d1-8a253cefbc5c"
        },
        {
            "id": "b7b4e689-710c-4988-b3d2-736b23289119",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "a3634be9-e2b8-4490-88d1-8a253cefbc5c"
        },
        {
            "id": "349a9223-ecd6-4ff5-8e77-9e017eabcf38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a3634be9-e2b8-4490-88d1-8a253cefbc5c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}