/// @description init shadow casting
surf = -1;
vertex_format_begin();
vertex_format_add_position();
vertex_format_add_color();
VertexFormat = vertex_format_end();

VBuffer = vertex_create_buffer();

LightPosRadius = shader_get_uniform(d_fog_of_war,"u_fLightPositionRadius");
