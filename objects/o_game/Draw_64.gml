/// @description draw the countdown
if instance_exists(o_this_is_a_game_room){
var width_ = display_get_gui_width();
var height_ = display_get_gui_height();
var text_string = string(global.countdown);
var text_width_ = string_width(text_string);

	var _x = width_ - text_width_ - 15;
	var _y = height_/2 + 80 ;
	draw_set_font(fnt_countdown_timer);
		draw_set_colour(c_purple);
		draw_text(_x+1, _y+1, text_string);
		draw_set_colour(c_yellow);
		draw_text(_x, _y, text_string);
	}
