{
    "id": "99e81dc7-105d-4d27-880b-9af2b30dbdf7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_game",
    "eventList": [
        {
            "id": "0fd20f24-52da-4c87-b709-6cfdb652459e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "99e81dc7-105d-4d27-880b-9af2b30dbdf7"
        },
        {
            "id": "9275f855-d484-4014-9369-cfdcd20a3b25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "99e81dc7-105d-4d27-880b-9af2b30dbdf7"
        },
        {
            "id": "8ee31ca9-eadf-4c49-92ba-57f7a88f6db1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "99e81dc7-105d-4d27-880b-9af2b30dbdf7"
        },
        {
            "id": "d339e0f9-8525-4e20-b803-bb766e65d316",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "99e81dc7-105d-4d27-880b-9af2b30dbdf7"
        },
        {
            "id": "c2ad2775-868a-40a8-9df8-aece0e8c3d55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "99e81dc7-105d-4d27-880b-9af2b30dbdf7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}