/// @description 

//what font are we using?
draw_set_font(fnt_countdown_timer);

//change the max amount of time countdown here
global.max_countdown = 5;
global.countdown = global.max_countdown;

//Our starting health
global.max_hp = 3;
global.hp = global.max_hp;

//if this is false, then we lost the game
global.win = true;

