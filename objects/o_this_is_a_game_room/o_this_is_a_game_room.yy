{
    "id": "b6cecd21-b09c-41cd-8e39-cdb17316e8a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_this_is_a_game_room",
    "eventList": [
        {
            "id": "5cd9d798-ac65-4ac9-b4e4-0fdf43b925f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b6cecd21-b09c-41cd-8e39-cdb17316e8a9"
        },
        {
            "id": "2bf1eabf-a86b-4b06-b18c-6a9f7aa5c784",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b6cecd21-b09c-41cd-8e39-cdb17316e8a9"
        },
        {
            "id": "e96602f2-b1b1-4075-bb48-e6d0b36822c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "b6cecd21-b09c-41cd-8e39-cdb17316e8a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}