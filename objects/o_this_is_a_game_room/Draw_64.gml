/// @description draw the intro text

var h_ = display_get_gui_height();
var w_ = display_get_gui_width();

var width_ = display_get_gui_width();
var height_ = display_get_gui_height();
var text_string = string(text);
var text_width_ = string_width(text_string);

var _x = width_/2 - text_width_/2;
var _y = height_/2 ;

if alarm[0] != -1{
draw_set_font(fnt_minigame_text);
draw_set_colour(c_black);
draw_text(_x+1, _y+1, text_string);
draw_set_colour( c_white);
draw_text(_x, _y, text_string);
}