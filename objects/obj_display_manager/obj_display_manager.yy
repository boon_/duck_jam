{
    "id": "35c7a8fc-770c-4b00-aaff-06e581471f46",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_display_manager",
    "eventList": [
        {
            "id": "00f8acc4-6112-443a-8281-26294ea9ba29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "35c7a8fc-770c-4b00-aaff-06e581471f46"
        },
        {
            "id": "e9613c1a-023f-498b-bdd7-55f512bf816c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "35c7a8fc-770c-4b00-aaff-06e581471f46"
        },
        {
            "id": "21262c53-004c-4a39-a35f-f89d5520860b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "35c7a8fc-770c-4b00-aaff-06e581471f46"
        },
        {
            "id": "01f806f3-0dbf-438a-a983-6e2faf4d367f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "35c7a8fc-770c-4b00-aaff-06e581471f46"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}