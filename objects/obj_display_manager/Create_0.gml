/// @description 

//Properties
ideal_width_ = 0; //Determined later
ideal_height_ = 1024;
display_width_ = display_get_width(); //Gets your monitors aspect ratio
display_height_ = display_get_height();
aspect_ratio_ = 256/224;//display_width_/display_height_;
//aspect_ratio_ = clamp(aspect_ratio_,1,1); //Restricted aspect ratio to a square, or whatever
ideal_width_ = round(ideal_height_*aspect_ratio_); //Automatically determine our desired width

//Perfect Pixel Scaling
/* Disabled to force a specific aspect ratio
if(display_width_ mod ideal_width_ != 0)
{
	var _d = round(display_width_/ideal_width_);
	ideal_width_ = display_width_/_d;
}
if(display_height_ mod ideal_height_ != 0)
{
	var _d = round(display_height_/ideal_height_);
	ideal_height_ = display_height_/_d;
}*/

//Check for odd numbers which make calculations bad
if(ideal_width_ & 1)
	ideal_width_++;
if(ideal_height_ & 1)
	ideal_height_++;

//Set the display properties
window_set_size(ideal_width_,ideal_height_);
display_set_gui_size(256,224); //Game resolution
surface_resize(application_surface,ideal_width_,ideal_height_);
alarm[0] = 1;
