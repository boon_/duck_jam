/// @description Recreate the camera whenever we enter the overworld

if((room_get_name(room) == "rm_overworld") && !instance_exists(obj_overworld_camera))
{
	view_camera[0] = camera_create();
	view_enabled = true;
	view_visible[0] = true;
	camera_set_view_size(view_camera[0],256,224);
	view_wport[0] = 256;
	view_hport[0] = 224;
	instance_create_layer(x,y,"Instances",obj_overworld_camera);
}