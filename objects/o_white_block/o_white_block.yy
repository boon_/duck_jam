{
    "id": "a6ba4621-f46e-4bbd-86c1-9fa518957fd1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_white_block",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "47d24869-fe49-4f7c-b25f-5c4b15fa5007",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e54673f-c158-45d7-96f3-6639464dead8",
    "visible": true
}