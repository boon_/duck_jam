///@description Initialize the music player

with (other) {

fade = true;
fade_spd = .1;

//Create the music emitter .4
music_emitter = audio_emitter_create();
music_volume = 1;
music_max_volume = 1;



//Create the sound emitter.6
sfx_emitter = audio_emitter_create();
sfx_volume = 1;
sfx_max_volume = 1;


//Create the emitter for random things
ran_emitter = audio_emitter_create();

//Store the current song, previous song, and next song
previous_song = noone;
current_song = a_silence;
next_song = noone;

}



