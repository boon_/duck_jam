{
    "id": "d8882d0d-fe65-4302-a8d9-4396f39e9fd0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_audio_player",
    "eventList": [
        {
            "id": "cabcb08f-260b-4c71-b68b-4efcc336aaaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d8882d0d-fe65-4302-a8d9-4396f39e9fd0"
        },
        {
            "id": "8ce7130a-92c4-4226-8fa8-ae3a28b491f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d8882d0d-fe65-4302-a8d9-4396f39e9fd0"
        },
        {
            "id": "4455041b-e6ce-42da-a2cb-d8d42cc9ef3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "d8882d0d-fe65-4302-a8d9-4396f39e9fd0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2b1213bf-44d5-40c9-8179-24ea1c7f0517",
    "visible": false
}