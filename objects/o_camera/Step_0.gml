/// @description 
/// @description follow

if !instance_exists(target) exit;
var camera_x = target.x;
var camera_y = target.y;

//lerp we move 10% of the way to our target
smooth = 0.06;
x = lerp(x, camera_x, smooth);
y = lerp(y, camera_y, smooth);
//Pixel perfect camera enables scaling
x = round_n(x, 1/scale_);
y = round_n(y, 1/scale_);
//clamps the x position to the room width and height preventing the camara from leaving the room
x = clamp(x,width_/2, room_width-width_/2)+irandom_range(-screenshake_, screenshake_);
y = clamp(y,height_/2, room_height-height_/2)+irandom_range(-screenshake_, screenshake_);


//set camera
camera_set_view_pos(view_camera[0],x-width_/2,y-height_/2);



