{
    "id": "2f799853-ffb3-4def-b2db-95133bee0f8a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_camera",
    "eventList": [
        {
            "id": "94eaf40a-3400-4048-a42c-87a647521cdb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f799853-ffb3-4def-b2db-95133bee0f8a"
        },
        {
            "id": "a2820aa9-7dde-456a-850f-927e87ee72a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2f799853-ffb3-4def-b2db-95133bee0f8a"
        },
        {
            "id": "4f9fe1bb-24a6-4a1d-9f4a-f94cdff2596d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "2f799853-ffb3-4def-b2db-95133bee0f8a"
        },
        {
            "id": "590b8793-9f24-4400-a3b9-9aaed7f27eba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2f799853-ffb3-4def-b2db-95133bee0f8a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1bd886e6-1d1e-4e99-8950-0bd4227a4068",
    "visible": false
}