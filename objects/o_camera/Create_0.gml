/// @description init the camera

//What object is the camera looking at?
target = noone;
//influence the screenshake intensity
screenshake_ = 0;
screenshake_2 = 0;
spd = .1;

alarm[0] = 0;
width_ = camera_get_view_width(view_camera[0]);
height_ = camera_get_view_height(view_camera[0]);

//our scale is 4 meaning its 4 times bigger
scale_ = view_wport[0] / width_;


stranger = false;


