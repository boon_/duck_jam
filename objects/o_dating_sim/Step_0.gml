/// @description 
if o_input.up_pressed {
	index -= 1;
	audio_effect(a_menu_select,9);
	
}

if o_input.down_pressed {
	index += 1;
	audio_effect(a_menu_select,9);
}
index = clamp(index,0,2);

if o_input.action {
	switch(index) {
		case 0: audio_effect(a_wrong,9);instance_destroy();
			with o_dating_duck{
			image_index = 1;
			}
			break;
		case 1: audio_effect(a_woman_gasp,9); instance_destroy();
			with o_dating_duck{
			image_index = 2;
			}
			break;
		case 2:audio_effect(a_wrong,9);instance_destroy();
			with o_dating_duck{
			image_index = 1;
			}
			break;
			
	}
}
