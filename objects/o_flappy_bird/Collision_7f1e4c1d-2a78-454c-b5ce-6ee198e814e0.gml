/// @description 
instance_destroy();
create_animation_effect(s_explosion_smoky,x,y,.5,true,true);
audio_effect(a_quack_neutral,9);
audio_effect(a_explosion_dynamic,9);
audio_effect(a_fire_dying,9);
screenshake(5,SEC*.5);
repeat 2 {
	create(x,y,o_drumstick_explosion);	
}
