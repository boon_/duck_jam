{
    "id": "b2f4308b-3d92-4710-91d2-c575e2cd1cdf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_flappy_bird",
    "eventList": [
        {
            "id": "1f8d45a5-a54b-4851-ad5e-6c08f0b94302",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2f4308b-3d92-4710-91d2-c575e2cd1cdf"
        },
        {
            "id": "0c9c212a-7be7-478d-973d-c9cb33e2db57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2f4308b-3d92-4710-91d2-c575e2cd1cdf"
        },
        {
            "id": "7f1e4c1d-2a78-454c-b5ce-6ee198e814e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "08d30b8b-8d9d-497b-bf32-c14f80ab020f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b2f4308b-3d92-4710-91d2-c575e2cd1cdf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e9b1cac-b62a-489a-b8ba-d18fdc96720f",
    "visible": true
}