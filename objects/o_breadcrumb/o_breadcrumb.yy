{
    "id": "39f22c75-74e0-4666-93ca-c1691254fb31",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_breadcrumb",
    "eventList": [
        {
            "id": "e46e717d-06cb-43e3-b6b9-030b3f9c2ee3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "39f22c75-74e0-4666-93ca-c1691254fb31"
        },
        {
            "id": "5351d11c-4ddd-4258-8653-c15b4419e235",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "39f22c75-74e0-4666-93ca-c1691254fb31"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "720bcfdc-35a0-4758-8c44-36dd735a513d",
    "visible": true
}