{
    "id": "660a2065-5850-42cb-881b-de1ba867a433",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_duck_head",
    "eventList": [
        {
            "id": "3d9e2295-72d1-4a77-b291-387816c8c43c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "660a2065-5850-42cb-881b-de1ba867a433"
        },
        {
            "id": "e233190f-9412-4720-a8ef-a25ca6213998",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "660a2065-5850-42cb-881b-de1ba867a433"
        },
        {
            "id": "ececb499-f079-4330-9ecf-388ef58cb1ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "39f22c75-74e0-4666-93ca-c1691254fb31",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "660a2065-5850-42cb-881b-de1ba867a433"
        },
        {
            "id": "72053d9e-06d5-4cee-9853-b5c5308a0cc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "660a2065-5850-42cb-881b-de1ba867a433"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "30179223-68f0-4e3c-8af9-8331ecab51cd",
    "visible": true
}