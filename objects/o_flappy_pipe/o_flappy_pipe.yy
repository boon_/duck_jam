{
    "id": "08d30b8b-8d9d-497b-bf32-c14f80ab020f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_flappy_pipe",
    "eventList": [
        {
            "id": "f25c9a62-00d1-4e47-992f-3703da7fbd0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08d30b8b-8d9d-497b-bf32-c14f80ab020f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d592c7dd-1f92-4153-ae15-a6d26b04f4f5",
    "visible": true
}