{
    "id": "ddf47e07-1bff-476c-aa5a-85f4707a3af4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_dating_duck",
    "eventList": [
        {
            "id": "d25e552d-8490-407b-bdc9-345274d747d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ddf47e07-1bff-476c-aa5a-85f4707a3af4"
        },
        {
            "id": "e0e581a4-9663-4386-8182-82f74c9fb824",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ddf47e07-1bff-476c-aa5a-85f4707a3af4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ecbf762f-2a68-48bd-af1e-7152e445e680",
    "visible": true
}