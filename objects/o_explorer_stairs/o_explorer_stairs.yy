{
    "id": "3a0a25ea-3e4b-4662-a769-2dbd1c27bc3a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_explorer_stairs",
    "eventList": [
        {
            "id": "a2a56f77-a916-4711-9190-e2c1f584a05f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a8a8efdd-a2e3-4e65-8c43-d1d7a5997cec",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3a0a25ea-3e4b-4662-a769-2dbd1c27bc3a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e759611c-cfbf-4e2d-b5d9-a451a93971fb",
    "visible": true
}