/// @description 
var _x_input = o_input.right - o_input.left;
var _y_input = o_input.down - o_input.up;


_direction = point_direction(0, 0, _x_input, _y_input);


if (_x_input == 0 && _y_input == 0) {
	image_speed = 0;
	image_index = 0;
	apply_frection_to_movement_entity();
	sprite_index = s_explorer_goose_idle;
} else {
	sprite_index = s_explorer_goose_run;
	get_direction_facing(_direction);
	add_movement_maxspeed(_direction,acceleration_,max_speed_);
		
}

if  speed_ > 0{
image_speed = 2;	
}

move_movement_entity(false);


