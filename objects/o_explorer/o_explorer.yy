{
    "id": "a8a8efdd-a2e3-4e65-8c43-d1d7a5997cec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_explorer",
    "eventList": [
        {
            "id": "f6e7e205-2c69-4f9c-ac19-c383dfe2cd06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a8a8efdd-a2e3-4e65-8c43-d1d7a5997cec"
        },
        {
            "id": "e8226fec-2ee5-42fd-a7f4-8b3ea7199a9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a8a8efdd-a2e3-4e65-8c43-d1d7a5997cec"
        },
        {
            "id": "dd24b16f-fe24-46fe-a573-c4c05d4e0bea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a8a8efdd-a2e3-4e65-8c43-d1d7a5997cec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "81caa78a-05da-4b6e-96b5-0e4bc05cdbc0",
    "visible": true
}