/// @description Insert description here
// You can write your code in this editor

cam_ = view_camera[0];
follow_ = noone;
view_w_ = camera_get_view_width(cam_);
view_w_half_ = camera_get_view_width(cam_) * 0.5;
view_h_half_ = camera_get_view_height(cam_) * 0.5;
xTo_ = xstart;
yTo_ = ystart;
if(instance_exists(follow_))
{
	x = follow_.x;
	y = follow_.y;
	camera_set_view_pos(cam_,x,y);
}