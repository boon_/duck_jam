/// @description 

if(instance_exists(follow_))
{
	xTo_ = follow_.x;
	yTo_ = follow_.y;
}

x += (xTo_ - x) / 2;
y += (yTo_ - y) / 2;

x = clamp(x,view_w_half_,room_width-view_w_half_);
y = clamp(y,view_h_half_,room_height-view_h_half_);

var _round = view_w_/surface_get_width(application_surface);
camera_set_view_pos(cam_,round_n(x-view_w_half_,_round),round_n(y-view_h_half_,_round));