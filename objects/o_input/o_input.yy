{
    "id": "7d012509-7e07-4e23-a6f5-e94b2158b4cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_input",
    "eventList": [
        {
            "id": "dab39d15-9849-43dc-8471-d633760183eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7d012509-7e07-4e23-a6f5-e94b2158b4cd"
        },
        {
            "id": "986e94da-528b-42c3-b40a-b2fdc9da7c49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7d012509-7e07-4e23-a6f5-e94b2158b4cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "05e64841-9f30-44fe-b3a1-eb341c9c2128",
    "visible": false
}