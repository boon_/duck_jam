/// @description set the controls, and also check them with other objects
//Movement Input
up = keyboard_check(global.up_key);
right = keyboard_check(global.right_key);
left = keyboard_check(global.left_key);
down = keyboard_check(global.down_key);

// Menu Navigation input

up_pressed = keyboard_check_pressed(global.up_key);
right_pressed = keyboard_check_pressed(global.right_key);
left_pressed = keyboard_check_pressed(global.left_key);
down_pressed = keyboard_check_pressed(global.down_key);



//Action and back input
action = keyboard_check_pressed(global.action_key);


//Action and back pressed input
action_check = keyboard_check(global.action_key);




//if !instance_exists(o_level_up){
//misc
//Disables pause if shop is present

pause_pressed = keyboard_check_pressed(global.pause_key);


m1_check = mouse_check_button(global.mouse_key);
m2_check = mouse_check_button(global.mouse_key_2);

m1_pressed = mouse_check_button_pressed(global.mouse_key);
m2_pressed = mouse_check_button_pressed(global.mouse_key_2);
