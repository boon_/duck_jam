///@desc nineslicebox(sprite,x1,y1,x2,y2)
///@param sprite
///@param x1
///@param y1
///@param x2
///@param y2
var _sprite = argument0;
var _size = sprite_get_width(_sprite) / 3;
var _x1 = argument1;
var _y1 = argument2;
var _x2 = argument3;
var _y2 = argument4;

//Actually bother calculating this out in case only part of the sprite is being used as the 9-slice
var _w = _x2 - _x1;
var _h = _y2 - _y1;
var _columns = _w div _size;
var _rows = _h div _size;

#region Middle
draw_sprite_part_ext(_sprite,0,_size,_size,1,1,_x1+_size,_y1+_size,_w-(_size*2),_h-(_size*2),c_white,1);
#endregion

#region Corners
//Top left
draw_sprite_part(_sprite,0,0,0,_size,_size,_x1,_y1);
//Top right
draw_sprite_part(_sprite,0,_size*2,0,_size,_size,_x1+_w-_size,_y1);
//Bottom left
draw_sprite_part(_sprite,0,0,_size*2,_size,_size,_x1,_y1+_h-_size);
//Bottom right
draw_sprite_part(_sprite,0,_size*2,_size*2,_size,_size,_x1+_w-_size,_y1+_h-_size);
#endregion

#region Edges
//Left
draw_sprite_part_ext(_sprite,0,0,_size,_size,1,_x1,_y1+_size,1,_h-(_size*2),c_white,1);
//Right
draw_sprite_part_ext(_sprite,0,_size*2,_size,_size,1,_x1+_w-_size,_y1+_size,1,_h-(_size*2),c_white,1);
//Top
draw_sprite_part_ext(_sprite,0,_size,0,1,_size,_x1+_size,_y1,_w-(_size*2),1,c_white,1);
//Bottom
draw_sprite_part_ext(_sprite,0,_size,_size*2,1,_size,_x1+_size,_y1+_h-(_size),_w-(_size*2),1,c_white,1);
#endregion
