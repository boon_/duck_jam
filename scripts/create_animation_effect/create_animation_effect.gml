///@arg sprite
///@arg x_pos
///@arg y_pos
///@arg image_speed
///@arg has_depth
///@arg return_to_overworld
var _sprite = argument0;
var _x = argument1;
var _y = argument2;
var _image_speed = argument3;
var _has_depth = argument4;
var _end_room = argument5;
var _effect = instance_create_layer (_x, _y, I, o_animation_effect);
_effect.sprite_index = _sprite;
_effect.image_speed = _image_speed;

if _has_depth{
	_effect.depth = -_y;
}else{
_effect.depth = -2000;	
}

if _end_room {
	_effect.end_room = true;	
}

return _effect;

