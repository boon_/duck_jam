///@arg percent
//Added by BOON 
/*
	Insert a percentage, and then the script returns
	a random chance of that probability.
	(EXAMPLE chance(.5) has a 50% chance of return true)
*/
var _percent = argument0;
return random(1) < _percent;
