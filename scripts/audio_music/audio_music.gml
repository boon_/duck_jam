/// @description audio_music(next_song, fade)
/// @function audio_next_song
/// @param next_song
/// @param  fade

//changes the song
var song = argument0;
var fade = argument1;

if (instance_exists(o_audio_player)) {
    o_audio_player.fade = fade;
    o_audio_player.next_song = song;
}

