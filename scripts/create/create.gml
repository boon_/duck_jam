///@arg x
///@arg y
///@arg object

//shortcut to instance_create_layer(x,y,"Instances",obj);

var _x = argument0;
var _y = argument1;
var _obj = argument2;

instance_create_layer(_x,_y,I,_obj);
