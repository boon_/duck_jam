///@arg sound
///@arg gain1
///@arg gain2
///@arg pitch1
///@arg pitch2

/*
	audio random sound effect, BASSICALLY this returns an audio effect,
	on the audio emitter with a custom gain and pitch.
	
	EXAMPLE
	audio_ran(a_sound_effect,0,1,2,5);
	this will return the a_sound effect with audio gain ranging from 0-1 (0.5 for example)
	then it will return a random pitch from 2-5 (3.33 for example)



*/
var sound = argument0;
var gain_1 = argument1;
var gain_2 = argument2;
var pitch_1 = argument3;
var pitch_2 = argument4;

audio_emitter_gain(o_audio_player.ran_emitter, random_range(gain_1,gain_2)*o_audio_player.sfx_volume);
audio_emitter_pitch(o_audio_player.ran_emitter, random_range(pitch_1,pitch_2));
audio_play_sound_on(o_audio_player.ran_emitter, sound , false, 9);
