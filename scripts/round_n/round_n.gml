///@arg value
///@arg increment
var _value = argument0;
var _increment = argument1;

return round(_value/_increment) * _increment;
/*
PIXEL PERFECT CAMERA!!!
WHAT DOES THIS DO???

if we're scaling our game by 4 then we should have 4 pixels
for every single pixel on screen

so our camera can move pixel by pixel AS LONG as it's in that
.25 pixel range

(x= round_number(x, 1/4); 

Example let's say we plug in 
4.6

then we plug in our increment which is .5
so if 4.6 gets divided by .5 we get

9.2

then this gets rounded to 

9

and then it gets multiplied by our increment .5
so then we return

4.5
*/





