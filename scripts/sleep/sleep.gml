/// @description  Sleep(Milliseconds)
/// @function  Sleep
/// @param Milliseconds

/*
	With this script you can freeze the game for a set amount of time
	VERY useful for making hits feel stronger
	
	SEC*0.5 = makes the game freeze for 0.5 seconds

*/
var endTime = get_timer() + ((argument0*1000));
do {
   var time = SEC;
} until(get_timer() >= endTime);
// added this part myself so it suits what i'm looking for.
if get_timer() >= endTime {time = 60;}
