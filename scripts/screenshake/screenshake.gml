///@description screenshake(amount, duration)
///@function screenshake
///@param amount
///@param duration

/*
	Adds screenshake to the game, with an amount and duration
*/

var amount = argument0;
var duration = argument1;

if (instance_exists(o_camera)) {
	o_camera.screenshake_ = amount;
	o_camera.alarm[0] = duration;
}
