/// @description audio_effect(sound_id, loops, priority);
/// @function audio_effect
/// @param sound_id
/// @param  priority

var sound_id = argument0;
var priority = argument1;

audio_play_sound_on(o_audio_player.sfx_emitter, sound_id, 0, priority);
