{
    "id": "f6050422-3dd2-4c76-bd0d-23839421b342",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_minigame_text",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "862af633-c4ab-4a28-842c-db4e79bb7e5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 34,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 65,
                "y": 218
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f8deef88-6681-4bd2-ae2c-1d6546f695b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 34,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 218
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d082bc17-6c01-4844-889c-3e547f1eed8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 175,
                "y": 182
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "bcd8381d-0f0a-4569-88b1-d62a76d7e031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 34,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9d43b0a2-e72f-403d-834e-ff079e0206b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 236,
                "y": 74
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d0bcf55c-dee3-4d3c-97ee-892feff5905d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 34,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "37039ec3-1893-4972-b813-6c44aa45e7e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 34,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "02fec108-44db-41e5-a3ee-893d6d5af090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 34,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 218
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ee1e9a84-9216-4103-b4b7-41a695d837ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 199,
                "y": 182
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f00cc7bf-307e-4f23-9881-1f5d986b4609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 34,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 187,
                "y": 182
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3a591ca6-f40b-4a6a-abc5-b28aad58abd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 34,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 66,
                "y": 182
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6f2f325f-9c87-4626-8f59-b2d1f3aa84ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 200,
                "y": 74
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5e7d974d-4559-4f7b-9ff6-7da3dcd0298b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 34,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 23,
                "y": 218
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "16feee69-245d-4598-8dd1-ec6444bbe9b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 203,
                "y": 146
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b386c372-bb84-4a84-8bd3-9e51a2b69ae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 34,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 30,
                "y": 218
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "782b4d0a-867c-4da8-88a4-057cd4b49ddf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 98,
                "y": 38
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "05b1f331-b52b-4908-a19c-09a3c278f7a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 146
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b9d36012-d9a3-473f-a5ef-f2fa9948d8cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 211,
                "y": 182
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5d665f5b-695b-4f39-9356-4d80b5621c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 146
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e857e2a9-60e1-41d7-b261-13f9ee9803e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 146
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "432227a0-af09-4ee2-8d5c-a13dc9af4f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 92,
                "y": 74
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c8829060-50fc-4f07-8e1d-1c6dfa2e7756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 206,
                "y": 110
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a62ecaa8-701d-4f40-b607-44c025179278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 182
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1247993f-6bed-4741-9207-21b8e3e8692f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 182
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b2beecfc-f2f2-439c-b7d4-efe01bd95364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 146
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a7422eff-adfb-4233-b6ac-cd20616bd771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 187,
                "y": 146
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "12e50af6-4967-4a64-85bd-683be89cd669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 34,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 44,
                "y": 218
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c9a763f6-3b07-4887-bf62-aa88873847b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 34,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 37,
                "y": 218
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9afcc245-d831-4c30-81e9-346dce659f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 38,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2e550ac0-2869-4b5c-92b0-a3d161021b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3d55a8e2-8485-49bc-927f-a3f3f87827fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e9d350b1-7c89-48a9-b61b-ac1da75b33e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 121,
                "y": 146
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4884a299-dd38-4cbf-840e-b606ef1f36f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 34,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c859c067-62be-49d9-98f8-a31f26bb4351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 41,
                "y": 38
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d9ca5850-12c1-42dc-9869-781c3a7eb5f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 212,
                "y": 38
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fc98b381-60bb-49f0-8688-009a769c5b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 146
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "61bc8451-9725-4493-989d-9ccc7c2ebedb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 146
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "edae032c-55ec-40f5-903c-e34206a9dfba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 235,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c26b952e-6c15-4b83-9f2f-80150fee6910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 182
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2e774466-c269-4511-ae44-36c0e8828f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 146
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c8bb31a6-d959-4c4c-b9df-cc7569f7b6de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "37118606-288c-44a4-a0d7-3a62e4755a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 34,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 218
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "92538ccf-2da4-4384-895d-6dac0fc50804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 137,
                "y": 182
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "da786748-2fb0-42ab-8842-6e18430229e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 193,
                "y": 38
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3becd33c-45b4-4b0e-91ab-ea1936ccdcf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 34,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 96,
                "y": 182
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "eabece6c-6feb-4d40-8b17-eed58e41fac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 34,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4689bb8b-5fc5-47f1-8a6b-3b39bf11791a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 56,
                "y": 74
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "548ea2c6-8298-4e2f-b17e-4f1463e6a6f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 189,
                "y": 110
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "cef1e850-355a-489b-a5bf-2ee155b28ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 172,
                "y": 110
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "52461130-3925-4c20-8984-417588d567b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 74,
                "y": 74
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9fc1e103-614e-4ef4-81a9-155fb91db456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 218,
                "y": 74
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "35ea6341-6faf-439a-859f-9367e040c62d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 128,
                "y": 74
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "502aa0cd-a912-4a9b-83f7-217fcb1f3511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 110
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "74e3eae3-1b52-4313-bbb3-875a5d1a8ad9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "098f5c09-afcc-4739-b1ee-b3f5b5067971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 34,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7bd18cdd-2777-418f-8583-696df7fd7bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 34,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f42fbf68-c19a-4811-9734-9f6c6b91660d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 117,
                "y": 38
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "583ecd45-b389-4287-ba90-0a336fce51de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 174,
                "y": 38
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c9fa590e-320c-4109-a97d-f53f33ebb2ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 219,
                "y": 146
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0133f061-3f86-4378-b839-bd94f1b606bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 34,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 222,
                "y": 182
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "18824bfc-dfa4-4d50-863e-6e49cc6e1ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 22,
                "y": 38
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7527fbfa-bc1f-4c9b-afe7-576d447621d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 34,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 242,
                "y": 182
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fe60d0fc-c55b-4858-98b8-5778e3bb9cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 163,
                "y": 182
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "105a3b51-3582-403a-9e17-63a6569f8da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 34,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "edb29559-0a1d-46f7-80d0-b30060d31dc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 34,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 232,
                "y": 182
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "25e6ca46-d8ac-414a-ba47-93337b23b19f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 155,
                "y": 38
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "dd318bc6-3361-4f9d-86cf-d746d10775c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 230,
                "y": 38
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ad0da457-2d30-4044-842b-d29a94512432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 155,
                "y": 110
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f1c9a1c6-925c-4d21-a6f2-f438b67c5ae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 110
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ca385a26-722d-4e04-9f52-602171683c05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 171,
                "y": 146
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d229cd9d-8532-4b82-8822-7291c125c1a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 155,
                "y": 146
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "01a8606b-01ea-4310-bc4a-751af61ff834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 110
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "13b0ce20-c3bf-4537-80e2-05fbe15d726f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 110
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1e8bf03a-43ce-4756-943e-09baaea7fe31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 34,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 218
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5529b206-4444-429a-ab34-e22432ddf470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 150,
                "y": 182
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "54a011fe-f9f5-4f5f-b081-014abc47b9ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 136,
                "y": 38
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9eaf053b-c0ef-4317-9f70-33e6b92ec27b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 34,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 81,
                "y": 182
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "29e747f6-1293-4e64-a99c-c3bf1c916f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 34,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a1894a41-25ae-4468-a794-83a307b3f95c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 110,
                "y": 74
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2cc98f5b-ddba-4a01-b30d-8ad73e3376ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 110
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b79d2b58-7c4c-425e-816b-d106a66976b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 110
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "17f7897a-a15f-4842-802d-c43de779bbb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 146,
                "y": 74
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2e530a99-df90-4a18-8910-ac857e46bce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 164,
                "y": 74
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5025312f-648a-4f18-9fa0-091a54a2df4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 182,
                "y": 74
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9d81b5b8-5e32-402c-8382-cda3cdf063cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 121,
                "y": 110
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7e1b4b72-9c90-4f80-bc6e-5ddba00272f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 110
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "92bed627-ed33-46a7-9853-6dea93dff873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 34,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2d6c52ce-b84f-446f-b72a-039c8d531538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 34,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "51e8bcf6-a5fd-4cd7-a421-ea857c266d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 79,
                "y": 38
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b31df5f4-c1f6-4f7e-b8cb-f204683b088f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 34,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 60,
                "y": 38
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d60d7c13-83be-436a-a1a9-363a8af09cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 34,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 182
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a97a2147-518e-4b01-88a1-d48c47bc0a56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 111,
                "y": 182
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4f68d38c-e1ad-4970-bb22-925341c4aa18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 34,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 58,
                "y": 218
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "447fe4e8-dbb1-4cd7-a067-f47b4bb25f0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 34,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 124,
                "y": 182
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d3e2cad1-1ebe-4296-a4b7-205187ace480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 34,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 223,
                "y": 110
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "44ed6a5a-b5b6-49e4-a8df-b4abf51f62a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 34,
                "offset": 0,
                "shift": 12,
                "w": 0,
                "x": 70,
                "y": 218
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "cf05ed32-9b48-481a-bfd6-7f4bcf1ca49f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 48
        },
        {
            "id": "7fa39473-610b-454c-aca6-49a60ea3d5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 49
        },
        {
            "id": "2050fc73-fc96-4d11-b519-f5bda660890d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 53
        },
        {
            "id": "06965805-bf6f-4781-b7cb-b4d39daa643c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 54
        },
        {
            "id": "e1045cbb-e4aa-48b7-8ffd-d0925b4ca19d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 56
        },
        {
            "id": "c76e6422-0e5e-4355-90d2-ce9ab2dc28b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 57
        },
        {
            "id": "144f39b6-8bf7-43d5-8f3d-08dbfcc549b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 66
        },
        {
            "id": "a5a7945b-ea48-42db-bb23-4344457df5af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 67
        },
        {
            "id": "a8a3b2ac-59d7-407b-a2b1-317adaec4560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 68
        },
        {
            "id": "f8c146b6-0d1c-4042-ba31-edfbbe02f878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 69
        },
        {
            "id": "f30b4536-0e07-4516-a0b8-7b9cbd7e9f56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 70
        },
        {
            "id": "77153080-498a-401f-b024-6372c5c9277a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 71
        },
        {
            "id": "42fe0614-ee46-4531-8de3-0089a48ebdcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 72
        },
        {
            "id": "4aa03916-fdd9-40f1-b07b-75ce3f47f5e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 73
        },
        {
            "id": "0ecfbb77-8dfe-40be-ad91-43cd4df71b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 75
        },
        {
            "id": "9ce5f836-3a3c-4f15-b2ac-8d8476f9f41c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 76
        },
        {
            "id": "c4a910e2-a950-45f6-828f-c6b4193384da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 77
        },
        {
            "id": "1c19b8a3-5d4d-44b2-bf6f-a9f6db60108d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 78
        },
        {
            "id": "6ce7f7f1-db10-4271-a64e-3d4e4b4c3b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 79
        },
        {
            "id": "82397b82-ce74-40ab-b4fe-f136e30d75f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 80
        },
        {
            "id": "461f46b6-c917-4e97-bed9-0202ad53708b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 81
        },
        {
            "id": "9d1a1910-5998-459f-b49f-614ad1064253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 82
        },
        {
            "id": "7cea05c1-ba4d-4bb7-8126-9a37e85a51e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 83
        },
        {
            "id": "63b1ec17-64ba-4daf-a5eb-815c2c3378e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 85
        },
        {
            "id": "45e49f6d-f09c-4281-9b6a-65a6d43a0d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 88
        },
        {
            "id": "d86b631f-2f79-43f3-b54e-6920dc7199fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 89
        },
        {
            "id": "af66ccf0-f5c5-437a-97fb-526b22e8105e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 98
        },
        {
            "id": "12f61766-4537-4275-acfb-9c9f48536b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 99
        },
        {
            "id": "9fcce0ba-8c8d-4848-8a17-00255983ab76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 100
        },
        {
            "id": "5d322c91-9249-41f9-be0b-a85978478537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 101
        },
        {
            "id": "b31283ab-fd31-4ae4-bcde-67fe5849a772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 102
        },
        {
            "id": "bd5783f7-3e59-4db7-94a6-f185ccfefa9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 103
        },
        {
            "id": "7060979a-4afa-4677-a3d3-15f82ea5eff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 104
        },
        {
            "id": "bd88b364-9562-405e-8cdd-01c7aa79b301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 105
        },
        {
            "id": "5623376d-0b7b-4577-a1be-9c0e663c3c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 107
        },
        {
            "id": "af848c83-88e5-4327-9b3d-3c725af3fa1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 108
        },
        {
            "id": "5c194f84-43a0-480e-a627-ae25db6dd98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 109
        },
        {
            "id": "3a50d672-e6eb-4953-8d53-dc5fc1c09142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 110
        },
        {
            "id": "39267bc1-259e-409a-a532-966d4b7f5981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 111
        },
        {
            "id": "c0f5c6e6-3b06-4fea-b2e8-d56bd670cd1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 112
        },
        {
            "id": "54835db7-998e-4d43-9b11-11fafc495024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 113
        },
        {
            "id": "9c7b3f63-d5a1-456b-aa97-6b8830a5a8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 114
        },
        {
            "id": "363c8120-0627-4b77-b10f-f4bc5a80b33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 115
        },
        {
            "id": "2ee3bc75-e78f-4dac-acac-8415864f3384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 117
        },
        {
            "id": "d2acddcd-e2b9-46e2-909f-79fcf6968df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 120
        },
        {
            "id": "a109b2a0-c5b2-4b53-bb62-67bbc7af5f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 121
        },
        {
            "id": "8fccc3ed-fdfb-478c-b3f4-7bade1ceabe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 48
        },
        {
            "id": "fd07058f-9fca-4b95-bb43-df7624208f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 49
        },
        {
            "id": "016b9838-20c8-4e58-9f2c-0113778c7d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 50
        },
        {
            "id": "47be8ad5-c749-4920-a08b-3f497a33680c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 52
        },
        {
            "id": "21aa64a8-aeb4-4134-807b-4c75fd3e0f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 53
        },
        {
            "id": "cf0ebd3a-173c-436e-a340-11d0269023af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 54
        },
        {
            "id": "2fc2250e-b8e1-4a06-be2a-4436846b7b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 56
        },
        {
            "id": "63f75899-b2f2-4396-8426-c822b539d664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 57
        },
        {
            "id": "927d75c0-0d72-4491-a062-7e78e853fe92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 65
        },
        {
            "id": "fc85ba59-ccc9-4688-925a-0e00722bc444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 66
        },
        {
            "id": "449c3438-10cc-4554-b7ba-bf3193f748c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 67
        },
        {
            "id": "fdb2fae7-32d4-46b3-85d3-6ef20b198108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 68
        },
        {
            "id": "f4eaf7d8-57ce-4330-a06a-38f13b000e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 69
        },
        {
            "id": "3c28ffdf-8c05-4334-aee0-209595d737ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 70
        },
        {
            "id": "22411117-ab0d-4212-bef3-f9a86ea864c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 71
        },
        {
            "id": "673262d6-d88c-4889-a551-820efd567e74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 72
        },
        {
            "id": "b312ccf1-eb0c-4196-bf23-adfa4d6a8e59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 73
        },
        {
            "id": "6f8041cc-df59-4239-8f4f-6ba0b5e323d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 75
        },
        {
            "id": "692e4904-ca9e-4e85-82f8-7606fc718461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 76
        },
        {
            "id": "cc3b0459-9e19-4148-a9d8-212cb8e54cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 77
        },
        {
            "id": "27a3007d-d6d2-4bad-8113-696c72152263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 78
        },
        {
            "id": "7a63046a-366e-47f1-b62c-b92dd1df403a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 79
        },
        {
            "id": "ebb7a735-5b36-4740-af79-e94bd269e42a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 80
        },
        {
            "id": "d1633f75-2298-4ad9-8a6b-c7f020e9dbc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 81
        },
        {
            "id": "3b86f470-06ee-4452-8012-6284e6d02372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 82
        },
        {
            "id": "cfceae04-e08f-4d04-bf18-a9b8ab1f0682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 83
        },
        {
            "id": "02815ec6-aaa5-4e80-a1cc-2403ac50e687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 84
        },
        {
            "id": "5e43f858-f457-4699-abfe-d9db967a0d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 85
        },
        {
            "id": "9eb4c6d1-d054-45d3-b0a0-9192bd1e5fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 86
        },
        {
            "id": "66a9d110-5e3c-4006-93a3-a35cb1dc259b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 87
        },
        {
            "id": "7c527970-50f0-489f-9f77-c5e19f291142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 88
        },
        {
            "id": "4c497cbb-3219-4b82-9ecd-d029293ee396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 89
        },
        {
            "id": "1cbf12e5-f35d-4ba5-b094-9d825a41389c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 90
        },
        {
            "id": "cb56b49e-4815-4187-b5dd-d4b961039159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 97
        },
        {
            "id": "82301d8e-f665-4a58-aae6-dcb03d8c6a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 98
        },
        {
            "id": "1c7134e5-707c-4591-88ac-cb636a512f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 99
        },
        {
            "id": "40a5867a-d57d-4196-8cbf-937e4ce6e7b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 100
        },
        {
            "id": "ab9215ac-e02a-4817-8c24-202ddacc8393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 101
        },
        {
            "id": "17c8c206-42b7-431a-8c91-c63fa8172309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 102
        },
        {
            "id": "b606b8b6-e8fc-401b-9c8e-823f3da8c474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 103
        },
        {
            "id": "524255c2-1dd0-426a-bffa-6fbf086599f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 104
        },
        {
            "id": "e64ebb9e-9579-46aa-afc2-75b58fb0eca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 105
        },
        {
            "id": "0af0ab7f-978a-4b8f-81e9-eb61d897dc4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 107
        },
        {
            "id": "16435939-a458-4ea7-93e1-9afb1e05bde0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 108
        },
        {
            "id": "d887f0e0-d199-45de-8dfe-cf4b7c8c4f17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 109
        },
        {
            "id": "72d0d626-caac-4484-ab0c-50079d176faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 110
        },
        {
            "id": "1a803b31-fdb4-4d66-b9b3-6d2b830da98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 111
        },
        {
            "id": "9c934b3c-5467-45fc-8813-25679a5d7194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 112
        },
        {
            "id": "0f508a04-d28d-4cf1-8e84-2a0e36688f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 113
        },
        {
            "id": "bc037b9a-f598-4afc-86b3-a14911c77628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 114
        },
        {
            "id": "998fff31-37c0-4234-9a4a-c696bea13ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 115
        },
        {
            "id": "13ffc250-ec62-41e3-8535-84cfcd7b79bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 116
        },
        {
            "id": "6c01591a-7ea6-4ece-97ab-0f4dc0a2011c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 117
        },
        {
            "id": "c5f8ee3c-cd44-4d1e-a680-641683f11b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 118
        },
        {
            "id": "37ff1f1f-f069-4308-b52f-d20271960e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 119
        },
        {
            "id": "080dde5d-c9b0-4fab-b5ee-35a236d421e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 120
        },
        {
            "id": "0ba8122b-979e-4f13-8c35-8008c52ff1d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 121
        },
        {
            "id": "bb43a608-743e-44a3-942d-4755d949bced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 122
        },
        {
            "id": "49e71768-dfef-4678-9726-6972a17b1346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 48
        },
        {
            "id": "c23ab258-9fdd-4e8b-8b2c-b084a6b03a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 52
        },
        {
            "id": "74bc08e7-7fc2-482c-83fc-acec0eb3226c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 54
        },
        {
            "id": "977057d5-dfa4-4579-af8b-78ea70fd7197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 57
        },
        {
            "id": "eb849a61-313b-44b5-af5d-a38c004b42ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 66
        },
        {
            "id": "61ea4096-ecf5-4d6a-ad98-63036bd28cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 67
        },
        {
            "id": "406c839e-77e0-4cb5-9a77-c4bcd4c0d7a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 68
        },
        {
            "id": "2e714d65-6243-463b-b67b-28faf10d1223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 69
        },
        {
            "id": "c7628145-8847-4596-bdfb-2a8abfc733b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 70
        },
        {
            "id": "7cd306a8-c9d3-4487-a6e8-c66055444934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 71
        },
        {
            "id": "1984f24d-a990-4a22-a7d2-29ca887f677a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 72
        },
        {
            "id": "3e8b7eec-7a3f-46b7-82a9-9d288fe59d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 73
        },
        {
            "id": "c41a41fe-b07d-4cdb-bb87-6e5e30e74f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 75
        },
        {
            "id": "31483702-90dc-415b-8a8b-348f3a233100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 76
        },
        {
            "id": "b8206868-6b5a-4482-af09-8d1b07f5ce32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 77
        },
        {
            "id": "2634c38e-0126-42d8-b4e2-66b950d40f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 78
        },
        {
            "id": "807a2fb8-8e32-4c57-8c98-b91f6f3a8f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 79
        },
        {
            "id": "dc5d291f-a4f2-4a17-88a1-65f03f3a8573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 80
        },
        {
            "id": "378d1996-7318-4e38-9f0e-1267ea9ba6a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 81
        },
        {
            "id": "0d311693-a2b5-4deb-8733-3174b47f3427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 82
        },
        {
            "id": "f5eede51-ffad-49f4-b7eb-0ce2854e6c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 85
        },
        {
            "id": "1b9616d2-ea3b-4123-95aa-c67cdae8f1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 86
        },
        {
            "id": "c66d623b-dbbd-45bf-9ad4-869b54ca119f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 89
        },
        {
            "id": "056f76be-0404-4fd4-932c-8e2af3911bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 98
        },
        {
            "id": "4bcc46d5-261d-4403-b8ba-d18e2b7f227b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 99
        },
        {
            "id": "a0d409ba-e154-410f-bcac-4e4ed1f378cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 100
        },
        {
            "id": "7adc9923-64d4-490d-86a1-3a8ab1663f53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 101
        },
        {
            "id": "8c040f75-6dff-433c-991b-09f273be5fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 102
        },
        {
            "id": "c36f86b3-f8db-498f-82f4-a4c2e2649a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 103
        },
        {
            "id": "ed031069-cd2d-4247-b09b-2202aac9a832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 104
        },
        {
            "id": "d4ee115a-3434-49aa-991b-ba0f4d06587e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 105
        },
        {
            "id": "a5eccc9d-f6fa-4446-ba05-03132a768044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 107
        },
        {
            "id": "abaddf28-8d8e-456d-819b-b1ca316e0dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 108
        },
        {
            "id": "6e318f29-6aac-4e00-89c1-1ef896918b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 109
        },
        {
            "id": "42461ba0-2778-44d7-ae27-681ff54ce356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 110
        },
        {
            "id": "da67bd0b-1d95-43cc-ba50-a1b42696e099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 111
        },
        {
            "id": "dd2b7e81-3feb-4626-9f74-da47aff68468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 112
        },
        {
            "id": "0fb9a175-e802-45f7-83f5-b38452dff3f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 113
        },
        {
            "id": "0845fcc1-45d3-4aec-bc53-a20076655a98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 114
        },
        {
            "id": "4dd9ffa7-d91f-43f2-9fa5-cb25abcaeeae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 117
        },
        {
            "id": "7fcd866c-33c9-4ebb-9d8f-5ccf3b919bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 118
        },
        {
            "id": "fa6652df-55d3-4161-97b9-6d30984d8436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 121
        },
        {
            "id": "823fcfe0-debf-4b6e-93d5-349f348dd290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 48
        },
        {
            "id": "86f16085-40e5-4dcf-acef-49632b00db92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 53
        },
        {
            "id": "05e47566-66f7-4716-96ef-2d6366bf13f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 54
        },
        {
            "id": "33425d50-3fb4-4066-8a1a-3ac10e28363a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 56
        },
        {
            "id": "cb521aa9-d217-4ae7-99cb-22c194fd48b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 57
        },
        {
            "id": "96c8f25d-61e6-4c81-81a4-0eca27e60ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 66
        },
        {
            "id": "80d0f599-c46a-4380-80ae-bf02fab722ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 67
        },
        {
            "id": "59baee2c-f3f3-4ada-8f03-93f4ccfe7297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 68
        },
        {
            "id": "5c420946-d2d3-4693-b855-3932d15d1789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 69
        },
        {
            "id": "f16dbaa6-8558-4d6b-a7f4-2e9e82c7858a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 70
        },
        {
            "id": "68003754-0804-4ee3-8103-1d0a2ba978b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 71
        },
        {
            "id": "35146239-d24c-41c2-88b2-b824ce208c7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 72
        },
        {
            "id": "3d5cede3-cc8a-4d2b-afd8-74d73989af17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 73
        },
        {
            "id": "49ecac6e-090f-48d7-96a9-704627a0d7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 74
        },
        {
            "id": "b93a9125-420b-4708-b1a8-3c9e076b7d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 75
        },
        {
            "id": "663ff579-5054-4375-bcbf-52debf15a330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 76
        },
        {
            "id": "7c7b33c2-ad17-4bec-9a00-37fd17f45086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 77
        },
        {
            "id": "e0d32401-4913-4370-8417-a55f9bc757af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 78
        },
        {
            "id": "d7c81919-8da5-405b-87b9-763b96b51614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 79
        },
        {
            "id": "f8bd37f5-4271-4eee-bf58-37ec8e2fed2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 80
        },
        {
            "id": "75be02ab-060a-4c72-8d5e-7d78e159d15b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 81
        },
        {
            "id": "95bc7a32-5749-4ca1-a898-b7bf6ecf4be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 82
        },
        {
            "id": "ddd73ab8-fb7c-4715-ac68-1f08f20b3c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 83
        },
        {
            "id": "27f027fd-6632-4b50-b065-36ad74a83aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 85
        },
        {
            "id": "f4149ada-aaa7-497c-9068-bea051a30d02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 86
        },
        {
            "id": "ca0a4a15-c794-499d-a780-2bc7af9e24aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 87
        },
        {
            "id": "2aa40daf-9d8b-4f4a-a586-4717a32a867a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 88
        },
        {
            "id": "843aa9c0-b7cd-49c8-b25b-29c4f895ac03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 89
        },
        {
            "id": "99a0f162-c1e6-4034-bc5f-c29be4d46e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 98
        },
        {
            "id": "523459b2-b797-4438-9b95-de24f5a3435a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 99
        },
        {
            "id": "709db7fa-6298-488a-9a8e-6fe3be69cf47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 100
        },
        {
            "id": "c3576922-3b85-4edb-b2a9-666c311c1586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 101
        },
        {
            "id": "5862b01b-8edb-4345-95ed-71524f7934db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 102
        },
        {
            "id": "021b8233-301d-43c7-b5e8-eb01a85b0ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 103
        },
        {
            "id": "d48b7bba-038b-4514-8314-a4ba7cddb756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 104
        },
        {
            "id": "8bf1c2dd-31e8-46cc-8602-0956beab26e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 105
        },
        {
            "id": "9aa3834c-b705-4f7f-b3bf-69be21eb5436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 106
        },
        {
            "id": "f533cee9-bc07-4e75-a278-5c09bf3cc061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 107
        },
        {
            "id": "a88ec4be-36b8-4b10-b131-7191f42f6a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 108
        },
        {
            "id": "1cd8702e-19e8-4f7a-80a3-1f82d70bc123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 109
        },
        {
            "id": "d0c01d62-2fe4-4d97-b1fd-e83f94d50daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 110
        },
        {
            "id": "e8962110-6b8a-4078-a1f1-5f220af0068f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 111
        },
        {
            "id": "7ec97a21-e016-4529-aebd-1820e9ff72d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 112
        },
        {
            "id": "f17bfd63-655b-4cb3-8991-50b7ec99fa9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 113
        },
        {
            "id": "3a9af373-4e8b-484a-8af0-17d42ad4ee65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 114
        },
        {
            "id": "6e09b2a3-f1b9-4e81-a63d-b024c5ce01a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 115
        },
        {
            "id": "0e94fa3f-22d8-4a11-a037-a73646f31f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 117
        },
        {
            "id": "c8d939a6-19c9-483f-b981-f6194a724251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 118
        },
        {
            "id": "ed503d84-9e6a-4462-ba93-ffdb0b966b27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 119
        },
        {
            "id": "14007b18-8d6f-4819-a454-ab2a098fdb68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 120
        },
        {
            "id": "c46c82b0-966e-4c7f-96f1-9e4f2c8e6971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 121
        },
        {
            "id": "3e353545-21bd-43fe-9e01-91627f4b88c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 48
        },
        {
            "id": "f55d1a08-3cb5-496e-8c72-c77c7c54ef7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 49
        },
        {
            "id": "f3205402-fbcd-41ac-937e-8f52978effc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 50
        },
        {
            "id": "50666be3-1a17-40c0-8c89-591325d346b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 54
        },
        {
            "id": "c0a6d013-6fb8-443e-a2ff-228f5a936ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 55
        },
        {
            "id": "e569f346-b139-4ae7-bf28-54c15f1a5841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 56
        },
        {
            "id": "b230370c-caa9-42de-b76d-b6ec9f123678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 57
        },
        {
            "id": "5fcf426c-f32f-4e07-a32b-0fb21b347ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 65
        },
        {
            "id": "a859cb79-bb50-454f-b215-820df0828d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 66
        },
        {
            "id": "45f56dc2-d43c-465a-99e0-5fd502f6364d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 67
        },
        {
            "id": "d56cff6e-ca6c-406a-88e2-00916a4c1839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 68
        },
        {
            "id": "f9453c8a-fc7e-4c96-ae58-609c6da37d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 69
        },
        {
            "id": "de46daeb-9f7f-41e3-8ea0-db1bac31f6d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 70
        },
        {
            "id": "1077da5a-741f-4633-b18d-5600b152811d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 71
        },
        {
            "id": "a47cee6d-749c-4d69-962e-83f745ce43ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 72
        },
        {
            "id": "ee67b4df-c86d-4947-86c9-02760aee844b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 73
        },
        {
            "id": "8fe27d0f-5d21-44f6-b760-9cf6306c2ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 74
        },
        {
            "id": "42e30d60-6557-4a81-8b30-d7ffaa8285e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 75
        },
        {
            "id": "939dc0f6-e191-491a-b4df-adeba05e4493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 76
        },
        {
            "id": "7fd45294-b13f-4eec-8de2-08e39093ed41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 77
        },
        {
            "id": "c18cf252-723c-445a-a3dd-b10446e2f43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 78
        },
        {
            "id": "b2dbec86-4cea-4ac4-b9c1-aac879106092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 79
        },
        {
            "id": "e7685a47-2366-4ef7-ac19-898ed4cd337e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 80
        },
        {
            "id": "e137b14e-e82a-47b8-8bec-b70491ec3c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 81
        },
        {
            "id": "ea960766-fa80-4167-86f2-23b647956d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 82
        },
        {
            "id": "990d0728-7c1a-4187-a0c9-0a345e6f1f4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 84
        },
        {
            "id": "f592556d-141a-4154-981d-cf3cd8f879d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 85
        },
        {
            "id": "acc623f6-7a3e-4eca-bc90-1aa9ec82537e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 86
        },
        {
            "id": "39bd6624-734d-4e9e-b8a8-3af032aca55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 87
        },
        {
            "id": "e3d0b1e6-3d8e-4ac9-9bb2-6dc87c25149e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 88
        },
        {
            "id": "3408a95c-f685-4d9d-b20c-87ba2b9ddab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 89
        },
        {
            "id": "152a443b-c6a6-4a5a-8c23-0b06673c0711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 97
        },
        {
            "id": "72400e21-83e0-42b9-a913-4d5871aabce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 98
        },
        {
            "id": "1cae187b-74d9-460d-8709-36ec6eb397e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 99
        },
        {
            "id": "78fb7f2e-f793-4cdb-8630-790f58b14746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 100
        },
        {
            "id": "5c4ac779-6206-43e4-a315-a9014a965ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 101
        },
        {
            "id": "a06166be-db58-491c-ba49-d22971dff148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 102
        },
        {
            "id": "3c295387-3af5-4089-aee3-6b9d4ce699e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 103
        },
        {
            "id": "805905a0-480a-484f-87ff-8938fb43b73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 104
        },
        {
            "id": "c69bf4a3-59d1-44b5-a518-e59221af8227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 105
        },
        {
            "id": "8e66d1cc-1d8f-4352-a661-3b28f3560545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 106
        },
        {
            "id": "0aa1293d-3984-418b-8cdc-3bd241ae6bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 107
        },
        {
            "id": "b44a5645-763d-4131-9d3c-5521ea387276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 108
        },
        {
            "id": "d86ef50e-1c20-49ae-a2e0-9c31535ccf11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 109
        },
        {
            "id": "f4043671-37f9-4915-8134-307b3e2c35c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 110
        },
        {
            "id": "39ea5914-087e-4972-a405-a2fe95a40237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 111
        },
        {
            "id": "71401984-b2c4-48e3-b95b-6fc503bd6487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 112
        },
        {
            "id": "f0b4cfda-6a1b-4536-a1c4-f2a89881ced1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 113
        },
        {
            "id": "e39d0553-8704-4505-b0ae-8cd6617e6f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 114
        },
        {
            "id": "ebe00c2c-cc92-45ba-a237-5ebcefb84093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 116
        },
        {
            "id": "0808e38e-3956-47f4-b937-9d36f0f3ff5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 117
        },
        {
            "id": "6de00c0d-3e8c-4b82-99e7-ca9be9e57656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 118
        },
        {
            "id": "4b4e3982-0cff-4e4b-8bfb-1cb55e07e1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 119
        },
        {
            "id": "bee42645-af7c-4244-856a-9e0ddb90561d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 120
        },
        {
            "id": "b2dcb2a0-fd84-4ef6-99f9-004f712f82c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 121
        },
        {
            "id": "07a0431a-ef2c-4730-8bbd-da7461928160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 48
        },
        {
            "id": "e204dc4c-76e0-4b7b-8d31-d3c43b6ce2c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 53
        },
        {
            "id": "8c489ddd-5118-4048-8b6d-e843d4d8de6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 54
        },
        {
            "id": "8d609757-a79b-4dd3-97ac-577aa769c85e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 56
        },
        {
            "id": "318cd577-6b66-47ff-b929-6525b15ea5f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 57
        },
        {
            "id": "830ecae8-4d08-4488-8954-78bce4273aec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 66
        },
        {
            "id": "662cbd9d-3f72-4faa-8f1a-9dd4bc1d698d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 67
        },
        {
            "id": "44a12ed9-b976-4fa1-8214-40b31299dbb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 68
        },
        {
            "id": "fede592a-2228-474f-9ecc-d7a2ab206ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 69
        },
        {
            "id": "7e732f8c-ffe7-49f4-8e7a-4817f8caae31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 70
        },
        {
            "id": "2ccd354e-8c2b-4c2b-8430-fcc016787f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 71
        },
        {
            "id": "e1df70c5-4531-418e-96c7-bab79564b12e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 72
        },
        {
            "id": "f07e1799-be91-42ec-a827-c259e738d35a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 73
        },
        {
            "id": "6f61b054-4b62-46d0-b90d-209d808160bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 74
        },
        {
            "id": "a14f769c-87cb-4d42-86ab-2e305cd277c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 75
        },
        {
            "id": "c6fef18d-58bb-40da-813e-221160bfdc93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 76
        },
        {
            "id": "563e6792-2239-46fb-bd6a-fd66eb2506f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 77
        },
        {
            "id": "aebb3e56-4491-4c1b-8dd2-83fe3085d0c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 78
        },
        {
            "id": "8ea91d65-a11e-46aa-857c-a8b392435d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 79
        },
        {
            "id": "8bf79c17-c125-4c30-bd5c-a0da840fddcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 80
        },
        {
            "id": "9a3b3d39-0043-4d8c-a731-03d51129fedf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 81
        },
        {
            "id": "f59ca6b9-f5e7-4071-9164-b4f2e36db8f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 82
        },
        {
            "id": "6d59a139-a613-49fe-9032-8c3435989b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 85
        },
        {
            "id": "8f7ed309-e71d-483f-9f3d-c320337dae0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 98
        },
        {
            "id": "e1293120-5449-4f81-8dc3-33cba1e41726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 99
        },
        {
            "id": "478cbb52-2628-4566-9717-97030b37c7e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 100
        },
        {
            "id": "7a9caa2f-bf36-464b-981e-44a25e591d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 101
        },
        {
            "id": "b2773d16-4446-4cd0-945d-2389ee2caca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 102
        },
        {
            "id": "5034f6d0-bff6-4f76-afa6-2ae83c90b5a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 103
        },
        {
            "id": "140c396a-d374-4a66-841f-11c57887dd43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 104
        },
        {
            "id": "9e2cae16-e3c1-420e-ad1e-2bcc008886a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 105
        },
        {
            "id": "9a854ebc-c1f1-43df-aa76-6aada0a3ed91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 106
        },
        {
            "id": "1f8e701c-8c5e-4800-ab8b-bc735ae7c3e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 107
        },
        {
            "id": "07d1240b-1424-4d76-869a-9d33be81dbcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 108
        },
        {
            "id": "baeb97cb-fce3-4f5c-b773-be6407b2d9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 109
        },
        {
            "id": "ee7ba941-c3ee-49b7-9807-5a8d62616153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 110
        },
        {
            "id": "a94bfbe9-f736-4887-a064-b08fe937e3fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 111
        },
        {
            "id": "985442da-0461-4e68-b24f-9ad46ac01126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 112
        },
        {
            "id": "af40c86d-0e01-4cac-a9fc-a29777167582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 113
        },
        {
            "id": "dc002eb9-f302-4da3-8471-d0091418222f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 114
        },
        {
            "id": "de0c61a3-b8b1-4d12-92c6-1b82cdad9cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 117
        },
        {
            "id": "ffd7b117-ebea-407c-8195-25cf64d69b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 48
        },
        {
            "id": "9dfb0602-4f4e-4fad-98b0-eb9d804e160e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 53
        },
        {
            "id": "2dc1334a-107b-46c8-87f8-c53ea30c8339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 54
        },
        {
            "id": "3d818bb6-d5d5-434f-b96a-28744b524fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 56
        },
        {
            "id": "c3908655-5b98-4d32-a5ae-fd010525084e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 57
        },
        {
            "id": "9aecb02a-30e7-4141-a06a-39f03802cd62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 66
        },
        {
            "id": "3045b62c-dcc0-4849-81d6-f2baf2206a56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 67
        },
        {
            "id": "ad8d96aa-0f67-421a-b73e-0d27c78dc315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 68
        },
        {
            "id": "bb8701fe-a619-43b6-bac6-1f3391f3a860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 69
        },
        {
            "id": "053d4264-d2b1-4c03-b558-0985d5b9db2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 70
        },
        {
            "id": "034eebad-fd7d-4612-a0d0-783f1d8f9587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 71
        },
        {
            "id": "44df60a9-8b31-47b9-a117-38af3d1f1528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 72
        },
        {
            "id": "13f1af20-18e4-494c-b5a6-de4c4ec21253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 73
        },
        {
            "id": "7b344a29-0db0-4026-bc25-ff493ed9dc19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 74
        },
        {
            "id": "5b7557f2-e25f-4cdf-b5df-90422a7b2460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 75
        },
        {
            "id": "458cb093-9b3f-4752-a518-2134cc9ac4d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 76
        },
        {
            "id": "16338ffe-ecdf-4f59-9777-c9aba93e25a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 77
        },
        {
            "id": "0a5e60e2-52f5-48c7-9af8-7aa33d3b72af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 78
        },
        {
            "id": "0f033b92-745f-470a-b351-d4bce22e9f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 79
        },
        {
            "id": "87226d19-bc62-434c-a600-938542211da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 80
        },
        {
            "id": "1eb896ab-c07b-4412-9b77-093eb022d6c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 81
        },
        {
            "id": "7e7197b6-6308-423e-956f-f98f6de64b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 82
        },
        {
            "id": "e7361747-8e76-424f-9e75-b3d5c0bce35a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 85
        },
        {
            "id": "24bf73c2-7523-4c16-a192-f57f340f5d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 88
        },
        {
            "id": "49de740b-455e-4eb4-bfbb-cbebdc5047d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 89
        },
        {
            "id": "b3195d10-861d-4689-a703-c7c000364942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 98
        },
        {
            "id": "f866b12c-afc5-4bd7-9558-51674783f320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 99
        },
        {
            "id": "04b9b8ba-3a97-40b7-ade0-31276a8274d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 100
        },
        {
            "id": "ae8fa24b-7962-4d37-8ace-36b96fd0e282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 101
        },
        {
            "id": "16e36ae0-b11d-4198-b4fa-2d2f5b6b3c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 102
        },
        {
            "id": "7138e453-ae12-4cdc-a443-780cfead8f65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 103
        },
        {
            "id": "a80743fb-af2e-4047-a5ce-3c211d0fd8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 104
        },
        {
            "id": "e3dbf5ea-6557-4b05-a00c-bd2dcfd494d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 105
        },
        {
            "id": "152b8f84-bf34-4197-a83b-d92375dbd3e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 106
        },
        {
            "id": "94d9e844-dc82-446b-b118-b01f41ca2191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 107
        },
        {
            "id": "6ab5cefc-3df1-4cc6-8e9f-2e82cb56a495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 108
        },
        {
            "id": "4bd0735b-8a85-4f54-b5e3-bf4a048cd48b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 109
        },
        {
            "id": "e4fe11c0-beea-4b7e-a5e2-6311ae980062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 110
        },
        {
            "id": "c032df20-80e5-43ec-be51-47ca370c1e1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 111
        },
        {
            "id": "d467adb5-dde8-4644-862b-fc45b7cefd29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 112
        },
        {
            "id": "0c84f0e5-4942-430d-a59c-a2eae39222d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 113
        },
        {
            "id": "f42acf5d-2f3e-41c6-ae87-404ce57749c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 114
        },
        {
            "id": "dc5fa521-56ae-42ea-8afd-08cc0233a0d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 117
        },
        {
            "id": "a525176b-057a-48b0-9334-5baf015aecd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 120
        },
        {
            "id": "84f326c0-408d-48fa-8ac6-fced07771493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 121
        },
        {
            "id": "f53fcf22-a54d-458a-bc0e-8148df44c293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 51
        },
        {
            "id": "a159127e-5086-4992-8e05-0ffbddb5a0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 52
        },
        {
            "id": "78018a52-23a5-4ee3-8785-e87472e45268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 65
        },
        {
            "id": "d3c35f2f-fee1-4fbd-8b57-0081cf03bfeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 66
        },
        {
            "id": "d9703440-8e1e-4bcb-9d93-f2a70e24f87f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 68
        },
        {
            "id": "49fd8187-9faf-4886-a399-e31118064d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 69
        },
        {
            "id": "cb6f633b-7177-48af-ac5e-d27a21ef0d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 70
        },
        {
            "id": "63479750-1f25-4b5e-9dab-00bf790e9a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 72
        },
        {
            "id": "ceb95bd5-fecc-4ead-a2fd-8f03fb0a9a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 73
        },
        {
            "id": "3c7b57bb-cd37-47f5-9f2f-9d841bcf2216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 74
        },
        {
            "id": "cf347e76-7634-4005-b889-bea7878ae21f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 75
        },
        {
            "id": "17e1d8e7-717a-4c56-9c66-a7752cf62be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 76
        },
        {
            "id": "0032bd6a-8662-4d36-90c9-d58fe557b0a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 77
        },
        {
            "id": "307f888a-05f4-41b3-803a-f29495c2e679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 78
        },
        {
            "id": "a24fe032-d8f0-47ed-b7ce-59c38c7e7955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 80
        },
        {
            "id": "a4a09134-f8b8-490b-9e2e-1ea6f183a6a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 82
        },
        {
            "id": "9c49b423-5cbc-45e1-8e6f-e8b3c6fdfff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 85
        },
        {
            "id": "98ee652a-11f7-4142-a377-63a85fbd48b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 97
        },
        {
            "id": "82294cdf-d2ba-4725-9d24-26cc061bf477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 98
        },
        {
            "id": "9aff8420-504a-4be1-aed8-42abd1d716d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 100
        },
        {
            "id": "b17c6342-a1a4-47c3-919c-cfb429286a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 101
        },
        {
            "id": "6eedac6d-2501-4531-bd64-01d6587fcd2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 102
        },
        {
            "id": "2b22887c-1d71-466b-8b39-807dc5509b0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 104
        },
        {
            "id": "50efe738-ccbd-4bd0-8ba3-90d8033ffc84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 105
        },
        {
            "id": "66c7e4fc-024e-46de-9716-434087501e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 106
        },
        {
            "id": "a941d3ae-ec8e-4bb4-90b1-b9a7be540b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 107
        },
        {
            "id": "20ffbbca-4849-4172-99a0-5a6d872d4357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 108
        },
        {
            "id": "49101ff4-6ffe-4b66-b967-f07f230de96c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 109
        },
        {
            "id": "b2615f5a-5638-4022-ad1a-aed74064f6f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 110
        },
        {
            "id": "8e494322-0fac-4caa-a222-ee967662ae1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 112
        },
        {
            "id": "d82f58e3-50f4-428d-8c4e-74ace43279c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 114
        },
        {
            "id": "1b516000-f312-423b-ab8f-c2af9e9a4ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 117
        },
        {
            "id": "155c1762-960b-434d-be41-aab4f4ca5051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 48
        },
        {
            "id": "c4ab6773-6591-42f8-91e0-21c18bb2e332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 53
        },
        {
            "id": "846afc47-477e-461c-bda9-835e81d07e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 54
        },
        {
            "id": "eed215f1-7fde-4b43-86ff-99cd185ae54e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 56
        },
        {
            "id": "4d134297-a625-4506-99ec-6cb8b7d4e9a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 57
        },
        {
            "id": "8b13c039-1a58-42b3-a505-50801893cee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 66
        },
        {
            "id": "fcdd7954-ff53-46bf-b072-54ba589eb1eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 67
        },
        {
            "id": "4a1b186f-8a24-41a3-90fd-10753ea4b7ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 68
        },
        {
            "id": "61daed6c-c381-4bd8-b825-2fd2e4af51a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 69
        },
        {
            "id": "68a8da73-98b4-453d-958c-b96f4d3075df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 70
        },
        {
            "id": "af635377-52ae-495b-869b-a10d896c6083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 71
        },
        {
            "id": "811273e3-ae25-49d5-b47f-56ef2ee8d88e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 72
        },
        {
            "id": "61755c58-72ab-4e52-91c3-1a4b9dbe763e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 73
        },
        {
            "id": "c0217784-b4a4-4c01-9c06-1204ba7fa840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 75
        },
        {
            "id": "3835c148-ac48-419c-bf90-43a29b313041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 76
        },
        {
            "id": "05485aad-3369-418b-b07e-69c63d6fa904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 77
        },
        {
            "id": "34ee131a-09c2-4519-b8c2-4a01a0ad4018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 78
        },
        {
            "id": "d9f30b69-14a3-425b-ac90-7fde59913eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 79
        },
        {
            "id": "7b0b87f0-8e0d-4afe-8289-256100e6d323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 80
        },
        {
            "id": "38f3678b-fbf5-4404-9b82-748fbd379c22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 81
        },
        {
            "id": "f41226b7-c230-40d7-bf1d-44b1b2293124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 82
        },
        {
            "id": "1947ae18-b605-478b-93cf-91c741d92ebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 83
        },
        {
            "id": "7b8d34af-c7b5-4abc-b0b2-1cc698d31ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 85
        },
        {
            "id": "73c19b6e-7811-49b8-babe-3ecb1735e34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 86
        },
        {
            "id": "74852ceb-e28f-4a2f-be51-0a3c0b2c0017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 87
        },
        {
            "id": "98995bab-23b7-4025-8678-3bca83193e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 88
        },
        {
            "id": "62ef8861-0253-419e-96ab-1213d2d1ef95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 89
        },
        {
            "id": "696fcea7-3ee6-436c-a20a-b98fdadb03bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 98
        },
        {
            "id": "8983e6c8-b6f9-4a39-9945-f4ea9eb9799e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 99
        },
        {
            "id": "4c1c9804-f3f4-447b-80ca-0bac60ab59f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 100
        },
        {
            "id": "d4e94786-63ba-4ef9-82cb-247aa0b61afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 101
        },
        {
            "id": "2e7819a6-9bfd-4239-80e7-e3b8581b5121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 102
        },
        {
            "id": "39269d70-d8c8-45fe-9b12-0c2ac9f745fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 103
        },
        {
            "id": "2d0d0e7d-cd51-4bed-8299-51e6f755ec60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 104
        },
        {
            "id": "864e75a9-3f2c-4735-a9b7-632a4ae7e85f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 105
        },
        {
            "id": "0e6aadb5-ce0f-484d-89a3-5b4edd611c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 107
        },
        {
            "id": "b4ec7bc0-ff3c-4bd1-839d-6447e1d532e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 108
        },
        {
            "id": "7b53a681-1dfd-4d18-acf8-3588b129ad29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 109
        },
        {
            "id": "5d59bb12-034c-4294-8667-fcc7fbe81c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 110
        },
        {
            "id": "7f5f15b5-31a5-4a09-bb98-14922082d331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 111
        },
        {
            "id": "781a2db8-026f-46e6-954c-ec874675d7e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 112
        },
        {
            "id": "40636fa1-95d6-4215-8971-bf804106d353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 113
        },
        {
            "id": "6e8a4a4f-c9a8-4fa9-badb-2775afd66edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 114
        },
        {
            "id": "6f18461c-b9c1-4c40-9955-03af6a4f218b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 115
        },
        {
            "id": "5b7c8eaf-9dfd-45dc-8db5-297ade944466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 117
        },
        {
            "id": "b3ba6704-94a2-4531-a5bd-4e6c5e7449cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 118
        },
        {
            "id": "214e3d1c-7eb1-4b80-8d0d-cfc947547d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 119
        },
        {
            "id": "ae0c4eab-dda2-49d9-a091-722c56042cd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 120
        },
        {
            "id": "c2b1c363-2879-40c8-8f56-ebf33f313ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 121
        },
        {
            "id": "df68eadb-348b-4359-88f2-37c2f5c1717e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 48
        },
        {
            "id": "1dc4794a-874a-428a-a4ce-f4618aad726d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 49
        },
        {
            "id": "f6d32015-aeb1-47d9-8b83-791d0a8fef67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 53
        },
        {
            "id": "04563e03-1b3a-40d1-bd97-c88a36f68fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 54
        },
        {
            "id": "bb0e2d93-c5d1-48b4-90bb-bec557dda046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 56
        },
        {
            "id": "1cd0263e-2e2a-49ea-9acc-2a3ebc2294bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 57
        },
        {
            "id": "06253f98-6c45-4ec8-bcfc-50d5d30f91c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 66
        },
        {
            "id": "1dd76efa-bef8-4f8b-a88e-d3c1dc543b8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 67
        },
        {
            "id": "c4a726b8-00b1-4fb3-99c7-78e8ca53f7d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 68
        },
        {
            "id": "869ce723-4898-4cde-8911-cb8b3fbd8344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 69
        },
        {
            "id": "e1823785-1de5-4a6a-bc07-1f3e09e5c052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 70
        },
        {
            "id": "38b4242e-f94c-409a-96d6-c1e97fe5b36b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 71
        },
        {
            "id": "6eb57b4c-13ba-461f-b067-e67341805273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 72
        },
        {
            "id": "a9a3de00-3a98-464f-9635-bd22b455a568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 73
        },
        {
            "id": "ecb3e199-2881-49a0-bd41-a5cca2fcdce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 75
        },
        {
            "id": "cc66a01b-dc1c-49bc-bd22-0d1b8fb07f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 76
        },
        {
            "id": "45f1965a-ea2f-4317-be1f-f6d0900a7527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 77
        },
        {
            "id": "d8efe09d-7e6c-4a19-be06-b879ee07a05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 78
        },
        {
            "id": "06efcc75-094c-43fb-a8be-38c9795eaf67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 79
        },
        {
            "id": "99c473ad-237e-45c1-bee3-a75315c6f429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 80
        },
        {
            "id": "83f9e718-6198-455a-b882-c9f8b16c7953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 81
        },
        {
            "id": "ab71b751-32a4-4221-8555-85dbc2beb6a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 82
        },
        {
            "id": "6149edc4-4879-49d1-81fb-fbaef9541706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 83
        },
        {
            "id": "5b326a6b-eb28-4150-8984-8803fa8beb82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 85
        },
        {
            "id": "567b3ea3-1997-47eb-bb1c-e98a83276653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 88
        },
        {
            "id": "3619cbe6-d89c-43d3-805a-2297e5f07c30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 89
        },
        {
            "id": "0bd9f65e-145c-4f34-9777-043ff4aa3090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 98
        },
        {
            "id": "1e451c48-0cf1-4371-ab15-a8337bfb9cc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 99
        },
        {
            "id": "3252e72d-af74-4add-9fd5-a8ae3d6e45c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 100
        },
        {
            "id": "9460c997-a88e-4b41-8e82-b3434c9849a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 101
        },
        {
            "id": "5a2efdef-edf0-459b-b258-d75e0ef4423e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 102
        },
        {
            "id": "4852c801-49a3-4d98-b8b8-f4389b416a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 103
        },
        {
            "id": "29306ba1-d802-4cd7-864b-a4d41f6c6916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 104
        },
        {
            "id": "52786943-cdad-4aac-bd89-50251f5ea1be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 105
        },
        {
            "id": "8c733ce6-405c-4a1d-bedd-96aae052ad86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 107
        },
        {
            "id": "a70c1768-c110-45ce-8b32-7af857f2745a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 108
        },
        {
            "id": "38fe792b-3c28-4881-b920-939508ab5928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 109
        },
        {
            "id": "e72263bc-167c-4380-a398-f5927ae2c1a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 110
        },
        {
            "id": "9a2284ea-1fb8-4cc8-8764-a1028b67c437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 111
        },
        {
            "id": "49d3fa50-e77c-489b-9ff6-f26be71357e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 112
        },
        {
            "id": "754099f0-2e57-42ab-b35a-57a1ad966c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 113
        },
        {
            "id": "880e1456-65b2-43dd-a003-5342b39a5a3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 114
        },
        {
            "id": "84c3cd1d-c6f3-4162-a96e-749326d66605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 115
        },
        {
            "id": "6a7fdc6c-a163-439c-ac91-a50a8587a483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 117
        },
        {
            "id": "4c1efb11-e5b0-4e91-8f7d-32238fd045ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 120
        },
        {
            "id": "6824b5fc-3162-4cf2-b020-1fe969a422a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 121
        },
        {
            "id": "ca4e7b5a-8c59-499d-9ede-edfa07639318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 49
        },
        {
            "id": "97529a35-1a7b-4089-8229-5ca8c3968346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "49e354ed-7783-4cf1-bc64-f2e1cb0db60a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 52
        },
        {
            "id": "f6428971-b583-46ca-acec-7e4ba188ab85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 55
        },
        {
            "id": "8dfbaba4-bcce-4732-bdf9-2ccb67d1589f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 66
        },
        {
            "id": "50d6fa3f-6fe3-47f6-bb34-1c36f89f2c6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 68
        },
        {
            "id": "fdd643ef-eca4-4336-933b-8f222da9af9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 69
        },
        {
            "id": "4eb896d0-c680-4f4a-872e-ed5e0784a405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 70
        },
        {
            "id": "9068bb97-0930-4467-a2a9-5da453424173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 72
        },
        {
            "id": "ae44c980-978e-4d1a-8632-d1eefe5e73c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 73
        },
        {
            "id": "76e573a6-0c56-42b8-b92c-79a51e686eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 75
        },
        {
            "id": "b51a0cc3-8d38-433c-b538-6116b67a38cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 76
        },
        {
            "id": "12ca4525-c9bc-4932-9a4f-834addfcd202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 77
        },
        {
            "id": "bd708263-8a8e-4bfe-a4f4-4f77e572647a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 78
        },
        {
            "id": "4cd8c5e7-4cbe-4184-9f57-17f0520ed902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 80
        },
        {
            "id": "6ee78d69-7654-4628-abbf-bb8b9034bb22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 82
        },
        {
            "id": "31c734f3-e11d-4de8-84e1-e3352433dc2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 83
        },
        {
            "id": "cdbb122c-cd24-46ca-8d7e-a91bdd3260b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "18e74fb0-6e84-42cb-bc5f-98fb7253bebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "197df3cb-77d6-402a-bf67-08275bccac1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 87
        },
        {
            "id": "aa6631cf-c0b9-4f55-882e-4998fed08c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "418f1d1c-f7dc-4673-9f36-1d162eee1ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 98
        },
        {
            "id": "e856251a-d4d5-45b0-a4a5-ddc4f19a002b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 100
        },
        {
            "id": "5c769a41-1a41-4603-9638-83ed3c0d7665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 101
        },
        {
            "id": "087665d0-c64c-4f2e-a8af-5e03e27d11b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 102
        },
        {
            "id": "f1a2146b-6f30-4690-a39c-dc746a77f5cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 104
        },
        {
            "id": "e09827b3-a8b9-4387-9e4f-9e697d6cbacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 105
        },
        {
            "id": "85f72b0a-e697-4fb9-b030-71d8ab3f44e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 107
        },
        {
            "id": "5869279b-8f0c-40b7-903a-2596d578ae6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 108
        },
        {
            "id": "e7c5fa75-0898-456f-b226-fe247780fbf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 109
        },
        {
            "id": "5290fa86-a7d9-4899-81e7-302dda504aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 110
        },
        {
            "id": "f8c07274-8a47-4d5c-8176-f7a9298fc0da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 112
        },
        {
            "id": "b1f001fc-8afd-4d68-9ed2-a7d0371254c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 114
        },
        {
            "id": "41017204-e43f-4d2e-aee9-0bc50482d5da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 115
        },
        {
            "id": "bc37522c-1d6c-45dd-a32f-91f9bb16bdb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 116
        },
        {
            "id": "021690fd-0098-4f09-8912-c9da64fe0eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 118
        },
        {
            "id": "bfe94e13-d8d4-4363-8217-fd3e0277aeaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 119
        },
        {
            "id": "4ac005ec-a7c2-429a-aec1-05af78850cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 121
        },
        {
            "id": "439cc332-6dd8-450f-b336-119cbb881038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 48
        },
        {
            "id": "a6fc6c60-d267-440c-946e-dec0d0fb3c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 53
        },
        {
            "id": "c023a041-cc28-4c23-aec9-09121b75701a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 54
        },
        {
            "id": "3c46a39c-1433-4cc2-82b2-eb389dc95758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 55
        },
        {
            "id": "e627b924-02bd-4e09-98ae-0d1d5a646448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 56
        },
        {
            "id": "59475b22-1acf-4cf2-8ad3-b2e478d97c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 57
        },
        {
            "id": "4633a1c0-798d-49e2-9afe-12e80f3be684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 66
        },
        {
            "id": "1f2bac38-6474-47db-b7e2-1ab25ff6fa8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 67
        },
        {
            "id": "a4d47fbd-3b34-4add-98e5-c23e741ec4e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 68
        },
        {
            "id": "be173ad9-7b33-423b-85e8-ab58b056c539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 69
        },
        {
            "id": "318a8b5b-2bd7-4644-84af-f8715b290c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 70
        },
        {
            "id": "b48f67be-8f1e-4f5c-865c-e225ed85f6c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 71
        },
        {
            "id": "bdaba803-ff0d-4cd5-95fa-89d81568e23a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 72
        },
        {
            "id": "bb74914a-db84-4ab8-879b-3e8106cf2242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 73
        },
        {
            "id": "97cd411a-8521-4725-bb95-4560ac2b47c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 74
        },
        {
            "id": "48cf4a40-f59a-4fc1-9643-deaaa750794e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 75
        },
        {
            "id": "423b9317-9957-4035-a61b-8881c6081e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 76
        },
        {
            "id": "c41a5706-a956-40fb-b69b-f55f846ed296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 77
        },
        {
            "id": "e5f82d03-559b-4247-b128-a1f690ee89a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 78
        },
        {
            "id": "d9a3c30f-d7d3-49d1-abcd-48d0268c428a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 79
        },
        {
            "id": "430a11f3-5c2b-4b4d-9c5a-23aed2356fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 80
        },
        {
            "id": "8fb90d36-1e7a-4d6e-ab16-c30fbfd512af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 81
        },
        {
            "id": "027ec089-a8fb-4b1e-a5db-86a217d20c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 82
        },
        {
            "id": "fe821957-847b-4e10-9dde-1f3a479d4164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 83
        },
        {
            "id": "943025fa-13b5-4eff-89c1-e64029bed0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "36f5bd12-709d-4bbc-a83d-8606fda8b818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 85
        },
        {
            "id": "9836b7b8-d272-4eff-93b9-3dad04bfe50d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "c2d91858-fed4-478d-b30a-ccb008316350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "82f741a5-6d91-48f6-b280-4cf81083d09f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "041973ea-8200-4a2a-9c61-97cb90d6e7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 89
        },
        {
            "id": "e75cc32b-a273-41a8-900d-d6195efd7a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 98
        },
        {
            "id": "fbe5f3ac-1601-4dbb-a130-ca81eed94ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 99
        },
        {
            "id": "8bb20fca-cbb0-471e-bbfa-7c1476d6d379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 100
        },
        {
            "id": "fc19e347-b582-43b7-9ca0-b0ba66a4be2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 101
        },
        {
            "id": "fbe9a36c-f18b-4491-b7d1-d039ad23f239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 102
        },
        {
            "id": "8d7d63da-5de8-4889-85c6-70f573556e1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 103
        },
        {
            "id": "be9b545d-cd05-480c-b6eb-d329a1320648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 104
        },
        {
            "id": "b70940ae-adf4-4872-84c1-ea99a2de69b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 105
        },
        {
            "id": "2532690f-dc94-4d09-b6ce-300b97a050c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 106
        },
        {
            "id": "b617e9a9-9a70-493c-907f-e6e1bb10c7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 107
        },
        {
            "id": "9be3926f-f72e-4841-86cc-ba1d65d7068c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 108
        },
        {
            "id": "000153a4-f6c0-42d7-9642-1bfcce148181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 109
        },
        {
            "id": "0792f2e8-2129-4bcd-a042-af9572dc1397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 110
        },
        {
            "id": "d33eb5b0-4ee6-4e4d-8e10-6a46234a361e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 111
        },
        {
            "id": "52768d11-3833-4723-8af5-89ea032ab400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 112
        },
        {
            "id": "fb13c845-08ac-4890-9843-fad30b74fa77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 113
        },
        {
            "id": "42792f14-4a96-4842-9a58-b165986b657c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 114
        },
        {
            "id": "00a7706b-7ac2-4b69-ab35-3d6ac58923df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 115
        },
        {
            "id": "33a7bb2a-e34f-4b73-b456-4b85180a41bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 116
        },
        {
            "id": "97499b33-6a6a-4867-a9da-e29af22d36b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 117
        },
        {
            "id": "bd5aa643-af57-4735-b4bf-3fad6fd7b715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 118
        },
        {
            "id": "b084ea1a-512f-425b-9a4f-e43fd22b5959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 119
        },
        {
            "id": "29fee628-722b-419b-924d-40260e1cf506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 120
        },
        {
            "id": "4a5fc244-005a-4991-a381-ab6587d6c50e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 121
        },
        {
            "id": "18bf79c3-5a86-4146-ba14-5e6ff65ad624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 48
        },
        {
            "id": "e51c18ca-8399-44d7-9750-f57d67f6d209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 54
        },
        {
            "id": "0d9dadd4-5a85-4a82-9e99-74b28f712674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 65
        },
        {
            "id": "6bd3c7e7-0a01-4b67-b982-e4e26400666a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 66
        },
        {
            "id": "7d66b911-2b21-45a3-b97c-e879399545f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 67
        },
        {
            "id": "2c37cd06-61be-4cba-938b-b4f679e1245d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 68
        },
        {
            "id": "49c6123e-eb53-4e32-9304-152c0a5c4f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 69
        },
        {
            "id": "a12f3933-3751-4dcb-af98-6f2962a527eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 70
        },
        {
            "id": "56649f85-10b6-44a3-99ae-6952ecd94841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 71
        },
        {
            "id": "38baed8a-84cc-4fa9-969a-4b82671a10f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 72
        },
        {
            "id": "8c8e69b4-a633-49fc-bf82-bfb110eafd0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 73
        },
        {
            "id": "74cf4422-c63f-4ecf-9669-2b65a229c968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 74
        },
        {
            "id": "200f8366-c630-41df-85fd-5d3ad21626aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 75
        },
        {
            "id": "427c7aa8-5fb9-4fc7-9b13-8168bab9f390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 76
        },
        {
            "id": "d9b6a5c0-b0e2-49d0-8cd6-e63de843c614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 77
        },
        {
            "id": "9bde518d-44ff-435a-a7fb-c4a8dcc032f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 78
        },
        {
            "id": "bd11fcfb-5f51-47df-9101-e2989fa603df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 79
        },
        {
            "id": "bfb5302c-d751-4b2a-91c6-42d89bbbc055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 80
        },
        {
            "id": "ba61fcfe-ca29-4be0-b670-9540d1f84c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 81
        },
        {
            "id": "505a4824-e023-477c-b523-e7175dbc07de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 82
        },
        {
            "id": "332afb6c-0cfa-476d-90f6-9acf5facc0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 85
        },
        {
            "id": "a6afd10d-d380-4f38-adc7-b66049900ca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 86
        },
        {
            "id": "bb7b08be-fe7e-4280-a141-cab742f0f28c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 88
        },
        {
            "id": "d85c26ff-1bd2-4ef0-bb66-3bfd4bc58908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 89
        },
        {
            "id": "c8f3dd47-7ddf-4781-97c4-8b6e38635508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 97
        },
        {
            "id": "2bfacf35-1347-4ee8-967c-a5a432a7173e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 98
        },
        {
            "id": "98e0a365-126e-4430-b5b5-de767691f05c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 99
        },
        {
            "id": "dcbd7ba6-da98-41dc-ab23-ad0fff39db83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 100
        },
        {
            "id": "be6204e8-b50a-4db1-8f7a-ad7097fce361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 101
        },
        {
            "id": "b46da164-9c65-4096-bda8-d2204dac46bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 102
        },
        {
            "id": "fc6c690c-86fb-4205-91a9-ca6592c7a19f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 103
        },
        {
            "id": "2cc51b02-f3f7-439c-b38a-697aeb007d87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 104
        },
        {
            "id": "93664efe-702f-4680-a1c7-37dce1b1cc0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 105
        },
        {
            "id": "5e1d0450-8713-45ed-864a-052abe10193b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 106
        },
        {
            "id": "e064fb3a-0f25-4f67-8a2f-bae05e40e13b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 107
        },
        {
            "id": "99fd74a0-e264-4c4f-aebc-0ee1050ce87e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 108
        },
        {
            "id": "6f92bf32-3f98-4b69-98a3-408da8d6d284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 109
        },
        {
            "id": "b38ecc2c-920c-4ac5-b8ee-9eeb469061bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 110
        },
        {
            "id": "28fd4a34-8738-4e69-b774-3697b182681d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 111
        },
        {
            "id": "c9128c5f-7a81-4455-8c85-e1fc94a852ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 112
        },
        {
            "id": "04769f69-47c1-460f-a788-79779255f533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 113
        },
        {
            "id": "8193aa77-0509-4dfd-a549-fabd0bf68701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 114
        },
        {
            "id": "070f84b0-5635-4353-b114-d2dbfc1984c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 117
        },
        {
            "id": "8a5ac088-597c-4021-8b8a-e6c2e56319ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "0cdb48ce-f1d0-4cd3-8ab3-9da2b7306d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 120
        },
        {
            "id": "660f02a7-ba47-411c-a88d-a07900c60c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 121
        },
        {
            "id": "d52e0a46-35a2-4a76-92f7-a9b6165f1b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 48
        },
        {
            "id": "0c586071-9c46-47fa-8f51-9bd14ece7f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 49
        },
        {
            "id": "945ed35a-305b-4100-8c21-a21962a9485c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 53
        },
        {
            "id": "4607d35f-bd04-4a0c-a1d3-b50d2d3fa205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 54
        },
        {
            "id": "953a1b65-d6f0-4da8-a884-78d8b2f3bb7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 56
        },
        {
            "id": "0d1fa0f9-286d-4eff-a8ff-f281834e690b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 57
        },
        {
            "id": "ff4639e2-6cc0-47d8-8f3b-0accede6aa52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 66
        },
        {
            "id": "298a4f93-5aef-42fa-a677-7fac1ed2ea8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 67
        },
        {
            "id": "16d3b5ae-0ae0-491b-bb63-42250d3bb008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 68
        },
        {
            "id": "bc79b1a4-d03d-4e7a-ab26-ed9610e96ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 69
        },
        {
            "id": "07ecd356-2cb3-4788-a810-3a3592c4b3f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 70
        },
        {
            "id": "82323a6e-070e-4f3e-b77b-70fb52c7056e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 71
        },
        {
            "id": "3098736c-f599-4b34-86a0-1a3a42edd5d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 72
        },
        {
            "id": "5e1d9af1-fa95-48f9-9554-bb135dc96cbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 73
        },
        {
            "id": "7d43557f-45ff-41b2-a977-602b358e9c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "4541618b-5b7a-4c92-a2cb-d6ed335999a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 75
        },
        {
            "id": "9a75c74f-1a46-45a3-8c4c-29819f2288a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 76
        },
        {
            "id": "4a0c8c4d-f1cb-4c60-aed4-5a0845abb163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 77
        },
        {
            "id": "dde4f73b-0695-41cd-ae6c-aa26ca28c980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 78
        },
        {
            "id": "6618f895-cac4-49ca-9d8a-ecd52316321f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 79
        },
        {
            "id": "69a7ae8b-4d7f-4e2d-aab5-7f19b9990a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 80
        },
        {
            "id": "65c7575e-20f0-45db-ab93-b6f88cc7fce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 81
        },
        {
            "id": "67325915-5779-4927-8dfd-fc973893c6af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 82
        },
        {
            "id": "709472ed-b0e5-4825-8cfe-8af9bd5058cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 83
        },
        {
            "id": "c7d93edb-30a3-4e6c-a191-05ef61f51a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 85
        },
        {
            "id": "f189f564-b3a8-4f8d-aebd-640669ed9cc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "2a493b01-bd0e-40d8-bc66-09c37f7f41c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "2adae7b3-9fbb-41e1-9e68-9a82d3a2040b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 98
        },
        {
            "id": "6185a19a-e624-4503-9959-e79d645cec4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 99
        },
        {
            "id": "2ee06112-0ca7-4b06-bcca-fc1e023706a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 100
        },
        {
            "id": "d5a471b1-cab1-49ce-94c1-bddc2b811ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 101
        },
        {
            "id": "0d0775b3-fcfa-4abc-be36-30d2734d3d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 102
        },
        {
            "id": "1b8fc31e-74c0-4fde-b945-9064dc605f85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 103
        },
        {
            "id": "f827d540-090e-4e48-8f36-5f124074f77d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 104
        },
        {
            "id": "c2f27e14-89b1-4719-bdba-a854adae3f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 105
        },
        {
            "id": "e9e5861a-c56d-4453-a5db-2c3880c0bba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 106
        },
        {
            "id": "9fb33932-1497-4704-a349-26d232b249d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 107
        },
        {
            "id": "eca4fda3-cd99-4a64-a4f2-c38a8e38c9b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 108
        },
        {
            "id": "7607b3be-24cb-4460-a9cb-ce7bf21dd8cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 109
        },
        {
            "id": "aab4acbf-dc30-482f-bf7d-f7f4ceb09bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 110
        },
        {
            "id": "be9bfe6b-1d46-47de-bc63-5f5cdc155ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 111
        },
        {
            "id": "9573ac1f-edec-4b39-a83e-56a4ee299f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 112
        },
        {
            "id": "4d134d37-f880-4fc4-99f8-3fd25e59db5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 113
        },
        {
            "id": "b301338a-b427-4e6d-8c47-5d84499dae9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 114
        },
        {
            "id": "5e35a6e3-0632-4f63-b326-713b7563c7db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 115
        },
        {
            "id": "00c6a430-4e2c-468e-b374-776e39027fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 117
        },
        {
            "id": "d6fc29ba-b3a0-46e8-8247-603a854e8a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 120
        },
        {
            "id": "0d1dbdec-b259-4435-bcd2-97e8c583f44f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 121
        },
        {
            "id": "bc03d8af-34c0-413a-a08e-19a353c61657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 52
        },
        {
            "id": "ac8a917f-1bf2-4811-859c-9142e35c910a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 50
        },
        {
            "id": "53207913-238b-4a58-beae-921d3fccbff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 51
        },
        {
            "id": "d01c5b14-1835-4530-af6a-0396d48788f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 52
        },
        {
            "id": "67ea7388-9053-4668-bf17-609e740063fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 65
        },
        {
            "id": "3132688c-3e9b-47e2-b1f6-f6936b34e27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 74
        },
        {
            "id": "ea6eb0fb-a1b9-4422-a70a-43997a1a8e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 83
        },
        {
            "id": "7fff51bb-58d7-45fe-854c-27b355a8bcf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 97
        },
        {
            "id": "1e5aede9-a7c7-42fe-afff-28ec93b741c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 106
        },
        {
            "id": "038661c2-1076-4b2a-9818-6583297e547a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 115
        },
        {
            "id": "7f43f50a-4d69-4bf3-b7aa-97a2e286bb18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 48
        },
        {
            "id": "e914d5d0-42dd-4cdd-9a25-d6891aec38d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 53
        },
        {
            "id": "f27f0f31-0a43-4372-a60a-1bdf184e94fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 54
        },
        {
            "id": "78d3721d-99d6-4f97-b9ac-893841e29f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 56
        },
        {
            "id": "79ec1d02-7405-412f-a938-20c5823ca0fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 57
        },
        {
            "id": "e4c578f7-49bf-4e5f-8b63-df67aba952d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 66
        },
        {
            "id": "9c2bcd27-eb41-4d88-85a4-a14309d2aeab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 67
        },
        {
            "id": "61caee78-ad96-4233-876e-f6be942939aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 68
        },
        {
            "id": "e1991765-234f-4ede-9ac6-0e653fe69f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 69
        },
        {
            "id": "71f3ae89-8594-47f1-b12c-e678545cb1bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 70
        },
        {
            "id": "6be8e5fa-94cb-4c68-8e15-2b558c62fd10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 71
        },
        {
            "id": "1df53344-eccb-4702-8f04-d39aa1599b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 72
        },
        {
            "id": "2fb12fb1-665e-46a8-894c-7da2e9cf4a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 73
        },
        {
            "id": "bf087b3b-ad58-46bc-8490-8fc5f547a8e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 75
        },
        {
            "id": "7b9104ef-4847-4433-9aef-6f1fb0081bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 76
        },
        {
            "id": "452f39ea-98c8-4ffa-a091-3887878fa310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 77
        },
        {
            "id": "f8489185-599b-40f1-9e8d-926f38642fbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 78
        },
        {
            "id": "d4cef241-7a2c-481b-b26e-f889356b7c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 79
        },
        {
            "id": "aaa02c97-43f1-4c17-81d7-dae784d6a336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 80
        },
        {
            "id": "f6f6355a-95bf-45ce-8087-05fa4f72c250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 81
        },
        {
            "id": "e0f2773b-9a01-4ef8-8de1-12f1528ac474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 82
        },
        {
            "id": "93cecde3-f069-4d2b-b6f2-5999bac1290c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 83
        },
        {
            "id": "cf466ed3-3c36-4b55-94c1-1ed499c775de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 85
        },
        {
            "id": "049017fe-0249-44b1-8472-606140ee0223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "02aeb9a2-4c21-48a7-b972-a5874bf83137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "35dc8d27-526c-4976-9439-98f209a6c630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 98
        },
        {
            "id": "f1b509f5-e708-4bd2-8426-c6facf70eef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 99
        },
        {
            "id": "fe6dda25-9e30-4b6c-8ba3-348ee5f611c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 100
        },
        {
            "id": "0a15f733-6ad4-4400-a72e-22be41967f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 101
        },
        {
            "id": "286a2bd1-649f-4660-9cba-e3096e67b447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 102
        },
        {
            "id": "55aa96a5-439e-4aa8-8df0-a65e61a70b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 103
        },
        {
            "id": "92eed1b9-245f-42dc-a03a-4f12b3ff64f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 104
        },
        {
            "id": "adbba514-4885-4519-b7f2-4d0dabab3fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 105
        },
        {
            "id": "d2a23a6e-b258-430a-9755-6d67c6152ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 107
        },
        {
            "id": "8633c76b-13ec-4832-8015-08155afad191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 108
        },
        {
            "id": "371e755f-c8a7-4a67-8409-4f81bd3641b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 109
        },
        {
            "id": "39b9f046-dfed-442f-b358-351f47d8c9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 110
        },
        {
            "id": "9d9f57d5-ac0d-4d1b-a3d1-1ea8195608f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 111
        },
        {
            "id": "4b704ebb-8743-42b0-bca4-04fb4a8c4cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 112
        },
        {
            "id": "33d573dc-1e83-4d47-9993-7a5d3780f8d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 113
        },
        {
            "id": "de4e390b-0682-49f1-938d-61debaa8b784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 114
        },
        {
            "id": "02ff3dc9-c06c-4750-8490-4d9aea26be02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 115
        },
        {
            "id": "5e96f4d7-726d-464d-a551-ab60e5d6b9b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 117
        },
        {
            "id": "1f7165f2-a5da-41d5-8226-9ca8f95e081d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 120
        },
        {
            "id": "40f635c5-1d74-4c8d-8a0e-7c66b13ceea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "8874b9f5-4416-4a16-9f18-24c06546c886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 48
        },
        {
            "id": "4167c1b8-87e1-4b02-bea7-5911187650d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 49
        },
        {
            "id": "ef89c8cc-9bfd-4bbc-ae26-e4048afef5b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 50
        },
        {
            "id": "73feee7b-07af-4eef-885a-1701401cba95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 51
        },
        {
            "id": "a5db4636-97d7-4d81-b5e0-abdc8b81090e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 52
        },
        {
            "id": "4a5336da-b0e1-4e38-9771-2c3304bcd90d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 53
        },
        {
            "id": "edca5d1e-b9d4-4bb3-bd24-95b37bfbce70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 54
        },
        {
            "id": "749e1ebd-f6fd-4082-bd9d-1a1230c4483e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 56
        },
        {
            "id": "1ce375f6-2bfb-434e-baa2-e27010bc8d47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 57
        },
        {
            "id": "43704e71-aa3c-4a1b-a4b7-70e1c0f1dc0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 65
        },
        {
            "id": "947f52b3-493a-4a15-ace8-1bdd63b1c0e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 66
        },
        {
            "id": "f22a6dc8-25d7-458d-8ee5-68dd3e95d09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 67
        },
        {
            "id": "c285d5d1-7953-4a7a-820f-e0f06c54b31c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 68
        },
        {
            "id": "2f7a0c11-5655-4c58-8682-23e47378ef50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 69
        },
        {
            "id": "f15bc67b-a22f-45f6-bc96-ffc8df3625bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 70
        },
        {
            "id": "fc5da5f4-f4e4-4a91-b60b-080e20bde29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 71
        },
        {
            "id": "4373267a-eccc-4f57-ae86-c53424b59f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 72
        },
        {
            "id": "d88800ec-e5ba-4576-93dd-bcf4932e2a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 73
        },
        {
            "id": "6d5d5dbc-beaa-44d1-8b2f-94c57e499b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 75
        },
        {
            "id": "3068a415-2653-4586-bcc6-f488e81d5526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 76
        },
        {
            "id": "2b509475-a926-4a99-81bd-6afc77b17201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 77
        },
        {
            "id": "f84c6144-954e-473c-b14e-a79785e9f1f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 78
        },
        {
            "id": "ed8a418c-a0ea-428f-87f6-d1611f5527c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 79
        },
        {
            "id": "4aea9c0e-32d8-47d0-b7a5-8c4e9d8ce1a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 80
        },
        {
            "id": "6d517582-5613-402c-91a7-f6c10299a69a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 81
        },
        {
            "id": "82fac776-be35-428b-b8b9-3a7c9fb7c4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 82
        },
        {
            "id": "7ccb602a-00cc-487e-823f-fca2520d6852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 83
        },
        {
            "id": "11fb1f03-ffbb-4cf3-9a66-130029d43117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 84
        },
        {
            "id": "5a095678-2759-4a2c-a2a2-1e62b3c7549c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 85
        },
        {
            "id": "5f223007-acf1-4910-a930-746934b48cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 86
        },
        {
            "id": "a3003e85-1a37-4ee8-a7ce-3adb49cf0e91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 87
        },
        {
            "id": "8ee2d2ba-0acb-484c-a880-ac919e243555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 88
        },
        {
            "id": "85fa0089-0a1d-485d-ac39-000ef523fe1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 89
        },
        {
            "id": "c77bc585-8894-46cc-893a-2539727aae5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 90
        },
        {
            "id": "6de0b76a-c181-4b54-8821-92a672fdd42a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 97
        },
        {
            "id": "bc0a9377-b086-4b9a-8477-2dc24f1f969b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 98
        },
        {
            "id": "72d9a5f3-fdf9-42c8-828e-cf936646a263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 99
        },
        {
            "id": "c01a2700-2ffb-42e5-89f5-e818a56d4f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 100
        },
        {
            "id": "096d4f19-3b49-41ad-ab15-f28b16e75f1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 101
        },
        {
            "id": "811944ba-56a8-4eda-be1b-f296286e1f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 102
        },
        {
            "id": "70bc48ba-6e26-4d7c-af0c-45faed787412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 103
        },
        {
            "id": "00bd1ced-96a0-4bb0-9970-34748f5f39e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 104
        },
        {
            "id": "1cbd6bcc-4ffe-4e3e-a040-f24f7603e226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 105
        },
        {
            "id": "50f00ab7-b05f-4c58-8629-d9033cbff21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 107
        },
        {
            "id": "f02bad3b-240d-4796-9e46-849285cefbc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 108
        },
        {
            "id": "a4bb2188-5184-4169-a1d2-98ed5cdf1784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 109
        },
        {
            "id": "e8fee0d8-d04a-42b5-b15d-9b14db54d8c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 110
        },
        {
            "id": "869bef00-3c30-41a0-9291-47373419f167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 111
        },
        {
            "id": "09d0da40-0861-4b49-84ac-f58985873b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 112
        },
        {
            "id": "b0c88acb-53aa-4ca8-aed1-a23a85dda33c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 113
        },
        {
            "id": "3b7b6321-c2d1-467e-93e5-588439f8ba52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 114
        },
        {
            "id": "692a9287-55f9-4e94-ba3d-5f7bcfe3d88f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 115
        },
        {
            "id": "f0cafdae-1ac3-43e4-97ae-c0e49b5040fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 116
        },
        {
            "id": "361f4a92-d836-4756-9d86-c14648394a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 117
        },
        {
            "id": "ba4d6d52-8909-4367-bfd6-80b6be5f2b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 118
        },
        {
            "id": "0bda0796-1023-4f95-93cf-008b559fa7ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 119
        },
        {
            "id": "49b0ce22-8b9b-403e-b6fd-b262083ae8d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 120
        },
        {
            "id": "93cfa0cc-f645-414e-a7a3-41dcb11174d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 121
        },
        {
            "id": "41244962-d0c5-4de5-97ef-1203156c0b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 122
        },
        {
            "id": "b9e55602-2f3c-4a4e-9d74-85800c63898e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 48
        },
        {
            "id": "79dc331f-69e5-4a05-b956-58de6eac27a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 49
        },
        {
            "id": "4091952e-c0f8-4693-ac2c-ae98cc5ffa0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 50
        },
        {
            "id": "1680a7d7-cf04-4c07-9e9b-3a7699d587aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 51
        },
        {
            "id": "6423241c-4722-4f04-9ae0-aa42a05483d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 52
        },
        {
            "id": "d5e9db6a-6661-4b4e-8eed-03b9527d2b61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 53
        },
        {
            "id": "a5db5ac3-f675-489a-9417-daf9b9362776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 54
        },
        {
            "id": "f56b61b6-8e0a-4baa-a260-346d98627462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 56
        },
        {
            "id": "93225029-4285-4002-80a7-751c1d443a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 57
        },
        {
            "id": "31e03e07-e24e-4340-9faf-b4b34dca784e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 65
        },
        {
            "id": "c24682df-39ae-4322-971a-8c8565e24c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 66
        },
        {
            "id": "1ce3c544-d8f7-41c1-b11a-ac66e57ce76b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 67
        },
        {
            "id": "ed94d93d-0a96-4982-adfc-9316c2d28965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 68
        },
        {
            "id": "bbb618b5-83b7-4808-ab70-ab5d3f7eeb3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 69
        },
        {
            "id": "89013a7c-89b1-4b5b-84e6-5e1954a34971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 70
        },
        {
            "id": "641a6e9d-3b50-45a1-b51f-7b793d8fefbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 71
        },
        {
            "id": "27cc31e9-4cc5-4b46-badf-4688aafd8476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 72
        },
        {
            "id": "fb807c71-e393-4abd-abb3-dc5b2a25118a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 73
        },
        {
            "id": "9f8dd818-3734-49b8-87d1-9d92d9981e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 75
        },
        {
            "id": "7870aa19-99ea-40f8-907b-be76fbd3ff14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 76
        },
        {
            "id": "7b81fcf3-d1a1-45bc-8825-5b323dc4b768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 77
        },
        {
            "id": "dd2fb6e4-c9a3-44ef-8da3-3831c94302f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 78
        },
        {
            "id": "d21006ce-34da-46c9-afef-018b3bed999a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 79
        },
        {
            "id": "8b04eceb-49a2-4710-ad98-c6f481e6aa33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 80
        },
        {
            "id": "0956dbe1-c3b3-414c-b577-5c62addc0c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 81
        },
        {
            "id": "6c4336ef-766c-478f-93a7-6a5e5de5b6a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 82
        },
        {
            "id": "7de98c4b-7fb6-4510-b2e3-e5f078589f2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 83
        },
        {
            "id": "681062fb-e6b8-462e-9294-272e339258a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 84
        },
        {
            "id": "4ebc9de9-672b-49dd-ba41-92dc4c67c561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 85
        },
        {
            "id": "d2101ae2-4e4f-4727-80e6-752465d24f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 86
        },
        {
            "id": "6b4926e2-749d-4c98-ab1e-9f14d0312356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 87
        },
        {
            "id": "96bcc801-a473-477a-b6d0-96ac60d813b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 88
        },
        {
            "id": "9379b9c0-1f24-4686-b2e3-63fbd9e2c8db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 89
        },
        {
            "id": "4f80b5a8-e8f7-481a-902e-9dee32c5e5a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 90
        },
        {
            "id": "02e76834-ea95-4d07-9036-f07a5cd92ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 97
        },
        {
            "id": "17304a40-cd2e-408a-9798-4f154ea19d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 98
        },
        {
            "id": "3502a02b-2b24-4b4c-b38a-d3aa78179221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 99
        },
        {
            "id": "999bff71-3f1d-492f-95dd-d06b3e85e1ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 100
        },
        {
            "id": "b02e536b-82f8-48be-9a98-310f2f34a38c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 101
        },
        {
            "id": "75260754-430b-4758-ac79-1dbbd0b17427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 102
        },
        {
            "id": "b63d0d05-c9c1-494c-bb84-421217b74bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 103
        },
        {
            "id": "7e0f5a07-08b5-4784-86be-dca9409271ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 104
        },
        {
            "id": "bd0b19f5-a1d8-4289-8ed0-7305e6c720f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 105
        },
        {
            "id": "18944e9c-dc49-4802-8d38-b842ca0b67c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 107
        },
        {
            "id": "7cd92507-bf7c-4a7f-ab0d-a77df92baead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 108
        },
        {
            "id": "c64b0e0f-b8c3-4cbe-a5ed-bae3bf35ab0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 109
        },
        {
            "id": "3fc41cd4-0cd8-4a42-b28e-42bf8034ab3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 110
        },
        {
            "id": "cd624a93-f113-4c54-bc39-bec2d57ca4aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 111
        },
        {
            "id": "fd36ac56-7a50-48d1-a5c2-b039d9af3118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 112
        },
        {
            "id": "2c842358-ac85-463d-a42e-8bc1edf84c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 113
        },
        {
            "id": "126643c5-d313-402a-a223-51f12e74e3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 114
        },
        {
            "id": "ce06260f-b06a-488f-8064-fd0ea633cb81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 115
        },
        {
            "id": "4c3f1541-80a3-4244-92b6-01af4541920f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 116
        },
        {
            "id": "d47b35b8-0565-4051-96f9-551a248ae66f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 117
        },
        {
            "id": "1ada820c-141c-46d1-ab83-9765ce69dbbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 118
        },
        {
            "id": "4ce50ce2-9e34-4990-a64f-7d74f8388242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 119
        },
        {
            "id": "e31754f9-70f4-4ee8-b87b-72ccd17f00f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 120
        },
        {
            "id": "fcda6237-a1a3-49fb-9b1c-25e86d9bc543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 121
        },
        {
            "id": "3da0f80f-349e-4002-b325-702306c65823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 122
        },
        {
            "id": "d0d5c6fb-3ccd-4b05-bdb1-08d8e1242f91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 48
        },
        {
            "id": "9528ab93-7e4a-49c8-8230-3aa10c81972c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 49
        },
        {
            "id": "335fb7a0-1376-4b30-b306-cba83fbf96b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 50
        },
        {
            "id": "7463f22e-c6bb-4602-9992-c52b1dbb8d79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 52
        },
        {
            "id": "df0e1442-8594-4e42-a2e5-bfc32ddbd916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 53
        },
        {
            "id": "360067d0-ed85-49f6-a3f3-7758212e9e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 54
        },
        {
            "id": "2ebdee7c-46a3-4c88-bc66-cc1bdf064e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 56
        },
        {
            "id": "70465dbf-3251-4da7-bf72-6ac95db3d135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 57
        },
        {
            "id": "4e1f2c04-08e7-4fe9-8f6a-4655cca5e294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 66
        },
        {
            "id": "7c2cf8ce-5767-44e9-ae39-ad55dd4c8976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 67
        },
        {
            "id": "8d388bc2-58bd-4f0a-ac33-65d0df072e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 68
        },
        {
            "id": "fba1b097-3df4-4f1f-99bc-bcca0e8fef5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 69
        },
        {
            "id": "5cdfd3cb-e705-4bbb-a163-9c6cb6cab21e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 70
        },
        {
            "id": "e2d1c4ce-b599-408a-abdb-071cc1fd747f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 71
        },
        {
            "id": "1937e0de-6c04-4650-ac0d-0607b1e78481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 72
        },
        {
            "id": "1789022f-5123-4ecd-95a1-01a9458333f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 73
        },
        {
            "id": "364c324a-a820-4e80-9b08-0fc45374dd94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 75
        },
        {
            "id": "5c866943-30b4-40ed-9dd5-affe35a550c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 76
        },
        {
            "id": "6eac937d-7545-45cd-9437-e1b76b4416a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 77
        },
        {
            "id": "52004862-a8d1-4a33-9ea8-04922d051f7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 78
        },
        {
            "id": "a2a842b9-e6bd-4e83-9d96-f238f2692c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 79
        },
        {
            "id": "739015e9-79cc-408b-ace3-7e9d2f986a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 80
        },
        {
            "id": "b12887fc-38f3-46a5-80ab-4befd4db751b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 81
        },
        {
            "id": "1945edab-119f-4e7f-a36c-586fcf024a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 82
        },
        {
            "id": "b6b11843-c832-4fd1-a360-c7dd6a8381cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 83
        },
        {
            "id": "dcebe000-9ab1-471f-bda6-bbc0271fd2b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 84
        },
        {
            "id": "0a34a76c-8b4d-4aaa-88d4-4ea7fa899337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 85
        },
        {
            "id": "66b6a4fc-0ec4-4b33-9392-0d1af2aed504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 86
        },
        {
            "id": "a12ce908-7396-4bd4-b0e9-37721a5fdc61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 87
        },
        {
            "id": "0d026eff-6584-49d1-b547-f341620c2b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 88
        },
        {
            "id": "c49501a4-7bbe-4a32-8115-a8131776c4c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 90
        },
        {
            "id": "55f14a85-3663-44d3-bd41-afff4aba4bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 98
        },
        {
            "id": "9ff9c33c-1c13-4a28-a37f-858791f2291b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 99
        },
        {
            "id": "449f6e63-1be2-40df-b1bd-3c1d0283cd47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 100
        },
        {
            "id": "26420495-807f-45b3-a023-05ce10867c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 101
        },
        {
            "id": "d3fa1faa-d31f-457e-8903-fb235b3ac3a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 102
        },
        {
            "id": "ab6d7f16-562e-45c2-be81-7dc9a8481e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 103
        },
        {
            "id": "a26c8006-b7de-412b-bbb8-2ddb82befddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 104
        },
        {
            "id": "8ea7d0f9-edb4-4ba2-a777-f12d5c1bf36b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 105
        },
        {
            "id": "0fa762d3-c922-4237-aae4-604f6691cf02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 107
        },
        {
            "id": "ab4c200e-f749-47bf-8d06-891d653f648f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 108
        },
        {
            "id": "81934d8b-3e6b-4872-97d2-3334f26156e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 109
        },
        {
            "id": "bd4c1f76-4f75-4b00-ae3b-06c119037a9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 110
        },
        {
            "id": "8f6d62a4-801e-4f77-85d1-56392f022167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 111
        },
        {
            "id": "8fa0dd3f-15da-4590-aa74-49c6aae3edd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 112
        },
        {
            "id": "ffc61f0a-015b-421d-a0b9-d2d00350d95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 113
        },
        {
            "id": "f6438a66-ee30-4a13-8e21-cc6b8e48b8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 114
        },
        {
            "id": "f2e80de5-1040-4f49-9a0b-a4493be7f85c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 115
        },
        {
            "id": "0d5d6a47-4901-4ce6-b4dd-9efde50271ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 116
        },
        {
            "id": "85e28a2e-cd01-41ad-bfec-b3461f8edc6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 117
        },
        {
            "id": "e17176b6-e17a-4194-ae13-e231ea06a0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 118
        },
        {
            "id": "760b24f9-bfc1-479d-ad96-ddb7783243bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 119
        },
        {
            "id": "240a2432-a1e6-4a1e-ab07-cd63f7d8a59c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 120
        },
        {
            "id": "22e43006-ebb8-4f00-a134-33a2ca068305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 122
        },
        {
            "id": "956b8579-7f4c-4a31-85f1-52ab7efb1b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 48
        },
        {
            "id": "1cec132b-637e-4e5e-806a-b9745b691a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 49
        },
        {
            "id": "10a93fe3-d306-490a-b9a3-220d030773f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 51
        },
        {
            "id": "5a478167-0f5a-4dc3-9fe3-06d4df37f158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 52
        },
        {
            "id": "e4ccc5f0-1629-49a8-90bc-e5f1ee238f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 53
        },
        {
            "id": "047fd7b7-e7f1-470a-99de-656d24e38a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 54
        },
        {
            "id": "9a0d2adb-7323-42be-81e2-407e4f6dc543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 55
        },
        {
            "id": "446c7528-da6f-408c-86c6-33762a310953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 56
        },
        {
            "id": "51ed36a3-bc01-4359-8c2b-ed87f2e6d290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "a0e9430a-ee21-4e04-9d76-9bcbe15fd387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 66
        },
        {
            "id": "7b469e89-d503-4add-9c0d-7c97f56733f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "2f63a240-ff6b-4a50-8d9a-463a6a1f90b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 68
        },
        {
            "id": "e774a15e-cbe4-4886-a4b1-1648ef2e31f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 69
        },
        {
            "id": "992c0626-e728-4840-9ae8-b6d6ca0f24f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 70
        },
        {
            "id": "f364cee9-7320-4e3e-ab9b-d61702e77f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "1ee49a77-292a-4d37-b993-04d913c8fca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 72
        },
        {
            "id": "4b2f96df-bf1c-4eef-b66a-ef7fa26cfc30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 73
        },
        {
            "id": "cd3042aa-bbc6-48d0-9bb2-a1ef2185682f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 75
        },
        {
            "id": "d548e794-9188-4887-80fe-03444aa03304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 76
        },
        {
            "id": "de919efa-8a3b-4cc8-ab18-c56cbd78385a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 77
        },
        {
            "id": "039945cd-c4ce-4a3b-b725-f812bfbea784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 78
        },
        {
            "id": "9051970f-4ed7-48b2-962b-f16651b879b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "8f0a87b7-b5cb-4aa7-81ff-95c028b5e85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 80
        },
        {
            "id": "bf52686c-cab4-4f16-8716-39d079ed98c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "3a98c582-1704-4ad6-9ec8-ecc8595f398a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 82
        },
        {
            "id": "b0fed5cd-89c7-4767-b855-463d62b888cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 83
        },
        {
            "id": "9b110d37-8fb2-483a-bf53-4418476dbbfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 84
        },
        {
            "id": "c641fee3-77a4-42ff-8997-60599d884a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 86
        },
        {
            "id": "1a668886-4620-41b4-af5d-2edfe639d8c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "af713420-2b53-4114-9f4d-1dcfa02e4069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 89
        },
        {
            "id": "d6101d27-908a-4ccf-94df-c793965e404d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 98
        },
        {
            "id": "5da834bb-46d3-47ff-8dbf-f75df87c7dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 99
        },
        {
            "id": "d276fc22-a7cd-470c-8697-c9c49719dbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 100
        },
        {
            "id": "9b9fbb6b-6322-43de-a64a-0e153c59ea3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 101
        },
        {
            "id": "eeb3c954-e413-46bb-b860-e0165eb61579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 102
        },
        {
            "id": "6e8cf3d8-4850-4058-91fd-dfbc502b23c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 103
        },
        {
            "id": "d0fe52ff-d0a6-42bb-88ac-7043ba210fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 104
        },
        {
            "id": "23e0ed1e-c87f-45c2-a8d7-9c16cb41c8dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 105
        },
        {
            "id": "3c1f78b3-bf43-427d-ab8a-7fb8a4316749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 107
        },
        {
            "id": "239ec5c4-c6db-4ba9-b429-060e733d4dea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 108
        },
        {
            "id": "bb93a26b-2db9-41a3-adff-d3c8fe3fce87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 109
        },
        {
            "id": "ce335f93-7c7e-4359-a48f-d892b04cab3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 110
        },
        {
            "id": "0fbd1544-5962-4e05-a088-812feca2074a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "e343b34f-4e90-4de8-a61a-5723901fbdfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 112
        },
        {
            "id": "53002bf4-0e59-4deb-8c50-0ef316a15424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 113
        },
        {
            "id": "7dd9b61c-e934-49b3-9937-c09ebd10261e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 114
        },
        {
            "id": "d3622967-4174-4d30-86b6-443372597e73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 115
        },
        {
            "id": "9db838c5-ae35-43ca-a720-7c8200891c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "2378ca8e-7b2e-42b5-ac41-007d4324e0b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "fc312f5e-8dd5-4b21-b1e0-6d7e12432268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "9793b43e-a18e-482a-80c1-6001fb3c3988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "06a88d56-f6d0-4a77-8c39-0d56c9f50b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 49
        },
        {
            "id": "66e6f96a-683b-4471-91d7-9e2fb3807430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 52
        },
        {
            "id": "cfb6105f-b68c-4f45-bb58-33094e14d7e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 55
        },
        {
            "id": "06e7835f-c37b-4eeb-b052-7098ee94aacd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 83
        },
        {
            "id": "b79ce0d6-f510-4559-ad90-330799fb7d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "468c5dc0-2d6e-403b-a85d-312f8bcbee04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "b36df782-f7db-48c0-bc88-c07501762820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "0f02befc-90db-4d05-9cfe-003a536c30cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 89
        },
        {
            "id": "f23bba37-2e1c-4bca-80a8-d17646588a70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 115
        },
        {
            "id": "da4211c7-a3a7-4fbc-b472-128743b6ad46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 116
        },
        {
            "id": "d464660d-bba5-4af6-adac-5fbedf1d83b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 118
        },
        {
            "id": "a6575543-c84c-4c8e-b44c-e4cfd55e8e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 119
        },
        {
            "id": "fde2e4e4-fb78-4d16-ae8b-cd5b0e71c78e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 121
        },
        {
            "id": "8357e292-13a6-4eb3-8238-45bfaa2b8501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 48
        },
        {
            "id": "bc06b4c4-1798-4d7b-80ef-af3182875f23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 49
        },
        {
            "id": "10b7462c-33bd-4085-882d-4bede69da418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 50
        },
        {
            "id": "9f1598bc-91fa-43be-951f-0a9796ce70af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 52
        },
        {
            "id": "158a9b07-308e-4f30-a8ea-98131b2e9614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 53
        },
        {
            "id": "2395fcaf-4043-42f1-b81f-13ae328821fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 54
        },
        {
            "id": "2a98500f-69ba-4372-8a46-0edbac056f0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 56
        },
        {
            "id": "d8f6d671-7177-4182-89dc-63407034b4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 57
        },
        {
            "id": "e14a55be-fc60-41e3-9ada-0bd84c9e19f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 65
        },
        {
            "id": "a77e3f34-d004-4987-a47f-fd35eaa9826d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 66
        },
        {
            "id": "720059e2-8ed2-401b-b010-564e45159c18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 67
        },
        {
            "id": "ee997589-5ba4-4baf-af51-ab5fe02f633e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 68
        },
        {
            "id": "6bf7ab5e-08e6-4579-9ce5-d19b814d1ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 69
        },
        {
            "id": "6632acc0-639a-4d93-a828-a826fe85b83c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 70
        },
        {
            "id": "ac32bac6-538e-4b6c-b990-213ef65402ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 71
        },
        {
            "id": "23215ef4-d809-49ce-b573-a7084a395f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 72
        },
        {
            "id": "ee27fa27-b361-4b1c-8e23-89e3ad83049d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 73
        },
        {
            "id": "4d2d6a8d-0a89-4a50-8374-3af3d0af132e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 75
        },
        {
            "id": "ae118f07-71d2-4663-bad4-a2288b30d1a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 76
        },
        {
            "id": "569abdef-d616-4bc8-8e30-d4659757f4b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 77
        },
        {
            "id": "8e7ed057-d1bd-45e4-92ce-4903bb51e3c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 78
        },
        {
            "id": "eed0e1e7-aac7-4dff-ad88-b7577d7a859c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 79
        },
        {
            "id": "d6f3d2b4-cf15-41c2-8267-5ffa3ab274c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 80
        },
        {
            "id": "2f92caea-fec3-45f3-bc12-c3e08005e975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 81
        },
        {
            "id": "3abc2d3f-0b44-490c-b6ef-166e53fc9163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 82
        },
        {
            "id": "414b49ec-93e4-4003-af78-04e8ec957066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 83
        },
        {
            "id": "eca76ff4-0618-405b-b90e-17c6bfdb40ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 84
        },
        {
            "id": "5dfc377f-e16e-4eaf-b4c3-5f1d04948439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 85
        },
        {
            "id": "7e5837b6-bb73-4fa2-bfab-b7510fa6d91d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 86
        },
        {
            "id": "82cb5a3f-16bc-4616-949e-605f1a9a18a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 87
        },
        {
            "id": "377be66f-400f-4c80-b397-894ad7319e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 88
        },
        {
            "id": "c67644ca-ce2e-4b0e-9db9-50bafa51a1e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 89
        },
        {
            "id": "156e6a6f-dcb0-4a65-bef2-6b7deba3e8e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 90
        },
        {
            "id": "53f183c5-d596-44bf-ab94-747a949924c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 97
        },
        {
            "id": "f5b38c19-b206-49f0-8ceb-374a02417f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 98
        },
        {
            "id": "71c025c2-4110-46f5-81d8-7a3a603a9210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 99
        },
        {
            "id": "7b62bba8-cfba-46c9-a575-02d72e6c927e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 100
        },
        {
            "id": "57f2fea2-623b-4147-9357-842ea4dcbaed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 101
        },
        {
            "id": "9737099a-030c-4e35-86b4-cb10d30e139e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 102
        },
        {
            "id": "d837be53-fe5a-4503-adde-7a8e80b75823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 103
        },
        {
            "id": "f5ab08d6-3c7e-40c6-a020-f17d6e0c807a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 104
        },
        {
            "id": "fda988a6-ab11-4c55-bc9c-44e352443897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 105
        },
        {
            "id": "5d1ea74e-ca5d-4e53-8f3f-1f520a1f2f7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 107
        },
        {
            "id": "f2a6b279-0ca5-485b-92b9-4ffee9751ef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 108
        },
        {
            "id": "99d6f89e-80ea-4a25-b258-5f9bc809cdcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 109
        },
        {
            "id": "e42297c0-64e7-4701-9f94-bac82cea36c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 110
        },
        {
            "id": "3bca4084-a9d5-4cfc-9a07-ad8964719768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 111
        },
        {
            "id": "5444fd32-034f-492c-9667-b7249360a72d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 112
        },
        {
            "id": "41513711-6e86-41c6-a501-30df9cc20c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 113
        },
        {
            "id": "73d86d3d-9779-4d4b-91c1-aff9840eb823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 114
        },
        {
            "id": "569654bf-aaf4-47ee-9e93-8edc985a66fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 115
        },
        {
            "id": "d8c571f1-bdb9-49fd-8aa2-c8b8ac7adeaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 116
        },
        {
            "id": "8ac1b75a-975e-44b3-b81a-df16b174046e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 117
        },
        {
            "id": "b0d93c6d-22c0-46df-b8bf-96fc27472fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 118
        },
        {
            "id": "81ff1c66-1496-40a4-84bc-d5716286f7df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 119
        },
        {
            "id": "9ae7a82b-e8a6-4b65-9430-504d97320fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 120
        },
        {
            "id": "0ecb8e23-804e-4000-b8a3-912672732421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 121
        },
        {
            "id": "a6990ed3-bd39-49a2-b9d1-0428f3d30b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 122
        },
        {
            "id": "cb52cd58-de2f-4941-ae3e-c2fad1a67fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 48
        },
        {
            "id": "ff6d151d-a6fd-4d80-8349-c670f2cb8cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 49
        },
        {
            "id": "6de41f4c-3cca-4d28-9227-61eef7887da6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 50
        },
        {
            "id": "fc4c3faf-e341-48e5-a360-9a68e5c08832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 52
        },
        {
            "id": "70719286-2c4d-494d-8f75-d6c53d95005d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 53
        },
        {
            "id": "6b528e92-019d-4694-b5c1-e3367ef74a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 54
        },
        {
            "id": "1d504f62-cc7d-4de7-adb6-bc289c215ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 56
        },
        {
            "id": "b4dfa0f2-3ab0-4f4d-9232-50caba95dc4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 57
        },
        {
            "id": "23d31d83-5928-4daa-b807-5cc9c250fd9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 65
        },
        {
            "id": "6a0d3c7d-e01e-48eb-82b5-6f51a9a8fe5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 66
        },
        {
            "id": "d1835d36-6ebb-4b18-af79-44cb1d9dd010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 67
        },
        {
            "id": "1b253816-1555-4b89-a667-02b938e69766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 68
        },
        {
            "id": "7eef0818-5b71-447c-aba9-2b323e7691a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 69
        },
        {
            "id": "6fc785de-6fb5-4190-be10-f96f746f67c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 70
        },
        {
            "id": "8d590a1a-c414-4f94-bf6e-712ef642b868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 71
        },
        {
            "id": "5ef2e56d-3be6-4c57-b815-168e34ceec7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 72
        },
        {
            "id": "3245ec1d-215b-47dd-92d8-c40039373e99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 73
        },
        {
            "id": "6f125047-5c13-41a4-a547-88b69be96d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 75
        },
        {
            "id": "16dabee9-1df9-4ba2-92d8-27257965eb73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 76
        },
        {
            "id": "42d7474e-078e-4060-ae25-62017c2178fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 77
        },
        {
            "id": "c54405fc-0f26-484f-987f-1d74ad6fe9fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 78
        },
        {
            "id": "bea2c54a-4990-4e96-baff-ef5801e57a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 79
        },
        {
            "id": "60b3a112-7dce-44c8-859c-c9a064a7ed6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 80
        },
        {
            "id": "402194cd-441c-470b-bfe2-91ad56f5737b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 81
        },
        {
            "id": "0cb294f8-e948-4239-83c5-15ff940be15e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 82
        },
        {
            "id": "bd2fd239-e8ce-4802-9500-201d3fbd6770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 83
        },
        {
            "id": "4d5758b2-3ed4-4911-a040-8d47ea0c972f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 84
        },
        {
            "id": "960e6220-ddf6-4856-9591-59c0a65fe054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 85
        },
        {
            "id": "0a5bebc1-7b85-48e3-b036-67e23c5ec121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 86
        },
        {
            "id": "77d79ec9-ec5c-4e3f-b83b-cf137cee4ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 87
        },
        {
            "id": "7956e4d5-dda3-4746-9437-257b55083164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 88
        },
        {
            "id": "870ae2e8-22ba-441d-a0a1-ab9e59f5880c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 89
        },
        {
            "id": "bcdceef0-8247-4183-a3c7-9654c062c98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 90
        },
        {
            "id": "57bd14b0-12fe-4c90-b602-b82428bf8bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 97
        },
        {
            "id": "a661682a-ff47-4cac-b14d-48d425ac79b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 98
        },
        {
            "id": "f2bb4e45-9f20-4971-a923-596137d020b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 99
        },
        {
            "id": "e8b8c22f-b548-4bff-b1e3-90cb92e21d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 100
        },
        {
            "id": "9c45bd79-ef4b-45fe-89b5-7bfe75601393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 101
        },
        {
            "id": "8b0e19ed-9c7c-44b4-9a57-476f265341b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 102
        },
        {
            "id": "9b509c33-4ca8-4a9f-a8e9-f9b147748027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 103
        },
        {
            "id": "8003f4d5-b064-4495-9f3a-6c19c4f82250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 104
        },
        {
            "id": "5f56e598-7834-4e80-964e-9f355f002f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 105
        },
        {
            "id": "2e4fdc08-c142-4d81-87b7-4beea22fe69d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 107
        },
        {
            "id": "c0834b2f-416e-41de-92d6-00dcdb73e4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 108
        },
        {
            "id": "67da79f2-b9c6-4420-8a9c-14e3ddb1bfeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 109
        },
        {
            "id": "eb2143b3-632e-4f80-8781-a9f7d49a3eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 110
        },
        {
            "id": "6df0edf0-8a76-4a70-a8e5-f0bd6a62dea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 111
        },
        {
            "id": "aef02bdc-9b95-4afd-8500-3f24a50461e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 112
        },
        {
            "id": "8e155ae5-17d6-4247-a034-6da01544045a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 113
        },
        {
            "id": "fb598aa5-8acd-4e91-96b3-51e734d5bac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 114
        },
        {
            "id": "548bd99f-96e5-4630-b335-1307b4f54178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 115
        },
        {
            "id": "600c5235-7036-42c8-ae48-e55dfdc1dd93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 116
        },
        {
            "id": "90395c94-2b6c-4e33-b2d9-8aa75249dc24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 117
        },
        {
            "id": "f2929c2c-df83-433c-a5e3-fe47f0defae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 118
        },
        {
            "id": "90e87aae-c826-4dd4-ac1a-98cfa9b600e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 119
        },
        {
            "id": "f5e369fc-3627-4cc4-816e-87880d13ba11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 120
        },
        {
            "id": "3211fa86-19d2-4cd4-8823-dc4a5675347c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 121
        },
        {
            "id": "f5f8fe89-a922-4c94-91bb-9788e34239fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 122
        },
        {
            "id": "5703aea5-b736-4a0e-bb48-cfe80e6c67aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 48
        },
        {
            "id": "a693281b-f077-48c5-a04f-a4357d352c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 49
        },
        {
            "id": "847e08de-4614-44e1-b042-fa2489b32e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 53
        },
        {
            "id": "5eed7353-3532-47e1-a8c4-6e2a4e247cbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 54
        },
        {
            "id": "4f810a3b-c388-4fd2-bf2c-d4a3849f9d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 56
        },
        {
            "id": "c006e574-79f3-4b99-a4d9-a72cac71de67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 57
        },
        {
            "id": "7a18322a-4b77-43b8-bc4c-b339f04b7199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 66
        },
        {
            "id": "7b1a66fd-a2df-4a7c-851f-54603337724c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 67
        },
        {
            "id": "448af106-3168-424c-b591-3c5fe7a9080e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 68
        },
        {
            "id": "c8064702-08f6-49a5-aa1c-d61b5badf278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 69
        },
        {
            "id": "83fd2c79-d796-4803-88cb-30af04e1e14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 70
        },
        {
            "id": "b3ae4a68-f4f3-4e61-bb5f-2d83b44cb9ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 71
        },
        {
            "id": "4e27dbc9-6f80-4532-93c7-9a98740b2be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 72
        },
        {
            "id": "216303ca-0711-4ac6-95b9-8c926e72ddd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 73
        },
        {
            "id": "21c7f08c-6b23-421b-a121-9510185bd1c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 75
        },
        {
            "id": "6af51bc6-8a4a-4327-820c-7208895182be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 76
        },
        {
            "id": "3515f757-fc1c-458e-add6-647ec0e8cdc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 77
        },
        {
            "id": "82785ca1-e105-4620-9655-c2fd148fa115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 78
        },
        {
            "id": "432d5ea3-85b1-4f29-8deb-66bba201b236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 79
        },
        {
            "id": "82edcf8a-21e4-4240-b670-b4a9c6ebab65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 80
        },
        {
            "id": "7bad8555-f7a0-4e5d-aab5-f28590016c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 81
        },
        {
            "id": "542ab819-98d7-4b28-8f5b-18050e09f524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 82
        },
        {
            "id": "889401fb-f1e2-43ec-b551-84e25f94d820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 83
        },
        {
            "id": "b9601592-50d2-4f23-94b1-d39a8653cce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 85
        },
        {
            "id": "2724aa32-aa1a-48e6-aa9a-b6e0e9903913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "e343de0e-77de-419f-8902-e2b88c2db558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "9419465d-f987-444b-b53e-ebf83153a2e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 98
        },
        {
            "id": "d8341c83-55a9-42ca-89e4-b2db3526a01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 99
        },
        {
            "id": "3f90b02a-41a5-4c01-96d5-efe20ee91ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 100
        },
        {
            "id": "b710f35f-9dc9-4acf-a685-c57b549bcd42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 101
        },
        {
            "id": "04751367-38f7-418a-9b23-508470f8e8f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 102
        },
        {
            "id": "328fbe15-afe0-44a1-a2fa-5719d849966a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 103
        },
        {
            "id": "43e9aa45-fd93-4af2-af35-9c29973bad14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 104
        },
        {
            "id": "0eaf6088-a9a7-4ccc-a94c-ce2b8504fed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 105
        },
        {
            "id": "68493a19-bbd5-4d18-839f-dcc536a49291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 107
        },
        {
            "id": "228a3908-a3a9-4873-bfdc-fa907b1d2519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 108
        },
        {
            "id": "0c51195d-5159-41ba-9cbe-c2c7577345e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 109
        },
        {
            "id": "4bc648a7-0d52-4bb5-a866-f9bbcf8b7945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 110
        },
        {
            "id": "30b075b3-88e0-4ed6-ac00-61968c08a2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 111
        },
        {
            "id": "e82c8d82-9b18-44c5-be60-6059b0400609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 112
        },
        {
            "id": "f2dcc459-ada3-4ed3-ba04-8778e180062b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 113
        },
        {
            "id": "243f0a9f-07dd-4702-b95b-2adc60f0d094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 114
        },
        {
            "id": "061f3e10-f95b-4ad6-9a1c-4e1b9486152e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 115
        },
        {
            "id": "1b1f4748-cac5-48ac-aad9-aa78a7c8ddad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 117
        },
        {
            "id": "df1db608-caca-4413-89a3-469d39a53e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 120
        },
        {
            "id": "0519bb1f-0f3e-43f0-9acb-b1d0b7044db8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 121
        },
        {
            "id": "ffcc8376-ad51-4956-a110-61c0e9f3bc09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 51
        },
        {
            "id": "c9861ac0-22f2-4500-8934-d653156a47e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 52
        },
        {
            "id": "839576c4-cb1c-4d94-ab00-c1ee2d050c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "da9964e1-2ee8-42f6-8290-2781d4736e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 66
        },
        {
            "id": "1cd98e67-5e23-4d98-92fb-2ac2b4fdd136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 68
        },
        {
            "id": "b5171ef0-5f3b-4baf-a649-d924fd3b8784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 69
        },
        {
            "id": "08f37e88-f7e5-43d3-b140-da681a3d668c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 70
        },
        {
            "id": "8b6b65c8-eec0-43b2-adae-a1c4e2aa3e7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 72
        },
        {
            "id": "11a64662-c6f4-4f68-9170-3a5754b1c822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 73
        },
        {
            "id": "80b633b8-36d4-4027-af1c-76c9ac1e2e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 74
        },
        {
            "id": "4b48b028-8277-418b-8c0b-e50449c77d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 75
        },
        {
            "id": "aba13c99-74dd-4366-8889-936ee526c46a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 76
        },
        {
            "id": "b81183ec-31a5-4374-a234-dcaa5999c427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 77
        },
        {
            "id": "dc484402-41e5-434e-aefb-3317d4a38060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 78
        },
        {
            "id": "3a29b00d-7e89-46cc-8e8c-6b8e830e1a85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 80
        },
        {
            "id": "99e2f953-b99c-4082-9d54-ab0118545f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 82
        },
        {
            "id": "19707fed-9166-422e-9633-3c75373376e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 86
        },
        {
            "id": "788a7154-e5a7-4fa6-b7e5-dd55790d0bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "086ffec3-6e9b-4c64-ad4f-7307ae6bfe7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 88
        },
        {
            "id": "491a6428-aee5-49e1-9c08-772c4a87b2d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 89
        },
        {
            "id": "e1a9f2c1-e6a9-451e-a7f9-b3ab4549a0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 97
        },
        {
            "id": "62e8f04d-bfb2-4ae0-b5f3-4147059d4036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 98
        },
        {
            "id": "9bfc4017-dc45-4221-9ecb-feb902d48f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 100
        },
        {
            "id": "d05d874f-aafd-4f65-91ac-54e7decea87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 101
        },
        {
            "id": "5fc859dd-ff47-4ffc-a941-78e5ad79738c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 102
        },
        {
            "id": "656b824a-1672-45bb-bb3b-9790fbe5305f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 104
        },
        {
            "id": "35b46ca8-cded-4617-8a0f-43ee75eba17a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 105
        },
        {
            "id": "6c9f796e-fa67-40b4-9148-72d139285a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 106
        },
        {
            "id": "e35b37f0-37c2-4c32-bc80-bea26d36cbb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 107
        },
        {
            "id": "b3907786-a263-4eae-b609-813ededfd23b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 108
        },
        {
            "id": "56a401e4-6af6-4607-bd19-83a872307a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 109
        },
        {
            "id": "d7d88ebf-6f0d-4836-9d4b-83464f979627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 110
        },
        {
            "id": "040f36de-3958-4a2b-8afa-ba793c81d4fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 112
        },
        {
            "id": "c63e3417-5ad5-4bda-891c-53b3b27f1914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 114
        },
        {
            "id": "c85b0ec1-0d80-40cf-a531-8aae3c06144f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 118
        },
        {
            "id": "ae4c91cc-08ab-4918-a48e-644f5ea5a895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 119
        },
        {
            "id": "206de7c8-f844-413d-afd9-4e6fafe96c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 120
        },
        {
            "id": "551f4973-0ff3-40e3-8905-0d476699af23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 121
        },
        {
            "id": "b7bf3730-e6bc-4e3e-849f-cf9069de64dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 50
        },
        {
            "id": "546df08d-1d79-49ce-b8d6-77626323b45f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 55
        },
        {
            "id": "d56d0ab4-6dca-4719-a677-052c9b0e45fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 65
        },
        {
            "id": "b6392c3f-1481-4f17-9ac4-46e400486926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 66
        },
        {
            "id": "887809aa-3512-4ceb-8754-0aef79ce57f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 68
        },
        {
            "id": "d8df7c38-3753-47d9-9ed2-f0344ae95dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 69
        },
        {
            "id": "b8fe92f2-b477-44a2-a649-71c1a1b7332e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 70
        },
        {
            "id": "b40778c2-2dcb-40eb-98d1-99f72683e511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 72
        },
        {
            "id": "ddc7b3e5-45cc-49b3-9aac-f7c13129a388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 73
        },
        {
            "id": "6a915cdf-8955-4661-99eb-249b33306437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 75
        },
        {
            "id": "ebe52014-537e-4cc8-82d2-03cf99d7c452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 76
        },
        {
            "id": "3e99d2ae-e7be-4cfb-83bd-4c8c6c8452b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 77
        },
        {
            "id": "3ff4ecc6-f5fc-4883-9b30-f5336f1df55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 78
        },
        {
            "id": "7a1e2ee7-f434-4d10-a65c-6f91b831e9e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 80
        },
        {
            "id": "dbd17229-0898-42c6-a7e2-4f3107b58443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 82
        },
        {
            "id": "7325b2d3-aa88-416f-9ff9-3c040318c5f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "875233ca-ffbc-4210-8d86-9250e37ff5ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "d7bf225b-005c-4a19-8359-cd561bfed602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "e6b64435-4f9a-476e-8dc1-578a5b31947e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 88
        },
        {
            "id": "1166e398-01dd-465a-ae91-03b32bfc0b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 89
        },
        {
            "id": "45cef0dd-a80b-46fd-85ac-03852b6042d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 90
        },
        {
            "id": "5300afde-f780-4e10-92f4-9e3335ca62ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 97
        },
        {
            "id": "eb9853c3-aa9f-4716-b04d-141aa3839268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 98
        },
        {
            "id": "3a09f69a-f034-4e4e-97be-bd421641e114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 100
        },
        {
            "id": "98c1fdbb-0404-4f28-bd41-96aaecbd6b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 101
        },
        {
            "id": "e26614c6-b718-4094-9303-10b7c13ec0f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 102
        },
        {
            "id": "6fac2a2f-fe80-4129-a954-586c05c4a328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 104
        },
        {
            "id": "494102c7-4086-4d91-865e-6f2b28e43968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 105
        },
        {
            "id": "57b5bd27-12b0-432f-b66d-844dd6cd3ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 107
        },
        {
            "id": "a373a9fb-1c52-4ad6-9a08-5f6136a8ff5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 108
        },
        {
            "id": "2bf745c9-dbdb-4a78-a21c-4f5ec6c9663b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 109
        },
        {
            "id": "ade8348f-365e-4d40-b35f-0f32137a8c7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 110
        },
        {
            "id": "85000214-4393-452a-88df-6b26821322dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 112
        },
        {
            "id": "52f64759-e859-4b83-a965-b1d5b745f54c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 114
        },
        {
            "id": "9dca208f-b266-4b64-8267-af90c92cb28d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 116
        },
        {
            "id": "178e519f-e33c-44eb-812c-69279d53c667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 118
        },
        {
            "id": "132b712b-8bd2-49a1-a86f-724205d2ec5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 119
        },
        {
            "id": "79085877-20aa-4e40-9af1-74f7feaa0304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 120
        },
        {
            "id": "2aff7825-e8db-4610-bfd9-9f661c38a04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 121
        },
        {
            "id": "afd1ccb0-4dc2-4600-843f-9b280a42bdfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 122
        },
        {
            "id": "6aebca26-3d53-42e3-aaad-c80c879ff57b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 51
        },
        {
            "id": "f8748689-2eed-484e-8909-4449362dbebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 52
        },
        {
            "id": "d2f10e8e-7b34-4d72-b0f6-fe8099a804cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 53
        },
        {
            "id": "7f81a717-72d1-4d36-ab6e-7e84008542bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 55
        },
        {
            "id": "b6ad8034-f9a5-47b3-9276-a0ee3b5d6729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 65
        },
        {
            "id": "53a1878b-dc0c-46ac-a3cb-b1a91092c74e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 66
        },
        {
            "id": "8ecc72e3-7ae7-4076-b775-bdebb87053d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 68
        },
        {
            "id": "165ed2c6-559d-47eb-92c0-b5910e7f1a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 69
        },
        {
            "id": "aa847102-70b7-44d4-8e14-3b3743cac84d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 70
        },
        {
            "id": "4a898151-d464-48ad-bb64-6f875876e25f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 72
        },
        {
            "id": "358d3bc8-2c52-40d7-9e24-1708b12249ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 73
        },
        {
            "id": "feefc572-c374-40d7-a692-38ba03434a97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 75
        },
        {
            "id": "7778253b-a385-4b20-bff9-96bb90b165cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 76
        },
        {
            "id": "90674f4d-515c-45f5-b7ed-3034ce8dbab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 77
        },
        {
            "id": "d0513c49-5d36-4b67-9428-a3361776b7ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 78
        },
        {
            "id": "a54e7af8-ea42-4bd6-b419-1c8e138bb092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 80
        },
        {
            "id": "c068323d-39b8-45ca-bb9c-6fca7940be1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 82
        },
        {
            "id": "198e913e-93c4-4656-8c1a-52101949200d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 83
        },
        {
            "id": "b3dbe56b-73db-43c1-9cef-d4e23e45ebd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "14bcb17d-9701-4f48-bfa9-833c07c8bf14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "98b20df9-8fc3-4757-bc4f-0eaf7e4cf722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "631a5900-360b-49c4-b916-ce1ac78f7b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "2b82e4bd-6f48-4e91-8661-65277b68a5c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 90
        },
        {
            "id": "ca955a0a-8e77-4927-83b4-5eb439f4aeb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 97
        },
        {
            "id": "570b31cb-090d-4c47-bd9e-782b181fc5b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 98
        },
        {
            "id": "29727c94-c889-43bf-9344-a7d0dbe49ab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 100
        },
        {
            "id": "4647cd42-b55e-4b07-8387-731c5528f64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 101
        },
        {
            "id": "d3266476-e603-4ff9-9628-a7f9b2d1ad8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 102
        },
        {
            "id": "4cbe0405-b53e-4b91-ad09-ac0376791ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 104
        },
        {
            "id": "122f748e-28a1-44f4-a5d2-d08eafc1b28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 105
        },
        {
            "id": "b2dd1799-b695-4b4e-ada0-6559aba204b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 107
        },
        {
            "id": "ec0b1029-c358-41a2-be06-989ae12a61b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 108
        },
        {
            "id": "c6af7ec2-33d0-4669-9005-cc3412fc058b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 109
        },
        {
            "id": "05946d97-ec89-4b91-86f0-a0a46cabc47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 110
        },
        {
            "id": "d748ddec-58e8-43b1-bd2c-4aa0e0327ad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 112
        },
        {
            "id": "eae340d4-d851-413a-b673-c71e7e20a2d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 114
        },
        {
            "id": "e39dfb4f-8d56-48f8-bc40-eab51eba518e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 115
        },
        {
            "id": "d0623f8b-1f6d-4d91-b5f9-33da17d77d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 116
        },
        {
            "id": "1244afec-df2c-43f2-875d-e1bb33717322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 118
        },
        {
            "id": "538fd6c2-2884-43ef-82aa-35c87e30be52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "cfdc2b01-c1c6-4656-9106-5e7b4264573d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 121
        },
        {
            "id": "ff3be5a9-fa36-42af-9ed4-0df47e0d0612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 122
        },
        {
            "id": "e9a3b35c-6973-49d6-9843-e65ea2dc9853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 48
        },
        {
            "id": "577845ab-f20d-46b3-bf14-d36696aec83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 54
        },
        {
            "id": "a9a2ec86-14f4-4304-b9b4-3a48135a874c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 56
        },
        {
            "id": "562ed077-fa9f-4d33-a2cc-031247153801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 66
        },
        {
            "id": "c8616afb-575c-4032-8b54-7ab07fa08ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 67
        },
        {
            "id": "10b62784-59e6-408c-9ba9-9ab480869914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 68
        },
        {
            "id": "ce9d101d-7584-4264-b46f-2cf814aa007d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 69
        },
        {
            "id": "462492d5-81bb-4e56-b03c-d806a597144a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 70
        },
        {
            "id": "37069889-da38-41bf-8314-998b4de422b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 71
        },
        {
            "id": "e7016224-a241-4e8d-b357-057b81f3051d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 72
        },
        {
            "id": "b615ac44-2f52-4d09-831e-62f19c9bc9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 73
        },
        {
            "id": "55057af8-0cb7-4eed-9da0-caf3476df08c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 74
        },
        {
            "id": "0b6564c8-1c9e-4c4b-9d0d-07b5a9eac0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 75
        },
        {
            "id": "23f1d649-42c8-4e0f-9b57-6c58a8e39a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 76
        },
        {
            "id": "fde31b46-1309-4b42-bb54-3bde88a59578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 77
        },
        {
            "id": "98aedf9b-5251-455f-a40d-a242f7499752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 78
        },
        {
            "id": "1e85fbcb-e730-410f-9dd3-82e324e1cb58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 79
        },
        {
            "id": "87c0ace8-3845-4412-ae73-1447c40bca97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 80
        },
        {
            "id": "8d13437a-f50d-4855-896c-6abadd8b8293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 81
        },
        {
            "id": "4ecb7a11-5a88-40f1-a57a-584f04b8e3f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 82
        },
        {
            "id": "20e715c6-2174-4452-867b-2da688005e33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 85
        },
        {
            "id": "c1873d55-0300-4a64-a539-dfef3b745b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 86
        },
        {
            "id": "038017f3-dad9-4321-8814-b4e73e1a68ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 87
        },
        {
            "id": "90fd943e-1ddb-4749-a2a6-3cd3029b0443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 88
        },
        {
            "id": "a06a66c8-7d7d-4b46-9913-7d5ec246af38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 89
        },
        {
            "id": "f054c1b0-e6d1-4fe5-aae9-dca18475999f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 98
        },
        {
            "id": "844b4e74-b30a-4c8c-8ecf-25be3062bd9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 99
        },
        {
            "id": "dd5fd91d-0620-490e-89e5-f74bf06623a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 100
        },
        {
            "id": "d9546c3a-4b4c-40fe-82bc-9b390d0d77f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 101
        },
        {
            "id": "d2ba69ed-adab-43f7-b276-05ae2b04b124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 102
        },
        {
            "id": "9b8c1a4d-20de-4fcb-a1ab-5721ad035fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 103
        },
        {
            "id": "841e8630-742e-454e-9a02-971a9c25aaa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 104
        },
        {
            "id": "66d24413-affb-4853-83f3-1b7ab5d92109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 105
        },
        {
            "id": "5ac66cac-e6cf-42d6-838b-72299d97b99a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 106
        },
        {
            "id": "31290754-4ad3-43ac-8e97-10ebc255f55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 107
        },
        {
            "id": "2428036b-de61-4f9d-8990-df7ab519fdee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 108
        },
        {
            "id": "3248687a-0ae6-4ab0-8fae-e62c6cfc2cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 109
        },
        {
            "id": "a235970b-e0c5-4249-a221-c838254ba840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 110
        },
        {
            "id": "3ccff793-de81-4078-8c26-7c42f6854a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 111
        },
        {
            "id": "6cde7191-4a2c-492b-b7cb-d89e04f4d967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 112
        },
        {
            "id": "6b05aadc-9853-4f3a-9783-5d42d28d69db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 113
        },
        {
            "id": "ee03fe26-70b8-468d-9ef3-53e91ebc8530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 114
        },
        {
            "id": "bc3985b4-c8e4-4e50-ad9a-cefb1ac33569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 117
        },
        {
            "id": "fbf7f08d-51e0-46d3-9ab0-3d9ad6d29f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "0c5febff-9468-45ba-8286-7c6d4f58aa87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 119
        },
        {
            "id": "d7750a6d-8bff-4fc6-982f-94a3c50f9334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 120
        },
        {
            "id": "36fdbfb9-4fa9-47be-b6d5-35e8d2d2eed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 121
        },
        {
            "id": "361a9b2c-825f-48bb-a69f-50fb0a5cceba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 51
        },
        {
            "id": "c865bf39-e1ad-41a8-a1a5-bf9ad92b4092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 52
        },
        {
            "id": "679f4cec-fe6a-4f9d-a219-92c5ac2e9f72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "7d0e0afb-1136-4447-b4fb-d96d9e5fbf25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 66
        },
        {
            "id": "050883c7-4744-4fce-b958-bb983eaa1fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 68
        },
        {
            "id": "8d7dfe81-edb4-41c1-b3bd-324d6e4854f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 69
        },
        {
            "id": "ad74f831-e3d4-4041-9dc0-063453c64932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 70
        },
        {
            "id": "d6266b9a-42ac-4856-9eb5-279d8337fc12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 72
        },
        {
            "id": "42569bf8-34c7-4caf-bd30-5d31ac499e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 73
        },
        {
            "id": "9fb05976-4815-4c07-9830-1d36a21dfac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 74
        },
        {
            "id": "4f68425f-3fdd-41f8-a93c-68e922cf5843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 75
        },
        {
            "id": "20d25191-9bac-43b8-9cb9-759a488b8a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 76
        },
        {
            "id": "70b651a2-c5f0-49a1-883b-defa8874f025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 77
        },
        {
            "id": "38aeaade-1eb6-46a6-a650-170292262b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 78
        },
        {
            "id": "063c03eb-e4d4-49e2-ad07-28c735d5a334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 80
        },
        {
            "id": "3a300824-61e2-404b-a7d2-f7d5f1cdf70f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 82
        },
        {
            "id": "a631f203-a571-454a-b86f-c205d0958755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 85
        },
        {
            "id": "15982258-a347-4942-9fb0-a44f44d45bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "77830433-0adf-4d04-893d-d75c15c00ea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 98
        },
        {
            "id": "d6906f48-fc21-45b8-aedf-73c8b58071be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 100
        },
        {
            "id": "b80b6119-ef60-40fb-9b04-bc51e63ea122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 101
        },
        {
            "id": "5c605649-c5a4-4160-9bf9-08dc20bf8800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 102
        },
        {
            "id": "01cec4fc-290a-43d4-b7c1-e1ac0ba377ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 104
        },
        {
            "id": "45601579-a611-45a4-9901-59aab5b5e456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 105
        },
        {
            "id": "560a2e6d-0662-4e38-8286-aae9e18f495a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 106
        },
        {
            "id": "930edde5-255e-42d2-aea9-894cfc5c45eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 107
        },
        {
            "id": "613ba896-d9c4-422c-8dd9-15daf3d9ac4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 108
        },
        {
            "id": "eb0835ec-764d-4697-9d0f-971b3484435a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 109
        },
        {
            "id": "ffb30d35-7f6f-409e-9915-5d2441b650ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 110
        },
        {
            "id": "43e58621-340f-4b3f-ae49-e830619834d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 112
        },
        {
            "id": "1fbd77e5-a400-4f80-9bbf-13ab159a2295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 114
        },
        {
            "id": "26d3224d-7d0b-41cc-afc9-797a81c2698b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 117
        },
        {
            "id": "a06c6457-ad42-4583-a9b7-c054508869e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 48
        },
        {
            "id": "09387fb1-9ac7-4f87-b124-f8db3283f1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 49
        },
        {
            "id": "fd68b756-04e5-416c-b006-5ec41137b073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 50
        },
        {
            "id": "7e864185-f831-4831-9023-47fb6948443f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 52
        },
        {
            "id": "80e2c53f-c56e-4857-a795-0ca0d4e37cbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 53
        },
        {
            "id": "6376f8ac-8d8e-4847-a26d-6c68797f079d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 54
        },
        {
            "id": "8bfcd13d-dcda-4318-b297-1f5abcb03188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 56
        },
        {
            "id": "238d2623-66ed-4d06-af45-7bd48cb0160e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 57
        },
        {
            "id": "0a4e500c-abb2-4310-ae12-6836775b49e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 66
        },
        {
            "id": "f7925e8f-3bce-4e91-97b1-cf06a0c00a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 67
        },
        {
            "id": "2d638d30-a8f8-457d-910a-8638f4e7ff49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 68
        },
        {
            "id": "3f03af5c-4b39-43ca-920f-ff3fa0cd74a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 69
        },
        {
            "id": "d2a9feaf-bfea-4392-bc6f-4837855e6810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 70
        },
        {
            "id": "8f7de92c-d7ae-4e9d-9041-a2856227a90d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 71
        },
        {
            "id": "f8cb65ec-0034-4ea6-83f9-b6a0938e6995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 72
        },
        {
            "id": "1c746ec0-e1f0-4537-8209-4d6452a1e3a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 73
        },
        {
            "id": "68e2f409-f63a-4a88-930f-0a4ff9a79f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 75
        },
        {
            "id": "764ec54b-a6ba-48f1-bb9b-31ab7b0c1869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 76
        },
        {
            "id": "100b1022-b414-40c5-9031-d6a8a05fa9bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 77
        },
        {
            "id": "bfa4af83-4166-4ca9-91cc-02d020299eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 78
        },
        {
            "id": "4b268a49-cd8a-4d35-a4b9-6cf3f413c8a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 79
        },
        {
            "id": "b7bd8319-4e69-4926-a4dd-1cea133af57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 80
        },
        {
            "id": "268da646-7ad7-4eda-9dce-31475da5072e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 81
        },
        {
            "id": "740c74c4-6455-4ffe-9d71-6dc15a2e250c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 82
        },
        {
            "id": "5617d57b-b7ef-4c49-ada3-7809d69e5a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 83
        },
        {
            "id": "823e0ea9-a8e1-4e8e-9f76-aa916d8a97d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 84
        },
        {
            "id": "23ff0135-4f24-4be4-a64b-657d5f559f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 85
        },
        {
            "id": "a619bf79-b974-4ac4-8fdc-5101c33e518d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 86
        },
        {
            "id": "e566b046-9191-42f5-b47d-abea0a61d8e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 87
        },
        {
            "id": "70e34983-7895-42fc-bd86-cb9a895bc935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 88
        },
        {
            "id": "1558a0e5-9382-4bce-9c00-2ed1132e3bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 90
        },
        {
            "id": "1967d406-35c8-4d79-8dc5-03fce232fd33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 98
        },
        {
            "id": "dee59d1e-cd35-4635-8224-f293efc5eedd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 99
        },
        {
            "id": "73c28aae-5ce8-4a9f-bf96-cdc2157dee44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 100
        },
        {
            "id": "f762c7a8-71e6-4134-a0fa-bd57b6d0c33a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 101
        },
        {
            "id": "e947cee1-e9a0-41dd-a18a-d2eaa8618f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 102
        },
        {
            "id": "faaf0d45-a5d1-4612-9ada-ae2d0f66e7ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 103
        },
        {
            "id": "280160ca-a673-447a-bbdb-9b38880a4b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 104
        },
        {
            "id": "41353941-3860-4416-b0a6-401a094fc959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 105
        },
        {
            "id": "bb47fc9f-604e-4ec5-b8da-a94207db419a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 107
        },
        {
            "id": "f433089f-5efc-40ba-aa47-338a20074032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 108
        },
        {
            "id": "1f1a79a5-b94c-4322-af94-37550f08b554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 109
        },
        {
            "id": "522df942-3e89-4bbc-ad79-5173cab9c704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 110
        },
        {
            "id": "17537eba-0892-401d-833f-8645e21a3749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 111
        },
        {
            "id": "cf6a0356-c131-46ef-9f6d-113a6122b02c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 112
        },
        {
            "id": "24060eab-aa85-482f-bed7-cc23767726c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 113
        },
        {
            "id": "983a9973-8865-4cbb-bced-0bc2faabd1b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 114
        },
        {
            "id": "fee28e61-d3c9-47ca-97b1-362e914ee1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 115
        },
        {
            "id": "b8b4098c-7fd8-4324-a25c-fe5eb5481906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 116
        },
        {
            "id": "44ed5071-1fcd-45fc-80af-f9dec07da4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 117
        },
        {
            "id": "1105b3cd-c30d-4f6d-b0c6-593039eb592c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 118
        },
        {
            "id": "7dc5f9f4-e7a1-4b82-be97-56747cbb2b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 119
        },
        {
            "id": "05834d08-b868-4dab-8f1b-2486b32fc7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 120
        },
        {
            "id": "fe5a37b6-17ba-4c8e-9a88-f21923da4cd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 122
        },
        {
            "id": "d0d31d67-e422-40b1-b542-997f1569a91b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 50
        },
        {
            "id": "4264687c-77c1-47b5-ba7d-d34bc0b08f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 51
        },
        {
            "id": "c90ee4cc-e303-4f5e-88c8-b4e562176751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 52
        },
        {
            "id": "4a21390e-958d-4bf2-949d-dc8ea8c3db49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 56
        },
        {
            "id": "2d33e0b6-1c00-4cbf-9845-a83ee696f86d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "9304da32-7f93-4e44-8fc6-f98026a76fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 66
        },
        {
            "id": "b79ff52f-2ffe-4402-a32d-2e112617aee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 68
        },
        {
            "id": "10bb3bc7-cbff-48a1-9acf-96be7d861164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 69
        },
        {
            "id": "76198ea5-c02a-44ad-a175-7b0a0514f668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 70
        },
        {
            "id": "fdc201a3-e071-477a-94d9-6b1b66d62855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 72
        },
        {
            "id": "4a7b4068-cac2-4fa0-aa0b-49cd6b615d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 73
        },
        {
            "id": "5b7eef7f-78cf-4aca-9080-508844c08cfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 74
        },
        {
            "id": "1456d49d-9aaf-4842-bd5a-4f45b8ddba83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 75
        },
        {
            "id": "ba20afb1-3bbf-496a-8f6f-7bc3b4fc8469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 76
        },
        {
            "id": "ac1506d6-f046-43a7-8663-b11ecafe32a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 77
        },
        {
            "id": "7ea171f1-bc44-444c-afa4-71bf66b394d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 78
        },
        {
            "id": "323a57e6-fdb6-47d5-9dea-85126a41ceba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 80
        },
        {
            "id": "af879baf-857d-4e1d-bbc0-204a78311eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 82
        },
        {
            "id": "b0947d7d-c731-4f8a-aa93-e35c7c21ceb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 83
        },
        {
            "id": "043faec9-f6b7-4bb7-acce-124ffd91f71b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 85
        },
        {
            "id": "9e7e3812-fc59-4f93-9285-023516ac88c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 97
        },
        {
            "id": "207a53e3-b2a6-4a01-ba5d-721f137b59d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 98
        },
        {
            "id": "bd99080e-c718-4144-a628-014bf280c59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 100
        },
        {
            "id": "0b76def2-60b4-4236-b75f-137f761de3e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 101
        },
        {
            "id": "b723a062-33ad-4033-a2e5-eaa9891e544a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 102
        },
        {
            "id": "c588287f-f849-4c3c-9adf-fb400d87bddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 104
        },
        {
            "id": "2810a157-b1f2-42f5-b9ec-f095b827a29b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 105
        },
        {
            "id": "47d3ee00-ae32-43dc-bad5-cd9bd3d1d0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 106
        },
        {
            "id": "529fa077-20ce-4820-af04-3d1394e8339f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 107
        },
        {
            "id": "78313cf6-19db-40e8-ac9f-2d652cee8038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 108
        },
        {
            "id": "2b1add92-96d4-4aea-b89e-60bc195c4151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 109
        },
        {
            "id": "212eb5b7-09bd-4155-976a-0bb594e9b419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 110
        },
        {
            "id": "336fc41f-c2a2-4576-b4ec-ddb0251890ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 112
        },
        {
            "id": "8c51c29b-96e6-4fef-b614-0eb1d70073ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 114
        },
        {
            "id": "21eac156-dccb-4a1d-867c-c66cdbf0cd2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "8bfbb98b-b57c-434b-b963-d3785d956fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 117
        },
        {
            "id": "f5dbf136-b724-4ed7-aeec-78222875b8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 50
        },
        {
            "id": "43326cd4-66b7-40d7-8b44-0e2e77216036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 51
        },
        {
            "id": "ce36f715-e315-4347-bef2-8dd8d2d0a8eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 52
        },
        {
            "id": "f9b119c0-c31a-4157-9c0d-4342ba7d3bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 65
        },
        {
            "id": "1b030d86-f455-4413-ba74-2f44c80612bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 66
        },
        {
            "id": "f6711426-32d0-4b1a-9c1f-f1050bb83eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 68
        },
        {
            "id": "033e5406-aa8f-4770-bfd0-0067256ef3cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 69
        },
        {
            "id": "7e1b203a-f1c1-4c92-85f8-be8e117ece91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 70
        },
        {
            "id": "163f7c4b-8fa9-45e5-9e07-b50ce171e927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 72
        },
        {
            "id": "35730dca-1481-409c-a854-324d71c99765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 73
        },
        {
            "id": "4954d37f-ad6c-4410-884f-0c865289a033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 74
        },
        {
            "id": "1da406b7-7a72-4211-a2a2-ffb8e85a1f7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 75
        },
        {
            "id": "b7a7ee4f-4d20-4a4f-b24f-f600eb830bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 76
        },
        {
            "id": "f465f697-a82c-4a4a-97bc-35e88a288b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 77
        },
        {
            "id": "b42edf6c-b10d-4beb-b1ad-9be5c0808269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 78
        },
        {
            "id": "e2113fc2-6dd0-4a80-b10d-21fb244d76c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 80
        },
        {
            "id": "eea25f73-7882-448f-94bb-ffffcb2802a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 82
        },
        {
            "id": "440034c8-f552-4ac6-b616-8de4c40f85b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 83
        },
        {
            "id": "f5006412-be28-43c9-acbb-e1a27413fc9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 85
        },
        {
            "id": "1902333c-f57d-4adb-a7b3-bf06c9744148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 97
        },
        {
            "id": "db04162b-1015-4f58-b081-bae12bde23fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 98
        },
        {
            "id": "6ce4b399-fa6f-476a-88af-7bece59e36e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 100
        },
        {
            "id": "6c23235f-bf71-452e-9fb4-1ce41e81c1fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 101
        },
        {
            "id": "c6bd5cec-2415-4e39-92fa-cceaf4ade53e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 102
        },
        {
            "id": "53c4046a-8c1e-488f-b44c-3eb9dd793e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 104
        },
        {
            "id": "63caf41c-07bf-4ead-b81f-c4eebf92fc6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 105
        },
        {
            "id": "2411d30f-1e87-47d7-89d7-646225b5544d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 106
        },
        {
            "id": "d639a2f9-1b75-4efb-9e6f-3e7b4e300375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 107
        },
        {
            "id": "6097ce0f-46ec-4195-bc80-29b46f6f7600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 108
        },
        {
            "id": "4cba66c3-21e2-4635-a973-f71b24063052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 109
        },
        {
            "id": "1b119d02-5013-4a99-8c71-e413baa14622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 110
        },
        {
            "id": "03bf1ff6-e073-4423-9dba-05a6c31458e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 112
        },
        {
            "id": "42ccf5db-b3b4-4e38-8cd1-9e5bbb3573e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 114
        },
        {
            "id": "d6c33d94-ca90-497d-97d3-b090ed2d663c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 115
        },
        {
            "id": "0e28fc70-d248-4072-88e8-c25f892810fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 117
        },
        {
            "id": "f4fb013c-7677-4cc3-9e32-00646b1b0be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 48
        },
        {
            "id": "9d756c56-8ff6-460f-b4bc-c76fff3737fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 49
        },
        {
            "id": "f5053983-e8e5-4254-bbdd-3129eacb6231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 51
        },
        {
            "id": "2fd50ede-8c02-4440-9a14-45a1835b1e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 52
        },
        {
            "id": "cd1ddc59-9b14-4d9e-9a6f-3ece33c8c1c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 54
        },
        {
            "id": "f4a1cf7f-32d2-49f3-b475-f7afebfd2169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 56
        },
        {
            "id": "fd2b3481-0e5c-44b4-8694-0b549a8518a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "4643da0a-40b0-4f72-ab63-d25ca24eda8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 66
        },
        {
            "id": "c2742b52-af19-46a3-92f2-509894232530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "bea4e16b-578c-4966-a910-10c59c4ccad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 68
        },
        {
            "id": "f9b13fd9-dc6c-4b7d-83fb-d16daf08e92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 69
        },
        {
            "id": "abd89ba3-9acf-42f4-b7fb-5f4bfa766387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 70
        },
        {
            "id": "0a2e05ce-1ac5-4961-b471-3698905cddb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "abf43bc0-ff2f-4dec-b9ca-dc90d99821f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 72
        },
        {
            "id": "8243256a-13b6-47ae-be5c-423614ffc6de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 73
        },
        {
            "id": "eaadd3dc-6e23-446e-a072-ee82646dc5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 75
        },
        {
            "id": "cf8c3bdc-be9d-4472-9dc8-f2ee5fb39418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 76
        },
        {
            "id": "d477b272-6287-4bd7-8d90-1c676c189b37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 77
        },
        {
            "id": "486386aa-a3f2-4dd3-b5e7-2e0ce05709cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 78
        },
        {
            "id": "f77d135f-fda4-487b-a9eb-0c4d3136f4a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "7d821b0c-6490-44d2-9d0d-aaa70f0983b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 80
        },
        {
            "id": "986e6ceb-0673-4539-8a64-ad2a1be9bd72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "99f75145-f768-47d6-9dac-f60e21497edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 82
        },
        {
            "id": "b55db8c1-ee20-423f-af37-5a957b6a70c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 83
        },
        {
            "id": "97f8d94d-6312-440f-a32e-f1799ebd4e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 85
        },
        {
            "id": "236ac4b1-ba28-4db0-bd12-c675cc07652a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 98
        },
        {
            "id": "867e6af0-ae7f-4069-8393-135afa80ecbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 99
        },
        {
            "id": "0dc50ef7-3926-44b6-b88f-bc81b841ea71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 100
        },
        {
            "id": "849111a8-7719-42b2-955a-c77e1a4578bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 101
        },
        {
            "id": "b28d868b-defa-4d26-a3eb-9802dcec8141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 102
        },
        {
            "id": "e7d50e2b-0295-4239-85fa-16f1ff2c2af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 103
        },
        {
            "id": "35f2c927-9a95-4f2f-83b8-7ca24459cbfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 104
        },
        {
            "id": "3fb6b839-f760-4ea2-8244-b6b2ae7f7a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 105
        },
        {
            "id": "10fd9e13-8769-40b6-a45a-56e6c2fb6fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 107
        },
        {
            "id": "5b5366a4-e073-4fa0-af10-96a914232e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 108
        },
        {
            "id": "60d7ee4f-146d-4a51-aab6-53a2f60ecbb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 109
        },
        {
            "id": "27cff48a-1404-48ae-8255-ac0dd7eb82a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 110
        },
        {
            "id": "1d443d6f-8de1-4152-8eda-946ffbb7a924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 111
        },
        {
            "id": "6e143e50-5252-47d3-b186-0fc58ac05d82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 112
        },
        {
            "id": "62e0b960-cad4-41fe-96f5-4bcde59a0d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 113
        },
        {
            "id": "c4f2bafc-edcc-4eed-8880-f7c5641ad98f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 114
        },
        {
            "id": "73fd6860-eb22-447b-a911-e21c67813ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 115
        },
        {
            "id": "2047e696-edab-4198-b10b-189246beb7bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 117
        },
        {
            "id": "a1be2ace-2110-403b-b431-c28c74ec44cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 48
        },
        {
            "id": "d3809957-9634-4d56-9967-7cb1a12cc457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 49
        },
        {
            "id": "0ae1863b-a308-4056-b77e-2eba8ddcb653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 50
        },
        {
            "id": "c4d66d6d-2297-4e38-9ee4-0c518cdf7a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 51
        },
        {
            "id": "e3a63822-d5a7-4af1-af7b-563593befd57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 52
        },
        {
            "id": "8a279799-acf8-4c72-b5e5-ff37b3fc0d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 54
        },
        {
            "id": "0bd9f866-9eda-47ba-b580-ee8c411de655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 56
        },
        {
            "id": "e269092a-f205-4d93-8a2b-77118291f684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 57
        },
        {
            "id": "48d1968f-bc93-4153-ac89-960a7e8ce72b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "9a2325bd-d1a1-43cc-be8d-18de973cb753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 66
        },
        {
            "id": "a4d81a31-af7c-4e51-bedc-f80d90d2d2b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "9d634862-9409-4977-abae-aa382338b714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 68
        },
        {
            "id": "a8fbdf40-aded-4d37-8441-5c79c32a1e18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 69
        },
        {
            "id": "f953b426-ddc1-4600-aef1-f9b9f0c547c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 70
        },
        {
            "id": "9a1a4de6-9e5c-482f-8af7-4079f582f7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "fb876f8a-2e15-4d40-8b0e-9f6ced01e5c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 72
        },
        {
            "id": "a39db092-d330-4ef7-8939-7a13d486e495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 73
        },
        {
            "id": "5e4367a8-aeb0-406e-93eb-3d0224ce33ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 74
        },
        {
            "id": "46216bb9-b7b2-4e16-aec6-42b6f36c20af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 75
        },
        {
            "id": "183f8912-9683-4475-a2af-b30d207c19c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 76
        },
        {
            "id": "8f3fcc6a-bd34-4788-8541-48ba1eda1e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 77
        },
        {
            "id": "026cc271-e46e-4e00-b0f8-e1cc63bd8496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 78
        },
        {
            "id": "18d48165-a2c2-4a8e-b590-d79a194d68c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "8674fe7c-250f-4060-9396-17b3ccde0def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 80
        },
        {
            "id": "3f9db68f-0ad9-4af0-9d00-e26d1c629a44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "cc71c215-d6ba-418f-aa92-5a9e51942cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 82
        },
        {
            "id": "b247a523-fe73-40d4-8ede-e7d3b0c06752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 83
        },
        {
            "id": "b98796be-8777-4e1f-a591-5067cfb6a236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "28d032df-9685-46fb-9e91-2a5463c74b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 98
        },
        {
            "id": "9e196dc2-65e4-43d6-9189-5a5f4dbd250e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "ce8bc83c-a7a6-4215-b6fa-7ada3fa97df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 100
        },
        {
            "id": "5ee7794d-c22e-414c-8cde-bf28229f4fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 101
        },
        {
            "id": "291acecf-a064-47a4-ad39-114bff9b2b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 102
        },
        {
            "id": "e6fd4c57-0288-4c35-9c9b-cac4a4988d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "d437cd10-aa04-47c0-9a32-31812fcba1e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 104
        },
        {
            "id": "39b11d37-d9c5-42ab-aa3e-92c3006c4543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 105
        },
        {
            "id": "63946b03-4fa9-4942-a79a-9e50e0208ba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 106
        },
        {
            "id": "58633030-c2ec-4efd-80b4-0768a5e8bbaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 107
        },
        {
            "id": "8fe3a212-406d-401a-97d0-a150fc48e246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 108
        },
        {
            "id": "25c5954e-b9cf-42f7-8017-50a71e83b47d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 109
        },
        {
            "id": "306ed59e-a831-476b-a824-de648ec08165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 110
        },
        {
            "id": "df5547db-855b-4af9-84b4-8924900bf28e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "7e52fb70-59d1-4004-a618-24929ff5a7d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 112
        },
        {
            "id": "e5820a40-a7be-4969-8206-8c10d03541f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "71a2e148-5945-4145-a644-5985db305cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 114
        },
        {
            "id": "5c4cb513-6aaa-413b-b0ce-7add9aef82dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "c738e217-5028-4ab1-a712-1c37f05fbe78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 52
        },
        {
            "id": "986cb69e-3ce8-439f-a507-b3492c59ecc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 66
        },
        {
            "id": "388d819f-f4a7-415a-9d4f-f17c20a044d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 68
        },
        {
            "id": "9b5bfcc6-3a6c-4b61-b399-cb91f34ef25c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 69
        },
        {
            "id": "84429866-24e1-482f-bc3b-9dc6e9bac219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 70
        },
        {
            "id": "bb12b464-d08b-4b05-b7ae-27b073ec2d44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 72
        },
        {
            "id": "38452297-59a4-4b50-8e53-55c1c34b32e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 73
        },
        {
            "id": "d649bf61-f705-4edd-972e-28a16182e627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 75
        },
        {
            "id": "e1f9a636-e4dd-4a58-9950-75ef9664bd4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 76
        },
        {
            "id": "9ec73f2f-a433-457d-8b96-6e901bb49579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 77
        },
        {
            "id": "5ac46f7a-092b-4ab9-89ba-788ed1d92a1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 78
        },
        {
            "id": "429a6c31-9349-41ab-94ac-5cf0563c7a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 80
        },
        {
            "id": "d4368007-180a-4106-a4d1-62e5a46ad843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 82
        },
        {
            "id": "422de451-653f-4d2f-9a89-78af898fce2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 85
        },
        {
            "id": "e8446f36-e515-494c-bcb1-f058a50c4b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 98
        },
        {
            "id": "8d2b10e6-2b86-46a6-80ae-7fc7afd8a4e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 100
        },
        {
            "id": "b1bff937-6743-4417-9f99-d0850e103d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 101
        },
        {
            "id": "5125ef1f-65f6-4e5a-a955-b9e2c0f890b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 102
        },
        {
            "id": "6d8c3c48-f98e-410f-820f-6fe15d159c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 104
        },
        {
            "id": "ef402ab2-5c3b-400c-a630-3e8230f0500b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 105
        },
        {
            "id": "d8f566f5-9a91-425d-aef6-57900f9075f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 107
        },
        {
            "id": "1a318389-7dc9-4923-a3f3-1446b810372d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 108
        },
        {
            "id": "d596952f-4fa5-43d8-b466-22827998c092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 109
        },
        {
            "id": "d44b4b7e-a74c-40ca-b36e-7ce48571bc36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 110
        },
        {
            "id": "e41b7397-2dc0-4ca9-aee3-fec0ca4262a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 112
        },
        {
            "id": "bfdea1a0-bcb7-4fac-9569-02e59494a08f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 114
        },
        {
            "id": "40396f16-e4db-4caa-8c73-19fe132f352f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 117
        },
        {
            "id": "1e55e072-6aa0-4190-8cc0-5c5a328ca7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 49
        },
        {
            "id": "dc37f259-9737-4c27-ba8b-a741e471c5eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 51
        },
        {
            "id": "195923bc-3f00-4f83-8419-c4e338ebadbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 52
        },
        {
            "id": "b3ec31ca-b685-4d64-8603-ebeb3e774584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 55
        },
        {
            "id": "e1e20897-2369-4924-8594-4dcd4f18a5fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 66
        },
        {
            "id": "ec4b6248-0514-4796-a55a-7694f01b6609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 68
        },
        {
            "id": "d1dbd12c-6e5b-4efd-8540-aeb2c2a60d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 69
        },
        {
            "id": "cec6ec54-1b13-4ea1-8f2d-c98793e4e3e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 70
        },
        {
            "id": "1982cb9a-0382-4628-922d-2eb572abf28e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 72
        },
        {
            "id": "6b4941e1-d68a-4388-820a-113b99e57243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 73
        },
        {
            "id": "68064db3-9598-4ee3-9f36-6c545f8ecff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 75
        },
        {
            "id": "6a53c17d-c388-49dd-aea2-8fe468480a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 76
        },
        {
            "id": "0b1419e3-e255-40a7-8654-c2f2a12fcd6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 77
        },
        {
            "id": "5c99414b-11af-419b-a335-abed454fdd8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 78
        },
        {
            "id": "d0249083-a7fa-4f1c-b9f3-236ce61b33a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 80
        },
        {
            "id": "a9840176-b6be-4727-baee-a22921b350aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 82
        },
        {
            "id": "ea01ab48-e0f7-4fd5-833e-11a5d00f73f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 83
        },
        {
            "id": "571efd30-c605-49ac-8f47-f64bb6ffceba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 84
        },
        {
            "id": "bd91642a-d576-4e16-bcf8-bd56a9913018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 86
        },
        {
            "id": "6634139a-e522-4f27-91ed-e84e6d4a1c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 87
        },
        {
            "id": "f932fb3c-6364-4714-9093-d260a7a12c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 89
        },
        {
            "id": "ecd9bb05-6d23-4fec-802a-2101e1d594f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 98
        },
        {
            "id": "884d2515-7b4a-47f6-9031-373400044605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 100
        },
        {
            "id": "37e6fe5d-9072-4e94-ad26-e148fedac099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 101
        },
        {
            "id": "3b686cea-07d8-4dc2-a25e-1ca29eea44dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 102
        },
        {
            "id": "54a7dd83-b68b-48dd-a21a-dfa8846431d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 104
        },
        {
            "id": "d49226f4-5863-43af-811b-43a556340107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 105
        },
        {
            "id": "a65378ae-c9d0-4f49-94c0-52799d57f9a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 107
        },
        {
            "id": "b79a870c-989f-4d22-a26c-e2fad5fa5ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 108
        },
        {
            "id": "4c14f785-f4b6-4176-9d11-41b60d26eaff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 109
        },
        {
            "id": "712f7c3b-9002-4624-ac7b-c0861461f3cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 110
        },
        {
            "id": "689daddf-6076-49c6-8d7e-ea7bdaae38aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 112
        },
        {
            "id": "9c5d6b5c-e25f-4806-8bdd-b45d3aac1b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 114
        },
        {
            "id": "5c518133-327d-4c47-ad07-eb9840ec8f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 115
        },
        {
            "id": "af60feb7-9983-495f-8995-826df75d2106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 116
        },
        {
            "id": "11270f38-2ec1-4f4e-b0ec-e6a59bdc9376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 118
        },
        {
            "id": "e6c9295f-ea57-4820-9e0d-addb30e3be66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 119
        },
        {
            "id": "d39eaeeb-77ac-45a7-868b-abb79c96c230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 121
        },
        {
            "id": "c519eec7-95dd-4ad2-bcee-457effb1b998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 48
        },
        {
            "id": "80323426-beaa-489d-8f77-4ba73c65dbb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 53
        },
        {
            "id": "10b421ff-770c-4566-8752-338d05c171f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 54
        },
        {
            "id": "0010af91-d8c9-451c-bf6f-a71300c5dd74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 55
        },
        {
            "id": "572d71d5-3bf6-4ef4-b31a-f80772824231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 56
        },
        {
            "id": "e9f637d4-2b19-47f3-b35b-345c4ad0c4cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 57
        },
        {
            "id": "14e020a0-74d9-4c30-9f15-985e86ec6724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 66
        },
        {
            "id": "58e83228-f526-4f1c-8ed2-2fd86e0e9bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 67
        },
        {
            "id": "9d8c64ac-8d6e-4ec1-8236-bf49c0983e1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 68
        },
        {
            "id": "65455fc0-a0c5-4319-9876-a251f0aab270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 69
        },
        {
            "id": "16931a43-eae5-4530-b8be-922fe2b0b82d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 70
        },
        {
            "id": "2498e129-ad76-4ade-a469-d94e4317a2f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 71
        },
        {
            "id": "1c944262-57d2-4929-9519-50ef7477eb19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 72
        },
        {
            "id": "424b1baf-62d8-4e53-a7b0-977f21153f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 73
        },
        {
            "id": "a947a79f-25e0-4767-b24a-29e6e2d6f250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 74
        },
        {
            "id": "b1dcaa6b-3eb3-4bb7-b79c-48b83521248f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 75
        },
        {
            "id": "a0842bce-b4fd-482d-98e4-a5a458f06c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 76
        },
        {
            "id": "d8f5820a-9e96-4fa3-813e-0f88c1b555d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 77
        },
        {
            "id": "a1c34219-42c3-49a3-b95f-4f35f9a6b473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 78
        },
        {
            "id": "36a1c248-014e-4b81-bb72-52cb6e7a3a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 79
        },
        {
            "id": "0b6996d4-923a-468f-89ec-74ef3a8d35bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 80
        },
        {
            "id": "4a185411-9c2f-4200-a8d4-7db18584702c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 81
        },
        {
            "id": "4bb1716c-4f67-47a2-bc87-123c44683b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 82
        },
        {
            "id": "935e434a-d073-47ce-abf8-bc05eb7592fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 83
        },
        {
            "id": "7acd68c7-fad5-40d1-8c67-7a30a6d7d85f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 84
        },
        {
            "id": "e0bd2713-91a3-44e3-97c9-e9a6d5ffc497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 85
        },
        {
            "id": "7d1dd074-84ec-4a00-8853-ebcc5bc4a691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 86
        },
        {
            "id": "db1b2390-81b5-4674-a7ed-7e5c3772ebe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 87
        },
        {
            "id": "a928d446-6fd0-4875-b846-1f255eba57ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 88
        },
        {
            "id": "50416330-94fd-4f33-84d9-fa10ceacfe07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 89
        },
        {
            "id": "2b0f5741-0571-4075-8327-c7ec38c0f613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 98
        },
        {
            "id": "55d89c32-fdcb-4f2d-a203-0068c76863a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 99
        },
        {
            "id": "0e8989f2-e588-4543-a195-628b20d0756c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 100
        },
        {
            "id": "69a6b44a-10e1-4aa5-8634-75f6ff8fc0f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 101
        },
        {
            "id": "493f2421-18de-41b5-9633-b9d3d32ffeda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 102
        },
        {
            "id": "89056eca-4419-4e2b-8655-4f2c1103b9cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 103
        },
        {
            "id": "cb1647b5-aa83-410b-b651-0af8f736c620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 104
        },
        {
            "id": "55b8efc2-1725-48c0-88c1-2bfc52828432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 105
        },
        {
            "id": "6e912673-f770-4995-9a19-b8a4bce8bf18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "7a286e99-82b0-4867-973e-77e22b12b3b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 107
        },
        {
            "id": "aa77489c-dde0-4484-9ba8-1baa6d982c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 108
        },
        {
            "id": "6a533176-8cf6-4287-9ec4-472de25b5dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 109
        },
        {
            "id": "34be424e-d74c-4e34-9e3d-cd1bf4496331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 110
        },
        {
            "id": "e9dc3262-1352-4432-94f8-2975d8f6a21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 111
        },
        {
            "id": "d6008ced-d8fa-405a-a8df-cde7040fd86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 112
        },
        {
            "id": "d70bfb38-c62b-4b11-beb4-918460c72e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 113
        },
        {
            "id": "53dd1bad-c81b-4cbf-87e6-81855f6c272d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 114
        },
        {
            "id": "201f062a-739a-4955-be46-a48e2dec397d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 115
        },
        {
            "id": "08170754-1b25-47ff-9256-99493ed6fbb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 116
        },
        {
            "id": "97fcb770-02ce-44cd-bbc1-cd98fdd7e8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 117
        },
        {
            "id": "0a18528f-f511-45af-8a5a-e775d1c0d95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "60257c11-1e84-480e-96d5-41549b4cf69a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "3deba447-5637-46fd-bcfe-24a4938a795c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "4cc3d058-dabf-4fba-a505-8ad18f93c9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 121
        },
        {
            "id": "6d39f883-b746-4f6d-8ef1-3150618a8112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 48
        },
        {
            "id": "a0bbc22b-98a3-4e8f-b903-328bf241b48f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 54
        },
        {
            "id": "6db3925e-53f3-444a-b4f1-4667e49f5c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 65
        },
        {
            "id": "08d8ffc0-0988-41ee-a83a-76a739c56144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 66
        },
        {
            "id": "eb10dbd4-0b70-41fc-83c4-abbbef078c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 67
        },
        {
            "id": "5312cd93-7c17-4142-9ffe-ae3d9fb560b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 68
        },
        {
            "id": "b0bc60f9-c229-488b-a7b0-f5ca4b541787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 69
        },
        {
            "id": "2314a8bd-3a4a-46a8-ac94-2cab4b087dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 70
        },
        {
            "id": "ef11d694-07e3-4f8e-9225-8022fd658e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 71
        },
        {
            "id": "5f9ca232-af6f-4f34-86d4-17bef15c8ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 72
        },
        {
            "id": "ed7f5c81-3437-4433-ae51-68282effc7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 73
        },
        {
            "id": "52cba486-db06-4b85-9f21-cc4b41dd9127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 74
        },
        {
            "id": "0689f75b-fe7b-459f-8ee6-c24d92aaf6cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 75
        },
        {
            "id": "a3583db8-bb00-4b14-bcc5-1c46f537fbf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 76
        },
        {
            "id": "4ea0a408-3c30-47ec-a9f8-87c5076cce92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 77
        },
        {
            "id": "84a470e8-c870-4f0e-8770-1c601d9cbbf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 78
        },
        {
            "id": "c526e846-e6b5-46e4-ae99-1b44be65f02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 79
        },
        {
            "id": "d6fe1222-4ff3-40ab-b556-80d71364a646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 80
        },
        {
            "id": "29ef02de-6812-4a1b-9ce9-074e705a5cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 81
        },
        {
            "id": "8c2f3da4-d01f-4cbb-ad48-49bf3c234851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 82
        },
        {
            "id": "7a2092c4-362b-42d9-9f54-b097ea6b9307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 85
        },
        {
            "id": "868779c7-ca45-4238-9df0-1b6bdd5f3c1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 86
        },
        {
            "id": "8e1d495c-5c40-42e7-861a-df2650e0312e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 88
        },
        {
            "id": "06c68af9-a79b-4866-a7d5-e275bbc99366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 89
        },
        {
            "id": "0b4d05df-c67d-4957-ad77-7e254fbb1cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 97
        },
        {
            "id": "bd1277c2-16a5-40c0-8e28-11b27cfe43f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 98
        },
        {
            "id": "a17d5346-7d46-4fbc-b16a-a293eca9ec0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 99
        },
        {
            "id": "1cddf4e1-6247-4089-9619-ea665a5f0298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 100
        },
        {
            "id": "c864d912-57ba-41f3-9e35-c8ae72a8379f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 101
        },
        {
            "id": "80aab3fc-c57d-4ab5-b965-63885f4f5546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 102
        },
        {
            "id": "e4e7bc32-7999-4394-b0ad-93bf79dd96a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 103
        },
        {
            "id": "082f1405-39d9-4a7d-947f-f1b09d207637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 104
        },
        {
            "id": "3f0aab21-9bc2-498a-8248-617fe4b27e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 105
        },
        {
            "id": "9fcee934-3ce8-4e5d-8e12-a5cb201f9bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 106
        },
        {
            "id": "b3e9e9f6-096c-4926-a679-0a7731df4af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 107
        },
        {
            "id": "1ce4a48e-d216-4638-8a7e-42a6bb2236a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 108
        },
        {
            "id": "01844256-3598-495e-93b7-ba6a691f0b87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 109
        },
        {
            "id": "4cb6de9a-4936-4a15-8b6d-1a539beebc18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 110
        },
        {
            "id": "8a633c84-7999-44ff-8b55-8f29ad0d2a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 111
        },
        {
            "id": "49d76485-b962-49fa-805d-fe4668c473c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 112
        },
        {
            "id": "93f0a17d-64d1-4be8-bf06-36b33acf3cb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 113
        },
        {
            "id": "a42129e6-2298-4c66-8cc9-39545b76e2c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 114
        },
        {
            "id": "63e81b7b-15d3-4e2a-9888-d31ffa6be502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 117
        },
        {
            "id": "d21d903b-e922-4d84-a2bc-065b439b9c89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 118
        },
        {
            "id": "06d0f1f5-2c77-4d24-9e2e-f74b76b23dc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 120
        },
        {
            "id": "349fad63-220c-41ca-b42d-bd914ca3b66f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 121
        },
        {
            "id": "40714036-cc05-4091-aba0-e7fb7cbe8de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 48
        },
        {
            "id": "6605fe13-0679-4ec6-acf5-87cf7a1f0df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 49
        },
        {
            "id": "c37e3c3e-9921-454f-93aa-2f98ea000e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 53
        },
        {
            "id": "4687cf3c-8902-429e-8280-aaab68ac6cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 54
        },
        {
            "id": "fbffaaf0-0252-4d81-8ba0-c2d1ff4bd368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 56
        },
        {
            "id": "09e66fb0-e42c-433a-b69a-5029d4b737e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 57
        },
        {
            "id": "23ae17bf-1e20-4b4d-b516-ef961c0c702e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 66
        },
        {
            "id": "97a963f2-aec5-4703-91bd-f01e460d1f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 67
        },
        {
            "id": "c0cd16be-fbea-42f4-979f-e3378447d2c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 68
        },
        {
            "id": "f984220d-1718-4d39-acb8-67a8844fc876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 69
        },
        {
            "id": "8aff81ac-bcf3-4d1f-b07a-2918794cefea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 70
        },
        {
            "id": "3e3a2007-45dc-479e-ad08-5d5b44eb4b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 71
        },
        {
            "id": "1c630f45-bc83-4beb-aa86-61e50770a56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 72
        },
        {
            "id": "ee049bdc-f379-4b80-9855-3324396533ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 73
        },
        {
            "id": "3bec01b8-5d2f-4598-a154-95c6b264370f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 74
        },
        {
            "id": "0ccd7637-e649-47a9-9a4d-434d97859c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 75
        },
        {
            "id": "f2137cdc-b8b6-4615-b519-6288419895e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 76
        },
        {
            "id": "dea4ccf5-b02e-4cb3-b324-ba826a02736a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 77
        },
        {
            "id": "fe86c937-900d-492b-853d-2c81b2a9bb61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 78
        },
        {
            "id": "88e3c847-e34c-46b1-a76a-fe50b7868027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 79
        },
        {
            "id": "50f3f05a-4a71-422c-8ea9-735db2b70231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 80
        },
        {
            "id": "5ce763ca-fdf8-4e3f-9297-cc0059c5dfa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 81
        },
        {
            "id": "c2ba7fe1-acae-4a6e-83d0-307900a0e977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 82
        },
        {
            "id": "fde3f5ae-9ee4-4ce7-ac5b-9a9829cc089e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 83
        },
        {
            "id": "58798e4a-ecbc-4ad5-af43-9c6e27ce6846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 85
        },
        {
            "id": "f604704c-b0f9-4e2d-bb00-6156ec39c534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 88
        },
        {
            "id": "6d8a8866-f515-4e73-a65f-b89790b3febb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 89
        },
        {
            "id": "80590289-30cd-4d43-aa52-88c72f479fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 98
        },
        {
            "id": "82fdeab7-a5c6-464b-8fd4-4b254bb6e985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 99
        },
        {
            "id": "c3d67321-60f6-4087-89b6-ae8ca1362e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 100
        },
        {
            "id": "6ee2087e-678a-4d3a-8ba1-acb4e059b1d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 101
        },
        {
            "id": "91d89fff-63eb-4d00-b714-f228c23db369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 102
        },
        {
            "id": "4b82fddc-72f2-4bcd-a523-6319c716b3dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 103
        },
        {
            "id": "133e9fa1-4953-491b-957b-77906d9902a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 104
        },
        {
            "id": "2472d662-4527-40e2-95f1-6dfd7420f060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 105
        },
        {
            "id": "5cb2a9d6-b912-4496-bf92-2f419fdb70f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 106
        },
        {
            "id": "3cf10268-b817-4319-9ce6-cd88ef90517d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 107
        },
        {
            "id": "aa804195-36dd-4ce7-88c3-afe31d742cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 108
        },
        {
            "id": "539c9ad4-8cd3-48f7-91d7-957fe903d1f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 109
        },
        {
            "id": "107f923b-45f2-4e2a-9824-21d9c4901d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 110
        },
        {
            "id": "ee71e535-aa4a-4331-8e03-2cb547849ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 111
        },
        {
            "id": "c5dccafb-047b-4dbc-9b9c-bc189563a235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 112
        },
        {
            "id": "b93e4c88-af1a-48c4-8ccb-51b0fa9df96f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 113
        },
        {
            "id": "f0efc0c1-740f-46bf-87e0-f5c0e5ca9012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 114
        },
        {
            "id": "a39c0b1f-23c7-4d30-acde-c2c2c6a3fb10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 115
        },
        {
            "id": "f6bceac3-3f8c-47d6-a0e5-48996a998f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 117
        },
        {
            "id": "e3a18c19-fbf8-4cf7-9c73-61aed2fa766e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 120
        },
        {
            "id": "ea9d6760-a1c7-40b1-bd27-5430f25324af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 121
        },
        {
            "id": "59a23236-d6c0-40ce-8bf2-2216bf7566f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 52
        },
        {
            "id": "b5c268e8-2ba4-4d13-b5c1-aa39eb714e09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 50
        },
        {
            "id": "f000a447-3c50-41d9-9822-372ddf6bae63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 51
        },
        {
            "id": "e55b9153-9568-4f63-8d76-54819fe576d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 52
        },
        {
            "id": "3b16756d-e49d-4506-a4f0-c3b3db73b9b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 65
        },
        {
            "id": "b130a3dd-8d59-46a0-86bd-5030245426cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 102,
            "second": 74
        },
        {
            "id": "2b0703fe-4ed8-4ba7-be68-9514653c4696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 83
        },
        {
            "id": "437ea800-b609-47b6-9ba7-ead9b76604c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 97
        },
        {
            "id": "dd505a1f-859a-4d1a-87ef-7d44023cd710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 102,
            "second": 106
        },
        {
            "id": "66195a85-aedf-4953-8ba1-41cefeb767d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 115
        },
        {
            "id": "21cff3c5-2e1d-4191-916a-8d7ce12ce01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 48
        },
        {
            "id": "3f61a985-9e5f-44af-a3a8-c0599b320b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 53
        },
        {
            "id": "062bef63-f68d-4036-aa04-1db8a4ac2c11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 54
        },
        {
            "id": "f8552e68-01eb-4ee0-af8c-7f41480ea4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 56
        },
        {
            "id": "01944753-2917-40b7-a122-d5fa73aa6fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 57
        },
        {
            "id": "c337d8b7-f42a-4545-a277-edf1dd3d9267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 66
        },
        {
            "id": "1202aed4-c1fa-4536-a04d-3aece87d8259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 67
        },
        {
            "id": "38b7b3e9-7173-4659-9e44-e06d627e3a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 68
        },
        {
            "id": "346ca5f9-a018-4cd9-9af2-225df8cb48d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 69
        },
        {
            "id": "e6785a6d-f515-466a-b2b3-75d4e1fcbba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 70
        },
        {
            "id": "a92094d5-e04a-4d85-9164-956853f549af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 71
        },
        {
            "id": "d39e6d4e-7d52-43ac-aa91-cf0c08114645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 72
        },
        {
            "id": "b54503fd-85bb-4cb4-97fb-339681a8e126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 73
        },
        {
            "id": "5b515494-ee06-4437-bae6-7e7b7cc60a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 75
        },
        {
            "id": "7f8239f6-7023-4c90-96c0-a09793fabdbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 76
        },
        {
            "id": "ec9fea49-cb4f-4574-83de-362cc22774ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 77
        },
        {
            "id": "f9891a76-28ab-42e0-8e93-f593b0984ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 78
        },
        {
            "id": "0ff60f6d-a0be-46d4-b641-4379279a0d47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 79
        },
        {
            "id": "0c1e613b-fb83-4466-af9f-a87e11cad623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 80
        },
        {
            "id": "6c6bf0a7-5316-4faa-aa2d-408ebac48f91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 81
        },
        {
            "id": "72d60a06-dd4e-4bba-9cd4-4abd5b4944f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 82
        },
        {
            "id": "75c21826-69f0-453e-b0c8-3090d10ff3a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 83
        },
        {
            "id": "9a56c416-aa29-44a1-b621-af3485ed5e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 85
        },
        {
            "id": "99692532-12e7-4df7-9356-671a5e4b9ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 88
        },
        {
            "id": "918712f3-ad5c-4751-8c84-e995169baf85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 89
        },
        {
            "id": "034c1611-d5a7-40fb-bffd-3a53679f7133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 98
        },
        {
            "id": "97d5619e-98ea-48a7-84dc-e1294b377141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 99
        },
        {
            "id": "8d2d0426-43e5-4447-a53a-c596ebbee161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 100
        },
        {
            "id": "196c1610-a237-4fb9-959f-b9a9995b0442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 101
        },
        {
            "id": "f08ed9e1-8ff7-4e5e-a6c1-7ceeac7e1919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 102
        },
        {
            "id": "2922a911-77c2-430a-ab70-1328ae55bdfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 103
        },
        {
            "id": "ea7f1c5e-f2fc-476d-9ee8-7029f9a2c99e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 104
        },
        {
            "id": "38e764ba-917e-443c-95de-d2cfb5b38363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 105
        },
        {
            "id": "a330d16a-fbb4-47c7-bfce-ad1085980dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 107
        },
        {
            "id": "daf32cd4-69a0-4387-86d8-7bc9ab5c6e99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 108
        },
        {
            "id": "adb92341-a87a-4b09-8061-91355d356553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 109
        },
        {
            "id": "98256c2e-592a-4315-9b18-33ecb7cefd5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 110
        },
        {
            "id": "a4f4c9f6-58a4-4341-aa00-da82aea67520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 111
        },
        {
            "id": "081b5e27-7002-48b2-a668-1c8bed96370a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 112
        },
        {
            "id": "fb0d62dc-7ffe-4c2f-9e00-768f54cc445b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 113
        },
        {
            "id": "9c9636e3-55b8-4236-99af-88e3c20a290e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 114
        },
        {
            "id": "c4c44d0b-6183-4563-b73f-063f9ace793b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 115
        },
        {
            "id": "0698f9af-d466-4796-9d41-4e158ebff2c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 117
        },
        {
            "id": "553989f5-4029-44e2-8ab3-d08d7247f10e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 120
        },
        {
            "id": "4b9d4ba0-f37b-414f-9261-943602c551f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 121
        },
        {
            "id": "7c9de859-847f-405c-98e8-8d869522ca28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 48
        },
        {
            "id": "71458dc0-c021-419b-adce-91fe4c1f467d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 49
        },
        {
            "id": "4a2a871e-1e15-448b-8ed5-0c45e82e0747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 50
        },
        {
            "id": "66284e62-3ab6-4ddf-8a0c-2efdb8920b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 51
        },
        {
            "id": "813016c5-d6d1-45ab-8a19-c28e0f9ace6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 52
        },
        {
            "id": "25886e6e-1853-4f2c-b9b0-4aebf91c82b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 53
        },
        {
            "id": "37656feb-1ea9-4037-bfa5-fda071d20a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 54
        },
        {
            "id": "50ef6fb4-af00-4415-8a95-33e34845ef4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 56
        },
        {
            "id": "cfaa4b01-e865-4c79-becb-b63ce900b30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 57
        },
        {
            "id": "e7a1aac7-d9f1-40ce-9999-d70270d62ae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 65
        },
        {
            "id": "cca8358d-9a47-4212-94c5-4b2af87b2998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 66
        },
        {
            "id": "836358ff-1030-4998-a2c2-9df770c35d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 67
        },
        {
            "id": "0834c6d8-f869-441b-8410-2f0036e0ce3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 68
        },
        {
            "id": "58a5192a-b38c-4cb5-aa4c-2a138e42f8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 69
        },
        {
            "id": "c23a79e0-d09c-4f06-a703-892e1afa4187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 70
        },
        {
            "id": "96e1982f-2b72-454a-8182-e5dcdcd58b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 71
        },
        {
            "id": "1a7dd1a2-8055-4479-9a0a-8fba4b9e0c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 72
        },
        {
            "id": "98c5199f-746f-44fb-90a4-f2a8d47ce295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 73
        },
        {
            "id": "998e8e89-88b3-473d-bab1-e7dac075acb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 75
        },
        {
            "id": "52b15caf-8886-42d5-9d1e-551fddd0f171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 76
        },
        {
            "id": "0facbc4f-c50b-4d54-a61d-9b91aa88dc21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 77
        },
        {
            "id": "0e32484e-eb09-45e4-8a3a-5609cf7db7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 78
        },
        {
            "id": "dc344f19-155e-4794-81ea-3e786604b42a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 79
        },
        {
            "id": "f8ec1810-c6f6-4ac9-b957-c5855bb74b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 80
        },
        {
            "id": "e9778812-eacf-4839-9305-2696cf7ef206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 81
        },
        {
            "id": "d5bbe692-dd3c-4db8-b91c-7e39a3afdef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 82
        },
        {
            "id": "d0a011d4-e5d4-49c4-b28f-20b1bc45ac69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 83
        },
        {
            "id": "5ff6cfa6-9fd8-451d-b947-5fa18da81e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 84
        },
        {
            "id": "da680fca-c1d5-43cb-a8bb-5c29b076910c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 85
        },
        {
            "id": "250ea6c5-7fda-40fc-b138-58cef10ca68a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 86
        },
        {
            "id": "96becadf-105c-4e39-8fc2-f6a0bb7b103e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 87
        },
        {
            "id": "9ee5f5fc-b1a1-44f9-be76-2623904c3a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 88
        },
        {
            "id": "a6c399a7-11c5-4044-9701-138597c05e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 89
        },
        {
            "id": "120f83ac-9cc3-4d84-8886-d971f879584d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 90
        },
        {
            "id": "1188f01a-1c1d-431f-abe1-e8fb9e15cc3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 97
        },
        {
            "id": "109b02c3-2484-436b-9b97-d7fddf57fb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 98
        },
        {
            "id": "f2868256-d30d-4447-b53d-2f34f8b9210c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 99
        },
        {
            "id": "5e614b9a-fb0c-4a77-8472-b03843bcdcc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 100
        },
        {
            "id": "a0eba528-77f1-4325-9e25-6f63c7ffa03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 101
        },
        {
            "id": "b842950d-33d8-4f38-9647-59fe831595db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 102
        },
        {
            "id": "a23ec11b-7ced-4c7f-8541-b9c2c99d302a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 103
        },
        {
            "id": "b3ea314a-46a6-44b4-af08-6c2afbda8906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 104
        },
        {
            "id": "3b3019ce-3ae0-4282-be37-f10bc56465c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 105
        },
        {
            "id": "74270d09-3f01-4fd7-93df-ac11592770a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 107
        },
        {
            "id": "71b3563d-c713-4248-999b-c10f2f99233d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 108
        },
        {
            "id": "fe56900d-13a5-4b7d-9804-0aba529dc421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 109
        },
        {
            "id": "7c75eb7b-f8ec-4564-9eac-77cd174646c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 110
        },
        {
            "id": "09d54c81-0c20-43c7-bfda-35efb259a2c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 111
        },
        {
            "id": "fc88d592-9386-4374-81fa-9438b15339b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 112
        },
        {
            "id": "9b30b142-23c4-43b7-915e-0fb40fc1de83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 113
        },
        {
            "id": "0c6f13f1-473f-4601-a80e-6eb45a4e8ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 114
        },
        {
            "id": "a249de45-b50e-47c7-b7fa-0a2489fda10f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 115
        },
        {
            "id": "5a848447-da47-4311-9607-adf95c96880e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 116
        },
        {
            "id": "adb104fd-9e11-4b84-886e-828ed58b5071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 117
        },
        {
            "id": "e24a6eb2-07b7-4866-90f4-83673fedcb97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 118
        },
        {
            "id": "b66be606-a196-40ea-b662-7daba8028704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 119
        },
        {
            "id": "5fbc814e-5fda-402c-a346-e787c16623a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 120
        },
        {
            "id": "d5a43959-355a-4035-938f-f23517779301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 121
        },
        {
            "id": "65082670-c740-4fa6-9c15-0e6ef099488d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 122
        },
        {
            "id": "748119a6-d380-46ac-816e-2e2df552dbf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 48
        },
        {
            "id": "d4f58dc3-f1b3-447a-860f-0ab4a0387051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 49
        },
        {
            "id": "1fff1af9-25e0-4656-83b7-9a776fd8d813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 50
        },
        {
            "id": "946bdd49-0dd7-477a-b845-e9a25392f687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 51
        },
        {
            "id": "845c6764-bd58-4c17-a1fa-b944d0fdecdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 52
        },
        {
            "id": "150b27e3-e7d9-4c6e-8516-ea3db66fe76d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 53
        },
        {
            "id": "14f9f8c8-2a71-432e-b3e1-454611f89066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 54
        },
        {
            "id": "9ee3b77e-8947-4ce3-a57b-e341e3a362d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 56
        },
        {
            "id": "af7696f2-95fc-4e8a-9da0-ac176ce328bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 57
        },
        {
            "id": "37fea693-2428-4c29-8b12-dd7d03835383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 65
        },
        {
            "id": "434ddfaa-a69d-41e8-9ca8-9f3197300edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 66
        },
        {
            "id": "7dc31a68-5809-4e9e-8d9c-b648bf776577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 67
        },
        {
            "id": "447a38ac-26be-4177-a2a1-0b06dced489b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 68
        },
        {
            "id": "83bf8cc0-d7e5-44d7-8f34-341dd30cb48f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 69
        },
        {
            "id": "49b07639-fff2-4261-b966-b66950fc07ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 70
        },
        {
            "id": "965f1224-c660-4471-a7c0-2d8825fb8c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 71
        },
        {
            "id": "6e0162a4-9e49-417d-b434-e098e75a9d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 72
        },
        {
            "id": "0cad78fc-5796-4577-ad3e-ce3059e265b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 73
        },
        {
            "id": "c3713be5-48cf-4b7f-ae96-0b487f659fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 75
        },
        {
            "id": "2f03c056-fe4a-4767-b4c0-af250ded3bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 76
        },
        {
            "id": "d1120e45-fe19-4541-b7b4-38e6de7675d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 77
        },
        {
            "id": "becace97-9968-491a-a71b-0b938362cac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 78
        },
        {
            "id": "97e2eda1-57b6-4819-a17d-24a09f70c9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 79
        },
        {
            "id": "08a38ba5-4283-410f-a2d7-6f376fb5d2fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 80
        },
        {
            "id": "3b9f059d-5f1e-4c39-b745-8ad03909e0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 81
        },
        {
            "id": "9ebe1811-9fcd-4b3f-ac32-982f85848fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 82
        },
        {
            "id": "62493d6b-e086-4285-b071-ed999784b91f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 83
        },
        {
            "id": "5e14211c-0aff-450a-9226-cab97782e556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 84
        },
        {
            "id": "30baeab3-287d-4eed-ae91-779fed835d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 85
        },
        {
            "id": "c9a6618c-8430-4b40-b9ab-387663d43377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 86
        },
        {
            "id": "9ebd0bb3-b3b6-4a18-b9a2-928f8e198b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 87
        },
        {
            "id": "94433f01-b627-4c02-831c-228c7d726d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 88
        },
        {
            "id": "8eaeb34f-ca77-48d3-aa9b-1c67e9939f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 89
        },
        {
            "id": "e1b126ef-9c57-4032-bdc5-534169fa6581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 90
        },
        {
            "id": "40c1e74d-494b-44d9-a2c7-8532a2783e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 97
        },
        {
            "id": "d4ece972-3b06-4e64-87c2-0a88648315ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 98
        },
        {
            "id": "bd0b08d8-6c33-4b9f-baa1-75482ff82410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 99
        },
        {
            "id": "88a5d0ec-c92d-48a3-abdb-1b824b9c9129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 100
        },
        {
            "id": "bd389d5a-d7e7-4f9a-b7d0-1ef79076df47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 101
        },
        {
            "id": "e4227cc4-6c03-4da5-be4c-d07b8533e898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 102
        },
        {
            "id": "5170ed3c-e7b2-49ee-bbb3-514f7692f4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 103
        },
        {
            "id": "279b5d22-02b9-49af-ac51-d4bb8b19f0c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 104
        },
        {
            "id": "5eb7766b-f072-4d1f-ac4b-e0e16f5a71be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 105
        },
        {
            "id": "17ea3dd5-26a0-494b-91ad-eccff322cc22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 107
        },
        {
            "id": "197cc6be-356b-45a0-b490-2212be600d43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 108
        },
        {
            "id": "38137f6d-4de1-42b1-81a3-6bceff6d5b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 109
        },
        {
            "id": "391993cc-2d24-4f6e-a292-75e2b079c30c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 110
        },
        {
            "id": "6552d8e7-11e1-430f-8921-d07478e79f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 111
        },
        {
            "id": "24d22e1b-66b4-45d7-8233-510e7090d48e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 112
        },
        {
            "id": "c272187d-53e4-4c4c-ab07-15a1794f5850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 113
        },
        {
            "id": "3f9a58c7-7c58-495d-a488-4e5ecce9117a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 114
        },
        {
            "id": "fbe3d8e9-bd28-4fdf-8c10-e6c1e548973e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 115
        },
        {
            "id": "2f17f919-1a82-4e91-9870-5bdc86549ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 116
        },
        {
            "id": "581a42c1-3d3a-4671-83e8-baea62adc079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 117
        },
        {
            "id": "79d32384-e080-4545-934f-096d9758ad42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 118
        },
        {
            "id": "8db6968e-0e4f-41af-b462-d09e1f0eb33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 119
        },
        {
            "id": "d4d293cc-1659-4884-8b8d-ac851ab9b0fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 120
        },
        {
            "id": "6b562705-c514-4ffb-b976-9bb2b126164e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 121
        },
        {
            "id": "60c1a6c2-1dd3-49d0-afa4-d8612fe417ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 122
        },
        {
            "id": "fa0390ae-dc4e-4fbd-aeee-b24da8f48698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 48
        },
        {
            "id": "eb21fda9-3544-4e36-82b0-c15c01e567ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 49
        },
        {
            "id": "6a5e4545-205b-4cc7-afe5-2b300d17b20c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 50
        },
        {
            "id": "4d7ae742-321f-4b9a-90b8-fee4281f3e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 52
        },
        {
            "id": "ed450886-2e22-4d3d-82c5-f0970fa02006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 53
        },
        {
            "id": "09dd425c-e39e-4574-8686-4a319d98d8f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 54
        },
        {
            "id": "d87108f7-978b-47bf-a3c4-071792cc9dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 56
        },
        {
            "id": "e13458f1-b2d7-4cbc-a68d-190a444e3f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 57
        },
        {
            "id": "144fe75f-cc24-46a0-937c-d75e5bdcc41a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 66
        },
        {
            "id": "87e614a1-ad37-4ac8-8ba4-972cb671d498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 67
        },
        {
            "id": "6ba3c257-d3ff-4176-b7b2-59bc1cb7149f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 68
        },
        {
            "id": "270e1986-199e-4776-9fb1-47ca611b4b52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 69
        },
        {
            "id": "401ab8d6-f6b3-4611-8b63-760de8a1bdfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 70
        },
        {
            "id": "22e94fa9-58ef-481b-8b2d-2f0cd49ed8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 71
        },
        {
            "id": "b13a2ff8-02b8-498f-8fd7-026b73444734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 72
        },
        {
            "id": "bbbc0474-0c41-4d43-b9d9-1a53aa848487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 73
        },
        {
            "id": "632cf510-9fb6-45e3-872f-17e3562aa79c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 75
        },
        {
            "id": "04594220-1668-489b-9c3e-f7090453a7d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 76
        },
        {
            "id": "33d8507d-c9d4-4b53-b830-6a3f874bc0be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 77
        },
        {
            "id": "7bee7ddb-cc19-4ac6-91c0-63b638483be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 78
        },
        {
            "id": "c3f74c95-45ce-46a8-8adf-54126afe2bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 79
        },
        {
            "id": "af2e1383-7527-40cd-b10c-3c90b17f99b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 80
        },
        {
            "id": "c8a9e83d-90f3-4dab-ada3-5ee431fbe49f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 81
        },
        {
            "id": "9f70eb77-3301-46e6-9aa0-afab58f8c022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 82
        },
        {
            "id": "efb6ae2f-c36c-4a85-930a-a4746061f5c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 83
        },
        {
            "id": "b21614cf-f629-46b2-bcfc-500149a8421a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 84
        },
        {
            "id": "43b99c90-cb75-4648-a513-9ff23f01ce50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 85
        },
        {
            "id": "fbf80831-3f2c-4f26-b9a0-cd66e5493009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 86
        },
        {
            "id": "03637b7c-52bf-4c1b-8a52-ee383b4ac892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 87
        },
        {
            "id": "a0489cf1-1272-4b94-8554-d7a4328df504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 88
        },
        {
            "id": "175c7bde-c3c0-4e5f-8f3d-b4240fe96480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 90
        },
        {
            "id": "010bb40f-e6fc-4a61-9c84-de2b1d5498a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 98
        },
        {
            "id": "d55e5909-8cf3-4e85-aac9-8c2ee5bf2a3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 99
        },
        {
            "id": "1911904f-7bf2-4cd3-b2db-c0d4eceda774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 100
        },
        {
            "id": "07c6c646-00a5-4c74-8810-bb26c038a474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 101
        },
        {
            "id": "f73aac62-d4c5-4b87-bebe-c800d04dd68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 102
        },
        {
            "id": "8da81b93-575b-4730-bf5b-0f4d571e9fd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 103
        },
        {
            "id": "a3128a4b-18bb-4e94-a9b2-4591ab11d6ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 104
        },
        {
            "id": "fc0f6bf1-56e6-4069-9771-ce14475269ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 105
        },
        {
            "id": "3a3aa510-41c4-46f4-b5bb-720abf967786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 107
        },
        {
            "id": "502d1150-974b-445e-8aa5-5583b4fa8414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 108
        },
        {
            "id": "e367a4c7-ec72-475e-947b-7dc5d8cd960b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 109
        },
        {
            "id": "30e95e21-fef9-4bb5-8372-333603cf482a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 110
        },
        {
            "id": "66e2433f-7eb5-4635-b3ad-1c7e3b4bf659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 111
        },
        {
            "id": "e0c36161-6967-4063-a9e2-25630f6b7c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 112
        },
        {
            "id": "9cef8b71-2b18-4eb8-bdb0-f0b297c71b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 113
        },
        {
            "id": "2061c134-8629-45a9-bc1e-97f6421f7354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 114
        },
        {
            "id": "e9c99ed5-a383-41d3-80de-a19e4b69f425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 115
        },
        {
            "id": "464007c5-95b1-4be2-9a55-eaf8deabf08e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 116
        },
        {
            "id": "fbfe9f5b-95f6-495b-a71e-49dc32e4f449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 117
        },
        {
            "id": "e761a0d9-018b-4067-935e-1e3ed32c7aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 118
        },
        {
            "id": "d6785ba5-d7a6-49e7-9959-d382dd7fd5bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 119
        },
        {
            "id": "d5907572-f661-4c97-a510-c570e8523195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 120
        },
        {
            "id": "89560b4c-4070-422a-b3e2-ab3d9530dca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 122
        },
        {
            "id": "4cf3be45-333c-47d1-b72d-64b0aee70a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 48
        },
        {
            "id": "8a652047-8e3e-42e5-9165-acc56447ad57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 49
        },
        {
            "id": "82ccbba3-97d7-4db6-8868-3679c66ff9d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 51
        },
        {
            "id": "071415ec-1734-4d6a-8e51-08917d106a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 52
        },
        {
            "id": "3e06b0bc-4d1f-4717-857f-7998eb84273c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 53
        },
        {
            "id": "ddf16394-bbf3-42a1-80d0-b65e7015fdfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 54
        },
        {
            "id": "c01cb505-e347-4e0e-a677-84cca0c647cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 55
        },
        {
            "id": "c02d42e4-6240-4a24-a2d1-afc20888edae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 56
        },
        {
            "id": "379327b4-03aa-4fe5-8e02-62aa085c550e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 57
        },
        {
            "id": "c7fe71ff-444b-43eb-bf03-201cda729b05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 66
        },
        {
            "id": "0262f4ae-a293-4e73-a99b-5836a7248ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 67
        },
        {
            "id": "c30dbf8c-620d-4fb9-8fd9-060935d07e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 68
        },
        {
            "id": "5132ec4a-9362-4427-b961-36d74f7ef400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 69
        },
        {
            "id": "2b316621-80f5-48b5-b281-bd3ec3bb6278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 70
        },
        {
            "id": "a0bd4502-c846-4b32-b29f-cd9d6f3f0dcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 71
        },
        {
            "id": "dcffd526-95f8-420f-872d-4e24aa22ebed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 72
        },
        {
            "id": "80eb6bf4-84e8-4ec0-808d-054681b6486c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 73
        },
        {
            "id": "76ce38bd-8cda-408d-a7d2-0e5a19f81660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 75
        },
        {
            "id": "285ca96c-17da-4057-9aeb-a1ee8c283df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 76
        },
        {
            "id": "6bd8c9e4-eb8e-4763-b4c1-4b9899a49750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 77
        },
        {
            "id": "12b84a2a-2e67-457a-9c19-6c2ed3ff8924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 78
        },
        {
            "id": "07993cc6-6367-46de-8a71-c64768d4b8c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 79
        },
        {
            "id": "cf9d87e4-fd2b-497b-990d-22e25e8b5ab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 80
        },
        {
            "id": "80aa3d93-77f9-48a0-98bf-2fe77693132b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 81
        },
        {
            "id": "0672881a-40ad-4fae-89e8-b54ee5ab566a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 82
        },
        {
            "id": "c1a7bbd9-7283-4e9c-9549-bbccf820f164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 83
        },
        {
            "id": "48eb08b3-8edd-4762-ba24-6aeae709fe42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 84
        },
        {
            "id": "524a311f-91dc-45c3-96f5-1e3e433b1d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 86
        },
        {
            "id": "c0e9c0e6-9a3b-47d9-ac86-71793ae5e3da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 87
        },
        {
            "id": "34fbee3e-a6f6-46dc-9e30-c9d1d5a61f7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 89
        },
        {
            "id": "cdd70c6d-e68b-448f-b3a0-4d3e8b43bd7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 98
        },
        {
            "id": "6456cba3-d7bf-4c4b-bc8c-a1c4276670f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 99
        },
        {
            "id": "e151df1c-6a10-4b3b-b518-b3af35f1e9cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 100
        },
        {
            "id": "e216d973-eff0-48aa-9298-bfcc369d1d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 101
        },
        {
            "id": "066df9be-0734-49c2-8d6d-09b54e3aba48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 102
        },
        {
            "id": "5f5f3b67-d22a-4c62-b7c1-fbf4bb5d89e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 103
        },
        {
            "id": "af25f724-7715-4170-9b1d-5214a0e9d496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 104
        },
        {
            "id": "55f59428-cf43-492a-9e38-06bd6bd6d9a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 105
        },
        {
            "id": "d07261f9-1ab3-466e-bddc-934d8850941f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 107
        },
        {
            "id": "96d6c3c8-1797-43fc-869c-00c38d5a7d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 108
        },
        {
            "id": "d0f68a0e-db7e-4572-9398-e096ddceafaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 109
        },
        {
            "id": "dfb09448-36c5-4b3f-b039-1a44405cf0c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 110
        },
        {
            "id": "1c27b6ea-dffd-47a4-b069-a427323df4b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "4d501c1a-606d-4ed7-9b6d-4b162742d248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 112
        },
        {
            "id": "84746e67-2ea2-44fd-8047-43156e3f2561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "3848e9d0-3d5c-4c8f-94dd-f627203e3ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 114
        },
        {
            "id": "134ea28f-6bd8-4faf-8d73-6dc2d14aeb78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 115
        },
        {
            "id": "e04d8700-0db5-448f-9c22-77684acb5c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 116
        },
        {
            "id": "c63de85f-910a-4c18-ba6d-291d138c5b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 118
        },
        {
            "id": "3f26d0b0-ab02-47f4-9c23-317b3e67c9c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "e32e648d-53ed-413f-8490-19aa5541b96a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 121
        },
        {
            "id": "36cbfbff-8bab-4857-b05f-1284bea3a70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 49
        },
        {
            "id": "6a5aa634-c9db-47c7-b6f7-7000fad794db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 52
        },
        {
            "id": "48c5f0fe-09d0-4d4b-8e6b-ccccaa7404db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 55
        },
        {
            "id": "b735857e-97f8-4e9f-9d69-86360bb2b641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 83
        },
        {
            "id": "f9c32336-bb66-472f-8723-51242d1ced51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 84
        },
        {
            "id": "7c41cc27-79bc-4989-ad1e-6fd5ec9acd7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 86
        },
        {
            "id": "256d0b7d-4dba-427a-8379-572f81a81db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 87
        },
        {
            "id": "b33a3f60-7de5-4842-986c-c976500d6cd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 108,
            "second": 89
        },
        {
            "id": "ab0f83b7-0fb1-471f-9e6b-eae3442b8cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 115
        },
        {
            "id": "55b9ae85-e4e6-41a6-930c-ab009aba37fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 116
        },
        {
            "id": "bae807ec-7337-4ce1-8b02-efd428b93bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 118
        },
        {
            "id": "cc511436-3138-4de4-8a29-fdf03cf10b3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 119
        },
        {
            "id": "58fda3fc-fddc-4315-8f02-1f85c9dae95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 108,
            "second": 121
        },
        {
            "id": "e9b6417f-a781-401c-9a03-8650ec069b2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 48
        },
        {
            "id": "f01f894c-840b-45f7-a052-cdb176af8785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 49
        },
        {
            "id": "f0801e86-abd8-469f-8d39-736e0267e5f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 50
        },
        {
            "id": "dd741733-e851-4f6f-a623-be7ca64342f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 52
        },
        {
            "id": "1374d8ba-db11-4b46-a72d-79f22604c884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 53
        },
        {
            "id": "a877ce45-3e11-42b9-8908-f0d8dd14634a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 54
        },
        {
            "id": "e162f8e5-d3ba-4db3-86fe-5523adff0e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 56
        },
        {
            "id": "fcd9e8ca-4b7a-494d-bcec-4bbcc1e7fbdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 57
        },
        {
            "id": "08c1ae69-48ef-4bb6-a248-2963ecc40ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 65
        },
        {
            "id": "fa4fc524-e0a0-4036-a6b5-853a928a0e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 66
        },
        {
            "id": "de618f06-0129-4f3f-8107-14cbe6a414f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 67
        },
        {
            "id": "b1084541-21e7-4b40-b2ba-6ec0a4358ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 68
        },
        {
            "id": "e9bfede3-c01f-4834-99aa-06ae2c19f451",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 69
        },
        {
            "id": "9e3bf13d-b942-4c60-a0fe-c229cc5708af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 70
        },
        {
            "id": "31d50b8b-6bdd-4f8d-9621-555dc8841ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 71
        },
        {
            "id": "a410c898-b35f-43ad-ade4-d584f1174837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 72
        },
        {
            "id": "649a5991-d7cf-4f54-a6a4-b0d49bb1fa42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 73
        },
        {
            "id": "d8a10c8c-dcda-4857-8e77-fff5ebb75a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 75
        },
        {
            "id": "91a972d4-03dd-4323-b5a7-021373157771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 76
        },
        {
            "id": "f9711e20-159f-4c10-8423-2d0b146fa446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 77
        },
        {
            "id": "dc087d6d-a133-4ce0-a7b7-874f806c699e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 78
        },
        {
            "id": "8595031b-821f-4fcb-913d-d0028fa08a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 79
        },
        {
            "id": "6edc440c-a568-439b-a93a-417d9d2c9941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 80
        },
        {
            "id": "081a28e8-7c27-42a1-bb11-9e93eaaae968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 81
        },
        {
            "id": "f36558f6-0a1a-43e3-bdaa-f877a2fb0ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 82
        },
        {
            "id": "d2bc82b6-3393-472c-8508-18711fcccad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 83
        },
        {
            "id": "ee041241-1f30-42f9-a1d0-03d3fcfcc9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 84
        },
        {
            "id": "a6bc37e7-d84f-4a0b-be69-806a01b742b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 85
        },
        {
            "id": "22f348c5-302a-4349-b781-6bdaac8dc89e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 86
        },
        {
            "id": "dfd61a57-fcff-4b3e-bcfa-6e0a8b76210f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 87
        },
        {
            "id": "dd70e285-f687-448b-bf95-78ca5834b7df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 88
        },
        {
            "id": "d6da77a6-719e-4a72-9b0c-6d13b49dd293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 89
        },
        {
            "id": "ef656fd6-5d98-44c6-ac51-5e72743225c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 90
        },
        {
            "id": "7f3b56cc-e43d-49b0-a694-fbe121ec0d88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 97
        },
        {
            "id": "6add329c-9a66-40ad-b06f-8c4dd666bf3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 98
        },
        {
            "id": "c136e7d5-8841-4736-8da4-41d4170062d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 99
        },
        {
            "id": "13dab16b-1ffb-4105-a3aa-5842d9aee305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 100
        },
        {
            "id": "16865788-9af9-4b84-ac92-08e1ba9c2c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 101
        },
        {
            "id": "0d3fd717-5383-4a9c-bf65-30c2614f0505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 102
        },
        {
            "id": "96448d34-48ab-446b-ba09-4d9e3ee008ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 103
        },
        {
            "id": "42b152b4-7a86-49fd-81d0-44fea408314e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 104
        },
        {
            "id": "21dea654-3e38-412a-822b-2b224a24f70f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 105
        },
        {
            "id": "bffcb624-a7fc-49b3-bdf3-caad4b5bff84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 107
        },
        {
            "id": "56968a38-c059-40fe-bf21-2af3168ee587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 108
        },
        {
            "id": "a6c0990b-7ed6-44ed-8c8c-fb17df1dcd3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 109
        },
        {
            "id": "a4f29261-aea9-47c7-bef2-eef39e76fb92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 110
        },
        {
            "id": "ef85642e-59c0-4ed9-9eda-ea9ca23c5ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 111
        },
        {
            "id": "322f7b56-c98a-4c66-aad5-36408c1d70c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 112
        },
        {
            "id": "184bc4f6-fe17-4d2d-b1c5-ce274aa5da99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 113
        },
        {
            "id": "7bceb6ab-b476-4740-8435-3d70f7b081c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 114
        },
        {
            "id": "fddab0fd-6c55-45c4-8f98-0c9dcbf6d1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 115
        },
        {
            "id": "b7eb89cb-1d3f-4c4b-8860-42a4d5540e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 116
        },
        {
            "id": "4b7dc34c-560c-4c10-a8c0-c2ef164a78ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 117
        },
        {
            "id": "a6a8878f-0486-45b3-b292-091a42983d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 118
        },
        {
            "id": "6d260480-38cb-42b9-964f-e4ecb0e75008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 119
        },
        {
            "id": "2177bdf9-5c0d-4870-ab51-63c5172b5b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 120
        },
        {
            "id": "9f85e7a3-ffb0-41e9-af2d-ba99abd608dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 121
        },
        {
            "id": "29cc8f11-cec0-429e-a2f8-62f4c303825a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 122
        },
        {
            "id": "2dc6fd3c-655f-4a8d-b65a-da1b58827bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 48
        },
        {
            "id": "c7e457bc-8aa6-424c-9258-d5289f50023e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 49
        },
        {
            "id": "7c55f809-8c42-4b13-b312-8bfe7142b31d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 50
        },
        {
            "id": "381d610e-ead3-4b8c-bab2-da3c6d367d45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 52
        },
        {
            "id": "10fdf18e-0512-4f65-8645-281339b17fdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 53
        },
        {
            "id": "028c8b6f-3b38-4054-9665-5c7282ea8d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 54
        },
        {
            "id": "396a0085-cf7a-419f-b2b7-f734556efffa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 56
        },
        {
            "id": "05a4c8de-93ec-4164-a77d-63e12ba606e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 57
        },
        {
            "id": "30091b11-d538-4936-89a6-e8473753eefd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 65
        },
        {
            "id": "83a11834-b820-4945-a222-baa965b1eb86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 66
        },
        {
            "id": "9404d1e6-c3a5-4b62-95f0-5178fa6f372a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 67
        },
        {
            "id": "1b9cfceb-d684-4b76-bb2c-9c42d5367c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 68
        },
        {
            "id": "503eac2f-dd72-4453-92b4-947319b77b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 69
        },
        {
            "id": "25f81973-44d9-4fcc-ad5a-c4faea6f6ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 70
        },
        {
            "id": "679990c7-d51f-41bc-b987-1be49f914438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 71
        },
        {
            "id": "5b3a3a2b-f38b-4c6f-8d35-35a4cef3478e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 72
        },
        {
            "id": "4a8d63f5-5463-4985-b7cc-b73d559c87b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 73
        },
        {
            "id": "a7f132d4-3f5d-4908-b8ee-6024e6afaecc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 75
        },
        {
            "id": "54350247-37cc-4564-8488-7afe7530a0f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 76
        },
        {
            "id": "39b4cdd8-2e07-4c0f-b3e1-7364a4550c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 77
        },
        {
            "id": "7d916943-ef03-4d89-b285-e59a38febeae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 78
        },
        {
            "id": "4583fa07-540c-4191-839c-831b70db92b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 79
        },
        {
            "id": "b40cd52d-91cf-4077-8df5-f84e97b9479f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 80
        },
        {
            "id": "22d71951-75a4-479e-aaac-6076c472a078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 81
        },
        {
            "id": "6b8830af-7ebb-47ea-bc2e-de4e40cabe40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 82
        },
        {
            "id": "811ecc19-bd97-486a-9b16-4441f1b1f3f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 83
        },
        {
            "id": "3aee368f-70d8-4a4c-9e09-256b7c08fcaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 84
        },
        {
            "id": "d10d4bdd-7389-44c1-a7d5-4c44d43f2e71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 85
        },
        {
            "id": "eed00d0e-cb70-44b7-92e2-bc3a91cdfd71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 86
        },
        {
            "id": "3550c735-f230-43f9-9e69-f94855e35544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 87
        },
        {
            "id": "1eb76341-041f-4d8f-a981-7257123aa250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 88
        },
        {
            "id": "f915b88b-2ea8-4838-aea5-a4486fc4de61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 89
        },
        {
            "id": "0006d1a3-b4c4-4924-b71b-232dd1f5f063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 90
        },
        {
            "id": "d6bd078c-d006-4548-97e1-29c1f38aaaed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 97
        },
        {
            "id": "b2a8dc49-2a7b-4ce3-b136-28a7ea08bc4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 98
        },
        {
            "id": "2528f3a1-89d0-48bf-bf03-c438bc763088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 99
        },
        {
            "id": "b769da74-b589-4c5a-93d9-a785f0798b1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 100
        },
        {
            "id": "cb1010c1-df07-4372-8750-e41144a4c729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 101
        },
        {
            "id": "5bee7bff-82b0-4c9b-b766-6c77d2258125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 102
        },
        {
            "id": "734998c8-d6b2-4691-a15a-a6c1ae254e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 103
        },
        {
            "id": "e886c077-fcab-4425-9b4a-e2d40cc1fe90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 104
        },
        {
            "id": "687aa5ce-d2b7-4b42-b691-384843fe15a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 105
        },
        {
            "id": "a90d898b-22ed-45f7-9903-d1df3302f4f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 107
        },
        {
            "id": "177c4a0b-9a85-482f-a445-e000567a8532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 108
        },
        {
            "id": "95509fa0-8593-4964-8843-68d565d4286e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 109
        },
        {
            "id": "eb39ae08-568d-4c79-8d39-3cfbb24d1f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 110
        },
        {
            "id": "28066cf0-2f83-4016-a48c-78bf6cdb8313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 111
        },
        {
            "id": "26fafc1e-4fde-4040-8a6f-fa6edea9fa77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 112
        },
        {
            "id": "25220fe6-88e3-4bb1-9f30-772893342a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 113
        },
        {
            "id": "8b6f0809-f46c-46d5-9e31-3055ac696392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 114
        },
        {
            "id": "dbaab1ee-1319-46e8-8f76-efbfdaf0c28c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 115
        },
        {
            "id": "e7bce2a7-d7fe-4311-8648-be10845647a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 116
        },
        {
            "id": "5e3d56d0-2057-48bc-ae84-af1f8d40ac7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 117
        },
        {
            "id": "37bf096d-3b8b-4656-95f5-6a81b7334f01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 118
        },
        {
            "id": "9b6944f2-22ee-44c9-b6b0-28d777a1840f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 119
        },
        {
            "id": "74fbb067-82a8-46ce-80f0-a2dfb52981c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 120
        },
        {
            "id": "a66400ea-37a7-4712-b01b-ad75662867e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 121
        },
        {
            "id": "46cfe247-8721-4a87-aca4-8a1e525cd8a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 122
        },
        {
            "id": "72e115c7-fb50-4b87-a074-487929b7f502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 48
        },
        {
            "id": "119fe404-1304-4626-9f28-60332738a88d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 49
        },
        {
            "id": "9a91e37e-77ea-413a-8749-b54f2e9db651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 53
        },
        {
            "id": "dc28ac75-a359-4e94-94f6-26fc18549a93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 54
        },
        {
            "id": "ce7f736e-af3c-4576-a1a4-5ab75ea6e774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 56
        },
        {
            "id": "6d67ebeb-0af0-4f1b-9436-e574f8f74367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 57
        },
        {
            "id": "967b08ed-2def-40e0-b345-2caa0815ec02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 66
        },
        {
            "id": "5503c40c-bbdb-42b9-981f-105fe615d73c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 67
        },
        {
            "id": "77206297-bf0e-4685-9ce1-24075a501505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 68
        },
        {
            "id": "1d925bf4-bd34-4ad9-b875-9a34c38a18e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 69
        },
        {
            "id": "82b0fc0c-bd7c-46bf-87d1-711bcc40725f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 70
        },
        {
            "id": "a8ff8f6e-e716-49f8-99a5-8a5362a03edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 71
        },
        {
            "id": "31bfa4a0-b9a7-4da6-a708-340d8a573a09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 72
        },
        {
            "id": "3c64f30c-92c7-4a18-a281-1a1d3961ae51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 73
        },
        {
            "id": "9e7ffa6a-98b5-4427-a6b9-4a25252ecda2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 75
        },
        {
            "id": "5672582d-4a50-4516-af04-3219082daa5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 76
        },
        {
            "id": "32e0d473-7af0-4910-9dbb-8d7202852585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 77
        },
        {
            "id": "a2c6b851-8eb6-4db3-b6d7-6cff271074b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 78
        },
        {
            "id": "72626d7b-2ffb-4300-b19e-323f1679c2aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 79
        },
        {
            "id": "5dbdaabd-475a-4b85-a86d-71714f37be5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 80
        },
        {
            "id": "d241b1c8-3415-4214-aa3a-33d8497969a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 81
        },
        {
            "id": "93965b73-609e-44db-bf69-b7e6be3562f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 82
        },
        {
            "id": "995e6978-8f12-45d9-9281-eec35af3291d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 83
        },
        {
            "id": "bf7c5c57-3bb1-47e5-88fd-dfc415ab14ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 85
        },
        {
            "id": "a5ef57e7-9432-41be-afef-d43e63d42226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 88
        },
        {
            "id": "6c9752fa-247c-4c88-949c-ef6fa2569c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 89
        },
        {
            "id": "1b376c54-f08a-4398-9e65-0cdf98c532cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 98
        },
        {
            "id": "ba3fada2-99f0-45ca-9dc1-0cdd23e6d2d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 99
        },
        {
            "id": "2ad6f274-07ac-4834-b41d-89013d42d781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 100
        },
        {
            "id": "a44c15af-26d1-4499-b9eb-338d3696d5df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 101
        },
        {
            "id": "5cfa4825-2937-4c4e-8841-39a918a8727e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 102
        },
        {
            "id": "7a16e7a9-fb72-4b71-a3c1-211b4e906ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 103
        },
        {
            "id": "5a3935d4-5697-499e-bd75-244f8079afd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 104
        },
        {
            "id": "20ba99e8-bf55-48e7-a379-e5a4d644fb76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 105
        },
        {
            "id": "04559e29-1538-4dab-8f31-6cb986f6d043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 107
        },
        {
            "id": "c69c41cf-5812-474f-aeda-a6d3a6075fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 108
        },
        {
            "id": "892bf7ca-b051-4892-922d-af557ce49b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 109
        },
        {
            "id": "f93ba471-b487-41e2-9386-ea7dd7792edc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 110
        },
        {
            "id": "3bf3280f-a4c3-46c7-8c4d-e007f8c8c7ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 111
        },
        {
            "id": "cc4c7e1a-fc0c-4388-94b4-e87d26927e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 112
        },
        {
            "id": "8e103a6d-4ade-4e08-9836-c5612b1975d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 113
        },
        {
            "id": "996ae25d-fef6-45b8-a5b0-da96d531f60f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 114
        },
        {
            "id": "c2db23c0-5acd-4937-bba8-b3c217cc92e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 115
        },
        {
            "id": "7eb9457d-85d4-41ac-af3a-3e56d9692d93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 117
        },
        {
            "id": "e69ceb4c-49f1-4d47-99cc-50c703d7d7ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "f6468362-199f-4fb5-ba19-2f612dfbc937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "cfcd0cca-8353-4d2f-be20-d81d85b5e780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 51
        },
        {
            "id": "5ee0e952-1e3d-410b-b063-577af133d579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 52
        },
        {
            "id": "24ad82e6-ffc1-4533-85b1-8246643fe229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 65
        },
        {
            "id": "1bab19b7-11ab-4578-8822-9b571fa8cd4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 66
        },
        {
            "id": "6d2d04e3-f02a-46ad-a2a9-419580d26e7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 68
        },
        {
            "id": "e69732f3-e29e-4025-9ca8-3f908d505cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 69
        },
        {
            "id": "b52ef1d5-484a-4cf1-ac3f-caf60e1b97d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 70
        },
        {
            "id": "4f673990-6e20-45d9-8173-0fbfe69a603a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 72
        },
        {
            "id": "1867e78b-78fb-4083-b102-79f089e745d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 73
        },
        {
            "id": "ec4f012d-9837-4bcd-9baa-28d88b19c20e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 112,
            "second": 74
        },
        {
            "id": "bbd5e07a-5386-4758-843b-7060b7a6bb83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 75
        },
        {
            "id": "50075511-92f5-45f7-b46d-47e1b708b5ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 76
        },
        {
            "id": "5c520729-5a9b-47b9-a458-a42ac1cfe2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 77
        },
        {
            "id": "c917e7e1-13d3-431c-ac6d-757dc66c7073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 78
        },
        {
            "id": "d14a5795-6b89-4611-962c-a30b1f6e29f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 80
        },
        {
            "id": "fbccf13b-d3d5-492c-8ca9-b8caaa3ce594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 82
        },
        {
            "id": "60518f99-a509-4304-9dc4-be0f26c311e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 86
        },
        {
            "id": "289a55b5-f61d-4231-a9b2-400b2bc523fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 87
        },
        {
            "id": "d215e3ba-2ba9-4e80-9091-72d54b4832c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 88
        },
        {
            "id": "eb57f35c-780b-46d4-93b0-e98ca9e4d055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 89
        },
        {
            "id": "16d2e597-ad6a-4bfc-87bc-af394bd31ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 97
        },
        {
            "id": "b5d5712f-6221-4014-a5e1-306038af5b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 98
        },
        {
            "id": "2112e0a7-4b1e-4f7d-9480-b9c25c7c2eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 100
        },
        {
            "id": "ea0ae338-382b-4f18-b40a-755e001c7e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 101
        },
        {
            "id": "0e78fa33-a275-40fa-9246-6e98217d425b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 102
        },
        {
            "id": "6d28311c-689a-444c-9927-12437c8345ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 104
        },
        {
            "id": "2329f2d8-d09b-48c1-92c1-bbe4b401e538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 105
        },
        {
            "id": "98daa813-6e52-4b1a-8e50-830945e3e3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 112,
            "second": 106
        },
        {
            "id": "eb2b5fc3-737c-4572-ada0-8c8b0efcaf07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 107
        },
        {
            "id": "9923c228-4176-4f04-9491-128957ef3fdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 108
        },
        {
            "id": "cc4695e1-59cc-4140-9c31-4dc80bd7cf0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 109
        },
        {
            "id": "45b95900-87d8-4887-b857-caf49b8d4eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 110
        },
        {
            "id": "7c195475-764d-458c-8980-05749533a589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 112
        },
        {
            "id": "bafcb92c-5d9d-479a-ace8-abd054235408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 114
        },
        {
            "id": "e6874163-349d-469c-b4f3-eff5d94f0e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "9f87850c-a8e4-415c-941f-d9131d415017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "a5fafb01-3e4e-42b0-bf4e-b8b4ddd1b492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 120
        },
        {
            "id": "db160303-0e47-4412-b976-a13ccfce2086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 121
        },
        {
            "id": "ef7b21b8-d3fd-458e-8a4a-18e5a5e0bcf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 50
        },
        {
            "id": "0622f261-3f39-4c4b-b3f3-d13ad5122116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 55
        },
        {
            "id": "eed42e59-1ed7-4f16-a9cd-6d363e05cc03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 65
        },
        {
            "id": "c266e3b0-a70a-4aa2-ac87-449ae7dcc4df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 66
        },
        {
            "id": "65893b3e-bb7f-4a02-a815-a7e3110224b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 68
        },
        {
            "id": "efa670dc-9a63-48a3-af3d-8d989084fa83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 69
        },
        {
            "id": "910bc2aa-85c9-44a8-9a19-68dd322c6dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 70
        },
        {
            "id": "87295080-53fb-42ff-8ffd-08d3978bc6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 72
        },
        {
            "id": "7ca1684e-03d3-4bff-b4a2-c39c85043829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 73
        },
        {
            "id": "bbafc3c1-4885-405c-ba08-6a90c39ed95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 75
        },
        {
            "id": "5ff86bfc-aede-4ab6-8bd0-86eda4d129f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 76
        },
        {
            "id": "03cc95a8-1d21-43e2-8005-789a328df7d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 77
        },
        {
            "id": "1199e445-582c-41e5-987a-f7b223aeb32a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 78
        },
        {
            "id": "a1033250-3493-43d2-9155-4dc224c864e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 80
        },
        {
            "id": "5516a81d-7c4b-4c9a-8316-974b200ba081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 82
        },
        {
            "id": "23805c88-9782-4122-aa10-982d8f9851d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 84
        },
        {
            "id": "8cd485ba-2a7c-4a66-b030-8f668334b5a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 86
        },
        {
            "id": "9a8d8008-eb6f-4358-921a-dd8cdbefb19e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 87
        },
        {
            "id": "851d7497-22b8-413a-aad0-5c2a4bbf4119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 88
        },
        {
            "id": "ee21e12f-84d2-4641-9d4b-c6493d15f94f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 89
        },
        {
            "id": "abdecfe9-96ac-4b1f-ac19-af462d09ed44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 90
        },
        {
            "id": "ab148731-f333-4d1c-95cd-d05458047c7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 97
        },
        {
            "id": "544c6f35-ca44-44e5-996f-2e10cc7b5a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 98
        },
        {
            "id": "e0300201-b7b4-4792-8f6b-88daea58ba8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 100
        },
        {
            "id": "b2e0dacc-6c85-4eb4-b206-0f2a7fd27159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 101
        },
        {
            "id": "b791e7b5-f560-48dc-b7ce-f469cd3b5632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 102
        },
        {
            "id": "5c2cbf49-a57f-485a-be2c-8fc8143b88c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 104
        },
        {
            "id": "28415d82-8012-489a-9591-f23c90062e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 105
        },
        {
            "id": "bd43a69a-b21b-46be-b5dd-3eafc9db762f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 107
        },
        {
            "id": "5b45ff64-cebb-4b72-ac2a-bef4a6ef460c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 108
        },
        {
            "id": "3db014af-1ddd-424e-9029-cc9196d855ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 109
        },
        {
            "id": "1911f60c-93be-473e-8564-114f40968fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 110
        },
        {
            "id": "894db429-b4a5-4e6c-ba5e-0f3e9edd9bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 112
        },
        {
            "id": "e6836db6-5ce9-41fa-a0e2-2dd0bdfdabab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 114
        },
        {
            "id": "c840dcae-e454-4226-89dd-440bbeefecc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 116
        },
        {
            "id": "c574a00d-993b-4b0b-acfd-d28b15839763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 118
        },
        {
            "id": "0d466d3c-45d9-4dbb-8aa3-ede17848cb79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 119
        },
        {
            "id": "9f89304b-7aac-4cf9-b87e-052becdba100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 120
        },
        {
            "id": "0a56ace7-bcff-4366-88a7-d7943cc6e620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 121
        },
        {
            "id": "8c509ebd-d3b7-44c1-becf-78879eb68013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 122
        },
        {
            "id": "64c2ab5c-0c65-4735-980c-8c15e9f1f123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 51
        },
        {
            "id": "ff314fcc-b052-4bd8-933d-2bc3c64670f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 52
        },
        {
            "id": "b327940b-dd3d-4c69-b909-6839f1e60431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 53
        },
        {
            "id": "31aa3024-f0ab-4f5a-88e6-15e5d9d9debb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 55
        },
        {
            "id": "28ec6564-84db-4930-85be-dcc97f34d905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 65
        },
        {
            "id": "39eda09c-56ca-4af5-9b7a-5a9f0cad784d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 66
        },
        {
            "id": "dd60d3b4-5c61-471e-84bb-b2515767b499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 68
        },
        {
            "id": "3b2e2fe0-c23b-4ab7-8edf-07ac0432d3c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 69
        },
        {
            "id": "e6ab5439-11c9-48f7-b459-5a43478e6e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 70
        },
        {
            "id": "28992d24-67cb-4cfa-a815-346d0cf16e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 72
        },
        {
            "id": "076cd972-b605-49e9-ba12-097547f9c533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 73
        },
        {
            "id": "d28f149c-55a4-49b2-9c37-f68afbfecadd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 75
        },
        {
            "id": "ceccc1a3-a314-4e20-b3f2-12112b95fa52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 76
        },
        {
            "id": "a51c323b-06b8-4a9d-999e-7db503652536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 77
        },
        {
            "id": "19d74b9f-10f8-4171-acb4-8229cf77aef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 78
        },
        {
            "id": "dc711abd-34d0-41ea-babf-c885d728ffbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 80
        },
        {
            "id": "c4efaeb4-a7a6-44de-b1fd-35b00c336ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 82
        },
        {
            "id": "f5996b2d-0775-493f-b330-000f558c2658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 83
        },
        {
            "id": "62f96b32-8660-4301-8799-ee67f682c526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 84
        },
        {
            "id": "1db83d2b-0766-482f-9b70-9fc3fea20de7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 86
        },
        {
            "id": "67f23dce-8460-435d-ad95-73693261b43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 87
        },
        {
            "id": "6bdd130e-d22c-403e-a0ce-fcd5016526bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 89
        },
        {
            "id": "aae94f89-37f9-45a1-b1cb-e15d3ee87fda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 90
        },
        {
            "id": "7ef1024e-b1e9-4de0-a79b-492c6f8ce259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 97
        },
        {
            "id": "1b1b99da-8521-46fa-88d5-ab8b44294d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 98
        },
        {
            "id": "cc325ccc-4a46-4862-bf2e-af01f77d6a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 100
        },
        {
            "id": "26c14244-1a15-4345-bd71-ccac98ffb00f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 101
        },
        {
            "id": "f13db3cc-666a-43da-ba52-612c9f2714b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 102
        },
        {
            "id": "b7b2631e-9917-44b8-b7e3-3a36eeafc2c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 104
        },
        {
            "id": "c1dd28bc-be34-4e4f-b2f1-366b2f51b422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 105
        },
        {
            "id": "df458c63-399d-4afb-9dbf-96bb93621605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 107
        },
        {
            "id": "6da40a69-b856-47d0-827c-56fbba13f1b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 108
        },
        {
            "id": "42c5cf76-74e9-4ecd-85a5-647f8026cf15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 109
        },
        {
            "id": "e38784b0-8618-4c55-a7d9-4ce8324b0bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 110
        },
        {
            "id": "6572471f-0c21-4bb6-b75d-41094c2230b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 112
        },
        {
            "id": "73120c8d-bf81-4642-916b-5da920b03782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 114
        },
        {
            "id": "97f0794e-479e-4f14-a1b9-ef64144a3ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 115
        },
        {
            "id": "d5cace8f-60e2-4425-bd9e-5736bc97cc24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 116
        },
        {
            "id": "60af0699-9bf5-4209-aa3f-66dd531a7577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 118
        },
        {
            "id": "581bf210-384b-4ec2-850c-4157d91f7e5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 119
        },
        {
            "id": "fb23da6d-b6f1-4ca2-9a8c-dab587b26882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 121
        },
        {
            "id": "fad25afd-e5be-4dff-9b2d-0925bcf524c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 122
        },
        {
            "id": "a0e235d0-318b-4361-82e2-2f403664885a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 48
        },
        {
            "id": "c31a6127-2ea4-4a47-8894-90cb876c1c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 54
        },
        {
            "id": "c74850fb-ffed-43b3-8684-7c819fbe9fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 56
        },
        {
            "id": "52c745e5-c7ef-45b2-8c18-32a6157f7f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 66
        },
        {
            "id": "36c41524-9a18-44a3-9dba-63bc2d4f5d63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 67
        },
        {
            "id": "426bd98f-20ac-47fa-ba23-46314afda1aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 68
        },
        {
            "id": "05ec3241-ffee-45c8-9b5a-abbaf2454103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 69
        },
        {
            "id": "eee0d2b9-c194-435b-9212-59df83068a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 70
        },
        {
            "id": "18315e2e-7c01-4fbf-805f-632d9b717371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 71
        },
        {
            "id": "71cf8a91-b91c-4613-9f49-c8ddaf6d6014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 72
        },
        {
            "id": "90f4733b-a6dd-4f27-ad01-29f386b5d37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 73
        },
        {
            "id": "5b87df24-4aa8-4ddd-9073-adfa8370c29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 74
        },
        {
            "id": "574ad549-b00f-4569-af99-70e3ae54fcad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 75
        },
        {
            "id": "36abcdc7-c3ca-4bf1-8e9a-fe80940fa02a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 76
        },
        {
            "id": "6f0c1fbd-c25c-4230-aeff-5fe416c8edc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 77
        },
        {
            "id": "dcc4ec00-3c08-4503-90fe-f9b4deb1e92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 78
        },
        {
            "id": "53218a26-1fc3-45d1-b4cd-f47746a877ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 79
        },
        {
            "id": "a6396a26-0f71-4ffe-abf5-562f4f9ff6f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 80
        },
        {
            "id": "4ba4afa4-b673-4402-8ae5-15c486f3ffe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 81
        },
        {
            "id": "617c512a-ae82-4c5e-adab-fcc1ff4cf821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 82
        },
        {
            "id": "4706c02c-b94e-42df-b523-9c14a55a4428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 85
        },
        {
            "id": "f88af516-0f7d-4ada-b939-5e23f1c6d238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 86
        },
        {
            "id": "72043d25-a0fd-4266-bee9-7c74827a4744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 87
        },
        {
            "id": "e69197e0-cadf-4e31-9743-211a8d97d72e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 88
        },
        {
            "id": "9b2fac0b-b2bb-4349-a10b-ae3b79b8c6f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 89
        },
        {
            "id": "7c8a74f1-0047-422b-9783-d36777b7871f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 98
        },
        {
            "id": "9816a5f3-325c-4b87-89f2-b2f5af38644e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 99
        },
        {
            "id": "e2319eb4-3d29-4384-8e13-026717e43733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 100
        },
        {
            "id": "614ebe91-042e-4bcb-ba0e-257ccc31c543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 101
        },
        {
            "id": "64fae6ef-814e-4459-8302-b959e8fb5d07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 102
        },
        {
            "id": "794a6ef1-9b92-48af-bb03-1fdcc9a6309f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 103
        },
        {
            "id": "9c97ba5d-ac0d-4d23-940b-956f232ea3cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 104
        },
        {
            "id": "8b480c51-f6ef-4c5b-930e-5bbd38d0e517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 105
        },
        {
            "id": "02c7f805-7b4b-42ad-8d53-705f3b91d688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 106
        },
        {
            "id": "720fdef3-bdd6-4cb6-8dd3-cddf6aff465d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 107
        },
        {
            "id": "d623621b-303d-43da-97d3-cc508b67e5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 108
        },
        {
            "id": "7ad355ef-864a-4289-be7f-db98ee36820f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 109
        },
        {
            "id": "e47ed322-2dcc-453e-ba00-61fb7e2614d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 110
        },
        {
            "id": "c6ad3e99-4fb8-4658-bf2e-64110003dac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 111
        },
        {
            "id": "7ed9be0f-ae0c-4b6e-8f48-ee8a6f597fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 112
        },
        {
            "id": "a413e240-7569-442c-abbf-72f677d876b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 113
        },
        {
            "id": "ba9ef6c6-7bbe-4f8b-ab82-235c1c6c168e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 114
        },
        {
            "id": "9799b296-5482-4474-95d1-53ebab823513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 117
        },
        {
            "id": "44681ca1-223b-461b-a858-6b8860be7732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 118
        },
        {
            "id": "72029733-21e5-4e1d-b253-126e165accba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "9d08f3b3-a504-4978-abc4-6beb24802cbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 120
        },
        {
            "id": "99b93eec-6945-4b5b-aee1-b4122dc2b2ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 121
        },
        {
            "id": "09be7712-9f7f-47ed-8223-f8018c7baad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 51
        },
        {
            "id": "ee3f1aaa-a43e-473e-af51-7eda707f003b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 52
        },
        {
            "id": "9d803e78-cc66-4140-aa16-a4a66879e8b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 65
        },
        {
            "id": "3536fff7-656a-4d02-8eb5-6d44f8fd40df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 66
        },
        {
            "id": "b1de064c-e57f-4f55-8d92-4a0546ab12f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 68
        },
        {
            "id": "92f61ee4-eb8c-40f9-9d10-06298771b8c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 69
        },
        {
            "id": "1346ae61-fff6-41c9-909b-3b9afe4aaf79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 70
        },
        {
            "id": "40d75df3-d299-49c2-a787-7de2f056b657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 72
        },
        {
            "id": "e9ea85c2-d655-4e87-b745-f380cb2a1c24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 73
        },
        {
            "id": "6cbfaaa7-cabf-4b91-8927-3a71c8070d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 74
        },
        {
            "id": "fce1a803-36ac-4198-abe4-b98b84871dd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 75
        },
        {
            "id": "2e09ffc1-1432-410e-92f2-b0867ffe6d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 76
        },
        {
            "id": "461bd447-9191-4835-9525-17f5dc09f623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 77
        },
        {
            "id": "f04ed115-5f3a-4f74-8239-6e15ff82ccf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 78
        },
        {
            "id": "368f0009-a914-4db1-9cba-4976e518ab24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 80
        },
        {
            "id": "3654693f-082d-4a25-b3d5-dda30f7ffc87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 82
        },
        {
            "id": "a1d473f2-3a0c-4a54-8d35-0ab3d3b25ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 85
        },
        {
            "id": "e3b62542-cdb9-4083-9944-84ed2fa420d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 97
        },
        {
            "id": "636fb98c-0cba-4560-986c-b73bc9bd039d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 98
        },
        {
            "id": "58c4e4b0-ae2d-4285-891c-48f30ba86ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 100
        },
        {
            "id": "c1d6e0b0-2352-4ca7-87bd-1cf74ec95566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 101
        },
        {
            "id": "1bdde639-78a3-4f29-81d9-95bc699251f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 102
        },
        {
            "id": "89ad0f38-1838-47ca-88eb-652ec5428802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 104
        },
        {
            "id": "a896119f-32bf-44f8-916a-0a55d902f98c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 105
        },
        {
            "id": "d8c593ca-2b95-47cc-922b-190e31ed2ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 106
        },
        {
            "id": "37a8b78d-7bcd-45c5-a96e-4bc22c70b8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 107
        },
        {
            "id": "da19a70c-5e08-4d8f-be73-8b38abd8ba85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 108
        },
        {
            "id": "2269c4c2-52c1-44fb-801a-1c9fc1defe74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 109
        },
        {
            "id": "c164bb0f-035c-461e-85c8-fa295779e58d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 110
        },
        {
            "id": "73e2b0e5-3baf-41a0-be1f-15e75a534f01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 112
        },
        {
            "id": "df8d67d7-c603-4696-92a7-e2dfd229d2f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 114
        },
        {
            "id": "b133f329-aadb-4ac5-81a6-a770d4643e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 117
        },
        {
            "id": "a840de8f-bc09-4fac-9dc4-77796081f31e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 48
        },
        {
            "id": "a56afe6d-ac12-4c25-9339-76c480f1a91f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 49
        },
        {
            "id": "0ff1b725-c200-4b9f-a767-58e5fa99cfb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 50
        },
        {
            "id": "e7d879dd-e281-4e4d-abb1-42599aaba86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 52
        },
        {
            "id": "0bae1c48-6d7d-42c7-b333-71026d457b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 53
        },
        {
            "id": "7b826a1c-febb-45a9-b6d9-ca29d6b61e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 54
        },
        {
            "id": "44deee71-8df4-4d12-87a9-160c17adf489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 56
        },
        {
            "id": "5060b6fa-5dff-430e-8513-4649ee4fe1f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 57
        },
        {
            "id": "611e07d3-ec02-438a-be2d-71907b5ac3b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 66
        },
        {
            "id": "cb70f5a0-e27f-486f-8143-425b8399a19b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 67
        },
        {
            "id": "fcd04ba8-378a-4b55-bc28-457c78cb841e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 68
        },
        {
            "id": "d2cfee1f-d8d0-4e97-9cdd-cbf81905d5fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 69
        },
        {
            "id": "e39a62a3-160a-4d44-a3f7-04b39c014ba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 70
        },
        {
            "id": "ad0f2c7f-d6e5-4f3c-8d77-d067915d3407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 71
        },
        {
            "id": "e2ab0fbe-14db-45ae-a30f-bcbd6bbf0b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 72
        },
        {
            "id": "c203f876-20c8-4515-a1b2-374053fe1c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 73
        },
        {
            "id": "42d8f924-98d4-4565-8e5b-48b7ffea9b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 75
        },
        {
            "id": "24f758cd-ed8b-45e3-94e2-3ccbfc60f635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 76
        },
        {
            "id": "06bd3b3f-e6c4-4931-a49b-f1b9e52ea9cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 77
        },
        {
            "id": "421f7ef3-44c8-4723-b070-a86cc984408a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 78
        },
        {
            "id": "5f69322d-454b-4f18-8fe7-c73a249ddc21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 79
        },
        {
            "id": "ecb61579-6a7b-446a-bef7-463dcc54fcaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 80
        },
        {
            "id": "cd4c5615-1c9e-487a-8718-99f68d4138a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 81
        },
        {
            "id": "8fe97bf3-ece8-46d2-a173-c455977f6b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 82
        },
        {
            "id": "d1df867a-8881-4119-b70f-ca95e8048b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 83
        },
        {
            "id": "e9ae351d-9c7c-462f-be2d-c8384bcc85b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 84
        },
        {
            "id": "621be53d-58fe-4f46-95fb-fbe46398bac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 85
        },
        {
            "id": "33b897a1-c43c-4f51-8b30-3c2e9403b901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 86
        },
        {
            "id": "27cec064-fe3c-4706-b73e-d2ac4e2b2ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 87
        },
        {
            "id": "9200bb5b-abe5-48a3-b967-a24e0f9a8b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 88
        },
        {
            "id": "349a2ddd-1f46-45b4-8abb-2beb28e3fbd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 90
        },
        {
            "id": "f0786700-53c4-4608-9b73-9ff1437463e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 98
        },
        {
            "id": "65e58770-4836-4067-bfc5-f04566381d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 99
        },
        {
            "id": "ff27efb9-1286-4097-af6a-a03cc0db06b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 100
        },
        {
            "id": "ce4b2512-0c69-4580-acb7-33696fde9bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 101
        },
        {
            "id": "113fd105-bc82-458a-8543-398e23b12d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 102
        },
        {
            "id": "a50c96a4-7a38-4495-9e81-f5102a4c9785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 103
        },
        {
            "id": "84708ae3-0132-495e-b2f1-e56afd2d0e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 104
        },
        {
            "id": "b375034c-949a-4410-b5e3-7c1bf5a17069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 105
        },
        {
            "id": "9ef15cbd-465c-4ccb-82c7-9793b58e4c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 107
        },
        {
            "id": "5f55a1f3-1e5e-4369-8bf1-cdb373a2a1ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 108
        },
        {
            "id": "c7d15c6f-eb9d-44ff-a171-793f8c9241e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 109
        },
        {
            "id": "6389bb37-2414-48b5-9c8d-c885f543328d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 110
        },
        {
            "id": "11c88cf1-840b-4f29-981d-986f9e9cd3ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 111
        },
        {
            "id": "740fe3f2-48ef-498c-ad63-88b2a3d8083e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 112
        },
        {
            "id": "9d88fa6b-5466-4c5e-b6f8-3f41b146bcd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 113
        },
        {
            "id": "e4665b64-4e89-4e22-93e4-62edccc87e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 114
        },
        {
            "id": "4baeb2a4-16c6-4854-8231-bbd91aeaabf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 115
        },
        {
            "id": "c78ed784-60fb-444f-8e27-6aa59fcb0101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 116
        },
        {
            "id": "4de337aa-2ab5-4c7f-871e-d4722f98d27c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 117
        },
        {
            "id": "b418270a-c40a-408c-8c90-1439f8ae23bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 118
        },
        {
            "id": "ce5228c4-8aa3-4d80-8c4d-9f07a45376a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 119
        },
        {
            "id": "287ef8f5-9154-418c-94c7-6a3d1a3de0f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 120
        },
        {
            "id": "7cc82859-fcaa-4939-8cac-771454a80704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 122
        },
        {
            "id": "aebd5df0-77f7-4945-b415-c556a8e64dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 50
        },
        {
            "id": "f5d86540-463f-4c90-ba25-80d6fef075fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 51
        },
        {
            "id": "0f7af472-aa5a-44ae-97eb-0f76907ea742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 52
        },
        {
            "id": "479d1306-766d-49f8-aae9-7dc4582bf13d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 56
        },
        {
            "id": "7808b3f2-3fe0-4444-97d5-df8ea91cc61e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 65
        },
        {
            "id": "e2a8cf20-7f5c-4a2a-a008-f58d7b8da512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 66
        },
        {
            "id": "3143d7b7-8d78-4871-9dd0-a20cd829d625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 68
        },
        {
            "id": "efa32d8b-4d77-4e96-822f-51f9bfa65fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 69
        },
        {
            "id": "8c0289ba-ee03-4655-bf74-53003940c71f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 70
        },
        {
            "id": "09394236-b8c8-4b0c-a510-6ac1edce6d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 72
        },
        {
            "id": "2475a33c-58cb-4bc9-b7cf-a2ef600f4be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 73
        },
        {
            "id": "194d7f77-6db6-4fd7-be83-a71660deda9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 74
        },
        {
            "id": "165a46c2-8c34-42d9-9141-aa3307c95a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 75
        },
        {
            "id": "0ab0f9fd-b846-4dd4-a989-d3d681abd187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 76
        },
        {
            "id": "9fe5915e-a77e-4174-ba64-220b7eb93245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 77
        },
        {
            "id": "8b736362-55e0-4d53-bccc-277906ee7b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 78
        },
        {
            "id": "4ae7e422-ef7a-4eaa-bd00-02f866b37e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 80
        },
        {
            "id": "5575ad1f-a21d-4f0f-a6d4-c1db4323fab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 82
        },
        {
            "id": "3bbbf5e3-682f-498f-95d1-7647b9a4a812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 83
        },
        {
            "id": "346cb4e1-473c-43ec-84b9-933674181622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 85
        },
        {
            "id": "a47d4ebd-47b2-46ba-ba8f-d925dc3e648b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 97
        },
        {
            "id": "e0c801ac-b088-4d05-a653-cd4fc7cc7166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 98
        },
        {
            "id": "c78754c8-a041-4542-a23b-a0e3899b28c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 100
        },
        {
            "id": "51d365e8-a284-4bed-a13d-224f3f73694b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 101
        },
        {
            "id": "e83eb4a7-24fa-4ca3-b8b1-367e9a69c5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 102
        },
        {
            "id": "d5de72b9-9c87-4b19-a277-4d8306b2b03f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 104
        },
        {
            "id": "b372e2f8-1b00-4646-ae75-726ac31b2483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 105
        },
        {
            "id": "10167e7e-142b-4408-be7e-659755ce4c83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 106
        },
        {
            "id": "eb7f3762-183d-42e7-ae8e-4a054a19851b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 107
        },
        {
            "id": "2e90c977-3a75-419f-be4c-aa309f6a04cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 108
        },
        {
            "id": "60600c95-0a20-4496-ab59-8c81402ca977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 109
        },
        {
            "id": "53dc6a5e-535d-4dac-a569-61d8f17f3f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 110
        },
        {
            "id": "061c4a34-bb0a-4a12-bb54-def86be6c104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 112
        },
        {
            "id": "0a5b38b1-70f5-4cd2-bc35-a837b773ae0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 114
        },
        {
            "id": "7de912f6-640f-429e-a4f6-fdcdde6c932d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 115
        },
        {
            "id": "7463e59b-db69-4725-8a0c-b6c9e3302348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 117
        },
        {
            "id": "73b54cb9-2864-4432-ac9e-a7617ef83eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 50
        },
        {
            "id": "ee1243fc-2b48-47e6-b4c4-a711d4301dfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 51
        },
        {
            "id": "74d8973e-3e38-4217-b02b-2a190394518e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 52
        },
        {
            "id": "2f04f5ec-73ea-42dd-9cb5-9ab371219c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 65
        },
        {
            "id": "3e563a59-4d1a-4ccd-ba88-498db3726d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 66
        },
        {
            "id": "37244b04-9a2f-41e3-973b-0d3d0b29d968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 68
        },
        {
            "id": "938b3fe2-77b4-40b4-966d-15c0403f4194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 69
        },
        {
            "id": "ea00e814-6169-4dbc-b3f0-ed5451de33d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 70
        },
        {
            "id": "4628ca74-1fb7-4eb6-9fa9-c1ea326b5873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 72
        },
        {
            "id": "4d3ac110-c64c-45ee-81c3-e74f35db52ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 73
        },
        {
            "id": "d48f0a75-9ae6-447c-b94a-b1fd390b4a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 74
        },
        {
            "id": "79590b9c-6c11-41bc-88bd-2bb9ca9084c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 75
        },
        {
            "id": "06d68a8d-c6d0-4c59-a86e-a420afd4a2f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 76
        },
        {
            "id": "88f7e102-3601-4629-acb0-eaf4b3816393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 77
        },
        {
            "id": "7124d3c9-e3e6-454d-b6c5-6e4b6cb2ce39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 78
        },
        {
            "id": "70a23aef-938b-4510-b3d0-c55b1bf67487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 80
        },
        {
            "id": "bf5ae432-de3b-4c9f-90f0-3352caaf0f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 82
        },
        {
            "id": "15bc7301-bb3d-40a9-9cd2-8a0a17f82628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 83
        },
        {
            "id": "e32c5baf-1299-48f7-9c49-50991e1ac72a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 85
        },
        {
            "id": "9efcfcbb-533f-4e81-85dd-c389513e7600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 97
        },
        {
            "id": "7cafe1e8-bead-4591-9255-47281ae3c8df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 98
        },
        {
            "id": "0f5b272f-94dd-40d6-84ab-7ab2e7929efc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 100
        },
        {
            "id": "9911f0cf-5252-4b62-9998-31276448a1c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 101
        },
        {
            "id": "b507d5d8-5f82-41e1-b448-f6144a68f0b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 102
        },
        {
            "id": "bd3844a8-e2ed-45af-8c65-6e6c7de1cc30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 104
        },
        {
            "id": "779a6bc3-9a01-4dff-8046-2075a7046b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 105
        },
        {
            "id": "0d26fcbc-7cf2-4761-a66d-f2e9f808e0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 106
        },
        {
            "id": "411d5d55-c93a-4697-97ef-e3fdc2d978ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 107
        },
        {
            "id": "d825bf77-a573-4143-8fa8-74c57d6037b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 108
        },
        {
            "id": "d416cf90-83bd-4838-a22c-82742a0fafa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 109
        },
        {
            "id": "6f63757a-20e3-4095-af35-1afedbbe2ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 110
        },
        {
            "id": "7f56ee2f-c55c-4960-9945-45fb368a8c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 112
        },
        {
            "id": "cabd9511-2968-4d83-8269-77d7f4b50630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 114
        },
        {
            "id": "c1b94cb8-e785-4f3f-84ae-03ed338e6239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 115
        },
        {
            "id": "34d4dc65-6b7c-4f04-b15f-1fc82eb98175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 117
        },
        {
            "id": "1e499a7c-a08b-49cf-b02f-440b26534319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 48
        },
        {
            "id": "7f2ea813-cf3e-4fcd-b5fd-b0df7cc3387f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 49
        },
        {
            "id": "f69c3706-b3c0-48aa-a2ef-c0607678a2b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 51
        },
        {
            "id": "83d64c52-aa58-4742-acc1-d5285bc057f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 52
        },
        {
            "id": "2da56be2-0e54-4f07-9513-6011664907c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 54
        },
        {
            "id": "7f7fd1bb-d815-4718-929e-116f9e5ace52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 56
        },
        {
            "id": "92887c65-0d4f-4549-95dd-8f5264d5282c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 57
        },
        {
            "id": "209801c4-2d7d-414b-b462-4d23f3c107b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 66
        },
        {
            "id": "c94631bd-25b5-4b4e-93d2-f69808531c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 67
        },
        {
            "id": "6eb23aa4-c58c-4d88-b969-855ecc27654f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 68
        },
        {
            "id": "2639ce74-183d-46bc-b684-cc823320996a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 69
        },
        {
            "id": "7c1fec3f-4873-4f15-9375-c4a2663b63fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 70
        },
        {
            "id": "8b6e8e95-f8b6-4c57-af3f-d7dc490143ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 71
        },
        {
            "id": "39a0a72e-481b-4afc-9ccc-f9c7effad3fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 72
        },
        {
            "id": "8adc21c4-8244-472f-974e-6ffa7d4e9194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 73
        },
        {
            "id": "7ad231e1-6afe-4ba4-8da2-9f26f8006e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 75
        },
        {
            "id": "a4209274-abc0-46eb-bbbf-779bcc14c4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 76
        },
        {
            "id": "3676f4ea-1d96-4400-9463-7b9df669233d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 77
        },
        {
            "id": "b96db0f4-a3c1-4989-8e2d-a4fec2dcd32a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 78
        },
        {
            "id": "d896c6fe-15d1-4dcd-8db3-229ab3c2732d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 79
        },
        {
            "id": "05e31f5e-cd1f-497e-b3a2-c13e1974f95b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 80
        },
        {
            "id": "d6d1bc73-3beb-4a9a-9bae-6597954fd5ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 81
        },
        {
            "id": "1698304e-3c1c-4fbd-8a3e-ec43de43a3e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 82
        },
        {
            "id": "4edcb9b9-2c6e-463f-b22a-6c4172c13b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 83
        },
        {
            "id": "efd373d7-f9c3-4f7b-82c1-91de52f4b0f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 85
        },
        {
            "id": "5c94e6b1-63fb-4de1-8749-add561e0a9b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 98
        },
        {
            "id": "e7d03dda-197e-414c-88be-77212f9c2f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "37f88b19-8541-4d2f-9f05-feab78999c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 100
        },
        {
            "id": "7b8442e5-3b9c-4828-b564-a000154d8547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 101
        },
        {
            "id": "a84a53c5-ae67-466f-adc4-3c4fdeb36532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 102
        },
        {
            "id": "6cc84334-1471-47d0-ad7f-0185d0cd7900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "144ebf8e-7b06-446f-afd1-07a9aa027024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 104
        },
        {
            "id": "7996455f-8b7f-4364-97a7-66eb24cb8f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 105
        },
        {
            "id": "8c9f92ee-21c0-4eb0-8d82-cb5555d94638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 107
        },
        {
            "id": "6b9be01b-6636-434f-873d-ed667bfeaf72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 108
        },
        {
            "id": "4caf137c-8dc1-4e6d-beaa-0fe11df0329f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 109
        },
        {
            "id": "87cfc4f7-a59d-4547-b8a9-25a81e7b3c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 110
        },
        {
            "id": "e5ed5839-63fe-424f-bf2f-cfbd17c2e48b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "95be8c0e-a5fd-4db9-98a7-856857240357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 112
        },
        {
            "id": "8ac79e8f-b59f-4dfd-9f88-dbf58ff68cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "e4dca74c-b671-42fe-8648-c4fdc11d2508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 114
        },
        {
            "id": "f1106bfe-5ab6-4a10-a74c-a4f137a7c58b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 115
        },
        {
            "id": "84e86cd6-74ba-4e82-8a2a-d0540982b69b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 117
        },
        {
            "id": "a26c74e7-0bc3-4943-9d83-8e6426b4833f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 48
        },
        {
            "id": "129fe596-2ee5-4263-a1df-a91406df7c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 49
        },
        {
            "id": "a1a07a6c-e842-481c-b844-13b1325a1916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 50
        },
        {
            "id": "70a60e86-08db-46bc-a8e7-8b05edec3ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 51
        },
        {
            "id": "c5c740c9-168f-4acf-bf7b-6992a41cebb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 52
        },
        {
            "id": "02665f68-04b8-4475-9071-b7cc0e10a684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 54
        },
        {
            "id": "de097b65-d1a6-444d-85bc-10dac1c77f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 56
        },
        {
            "id": "ea72adf4-4ea0-4506-8cb3-83f9ff710e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 57
        },
        {
            "id": "3213109a-b87c-4df4-af98-40bdab72c6b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 65
        },
        {
            "id": "4ac1c31f-3bf4-4d76-83e1-b8b26aa84839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 66
        },
        {
            "id": "73604663-981e-41d5-8b80-69185708f7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 67
        },
        {
            "id": "7944d51e-40b7-42fc-8db0-e959289218c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 68
        },
        {
            "id": "220a4b6f-6e76-4860-9253-ec89de6bbab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 69
        },
        {
            "id": "18f82d94-9f39-4bd3-9425-6f45c4bed7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 70
        },
        {
            "id": "9e4afe4f-40c9-4476-ab46-68ceff161cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 71
        },
        {
            "id": "5e8c6855-aebc-455d-a502-4c6831986095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 72
        },
        {
            "id": "d0a045ec-5b4d-4cdd-8210-8cb761e61202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 73
        },
        {
            "id": "1e6cd4f7-8ee5-4089-a16c-f4bfe2deb953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 74
        },
        {
            "id": "4d8d725b-d63e-4143-9d49-f1a5b5e7a4e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 75
        },
        {
            "id": "78a651a5-587b-45db-8838-7142e90586a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 76
        },
        {
            "id": "8b254163-3a3d-438d-b018-cf0980aa0951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 77
        },
        {
            "id": "ac3113bd-2ab0-4881-a618-f4c7e780da12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 78
        },
        {
            "id": "2ae576da-8593-4ba9-96b4-64cf9d949625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 79
        },
        {
            "id": "58510a24-2365-4aba-867b-ced8ed419452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 80
        },
        {
            "id": "d82fadd6-75af-4c89-976f-b1ca09ff934a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 81
        },
        {
            "id": "be8e92f7-7c21-4359-aaaf-00023a092e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 82
        },
        {
            "id": "5ccfa07f-8698-4826-9ed8-f25bf5e7b17d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 83
        },
        {
            "id": "2f2a50f8-3488-43e8-9d30-b17c68d4847a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 97
        },
        {
            "id": "ebe8f095-8a7f-4e8b-9a62-31de7dbc5688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 98
        },
        {
            "id": "3d0adaf9-ca10-4b50-aa81-85df230b9069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 99
        },
        {
            "id": "b9dd6f04-03e0-4661-92fd-d2eeef940e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 100
        },
        {
            "id": "7b45cef6-5ff2-4396-8717-5c954d298320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 101
        },
        {
            "id": "2c601475-b1c4-479c-a990-fcc3b77eedc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 102
        },
        {
            "id": "b81823d8-bf67-4240-aee4-0287f6d7f1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 103
        },
        {
            "id": "55a94d14-b0c9-48a3-82f5-ecc5a87396ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 104
        },
        {
            "id": "616c6bc5-9ee0-410e-88e7-39b519bf2edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 105
        },
        {
            "id": "ecd4cd16-70a6-4b8c-96f2-4919a96511ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 121,
            "second": 106
        },
        {
            "id": "49acc6d0-5ef9-4468-8521-eb7f715ed328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 107
        },
        {
            "id": "565f375d-7cd6-4af5-b8ef-a32af271b90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 108
        },
        {
            "id": "e4cdd425-6269-4243-9b06-b1bce1f0f1c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 109
        },
        {
            "id": "13b497d6-b17b-4d8a-a9f0-3ba0d6ac30e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 110
        },
        {
            "id": "bfbafe92-c4bb-4d2b-b9e5-c499746167c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 111
        },
        {
            "id": "88f451ca-4ff0-4060-a472-313d5af36309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 112
        },
        {
            "id": "7fcee392-7eb9-4d34-bddd-f828d274660c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 113
        },
        {
            "id": "1adf36c9-bb1b-4465-93bd-a7359ce1e2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 114
        },
        {
            "id": "9894e67b-b852-48b1-a204-fe9a29be37da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 115
        },
        {
            "id": "d0bb865a-ee19-4bf9-9191-882fa0a7f879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 122,
            "second": 52
        },
        {
            "id": "8c4c18b3-24ec-48d1-bd55-5fc718fb9743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 66
        },
        {
            "id": "c1613984-acd8-47c1-9b0b-2dd6d1e8f32c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 68
        },
        {
            "id": "9f6f9bf3-ed53-4619-bd3c-8f7cd7e0cf19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 69
        },
        {
            "id": "70dc2729-d225-46f2-91a5-12530fcb2f0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 70
        },
        {
            "id": "e00515cc-08ee-46a1-97de-9cfb1356b8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 72
        },
        {
            "id": "24aa032b-4590-45dd-bfc8-449410dfd8e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 73
        },
        {
            "id": "601708ee-2cfd-44c3-83ab-5faf399da028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 75
        },
        {
            "id": "5bfac94a-12c8-4adf-9a4c-fe432d8a8fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 76
        },
        {
            "id": "7b4229ad-e689-45c4-88ec-1bc6eb2ced12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 77
        },
        {
            "id": "78125a53-b058-4e7d-b145-ce08548ebd83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 78
        },
        {
            "id": "d5e07052-e78b-4450-be46-aaf99bb9750b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 80
        },
        {
            "id": "7d5f88bb-1bef-4d9b-9323-bfb2a38b354b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 82
        },
        {
            "id": "62729ef7-47fa-4014-9ad4-77741c54141c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 85
        },
        {
            "id": "85a18645-cfd3-468c-9304-e58fdec1d99a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 98
        },
        {
            "id": "4241ac1e-9dfb-4bc3-bb74-5478bc9ca48f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 100
        },
        {
            "id": "b125f44f-e7d3-4fa1-b63b-a267e58c42bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 101
        },
        {
            "id": "c251f2b0-ebbf-4e3e-89e0-82955cc43f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 102
        },
        {
            "id": "c2eedf2c-218f-4451-b463-318ed7c211fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 104
        },
        {
            "id": "2e5253c4-b438-4631-a520-7b2aa2780960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 105
        },
        {
            "id": "d060f625-debe-4073-af25-46048d07de30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 107
        },
        {
            "id": "b519a24d-4087-4120-bd00-25f8bfaf6c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 108
        },
        {
            "id": "394f0057-95ac-49d6-8fd1-3c7792ef20a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 109
        },
        {
            "id": "49abe763-3c47-4a14-a5ef-99c5093a7a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 110
        },
        {
            "id": "363c510f-7c00-4f9e-a147-0efde275543d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 112
        },
        {
            "id": "bbb0c866-6321-4305-90c4-5ad507a0ec84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 114
        },
        {
            "id": "2d57f2c0-3f61-417a-b78d-db4c8552966d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 117
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 25,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}