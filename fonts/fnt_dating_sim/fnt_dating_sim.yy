{
    "id": "ba49878d-4650-4e88-b623-479a6b571e0c",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_dating_sim",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Times New Roman",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "75796295-c789-4947-b870-b9548cee5e54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 66,
                "y": 119
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "153d9409-d18f-4721-8443-a11bb28858c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 99,
                "y": 119
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "dbe68faa-e91a-4872-b0c9-926fcd03937a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 35,
                "y": 119
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2094446f-b2cb-4434-962c-f8c0555992f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 473,
                "y": 41
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a0ce9277-7fb0-43d5-89b8-46e825c26f1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 185,
                "y": 80
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1913469b-9442-416f-b7ad-3e88b06504fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "12e19e14-2f49-411f-9cd8-4452f4780063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 300,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b42d9fd3-aa8f-4754-92bf-d987cb09c583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 113,
                "y": 119
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6571f0e8-d72a-4c78-865f-45615cc98082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7b16c3ce-0a16-434f-93b0-1efd93d4e388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 405,
                "y": 80
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5a1cc5cc-d333-45c4-b94a-d87047cf1966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 353,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0fe78647-f3a5-4421-ae6d-6ee8d866d53c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 153,
                "y": 41
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "71123a23-f457-43d1-95ab-575ed31866b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 84,
                "y": 119
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "be06710c-f8d8-40dc-9eca-512c3ea027dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 119
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8dbc999c-d3ae-4276-b6ae-bb208e3c8c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 119,
                "y": 119
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9dcb3493-c1b4-4d87-8023-34717481fe0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 119
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "36da66db-19b0-4400-afbc-097789102b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 217,
                "y": 80
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "96f84da8-36ad-4e3c-9ca2-a7f7b3c90235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 417,
                "y": 80
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "89a0cda1-6ea4-471d-95b2-df80128faddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 20,
                "y": 80
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4266eb11-bbf9-4cd2-9209-3e428093f4a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 324,
                "y": 80
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8ddcb147-3945-421b-9dd8-3b9601ff03c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 54,
                "y": 80
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "85d481db-c3b0-4ce2-8fd0-488b4345ac8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 309,
                "y": 80
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "161416f8-bf69-427c-9de6-4b75420ae475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 201,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f3df0809-a639-495d-91a3-bae9beb8e268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 153,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9a0ade05-67a8-48ed-b009-af0b6b7d25bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 137,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8d9e4ff3-d501-4e04-854d-c0696ba45f5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 121,
                "y": 80
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a3c6e20b-bdf6-4975-8120-f1e2758dbfab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 92,
                "y": 119
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "313bb012-f587-45e0-9da3-da3fabb12a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 106,
                "y": 119
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "826499ca-ff70-4e8b-92eb-65ca2042a1b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 193,
                "y": 41
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f45f761f-b9a7-450d-be27-68540dd90eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 213,
                "y": 41
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2d5c1a3b-9952-4c99-9c77-c3065c49f778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 233,
                "y": 41
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "70151b3d-86d7-4ad7-997f-9bc624a90200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 339,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "477180ca-bc7e-47e4-9a32-3a2d2d4e87b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0b4d348d-7017-4d40-8d9a-0dbc4cac9e96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 375,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "36d802a7-fc00-4121-943c-4220e848d1c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 47,
                "y": 41
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9559809c-c4ad-478c-a94b-d20f6ac507c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 25,
                "y": 41
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9e7aea9f-ac4f-4a9a-a6af-fdba7819dd59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1611aafb-3f56-4e92-80a0-e96b107bacb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 132,
                "y": 41
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "aba2a3e0-072d-4ddb-8579-1649fda091b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 253,
                "y": 41
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c705cb47-95d4-4dce-b6c2-18578ccbc56a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 400,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bb8e69ad-ddb2-47e4-ab80-798bd4956e1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 275,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f84383f6-8a28-4531-8cbc-2171f48068d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 393,
                "y": 80
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "de28ce7f-c9d4-4435-a648-7cbb4b4d08fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 249,
                "y": 80
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ffa3a4bc-9147-486c-bb9b-96cb6e92405c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 24,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ca0c74cd-d330-4f8d-981f-6e0eb114b860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 90,
                "y": 41
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1cbc4987-295b-44ab-860a-feb566b05be5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f6bad13e-c713-49e0-b67c-c6dcf5421b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": -1,
                "shift": 23,
                "w": 24,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "985c4b3a-9f31-420f-b653-6d39c38c0379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2a8abda3-62ed-48ba-a677-dd4f1a8d0e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 291,
                "y": 41
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cb007f55-5991-4d6d-86fb-bbcbc2330e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 472,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e7288d28-b0aa-41c4-a8e1-52b3660ddebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "13435fcc-7dcd-44b1-8b6c-3506b650fcac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 37,
                "y": 80
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cb668c20-f5b0-4bc0-b0c7-b37caacdb5e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 69,
                "y": 41
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "35fbf157-49d1-4524-b230-0c46e77a4671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8c365948-9556-45e0-9902-53b7558a264e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1e310fd4-c0fd-475f-8ab8-5dc911d2ba56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7fccc0f2-e799-4313-8681-57ef9492c7c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 350,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "311bb083-b92e-4b65-82a2-234a9c8bcf9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 250,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "50523559-a1db-4ae0-9902-3d0c8ae44396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 111,
                "y": 41
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "94b03f44-866c-4db9-8d29-e998dd5c2086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 56,
                "y": 119
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b0ae4a20-f6ae-4042-9bf7-f1f21b4aded5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 476,
                "y": 80
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7e2c91de-d0f1-46c0-a172-6a14d93aabe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 46,
                "y": 119
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "420a1d46-b506-4d23-820a-e19e824c28b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 71,
                "y": 80
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a54567f3-1b8c-4c89-b993-ef97189d37ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 173,
                "y": 41
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c26008e4-c2f9-476c-807f-90c29e27cb2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 6,
                "x": 76,
                "y": 119
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "89ac991e-770b-4105-ab14-af1f9969c366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 169,
                "y": 80
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d3ad58e5-37aa-47b4-873c-b2de40ba2ef4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": -1,
                "shift": 16,
                "w": 16,
                "x": 401,
                "y": 41
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5cf52e60-711b-4138-820d-ca939094e510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 294,
                "y": 80
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4c9589bc-5a2c-48fb-981f-8873f92da7d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 383,
                "y": 41
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "85c8d4d5-d8a6-46bc-8f5e-f15e02d0c47e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 279,
                "y": 80
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e9db2f3b-45ee-497c-aca0-388ed766311d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 13,
                "x": 264,
                "y": 80
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d4194e2d-1172-43c2-a1fc-beaa4b570522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 365,
                "y": 41
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ef76faaa-76c5-4f03-8b80-8fdf4dc4acd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 347,
                "y": 41
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "54a10a0c-563b-4e1c-95ff-05744b2e522d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 498,
                "y": 80
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "542493fb-633b-4299-88c5-170d06ad486a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": -3,
                "shift": 9,
                "w": 10,
                "x": 453,
                "y": 80
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9e078264-d0d9-416b-b3e7-97da7fdd159e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 272,
                "y": 41
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "39055a86-09b2-4321-8a9b-dd3f86987eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 487,
                "y": 80
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "1bdcc4fc-d302-432d-8708-0e987c97d0bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c15fd04b-1ca1-42da-abc3-d81d8fc62567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "39e6a6de-0689-4158-bb93-7b38284f9926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 105,
                "y": 80
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5ad4b4f3-9380-4349-a85d-f54e716687d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": -1,
                "shift": 16,
                "w": 16,
                "x": 419,
                "y": 41
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "020472dd-e942-4140-9870-7d69b2eee42f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 88,
                "y": 80
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "17677a39-389f-40c1-950c-8ec4a7a65bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 367,
                "y": 80
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8d885f1b-6728-4797-83ba-c0fbd30db3ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 380,
                "y": 80
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c954ccd5-c387-4097-8c91-dcf52eb79110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 465,
                "y": 80
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "041679e7-a7af-409f-bd67-420e6a35336d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 437,
                "y": 41
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7dddab75-5a92-4520-a1e6-ecabf5ce075d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 329,
                "y": 41
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b121faa3-12df-4947-95ab-21bc6613de26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 325,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "066428dd-ca50-456f-83a3-889211ca96a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 455,
                "y": 41
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "28de7ae7-07d3-49ef-a0fb-cdf83f4001db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 491,
                "y": 41
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "48b80c11-9d9e-4761-91f8-c48c845f80f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 233,
                "y": 80
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a524bdc2-527e-417c-8b8e-5c5104837517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 4,
                "shift": 15,
                "w": 10,
                "x": 429,
                "y": 80
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ee508263-00ad-4745-95ee-8186d33b3b97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 125,
                "y": 119
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a54d3d12-789c-49f3-ab8c-32913e67927d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 441,
                "y": 80
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "222ce090-7b00-4080-8072-c4c7562a8967",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 310,
                "y": 41
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "70f6113a-1fa1-49a0-8733-984096d8b3a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "eaafccf4-b053-4811-90ea-c7755c1e5f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "08553b77-2b13-4413-8185-9f412f48cd45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 86
        },
        {
            "id": "5bf29e7e-6862-4a59-802e-95d26f4baaf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 87
        },
        {
            "id": "972598bb-2a05-4403-9038-b882614a9fd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "f1542fef-e9d7-47e5-b354-f3eb6996fcf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "e343be2a-9068-42cf-81dd-1678ad3075c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "afe823a4-fbf1-4e99-b43c-0d76ab21c82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "ece48a4d-66a2-4ae6-9cd8-7d12a92963cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "2aa8434e-f9fc-4505-a36c-ffb4cae9c0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "b0b989de-b0f9-4126-8436-8b379f78306b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "1c830896-3669-4677-b095-b12587631ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "979c103f-5d66-4b11-99f1-d53985c8810a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "1c0f0b25-7e26-4c92-827a-cbfd8693b2ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "45b8b3e2-d2eb-442d-8116-ed707d04c5ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "e0bf43c4-91bc-4e6e-901d-c4be0da3bb0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "e0842147-9d61-457e-aa24-c2e9f3b9eae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 87
        },
        {
            "id": "62b8dd11-f07e-4425-a91d-8ec542925abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "a75aeedf-3a7e-48ba-9e4c-362cddcc1cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "a17a3265-205d-4d14-928a-5bae56c92c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 119
        },
        {
            "id": "bf6b4b8b-4033-4748-98de-94897eff51bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 121
        },
        {
            "id": "30963150-31e4-475d-b9ac-5fbb715c6848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "215c232a-a540-4e16-a049-8dc3e30b857b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "f0312134-e660-4fb0-9256-054892b24a6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "5dc3cf79-2942-4a02-b073-8375bcbb6825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "e9b6b291-65ee-4548-881b-336125413628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "e0b3d5d2-1f6e-45df-b24f-2726097f544e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "348e44e2-ba87-4bef-8a81-9d80b3dbc41a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "e49ea17f-0315-4118-8ea7-cd514a55c9bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "f982c99c-528d-4154-9563-83f4e58c926e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "675dde62-b092-4d91-a629-7453d475fb0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "3a03b391-44fd-4946-ac6d-4922414eb328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "3ee87c76-14b6-436a-9ddb-43eaafa640ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "bfd25ab7-fd1b-46b9-a06d-b47f0d0f3163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "025028f2-6f2b-4a1f-9753-5b7698184c20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "e9e2ed08-be2a-482e-81e1-8878f2d4e8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "fb3fd3bf-f0ca-4e93-a7a6-7361f223a262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "2cda29a6-6634-4f10-8bf4-1837f7565a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "76f5e805-5c8c-4c2b-828e-79de17447f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "8fb7ae63-7937-403d-b26e-636e0edaafb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "b9e9f6e4-2928-43db-9dfb-59b5bf77f7f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 86
        },
        {
            "id": "173ba3e3-6289-4b4c-b93b-4ba3becbe510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 87
        },
        {
            "id": "1d9c7413-f7d8-47f0-8c93-952d2a520fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "96054dd5-4168-4320-913c-cd061b5a6ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 121
        },
        {
            "id": "4b19c366-b298-41a2-880a-945ef1ce8f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "91124b83-9662-41b3-800b-7599b1aa2005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "4af81a66-6134-45cc-b0d7-476587a320ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "596f7675-d195-4f06-a468-644a81c9a6be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "4ceec48c-754b-449a-85d6-acca7a01c23b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "ccfe906d-f447-435c-adb4-5c621631a656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "f138e91c-6ae9-400e-b462-47326ef68b91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "95548b9e-7a3d-4456-99f7-a9889390c2df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "cfb83aae-631e-4383-9daf-1ada5e2c64d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "f5b709a6-610a-4389-833b-8891a6e17226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "1b800e93-f9f6-47bb-8e2e-8c4a19012c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "a06edc1c-5a35-44cf-b333-8e50632017e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "739aa63b-0d71-40bf-ac29-45b9d42553be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "fb588cf5-a8ce-4b8d-88a4-75d98adf0522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "da4946ad-df41-436e-9aef-455626c945e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "eb59a14d-93ad-436d-ae71-728bb2befb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "97db7403-2630-422a-9c88-5d2915215c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "d1288690-a7bb-42e3-bac0-04c1700f826b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "fb5d615e-67c3-40bf-a800-03a2fb48d1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "f9d68002-e285-407a-b9d3-bc3a2a07e30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "281f9143-ed05-4996-bac0-ad336dc709a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "06d1563e-1247-45df-bfa4-1ced46dab842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 32
        },
        {
            "id": "8e25e8c0-4c9c-4401-8bc9-849264cad259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 44
        },
        {
            "id": "919383d0-09db-4376-982c-0010990a5721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "1f28f784-e31a-4728-bbdf-617524356a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 46
        },
        {
            "id": "5385ca4f-9fc5-4f93-a001-08ec4c350240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "b49e77ef-748f-4d32-8e19-631dad68d669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "13e1bbea-b061-44eb-9a68-a6724a019015",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "12cc5112-5c8f-4ac0-9741-a1a5389cdf94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 97
        },
        {
            "id": "eb3355fe-c488-4872-956b-d631c3daf573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 101
        },
        {
            "id": "773f8765-def9-4637-927a-ab4860565ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 105
        },
        {
            "id": "3516dbed-8cee-4bf5-bc38-7b4e85bd41a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 111
        },
        {
            "id": "53ef8092-3b3f-40e2-9c28-b09483966e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "be691e0c-33aa-446c-8330-bafddca71fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "45411441-3fa9-4430-aa59-5908344803f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 121
        },
        {
            "id": "11733d71-14e0-4a20-9768-778a28c87738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 160
        },
        {
            "id": "6238f3a1-4990-4e9d-b464-13484cdef05d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 173
        },
        {
            "id": "6e2cc3eb-94f2-4b18-9d7c-e647ff9408e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "92786577-9117-4b57-9060-d27e7d9575e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 32
        },
        {
            "id": "3c937201-132d-441e-b999-7d4fb3cc5105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "c79085f1-3965-4271-8359-76dec92bf2b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 45
        },
        {
            "id": "dfaeae11-a03d-4b1c-a7dc-3a8b6f747a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "16388934-716d-4ee5-bedc-e925a2bc8bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "ce4847e7-4fa7-4935-9978-5b22f100ea78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "5bd0bb42-b42a-49e3-8235-2e128ed20be2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 65
        },
        {
            "id": "81f4f2b7-cf18-4139-a3ba-694eb1ddfa18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 97
        },
        {
            "id": "8b85a561-9e6d-4ee7-83e3-e8726adc7668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 101
        },
        {
            "id": "c2293df5-e9fc-4251-8731-16aa24921853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 105
        },
        {
            "id": "f61576f0-d13f-41e6-99fb-6a2fe82c6ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 111
        },
        {
            "id": "fe5511a4-4c75-47b1-b08b-ba57c62b5f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "e880a465-0619-4b22-acad-d5e59d6562c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "39032350-4c8a-4b6b-9854-d833853383ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 121
        },
        {
            "id": "9f1f7806-5b83-44d4-bace-760de01aa237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 160
        },
        {
            "id": "8073c796-ed70-4375-b1d8-19b000279a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 173
        },
        {
            "id": "686a903a-7901-473e-9c5b-8e4ceae6d650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "416a4390-d529-4ac2-8ee8-542bcabe1afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "7c11faa1-2930-4e17-9ed6-8c06ac8edb4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "ff670d3a-be98-4870-8c61-116f98894faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "f46a3fd5-27a0-421e-8a89-3384fa8ab4cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "ebeeb2c6-8ef4-4e22-946c-31d4d03afbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "952eea0c-f12a-455c-85d8-fdb688469391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "3c94ce4e-6edf-481c-a718-57b62815cd5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "2ef4ca15-d07b-407a-9be2-5d3c94f942c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "7bd81f24-356b-48d9-bc53-34ae65b6b7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "fc9dc45d-51c6-47e6-8a2a-9b75aca4762f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "21de8521-8fc4-44f5-91e8-535edc0a6d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "31176f39-aa77-4d97-9e95-80e7a60fa6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 112
        },
        {
            "id": "840ade41-440f-41e3-85d2-f0ae4516854e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "833770b0-24ab-4879-9896-cb01e670ca8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 117
        },
        {
            "id": "23a3113f-cb08-4dd9-ac6f-3a2441ce2e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 118
        },
        {
            "id": "72eff615-1939-4168-914f-1373f63e493c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "b89391b6-6885-46fa-b0dd-33052fc1e8a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 173
        },
        {
            "id": "2df3d191-7a11-411a-9245-aab1327c20b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 894
        },
        {
            "id": "9a5dbacd-c1f1-49ad-92a8-3dae3d653f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "4e534bd9-05db-411c-9397-94f34f02db54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8217
        },
        {
            "id": "50402fa7-99f3-4536-a08a-0ff43f4eca51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "56bb3d58-76ae-4aa5-89f5-7b86e483865b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 45
        },
        {
            "id": "1386a26a-c7f4-4880-a4c3-3a6a7e7d7789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "39e3bed5-2f3a-4c4d-9037-c5c6b9b6138d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "33fdfabd-41ce-437f-a122-4948f25daa4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 173
        },
        {
            "id": "28e15ef9-d96f-404c-a466-e70069d2a8ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "db036e45-b9d4-4e24-914d-d6ff8f6e5dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "5cd2266d-8a40-486a-a3d6-a1e37cbf2faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "4b898f82-b1bc-4f34-9402-c1c590c515ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "f530ee9f-bcb4-4181-89d8-501920c2daff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "3feb4727-2b62-451a-8ded-2f37efb18bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "c2b38991-05ed-4332-b1d5-f55e2766bb9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}