{
    "id": "657743e3-1bb2-4126-972e-b90a89d4e457",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_countdown_timer",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Action Comcs Black",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "61c3cca3-86d8-4af4-93cb-c23d2ce26e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 123,
                "y": 212
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8131ea21-bc2d-4a09-8906-f29238bc3543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 5,
                "shift": 12,
                "w": 4,
                "x": 197,
                "y": 212
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8f1e5f50-6146-4005-b737-95d977186653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 93,
                "y": 212
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7a003522-53c0-484d-94d4-2c9074cab653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 166,
                "y": 142
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "de02c4d4-a97a-4a3f-b386-fcedf0fd5e65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 218,
                "y": 177
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5ead0c52-2cb8-4a26-8960-3044fdcbf840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 194,
                "y": 142
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9d05e9eb-38cf-4a8f-a4a9-cc6f9ec7f7b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 208,
                "y": 142
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2dae8db4-1a1e-49f1-b989-1562b688d4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 5,
                "shift": 12,
                "w": 4,
                "x": 191,
                "y": 212
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "63ad330e-a0b5-4f40-b158-89c3f0e8a447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 103,
                "y": 212
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5cec0a18-c7eb-4c73-b078-ad9dcf104f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 113,
                "y": 212
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ba104e98-e81e-492b-96e5-368c09c427b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 212
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ed6a0f18-ce3e-40d6-8579-fe3a4c6ce897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 212
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "278d69dd-cc53-4af0-aa58-8010e2aac29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 203,
                "y": 212
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3a417abb-1ead-42ca-87af-ed917e18dc78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 43,
                "y": 142
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e58ceb04-be61-4036-8c2f-75c7fca6e5c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 61,
                "y": 142
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6e2bdb02-4276-4513-a656-a8fe8a22a2a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 73,
                "y": 212
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "534cdeff-90eb-4499-99ad-8ac76bdc74cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 195,
                "y": 72
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d06fe3b1-9d6d-48a1-bed2-77bc9f1b9d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3f18a977-dd30-421f-8816-44dbcb4debd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 1,
                "shift": 30,
                "w": 30,
                "x": 35,
                "y": 37
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e73ec8ee-ff4a-4be5-8b18-bd96e57b6d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 1,
                "shift": 28,
                "w": 28,
                "x": 67,
                "y": 37
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a76fa10d-b40a-48cc-9def-29705543e579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 86,
                "y": 72
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b007d594-fa97-4b0b-94b1-8cd437ead6fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 1,
                "shift": 25,
                "w": 25,
                "x": 114,
                "y": 72
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fb5e0d97-5fc0-43f4-8601-4151689a7832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 30,
                "y": 72
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f3138230-94f7-43dc-bc63-6fd827369243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 1,
                "shift": 26,
                "w": 26,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "97e1a1f6-55fe-45d7-a6c8-b9eecec6afa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 141,
                "y": 72
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "07774715-8b6d-46cf-b73c-1d78d7c1b918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 185,
                "y": 37
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f3b7472a-dbbb-4364-a1b9-42f57a5300d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 5,
                "shift": 12,
                "w": 4,
                "x": 179,
                "y": 212
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "933b0ab8-6e53-4b21-8d06-3ad75906f061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 185,
                "y": 212
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ff3bf282-fb1a-4e43-821b-3e58b012d778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 212
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2902f72a-9415-432f-9f93-4ddd3eb3a14a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 212
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b84e6e7c-5fbe-4d91-ae2a-2edabf2f9b64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 177
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "aafff53e-ad47-4efa-a84c-47b7082cb8ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 230,
                "y": 177
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "112c2c27-d9e1-45fe-ad97-c2a8a2546563",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 110,
                "y": 142
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "382b52ca-b85c-4960-bf2f-fac85cbf3a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 200,
                "y": 107
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "af5880bc-a215-47cf-bc89-2d3b94993560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 176,
                "y": 107
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e07ebe4b-2588-4d72-b158-24e440c490e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 152,
                "y": 107
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d9d5d084-58fe-4bde-9b07-d5b03d992690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 104,
                "y": 107
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8e227780-267d-47cd-af80-afc4cb8a2b61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 54,
                "y": 107
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "37d481bb-d0c6-4540-817b-985846116676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 224,
                "y": 107
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0e97d054-ce67-4beb-8479-5ec8c6d350a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 23,
                "w": 23,
                "x": 29,
                "y": 107
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5434e1e7-cbda-48c5-b4d3-764a02a8b28b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 79,
                "y": 107
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "40dacde4-1cd9-4fa1-a814-64a29d39ee8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 23,
                "y": 142
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d4462e64-f873-444d-8061-08aff5739082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 58,
                "y": 72
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "21428a27-46fc-4459-adb7-4f267811c683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 1,
                "shift": 31,
                "w": 31,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ca48849f-2bbf-462b-8613-21ce56d5a3b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 128,
                "y": 107
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "814a6c3e-b9c9-4f4c-9c31-b32d77f8fd0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 1,
                "shift": 33,
                "w": 32,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8d84150b-fd6f-49a7-a218-49468843c7cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "5266617e-3dea-423d-8070-8a4075d7c99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 168,
                "y": 72
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b01d24a7-7857-4ee3-af29-5ab06a7fd127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 214,
                "y": 37
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9476c927-6860-40b4-a3e4-255ae91ff7e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 222,
                "y": 72
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7a9400ff-548a-49ce-b731-35a511740ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 127,
                "y": 37
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8bb46c76-b52f-4778-bd06-8bdf0ca7c673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 1,
                "shift": 33,
                "w": 33,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "51986925-63ea-4713-a688-f14898938334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1df89207-302d-4a4b-a056-59599bb53375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 1,
                "shift": 28,
                "w": 28,
                "x": 97,
                "y": 37
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7fb6fc5b-9077-4706-be0e-02555e211033",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 1,
                "shift": 27,
                "w": 27,
                "x": 156,
                "y": 37
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a5b0c182-c3a7-4ee5-9032-16926942167c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 1,
                "shift": 33,
                "w": 33,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7f9edb65-bbcb-4792-b787-a7a6e63188ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 1,
                "shift": 32,
                "w": 32,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "77a11d2a-bddc-4cf5-8fe4-cfb8090b377f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 1,
                "shift": 31,
                "w": 31,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2b4fa776-c016-4f0d-88ca-9fd83c4f9cf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 1,
                "shift": 33,
                "w": 33,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3c1deec8-3d8d-4392-a745-74d051c4e849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 4,
                "shift": 12,
                "w": 6,
                "x": 163,
                "y": 212
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bacac0ca-e37b-44f5-b39c-5e447ee16fa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 153,
                "y": 212
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f8f0762f-d365-49a6-822a-61158fbeebb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 171,
                "y": 212
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5fe56e3f-d3f2-4f20-9d0b-a3345181b88a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 177
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4bd5bf82-975f-4a4c-b0b2-3cb0a432ea3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 14,
                "x": 94,
                "y": 142
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d20f9277-103c-4e2f-8146-f74a21702c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 209,
                "y": 212
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d0a01040-e6f3-4f16-9565-566bf7430f22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 212
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7e4a4cfd-116b-4bd2-bcbb-3c54c363efe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 177
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e08edc4f-28af-4af8-a9e5-8ff0dee340e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 177
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "72470227-ac1d-4ff7-80df-97290a1a165d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 234,
                "y": 142
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "45959443-125c-4fca-a5cb-02240fba8ac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 177
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ab6f2beb-3ddb-4a74-8cc9-6c9839a9da47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 177
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8a264cae-bc18-4036-864d-184e6833f132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 177
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5438e59e-9afd-4cb9-887c-e3fd38b3e998",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 177
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3a2bb0a2-44b8-4b84-bbe3-b60809d75bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 177
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9364cb9e-c5bb-4db2-9133-b9e3973a4e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 133,
                "y": 212
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0c458622-0c29-4b2b-9db2-d198299bb56d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 177
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "481a9630-ce10-467b-b943-06217214c732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 177
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "75e564fe-0d01-4a2e-bb89-0ea4090f1ff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 138,
                "y": 142
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cc11da1b-f38d-4ce3-9c2b-a6f4a58c3ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 134,
                "y": 177
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "aeaddff9-0009-4955-8134-ec9e6327779f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 177
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4ac789e6-a104-4be9-b7c1-216858457097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 177
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3ecb0469-3fa8-4d3d-ae1f-a8f0d4e8835b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 177
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "15971e71-b4e0-49cb-bf92-f96fca2f47b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 3,
                "shift": 12,
                "w": 9,
                "x": 62,
                "y": 212
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "265aabcb-5422-4730-bf8e-d153edd95af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 177
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "20a1003c-6b1f-48a3-bdc8-3dbe294df8b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 177
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5619184b-1c99-4b08-a6b9-057225ef0bd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 177
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "70688ca5-c794-41ca-b7b6-d67a6fd02cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 124,
                "y": 142
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "34c9ae8e-4b43-4e2c-a732-b4f6de6ff1e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 14,
                "x": 78,
                "y": 142
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1a8183d1-ca47-447a-8012-cfcb46dd3cb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 180,
                "y": 142
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a13e35bb-e938-4d57-a0c1-dea64e1f4b08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 152,
                "y": 142
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c8072647-e479-4880-a5fc-bb4f43bb18d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 222,
                "y": 142
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "94ff9ca3-5008-4f5e-8642-a3a4d1ffd88c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 83,
                "y": 212
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ea498d05-c0a6-47e9-ac2f-ce35118c2546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 5,
                "shift": 12,
                "w": 2,
                "x": 215,
                "y": 212
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f8cae633-af1a-4fc2-8af9-f9c0a6eee6c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 143,
                "y": 212
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "83216f6a-3169-433a-9cef-45ddfc3218fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 177
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "3766d172-1b20-4c8f-8ac4-9685350fbcd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 48
        },
        {
            "id": "1c9c5869-d063-47cb-9923-77c01d31c3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 49
        },
        {
            "id": "4c9d4634-218d-43f3-90a6-ffa95d452c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 50
        },
        {
            "id": "1a8c19af-c4ce-4cb4-a9e9-8544d0bab0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 51
        },
        {
            "id": "7ff7e8ff-9134-4c28-8101-cf4e40ddcb22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 52
        },
        {
            "id": "f2566d58-7a3a-41ae-8d67-3d53c5331d63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 53
        },
        {
            "id": "2eafbf5e-53be-434c-9e17-fd343bc89367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 54
        },
        {
            "id": "b00d404a-e024-43fd-8ed0-d8485b99154d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 55
        },
        {
            "id": "8e38072f-a8c7-4d29-9524-e545868c2e7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 56
        },
        {
            "id": "6defd4f3-5c80-4c5a-9ed8-73e66fc57f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 57
        },
        {
            "id": "6d99d122-b7aa-4f77-b399-0ae1643cb751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 65
        },
        {
            "id": "5676fb60-c30f-4218-a0ab-45559c7d0eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 66
        },
        {
            "id": "0321e5e5-a410-4129-b688-2415fa7d225a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 67
        },
        {
            "id": "2bdfc41b-0184-45c6-96c7-471559d65b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 68
        },
        {
            "id": "ad7c71e4-d2ef-4227-939b-317fbe02a028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 69
        },
        {
            "id": "8650ca43-1544-43f5-a083-ad83c94bdb20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 70
        },
        {
            "id": "86b01962-e24a-41e9-b789-a5f66b596a60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 71
        },
        {
            "id": "dd50e235-e01f-4e2b-99f8-6bb1494df359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 72
        },
        {
            "id": "b51876f5-8f6e-4fdd-9f13-1172382e63b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 73
        },
        {
            "id": "7c107a8c-1d0b-4c5c-b15e-57564feb11bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 74
        },
        {
            "id": "ce13497f-584b-4791-8ed7-62454e177fd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 75
        },
        {
            "id": "e819fa3a-a053-4d35-8c1b-4730e86a9609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 76
        },
        {
            "id": "1dc82118-bee4-4d93-b565-a77adff9fe66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 77
        },
        {
            "id": "6ee5db4c-9411-4dc4-af31-84a22c5a9e69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 78
        },
        {
            "id": "e53c7008-4e85-4697-a508-11b1199f8128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 79
        },
        {
            "id": "bc1cf050-ce92-43e8-b81c-ca0ceff72e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 80
        },
        {
            "id": "b0f61aa7-b531-4f15-a8fd-b2dcc76011cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 81
        },
        {
            "id": "c962ed18-a519-493d-a305-177f6c233cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 82
        },
        {
            "id": "2f27a35f-9804-42ef-a968-e81e515beadf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 83
        },
        {
            "id": "f8549285-9fca-4f71-bd17-cf88c79b6a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 84
        },
        {
            "id": "c81951d3-d9e3-46c4-8a55-b594c50e28ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 85
        },
        {
            "id": "b098defb-00af-4d3d-ab07-0182cfcdac6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 86
        },
        {
            "id": "e6a004ab-3c52-4a35-8714-618365edc3e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 87
        },
        {
            "id": "f0473f3c-e25c-40a4-b3cc-061bce04a2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 88
        },
        {
            "id": "0f3bb8a6-f7c0-4a0b-a9e0-9dc719881694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 89
        },
        {
            "id": "ba271642-f630-4d2b-8701-1d80dcf6bcbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 90
        },
        {
            "id": "473bc7f6-e58a-460c-b3c2-168a1c49e189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 48
        },
        {
            "id": "e953bdd6-2eee-47d1-9f70-3d113b2c93d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 49
        },
        {
            "id": "a8fd622f-46ca-4497-84ed-44f5a61ebe71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 50
        },
        {
            "id": "e7d931b3-948d-407d-beda-3480e14dd8a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 51
        },
        {
            "id": "2f56feef-8d1f-4247-a60e-fc4b6e6da595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 52
        },
        {
            "id": "2ee1efd2-254d-4673-af80-3f12fc25e07b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 53
        },
        {
            "id": "7a9bfab8-b7cc-4321-b5c8-d939ef2085dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 54
        },
        {
            "id": "e1666429-5c4d-4900-8280-f2cc285feaa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 55
        },
        {
            "id": "6be25046-b1ad-4459-8119-d141de31d273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 56
        },
        {
            "id": "42883abc-c94a-46a7-b1b2-4eb2c8096834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 57
        },
        {
            "id": "24adf3c3-b0f2-4d25-bff2-171391d1ef6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 65
        },
        {
            "id": "d3fc81d8-dbc1-484f-9c0e-1a597bc2d25c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 66
        },
        {
            "id": "4295ae7d-3d14-43fb-93e5-319fb2476831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 67
        },
        {
            "id": "902edc21-8fa4-40bf-942c-45efbab9bc50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 68
        },
        {
            "id": "e19ce15c-47f9-49c1-b95e-9eb05c9706fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 69
        },
        {
            "id": "608aecbf-5f37-4b48-ab50-6d2ee9533a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 70
        },
        {
            "id": "a623ac5a-bb50-494e-8cdf-8f34b9d4022e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 71
        },
        {
            "id": "730c15d3-f913-43b2-88a9-a7f8d159416a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 72
        },
        {
            "id": "3e97978c-382f-4c70-b415-f4cdd34bbfae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 73
        },
        {
            "id": "4107abfe-c78a-4c15-b6a4-9c88c79a6e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 74
        },
        {
            "id": "53de5ffd-5289-4854-a793-c161a7e16363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 75
        },
        {
            "id": "fc1376cd-8140-4683-a99d-99f4babaaba8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 76
        },
        {
            "id": "863e852f-7ece-4c00-8385-401a49adbec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 77
        },
        {
            "id": "a02e1867-8f71-4cee-b5d7-f3a57a9fe027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 78
        },
        {
            "id": "962de4c6-ce98-4658-894c-375e0216608b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 79
        },
        {
            "id": "a7e3226d-d7b3-47ff-b81b-1e06c7244740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 80
        },
        {
            "id": "d8f3089b-d160-4d6a-a982-5dc777b10430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 81
        },
        {
            "id": "cd90e22e-946a-46eb-b908-0d312e3d62df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 82
        },
        {
            "id": "9cd1835e-4011-4738-904d-48f0ea50761c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 83
        },
        {
            "id": "a702407d-a3c8-4d96-89e4-885c70db8d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 84
        },
        {
            "id": "baf9d4c9-b0df-4165-82fa-703616e7a8cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 85
        },
        {
            "id": "d835fda1-4fcc-422e-8f1d-bbb6d3fb8930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 86
        },
        {
            "id": "a86b5583-50fb-4ba6-87f1-01c7059df17b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 87
        },
        {
            "id": "7f220bca-dafc-419b-878a-81a836ebfa9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 88
        },
        {
            "id": "4ffe908e-4fa5-4b02-92f5-9e7a1c367ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 89
        },
        {
            "id": "8beec4a6-430a-457b-8af6-2ef7e8057a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 49,
            "second": 90
        },
        {
            "id": "3b7f41d1-f109-4d6c-ab31-b244d5b93028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 48
        },
        {
            "id": "c52d2b32-cbc9-46b3-ab15-ea82ea9095c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 49
        },
        {
            "id": "934ddbb1-dc89-4ca2-9a08-d3ede06fecec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 50
        },
        {
            "id": "c7d76977-e2c6-439f-b50e-d9cab00f5db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 51
        },
        {
            "id": "41010236-880b-4802-8a3c-181790c5d676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 52
        },
        {
            "id": "e7588469-8056-4805-aa68-d3f4633725ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 53
        },
        {
            "id": "2dc9b03a-1ade-4732-97db-1abff728117c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 54
        },
        {
            "id": "0b616fcd-fc3f-4708-b925-74b63c73da2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 55
        },
        {
            "id": "7209c76e-041d-4973-b636-d9b1f4a4a181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 56
        },
        {
            "id": "49677ff8-c0a0-4370-b77e-444118145ea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 57
        },
        {
            "id": "ce988421-8f26-4a45-9394-b0332ad3570a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 65
        },
        {
            "id": "e50e3b25-03ed-4621-9573-8d772de923a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 66
        },
        {
            "id": "f056a75c-6f42-4043-8fcb-5686c79d87d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 67
        },
        {
            "id": "6e927802-d806-4ff6-93a3-bef09ae8d9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 68
        },
        {
            "id": "46f5e5a0-5a77-4b03-be54-a3ba13da8084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 69
        },
        {
            "id": "fcacb261-f27d-4562-92e5-76cfbd16d7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 70
        },
        {
            "id": "e07d5696-7919-433e-bfb9-5558bddf5b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 71
        },
        {
            "id": "286086dd-69de-485c-897e-d9442f1b70a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 72
        },
        {
            "id": "c6845a5f-f5f4-42d7-af86-96f57591f01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 73
        },
        {
            "id": "76fb9084-85b0-40ce-b90b-338cca6c174e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 74
        },
        {
            "id": "40087b65-aea4-4bcd-8fb8-4948462d802a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 75
        },
        {
            "id": "4a527881-94c9-416a-85f7-0781b97d982d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 76
        },
        {
            "id": "399247d1-ce0a-474c-ab2d-cc972f8804bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 77
        },
        {
            "id": "e5a4ab33-176a-4def-854e-c64c878773f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 78
        },
        {
            "id": "45713740-73a4-4412-8782-957065589e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 79
        },
        {
            "id": "572507e3-7509-4376-93c2-61b351a25555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 80
        },
        {
            "id": "3dfe1d36-43e4-43a5-9717-82594b2b9d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 81
        },
        {
            "id": "2cd8282a-f2ce-440f-9571-d4ab5aea9432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 82
        },
        {
            "id": "3ebb2a8f-c47f-4bee-bd72-e12305c75779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 83
        },
        {
            "id": "de1fb779-0a1c-4ef2-aec1-937fe03e5b16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 84
        },
        {
            "id": "9beedd1b-7736-4500-9160-addaf6f74fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 85
        },
        {
            "id": "4fc3ac8a-9d47-4631-86b2-1f91a1e0d2de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 86
        },
        {
            "id": "a512d1e3-47e3-4077-9ca0-99eb3e448df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 87
        },
        {
            "id": "283d58e8-6824-4959-aa2f-b499ffa4f0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 88
        },
        {
            "id": "b14b361a-5d67-4774-a0b0-707d6d857b1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 89
        },
        {
            "id": "8a766f04-4708-455c-9a6c-f725e67316b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 90
        },
        {
            "id": "266f772a-0871-4f4e-aa5d-beea9d588f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 48
        },
        {
            "id": "a09c6315-3e01-4050-a6c3-31bcf8eb845a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 49
        },
        {
            "id": "fd42c73e-6a72-4d27-8615-9b611b1d1298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 50
        },
        {
            "id": "a5e62653-1e58-487a-9dc1-cd92823706c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 51
        },
        {
            "id": "a7824969-e215-43a8-9f72-025662f768b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 52
        },
        {
            "id": "f8b3c69b-72c1-4cdc-afad-8d8854d2dda6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 53
        },
        {
            "id": "03a8d66e-1ac5-44bf-9813-289412563e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 54
        },
        {
            "id": "963156bf-fd16-47e4-9d11-1bacceebe368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 55
        },
        {
            "id": "1978e93a-eec2-4069-98ae-24a7092b230e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 56
        },
        {
            "id": "c322fa16-e268-428e-8de6-be1705ce04ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 57
        },
        {
            "id": "ce445b4c-ae92-4ec0-97e7-3673a5600f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 65
        },
        {
            "id": "e73188fd-2a20-4c19-9683-687f8f8ae176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 66
        },
        {
            "id": "a27121be-115a-4dd6-b5a1-8e6163d34ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 67
        },
        {
            "id": "2a022c20-400b-436c-97ef-0cf8dbadf6bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 68
        },
        {
            "id": "cdf78ca1-8491-4f41-83bf-48bdc40005a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 69
        },
        {
            "id": "c8354fc8-2d66-4e5c-80d8-18e8f9f43a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 70
        },
        {
            "id": "05a8c280-6e0b-47e5-a722-686c7509afcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 71
        },
        {
            "id": "e5ea2c6b-4b7c-4174-8cd6-c2b0e6844bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 72
        },
        {
            "id": "9e065833-36bd-4625-a6bd-7d0563fbbced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 73
        },
        {
            "id": "5201923d-57ac-4e4e-8b34-b7991e130a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 74
        },
        {
            "id": "71a265fb-d756-49c1-a477-8d8b2d162116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 75
        },
        {
            "id": "0c7a8083-014b-4a82-bd69-e6b38fd30f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 76
        },
        {
            "id": "8849354c-a79d-4997-9fd2-e1d891895fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 77
        },
        {
            "id": "14b6c61f-065c-4dfb-bbcd-a0e145337c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 78
        },
        {
            "id": "63d6d089-4ba1-435b-a66e-9dcaeea5250e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 79
        },
        {
            "id": "913b2fa8-6cde-456e-9988-8bd4923aadcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 80
        },
        {
            "id": "97755c01-4fbf-4376-bdc8-d8d8bd3926e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 81
        },
        {
            "id": "395dadd2-8597-47ea-b623-c3f98009e703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 82
        },
        {
            "id": "c2fca515-14f9-492e-9388-94fa7fecf634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 83
        },
        {
            "id": "854293c0-f015-40d1-ba78-b97dd39cf90b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 84
        },
        {
            "id": "2d72b1e1-df48-4726-9bcc-d923ad792849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 85
        },
        {
            "id": "80f338d9-dc17-4c10-a351-15ed2242b36b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 86
        },
        {
            "id": "0b19316f-a607-4b0d-a530-1ed2b6294b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 87
        },
        {
            "id": "860d1c44-f319-4886-ac69-beece5455967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 88
        },
        {
            "id": "35fbb474-a965-4783-ad9f-97bbf2d13d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 89
        },
        {
            "id": "79a389aa-0687-4a02-8142-f3c04c27f84d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 90
        },
        {
            "id": "32d27ee7-5bbb-4775-ab9a-91234da65a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 48
        },
        {
            "id": "ae9df03e-8415-437f-829c-3d33606e26c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 49
        },
        {
            "id": "b78b2f22-bd1b-4646-952b-74358841e06a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 52,
            "second": 50
        },
        {
            "id": "c8aaf370-ca81-4fbb-b7d9-d34d03192b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 52,
            "second": 51
        },
        {
            "id": "e3daf338-ad7a-4b39-85b6-bede318d29f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 52
        },
        {
            "id": "920048c9-bbe8-4ab0-b40d-7461d50ccc01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 53
        },
        {
            "id": "a2463996-80b2-4d85-9c14-8e86cc847de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 54
        },
        {
            "id": "9fdda1a1-e28c-4ac9-b633-394312debd1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 55
        },
        {
            "id": "5fbd80de-74a9-452d-9495-94a4cabea85a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 56
        },
        {
            "id": "0d913203-9387-4bf4-90b7-1c238ee045ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 57
        },
        {
            "id": "f2232dc6-860d-4b68-a768-e60660e34fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 65
        },
        {
            "id": "5b6c39bc-b10b-4263-ba10-638c57d844f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 66
        },
        {
            "id": "fa17d6a1-fade-422a-a023-5b50a1d68fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 67
        },
        {
            "id": "817f56f4-1e55-4b9e-bc31-3af9a0a895d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 68
        },
        {
            "id": "faf26bd4-b124-4231-9dd2-2841ea4424af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 69
        },
        {
            "id": "552d5011-43a2-4018-8fdf-3915cc6948e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 70
        },
        {
            "id": "7b9ec700-4dcd-4555-8776-608507c0feb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 71
        },
        {
            "id": "c2e6bae6-6a68-4478-aa03-37aa64cef75e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 72
        },
        {
            "id": "3eda7ec6-e7d2-4bda-854c-f0a92f66a7ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 73
        },
        {
            "id": "10c901ba-59e6-4204-acdb-aeeb031b04bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 74
        },
        {
            "id": "26a256c2-ac74-4e4a-a7bb-7a84d00f4eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 75
        },
        {
            "id": "0af950df-22e6-4389-941f-19b7718655df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 76
        },
        {
            "id": "745e8ba6-fc88-4835-b569-92d593ccb05c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 77
        },
        {
            "id": "bb4ccc3b-4067-4774-85e5-2d09584921e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 78
        },
        {
            "id": "4bf74839-4ff1-4a15-bcc6-2b1ef9d71caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 79
        },
        {
            "id": "7f63ff8d-6f60-4e25-be3a-278be88e561d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 80
        },
        {
            "id": "c634c53c-1f1e-4474-9096-a78794253f7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 81
        },
        {
            "id": "db59912d-b994-426e-8e62-6fd0021654c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 82
        },
        {
            "id": "e3541d3a-64ad-4eb2-a912-0379dd998b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 83
        },
        {
            "id": "399af0b5-1909-4930-b372-99c10dade829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 84
        },
        {
            "id": "54f6f1e6-52fa-4430-abba-b77e12f1420b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 85
        },
        {
            "id": "a0aa7223-0c84-43c4-8b70-d582511f6c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 86
        },
        {
            "id": "d6824402-bd5b-4bed-99c8-9a6e9a718bf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 87
        },
        {
            "id": "5193e909-bb1a-4e06-8c72-3b803a605a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 52,
            "second": 88
        },
        {
            "id": "569c6d45-2bc0-4eda-ba09-b71769c1a162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 52,
            "second": 89
        },
        {
            "id": "b9a5bd73-6794-47fa-92e8-3e803830636e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 52,
            "second": 90
        },
        {
            "id": "a799307b-b7e0-4594-bf73-fa84843d17c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 48
        },
        {
            "id": "047f5315-25f9-4cdb-a8e1-a9a4be219e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 49
        },
        {
            "id": "41bc3bb7-03a4-4d6e-9137-7997ba93ecd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 50
        },
        {
            "id": "508ad0e1-6d21-46b4-ab3f-ab47e7956355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 53,
            "second": 51
        },
        {
            "id": "b8d63d04-92c2-479e-8792-61f5c169a9b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 52
        },
        {
            "id": "6bc461b3-c2c2-4e7b-8257-dabf0c070fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 53
        },
        {
            "id": "d75b59dd-c1d7-4dea-bd6d-9e0b5556cefd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 54
        },
        {
            "id": "5db050e2-0f8f-4f6c-be91-a1b47ddf9e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 55
        },
        {
            "id": "799d2a5c-04bf-42d0-a76c-23707e1491e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 53,
            "second": 56
        },
        {
            "id": "ffad387b-af24-42a4-8a2a-31ae87d93494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 57
        },
        {
            "id": "38872d5c-3e51-4828-b752-7e3022ce4347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 65
        },
        {
            "id": "6fdfe65c-d6e9-4514-9c1f-1e8bcddae481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 66
        },
        {
            "id": "223eaae2-0aca-4660-9111-64143bffef9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 53,
            "second": 67
        },
        {
            "id": "449d26a5-58bf-45db-a53a-d93570bf3e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 53,
            "second": 68
        },
        {
            "id": "f0e801be-731e-4316-be66-9d5db818654a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 69
        },
        {
            "id": "20a90328-7f85-4544-a2ae-3e4e0e70f5a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 70
        },
        {
            "id": "ab9f74a1-2113-490c-9ce6-21063c67569e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 53,
            "second": 71
        },
        {
            "id": "6d6ac6f8-e92c-415f-8cc5-07327b3c3564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 53,
            "second": 72
        },
        {
            "id": "0401935d-0a07-4c3a-bd5e-208d42a275c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 73
        },
        {
            "id": "b6c2fc72-bfef-4c6c-a80f-fa1b29e1b60e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 74
        },
        {
            "id": "1f3d26fd-ca9a-4a12-9ed5-b29c64b8f215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 75
        },
        {
            "id": "5ff3a5dc-7bef-4ed4-a625-b647eabb0f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 76
        },
        {
            "id": "43602e73-4609-4f5c-bce2-6d838d4c3abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 77
        },
        {
            "id": "836cd677-a5ab-40eb-b785-af318953c14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 78
        },
        {
            "id": "fd2a626a-6c98-487c-89a8-693e040a8b2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 79
        },
        {
            "id": "ea292b20-571f-4257-a648-860b3856b1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 80
        },
        {
            "id": "67650585-8115-4d8a-8d0a-bdd6ad0d5f0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 81
        },
        {
            "id": "5a146c08-cd39-47f2-8f54-62bbdd68b672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 82
        },
        {
            "id": "88c20b53-87b6-40a5-b688-e7eb1e8a8d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 83
        },
        {
            "id": "5c7cf5e1-23a7-427b-ab0a-28eb17737144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 84
        },
        {
            "id": "212983ce-461a-4e03-9885-07340c0d6c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 85
        },
        {
            "id": "bafd3919-1e78-4cb1-b42c-663a16476a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 53,
            "second": 86
        },
        {
            "id": "6210c575-d8a6-4340-9d23-5e824339fb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 87
        },
        {
            "id": "13c91ee4-a010-4763-8ceb-0928866780ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 88
        },
        {
            "id": "01a584c7-0829-43e9-99c1-0e1cafc5dc8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 53,
            "second": 89
        },
        {
            "id": "e2853408-4ceb-402c-a5ba-4fd5ce7a78ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 53,
            "second": 90
        },
        {
            "id": "9da14dbf-469b-4940-9432-dc12ebbf495a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 48
        },
        {
            "id": "58955eeb-4a66-4fe7-9444-065215fa620f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 49
        },
        {
            "id": "f1afa47b-8d41-4c4c-a9ca-7f2ddfcca429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 50
        },
        {
            "id": "c3394b5e-5dc2-4166-aa00-977c1b142e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 54,
            "second": 51
        },
        {
            "id": "cb902ff1-12ab-49bb-a824-da1707696e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 52
        },
        {
            "id": "b0816572-7f58-42d4-ae8c-ad9f0f0997ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 53
        },
        {
            "id": "e8b7d1ac-bf0a-45ef-b091-9e8b9568b77b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 54,
            "second": 54
        },
        {
            "id": "f2d0b022-bfe4-462d-bef7-49cbc5f25e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 55
        },
        {
            "id": "260d0e6b-a8ba-4bef-8ee5-a7c9c449ae16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 54,
            "second": 56
        },
        {
            "id": "339a5708-f0c3-4424-861c-8ffedaea5c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 57
        },
        {
            "id": "af96c952-a118-44d8-af95-a74d396e1ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 65
        },
        {
            "id": "1e890959-4815-4332-9cde-74a5f2e7c7d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 66
        },
        {
            "id": "4f95e904-9502-4aa0-88ed-89bc4a5daf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 67
        },
        {
            "id": "aa4aea63-019c-4f48-9b1d-0888a4bef558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 68
        },
        {
            "id": "b0640716-1803-41c2-ab8f-8aeac926d4aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 69
        },
        {
            "id": "e98d33ff-801f-4bfe-bc05-6f3e434bd4b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 70
        },
        {
            "id": "b75b9df4-4768-4270-8672-74149513dec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 71
        },
        {
            "id": "99c11f55-b819-439a-a2a8-eb0a5c936b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 72
        },
        {
            "id": "cc763a9f-aecb-44bc-af82-312c35afca7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 73
        },
        {
            "id": "82364797-651f-4833-a2df-33ada14b5252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 74
        },
        {
            "id": "14b2a170-cec2-4af1-8431-0ce29fedbd13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 54,
            "second": 75
        },
        {
            "id": "401604f9-39ef-443b-9fea-d2e54d20fd64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 54,
            "second": 76
        },
        {
            "id": "982691c2-4ac0-4f97-9fcb-b42078d08961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 77
        },
        {
            "id": "d902eaad-550e-40a2-b9bc-a7282d1c420c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 78
        },
        {
            "id": "2643ce83-bf75-4010-87a6-5b6244061d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 79
        },
        {
            "id": "04e34a5d-7493-43b9-82e3-2c49965f2ede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 80
        },
        {
            "id": "a4ef3ae6-9c57-4d5b-83ad-c32d1c14cc4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 81
        },
        {
            "id": "ff4fea02-45a5-40ee-965f-d46250eb3ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 82
        },
        {
            "id": "5a1cf25a-978c-4014-ad42-ce4c93f886a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 83
        },
        {
            "id": "4c538b01-e947-4cb5-8862-a2bd90b8c55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 84
        },
        {
            "id": "4e1fade7-017a-4bbe-8ff1-74bf5763dac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 85
        },
        {
            "id": "0b392989-c609-40c3-84af-c421a9b5d21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 86
        },
        {
            "id": "617a4ec8-9abd-428c-aa57-89955d7f2d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 87
        },
        {
            "id": "14fdec23-bee9-4f2e-91b6-84082c0897e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 88
        },
        {
            "id": "8c5d8d35-305c-4b31-9783-a86f7be567ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 54,
            "second": 89
        },
        {
            "id": "163a44ab-256a-4060-8275-189575e5f69e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 54,
            "second": 90
        },
        {
            "id": "94717dd4-8acf-42fe-8855-ee54111c8bc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 55,
            "second": 48
        },
        {
            "id": "aa2fd854-fae9-4090-babe-dcfda2097b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 55,
            "second": 49
        },
        {
            "id": "839dd307-af58-4c90-96d5-41fe5963f641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 50
        },
        {
            "id": "74f3c727-2e34-4ac4-8425-21d90d93cd95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 51
        },
        {
            "id": "20ce7bb0-d3b8-437d-a332-853ea3f1d163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 52
        },
        {
            "id": "e10556e0-c550-408d-80b4-5b1485a8d8f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 53
        },
        {
            "id": "66dc73d4-9ac6-4b71-a4d9-3566cf17eedb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 54
        },
        {
            "id": "b45fe630-f7a2-45d0-aa26-ea4c8a14aced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 55
        },
        {
            "id": "46729bd0-dea8-482b-aa9f-156e4b8c323e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 56
        },
        {
            "id": "591d53fe-01d9-466f-adf6-2394a554dadd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 57
        },
        {
            "id": "cccb7529-9271-4d95-b1d5-fdc755c21bdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 65
        },
        {
            "id": "f75dd3cf-18c9-45de-9767-9730ff11a67e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 66
        },
        {
            "id": "69b0734d-3e25-4ed4-a8b3-380885efe8d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 67
        },
        {
            "id": "93c73969-89d6-4f57-abe9-8f4deb2b257f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 68
        },
        {
            "id": "a121da78-0413-4fcd-929a-ebbedf0e4ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 55,
            "second": 69
        },
        {
            "id": "798aea02-fdd0-4de4-b048-7bc323aaa48f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 70
        },
        {
            "id": "7cb515b9-ba8d-4d81-a9d2-e85af4b4b39a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 71
        },
        {
            "id": "11c24fb3-0acf-46cb-b4b7-550f82c82360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 72
        },
        {
            "id": "fb18829e-3127-40bd-afde-f54428a5064b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 73
        },
        {
            "id": "ee375d90-a54b-4036-a5e9-45bba8578bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 74
        },
        {
            "id": "e5b8be1d-6fd9-448a-92bc-601a09a03cd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 75
        },
        {
            "id": "1b4af024-4c4c-45d3-88a7-f700d7257e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 76
        },
        {
            "id": "46147451-5f32-4205-b22b-a5d055dcb923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 55,
            "second": 77
        },
        {
            "id": "d144701f-0d74-4ee9-9938-3ff9ca6dd3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 78
        },
        {
            "id": "10a8c5f6-664c-476e-bdac-712dff1c16d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 55,
            "second": 79
        },
        {
            "id": "be59e1b2-f6a3-447f-8bc9-209ec079c550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 55,
            "second": 80
        },
        {
            "id": "8d42fb9d-5406-45ef-aeb1-3d330f8d650f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 81
        },
        {
            "id": "4702a714-43d6-4964-9ac2-64f6b8050c11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 82
        },
        {
            "id": "145c2c6d-4925-4348-81fb-71251622605d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 55,
            "second": 83
        },
        {
            "id": "d760b814-4bad-4fe8-a53f-c8caf5b1ba4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 84
        },
        {
            "id": "491b9a35-342a-42f7-b7e3-389fbcb52acb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 55,
            "second": 85
        },
        {
            "id": "bc5ca204-f577-4a1c-b637-662ff1e8872c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 86
        },
        {
            "id": "34bf36d6-f2cd-4828-9563-70d65e8673aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 55,
            "second": 87
        },
        {
            "id": "7e9984db-b1a6-485d-a221-e6c7e729b89d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 55,
            "second": 88
        },
        {
            "id": "acaae875-a6f1-4377-b0e1-a3acbe43eadc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 89
        },
        {
            "id": "79431339-04fb-478d-8b14-d7569b3e31d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 90
        },
        {
            "id": "e701621c-86fe-4122-9d08-d6f3805fc963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 48
        },
        {
            "id": "436cb060-2fd1-4bc3-983d-14518024c5aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 49
        },
        {
            "id": "bcaf34e0-9706-4932-9f39-219833c58ec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 50
        },
        {
            "id": "eb089474-16cb-49e0-9d96-8fdd2377a988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 51
        },
        {
            "id": "013230c1-b5e1-464d-bcc3-07d733da9e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 52
        },
        {
            "id": "eefa507a-08cd-4675-81bd-bdabe3cccd0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 53
        },
        {
            "id": "5d9f293c-7676-433d-87f8-381067c64963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 54
        },
        {
            "id": "224e6107-f6c0-4e10-8bef-41e460bb7943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 55
        },
        {
            "id": "2c3822a3-3d46-468b-a26b-b7912ca72523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 56
        },
        {
            "id": "8146cd02-6cf1-46bf-97e0-853f35264838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 57
        },
        {
            "id": "b454c20d-8e54-4619-911f-b3b345c72987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 65
        },
        {
            "id": "7398fa05-95a3-423c-9267-8dbceba2372c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 66
        },
        {
            "id": "6cb553d6-4cb3-4262-8e07-afc3aa5b1095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 67
        },
        {
            "id": "6145ef01-8256-47dc-ae44-a91487713aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 68
        },
        {
            "id": "42f45624-c29d-406d-add8-3c84758bfd4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 69
        },
        {
            "id": "7b8e452d-b3ce-4f58-964d-6d9eef6407f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 70
        },
        {
            "id": "5f24714e-43e6-4ba7-b419-6efd0fa7c6ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 71
        },
        {
            "id": "7d21c1e4-7105-4d73-935d-5667a52ffe6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 72
        },
        {
            "id": "da13e991-57f5-46fc-b186-95c646cf9a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 73
        },
        {
            "id": "30dc3836-ed55-4bd4-ba70-a3334c34118e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 74
        },
        {
            "id": "43639e9b-2e52-47d8-a0e1-de6663b928fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 75
        },
        {
            "id": "1d852dea-8fcf-4013-80bb-68f5fe12061b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 76
        },
        {
            "id": "78192905-bca3-4474-953e-bc46900cec69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 77
        },
        {
            "id": "eb20fe19-c99b-4ef2-a56b-e2fb876e3b87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 78
        },
        {
            "id": "599d4cc4-b57b-44d6-bb0b-c5977fbd633d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 79
        },
        {
            "id": "d68c6e0b-e1c8-4fc6-8fc5-748913932231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 80
        },
        {
            "id": "a275a28d-1d86-45d4-a9c7-13351a0b9b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 81
        },
        {
            "id": "2ce3370e-167a-4067-b937-5669671d723b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 82
        },
        {
            "id": "00a70a88-23b0-4382-88b8-e8438c5291c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 83
        },
        {
            "id": "243af02a-d3df-4ae3-96ab-3d1bd49d8c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 84
        },
        {
            "id": "d22cd787-7d3d-43f8-9bb1-0bc21503e9c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 85
        },
        {
            "id": "5af4883a-398f-4f61-8dac-05d114b78506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 86
        },
        {
            "id": "10920516-ce8b-4de1-9947-ed81829db482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 87
        },
        {
            "id": "4a043d88-d5c5-4580-90b6-f8b04c47bc35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 88
        },
        {
            "id": "f7d2b520-fd27-4775-afb1-f2ea2c76a4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 89
        },
        {
            "id": "664536b8-c90e-41be-8728-09c2e4428da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 56,
            "second": 90
        },
        {
            "id": "d86598ea-ba9f-4176-a391-af214792f0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 48
        },
        {
            "id": "7699fb67-b589-45d8-80cd-bff4807d94e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 57,
            "second": 49
        },
        {
            "id": "e6c69568-fd3f-45a4-bbda-4b7d2ab34d95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 50
        },
        {
            "id": "f3478b20-af31-4aef-9c6d-f55d6d2d956f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 51
        },
        {
            "id": "29c152b1-7f37-4bb1-a826-b9a94d2a4681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 52
        },
        {
            "id": "8698f28e-fde6-40ce-8536-3046751024db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 53
        },
        {
            "id": "4da0a01b-a3e4-4c04-b1c8-9e844bba7662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 54
        },
        {
            "id": "d36fa0e8-73f9-4022-80ef-4015c6d5db72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 57,
            "second": 55
        },
        {
            "id": "ca0df3f3-6018-4ed2-bdec-a97d747b0ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 56
        },
        {
            "id": "9479b691-6e1a-4835-b4b4-39c66e85b80d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 57
        },
        {
            "id": "995e7ac2-c159-4069-9417-34c3b64bb4f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 65
        },
        {
            "id": "f4a0c357-93d9-47ed-9c08-4359167e6810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 57,
            "second": 66
        },
        {
            "id": "b7c5c6f9-4090-4729-97c4-205592154c53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 57,
            "second": 67
        },
        {
            "id": "4993ebd3-12ab-4489-b3a9-a568f755137f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 68
        },
        {
            "id": "fd1f1103-021e-415c-9a99-e4fce6851180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 69
        },
        {
            "id": "248b2b9b-3af6-4a92-98e2-8bdd742d6e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 70
        },
        {
            "id": "dc0240a0-0a8d-4af0-9371-bc653bf396d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 71
        },
        {
            "id": "9fe1eb9a-e2a8-4b20-a51f-31b683303d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 57,
            "second": 72
        },
        {
            "id": "b32779a5-bd52-4907-a820-f4ab0942c248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 73
        },
        {
            "id": "541fbce1-3841-4aa4-8772-51b4b444d0c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 57,
            "second": 74
        },
        {
            "id": "7249025d-64e3-4295-9329-7372a4a53e70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 75
        },
        {
            "id": "685181f8-db10-464c-a441-35d2c88a1c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 76
        },
        {
            "id": "5a70dd3f-5cb0-46ab-8395-e563fb65a7e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 77
        },
        {
            "id": "bdf735e6-e297-44c7-8a14-5526f18b1c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 78
        },
        {
            "id": "4a8d574e-f80e-4ada-9203-8b05a3eae425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 79
        },
        {
            "id": "eb6ee819-5ce2-4d57-95b9-2c527aedb309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 80
        },
        {
            "id": "54b65584-c355-429c-973e-90de1ea3eb63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 81
        },
        {
            "id": "85906a3e-11d6-475c-a78d-460454110cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 82
        },
        {
            "id": "86b1dcfd-49ba-48bb-a9f3-c6f13c5f4c10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 83
        },
        {
            "id": "574eaa5b-7c07-4e32-99cc-1e818f337e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 84
        },
        {
            "id": "4574cb54-90a1-42b3-b578-2289aaeaef0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 85
        },
        {
            "id": "3e13a78f-bcf2-4449-90af-37206d6d258d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 86
        },
        {
            "id": "f97fd066-fe1e-4c4c-90f7-452cb7f855b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 87
        },
        {
            "id": "2826ba61-fabe-4845-a131-37f6572ebc57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 88
        },
        {
            "id": "01fa0a15-a680-40c2-8b20-c6f8cd639d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 89
        },
        {
            "id": "574b9dbe-1a2f-4a12-8310-02619cece5ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 90
        },
        {
            "id": "2319c7bf-9efa-4c29-a920-2efa0fdd3b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 48
        },
        {
            "id": "041d47ff-0b0c-4209-a916-ab8db50f564c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 49
        },
        {
            "id": "f08388b5-f9af-4618-8a43-19ad82b78295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 50
        },
        {
            "id": "fe55b30a-36ff-4ffe-86e9-d52b1a6be25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "8f775bda-d3a9-4612-a700-5211b3eb1880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 52
        },
        {
            "id": "60fe5512-2082-4463-b505-cca337704981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 53
        },
        {
            "id": "06a2d732-7b6a-41c5-8c55-aebf3adf9942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 54
        },
        {
            "id": "5358409c-d444-44bb-b8f6-b915df147bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 55
        },
        {
            "id": "20132684-ebf3-4538-80ac-f2149835ee9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 56
        },
        {
            "id": "bf3635e2-6e55-4769-9e8b-552346d7d32b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 57
        },
        {
            "id": "6a4c49d0-684a-4fd7-9d46-ec83fad4130b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 65
        },
        {
            "id": "01d5956c-5b4a-458f-b2bb-372120fb0c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 66
        },
        {
            "id": "36a5c298-8efc-4018-af49-fd823427ed9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "6769b418-8d01-4105-bac5-0359a2bb35da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 68
        },
        {
            "id": "3bdf34b9-b318-4550-b0f5-3100f2938c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 69
        },
        {
            "id": "a263ee30-b9c5-45a2-b505-2086a8105332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 70
        },
        {
            "id": "92eec453-25ff-4956-8e28-759ad2869e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "b29c98c0-8506-4a41-847c-30a57b98d4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 72
        },
        {
            "id": "fabdec97-c1bd-49e1-875c-117bbcf4a140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 73
        },
        {
            "id": "ba9ce08a-01fe-4f40-8dab-dd6f7aa4d5c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 74
        },
        {
            "id": "ba14cd88-417a-4766-a0cb-dc2bba9aa3ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 75
        },
        {
            "id": "01c66f91-a838-4b6b-8605-ae4130429687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 76
        },
        {
            "id": "3126ad28-503e-444f-a944-fe136b6088b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 77
        },
        {
            "id": "d990f795-e721-4095-8c49-79b13801a043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 78
        },
        {
            "id": "552d33d8-68f7-4217-b066-15ea65e6ba67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "d7bfae0b-ad0e-46f2-89f1-da199326d5ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 80
        },
        {
            "id": "c519b342-df84-4939-8312-d67d4b85dbbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "4cf1f277-8224-449e-bc98-f00a7cb09fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 82
        },
        {
            "id": "26166592-155c-463b-8170-ba7af414e642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 83
        },
        {
            "id": "9bcca9b5-ea4f-45b1-b6b4-1bdc9542c52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "cbddc7f8-9828-4856-b1c9-876b60bbc986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "2d0f8b54-1bf1-4338-9d1e-6ae960ff9198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "0055ea57-b3c1-4211-9688-8e6bba1d7da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "eb03684d-8d70-4b04-ad97-163151b2f837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 88
        },
        {
            "id": "731ff44e-70dc-4f56-8061-6612ea6ada69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "c77170d9-387c-4733-85ad-4cfc1f79992c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 90
        },
        {
            "id": "4eeb041b-23dd-48b5-88ef-978ebb9f8f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 48
        },
        {
            "id": "a7754d8e-1861-43a4-8545-d395cc1ca12a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 49
        },
        {
            "id": "711c1624-61f0-4d78-95a7-49ce7810bc2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 50
        },
        {
            "id": "cd441371-dc10-4a52-a6d0-32e2112e0569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 51
        },
        {
            "id": "fd795cc7-bd33-46c1-aad2-ff7d811e2dd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 52
        },
        {
            "id": "2ef2c1f9-e611-49f1-bc85-5fc82eaa12f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 53
        },
        {
            "id": "83a4a50e-ba0d-4aad-b3c9-60d1de8245be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 54
        },
        {
            "id": "88cf49f2-40f8-4a9f-8b64-8ad61487c00c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 55
        },
        {
            "id": "02feb713-8e7f-4f49-819c-d81cbc41877a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 56
        },
        {
            "id": "a333a3f4-a150-4cbf-a023-bb19794a29a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 57
        },
        {
            "id": "728856dd-96a4-43f9-ac2c-e2b287a76b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 65
        },
        {
            "id": "0d7f6f8d-cad1-4e85-a7f1-4e10064fab3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 66
        },
        {
            "id": "e0ed38db-ecfa-41da-a087-99da95c7a716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 67
        },
        {
            "id": "f69bf800-b1ff-4c81-95a7-ceadf6ce9174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 68
        },
        {
            "id": "1337c960-6076-4b67-a955-0410805697d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 69
        },
        {
            "id": "2ed5a56a-bc11-4849-9e55-8984bb72218a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 70
        },
        {
            "id": "9c9e23dc-b173-4439-9e44-49bf4df6861f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 71
        },
        {
            "id": "e8b1c2f2-0e2a-422b-9cb2-e63122736d43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 72
        },
        {
            "id": "5c5667c0-995b-4ca6-822d-363e04fb7085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 73
        },
        {
            "id": "539e1de6-83f2-465c-853b-f07cd822c3ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 74
        },
        {
            "id": "6fd59b23-22e3-4d2a-b05d-6a9a3edb1c71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 75
        },
        {
            "id": "4b1c1b3f-6750-4e15-8ae8-924831b25965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 76
        },
        {
            "id": "bee02d6b-e18c-4a7a-81ce-efdfd0222b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 77
        },
        {
            "id": "5edad52b-6605-4c14-a82b-fb0d2692fb28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 78
        },
        {
            "id": "b2c77924-722d-48dd-a4b0-5871345132c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 79
        },
        {
            "id": "f771ff03-9ceb-4384-ae9d-165ff993c30c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 80
        },
        {
            "id": "24c65996-d19b-4b37-bc34-b07a54fd356d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 81
        },
        {
            "id": "2b45bec0-89e7-4587-a3ae-aac7ae5dba96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 82
        },
        {
            "id": "5f87c5a1-5b03-474c-b7be-df0912f1931a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 83
        },
        {
            "id": "eacb4b76-52ab-4e4b-8602-75f3eea6775b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 84
        },
        {
            "id": "f6670032-aa1e-441c-8164-09db22192729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 85
        },
        {
            "id": "57d70405-d2ae-48fc-8bc5-e0e60fb4a42e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 86
        },
        {
            "id": "3d1173a8-24b4-4044-80fc-1e2fd4da6468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 87
        },
        {
            "id": "a391a366-c9e0-4be0-99d0-040897f8613b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 88
        },
        {
            "id": "eec4dff8-fae7-4096-a6f2-fe6ad617569c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 89
        },
        {
            "id": "b8aa0108-19c8-4782-b223-954862f17dcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 90
        },
        {
            "id": "789edc3f-3e13-43ba-b4c9-5f6e0f2737d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 48
        },
        {
            "id": "f280a6a3-6e71-45c1-b8da-00dcbcbf36af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 49
        },
        {
            "id": "17522dd1-c1a2-476a-9283-c6ca93c18ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 50
        },
        {
            "id": "69f5f6a0-474d-401e-82e7-defea1a5bd67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 51
        },
        {
            "id": "2e231082-ddb9-4cf1-b1a0-b8460a4e2762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 52
        },
        {
            "id": "ff38d481-1dff-4cf6-9674-2940d26aebdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 53
        },
        {
            "id": "e5c9e9de-1e35-4a30-8b75-222161aa078e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 54
        },
        {
            "id": "ae804b18-3d39-44b5-bb52-c4963937a305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 55
        },
        {
            "id": "964c3e04-f715-409d-aa97-f3dd93e97545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 56
        },
        {
            "id": "dfb11c7a-0b73-4da3-8de8-8d894eba6ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 57
        },
        {
            "id": "00a45353-8bca-4f92-b815-0e319fd637f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 65
        },
        {
            "id": "6cf724d3-d3e9-4719-ace3-f0363e899a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 66
        },
        {
            "id": "2ed2c4be-de3d-4360-8ac8-255e08c48e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 67
        },
        {
            "id": "91f5599b-c0dd-4c0a-9a88-b65d23085b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 68
        },
        {
            "id": "e18b601d-532e-46a2-adfa-e9e3610eff92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 69
        },
        {
            "id": "2e9e8168-0511-44bd-869c-2d3b8b8e60a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 70
        },
        {
            "id": "130e5d5f-83fe-4a1d-af73-ec3ea4ab948a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 71
        },
        {
            "id": "4c768794-700b-42c9-8c3b-1c56925e6f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 72
        },
        {
            "id": "493bc87b-7d5b-485b-9bde-3cbcd7d80238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 73
        },
        {
            "id": "87164f77-466b-458a-aa0b-53d177f52c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 74
        },
        {
            "id": "34d0bedc-d81d-4a3b-be19-376784e71bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 75
        },
        {
            "id": "2a7a6205-fd0a-41b6-82ab-967a59985cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 76
        },
        {
            "id": "9c07d7fa-0699-4d9d-98c2-c8e355521e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 77
        },
        {
            "id": "ea0c2334-9ec7-4a50-82c8-c75413e57447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 78
        },
        {
            "id": "0b65562e-33fc-42ff-90f3-b18816aaf794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 79
        },
        {
            "id": "f308ed05-9605-4aa2-96e8-a959f2525d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 80
        },
        {
            "id": "d7e44bbe-c5ae-4bc0-b43d-9b750648de27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 81
        },
        {
            "id": "ae871f20-205e-44ff-9394-cfb4db12939a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 82
        },
        {
            "id": "813bf348-f554-4117-8ce6-8f75b3f3ca31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 83
        },
        {
            "id": "84a7f708-c875-4038-8a6e-8001ccf4371e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 84
        },
        {
            "id": "4ff6e0fe-e4af-4169-985f-ef51bbec0a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 85
        },
        {
            "id": "5ef2a0f2-1f7c-41c0-a3a3-c2bb74b16adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 86
        },
        {
            "id": "71537b7c-2779-4751-8f41-8d0f59256a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 87
        },
        {
            "id": "2b342f40-99ce-42dc-abd2-007e58e5cd6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 88
        },
        {
            "id": "717062f5-2b67-4678-9fb1-68f3e23992ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 89
        },
        {
            "id": "afdf262f-88cb-4e29-b4e4-8a9e4d80006b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 90
        },
        {
            "id": "7fb54efb-50e4-4d00-a8c2-f43dac77bef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 48
        },
        {
            "id": "4c5bbf61-247c-4d5f-98bf-6303a2eb30a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 49
        },
        {
            "id": "9ff6bc81-92ca-4f4b-b0f6-66a279c9fc35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 50
        },
        {
            "id": "64535fd4-6d7d-4631-875f-618f1c302446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 51
        },
        {
            "id": "44fb811a-3961-4eeb-9450-8e57c40ff6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 52
        },
        {
            "id": "873bc528-764f-48f9-8f04-565757ceeb89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 53
        },
        {
            "id": "bf0b00ca-ee9e-4fdd-aa5d-a7788d7b7b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 54
        },
        {
            "id": "9d6a6e3c-2742-4d5d-bdc3-9013e8715252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 55
        },
        {
            "id": "ab89f1cf-029c-4a96-9273-6c1321a9b8b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 56
        },
        {
            "id": "0c0baa44-0363-4be0-a2fa-e2da6839f088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 57
        },
        {
            "id": "f40dfc75-23b4-4ecf-9de7-12588977b293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 65
        },
        {
            "id": "9cd0dcd5-39a5-4e61-a988-ccf3aa23040d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 66
        },
        {
            "id": "32475720-c6f8-43b0-a791-c921a940fc74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 67
        },
        {
            "id": "77c67fc8-e90c-45ef-84ba-895aa36e4853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 68
        },
        {
            "id": "9ad4a806-b96a-4423-a536-66cf9ab38a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 69
        },
        {
            "id": "c54b4ce5-70c4-4009-a620-91ed5a29e027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 70
        },
        {
            "id": "bd5a2aa2-467b-44cd-97ce-979eb11c11b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 71
        },
        {
            "id": "0619070d-5c98-4d25-bc11-b39bcb0cf82a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 72
        },
        {
            "id": "fba5cac0-4f2b-458c-b32f-e2433a655a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 73
        },
        {
            "id": "39a60587-15a8-46bd-8c86-a5ddcdddc806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 74
        },
        {
            "id": "af4f22e1-f42f-44c2-a1d9-3c578f14a10f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 75
        },
        {
            "id": "745fc388-96b7-4488-a73a-ac8e8cd112fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 76
        },
        {
            "id": "93233083-1e99-46dd-831f-dc1e02fb2eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 77
        },
        {
            "id": "0ccfa36d-32ea-427c-a6f0-90d3aaad87b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 78
        },
        {
            "id": "e9477612-958b-4213-a0c5-c85d777584cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 79
        },
        {
            "id": "b7f4c3e2-c66d-43f7-b581-549781c6ef8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 80
        },
        {
            "id": "3bb2baed-27aa-49ee-a867-7163695404ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 81
        },
        {
            "id": "99cda76b-0848-4d51-8e55-5bdfab73388c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 82
        },
        {
            "id": "122c6280-3cf5-42d9-be1c-310c41483d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 83
        },
        {
            "id": "d171894b-0bfd-42bb-a0ad-9c1e639db7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "3a70f0c4-2c4c-4d55-a2d1-49ce8c10d946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 85
        },
        {
            "id": "3c417edf-64da-4633-9536-a1e7f7660d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 86
        },
        {
            "id": "0a6800aa-bd75-4bfe-98bf-1f859bae3a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 87
        },
        {
            "id": "c1916381-496a-485f-8410-fe5040c57f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 88
        },
        {
            "id": "557ccb7c-d9ff-4981-836e-14c1db24fdf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 89
        },
        {
            "id": "53ec81d8-d881-4fe2-b2a9-d0c038711a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 90
        },
        {
            "id": "bb17752b-6278-40f5-8824-1568ad598945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 48
        },
        {
            "id": "072a4319-2067-4cf6-9d72-52dfd488a22f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 49
        },
        {
            "id": "434dec32-c582-4762-973c-59d117ea8b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 50
        },
        {
            "id": "8fa28b1e-a0d4-4897-b6d0-9fa883987467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 51
        },
        {
            "id": "ee19b86b-031d-41b7-b1a8-1bd1577f3440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 52
        },
        {
            "id": "0c51736b-5401-4756-a14a-ff64cdeade67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 53
        },
        {
            "id": "cf45609d-d376-4747-9552-15ebd6b5779a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 54
        },
        {
            "id": "2c2172e4-d232-4050-8cec-82dde4b6044c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 55
        },
        {
            "id": "8fedddd1-ce14-445c-8f34-fc953ac67084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 56
        },
        {
            "id": "ed42d9d2-d9a8-4af3-bea7-482eaa8517ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 57
        },
        {
            "id": "cd88a749-2d07-45a8-8858-53c201ccec1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 65
        },
        {
            "id": "291e9343-a753-4403-9457-cfb165bb034d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 66
        },
        {
            "id": "28c04a08-56fb-47ee-900c-785f098e8501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 67
        },
        {
            "id": "a5b04559-bddb-4995-bdd4-52c74ac2c1aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 68
        },
        {
            "id": "97f2f32f-f1e3-41a4-abd9-6ad8bb44bfa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 69
        },
        {
            "id": "b4c3571c-74ef-4ab2-aa45-9ae14decd664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 70
        },
        {
            "id": "48a406ff-2657-48cc-86cb-85a627be74a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 71
        },
        {
            "id": "eed22b2b-4971-4b97-bf57-b78ee7b6a787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 72
        },
        {
            "id": "341c6060-dab7-40c2-b301-45c8dfcd5f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 73
        },
        {
            "id": "7c49cc15-059c-4dcd-b559-55566df7ab89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 74
        },
        {
            "id": "9f5d1d1c-1d0f-4a39-b87e-63d29ce9b2c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 75
        },
        {
            "id": "2608ecbb-f904-4d18-85e2-e5f669e112e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 76
        },
        {
            "id": "96b0371a-24dc-4c5e-b54c-75c194b640d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 77
        },
        {
            "id": "0ebce231-96e1-4d15-b9a3-96dc7eac4f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 78
        },
        {
            "id": "e00bf40e-d152-47ce-825d-9449563ba8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 79
        },
        {
            "id": "bd92ecca-d4ab-410b-af76-68e1700b8e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 80
        },
        {
            "id": "a1b31ddc-f834-4b2b-a13f-b14e473232d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 81
        },
        {
            "id": "96aa6734-eeeb-4987-863b-2d9ded232969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 82
        },
        {
            "id": "68238a6f-612a-4c14-9d64-ed6241275790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 83
        },
        {
            "id": "d4d9e92a-0e0e-4f12-b7dd-8492396eee63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 84
        },
        {
            "id": "aa8cacb2-2468-4c9f-8fc9-e4bc8236a163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 85
        },
        {
            "id": "871bd152-4ac0-49c8-9988-3f88bf2a06a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 86
        },
        {
            "id": "84522b07-0607-4334-bb19-8082d01149ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 87
        },
        {
            "id": "01787976-6bb2-440a-bbf8-e1b43cca666e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 88
        },
        {
            "id": "acd24887-a0b0-4805-9f0c-c29dae372d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 89
        },
        {
            "id": "16246406-e63b-4fc7-b780-071b5c33e7cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 90
        },
        {
            "id": "f12c21e0-15c9-4dd5-aa16-f49a26283638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 48
        },
        {
            "id": "ac38813a-5eca-48a3-9ea3-380f79be9d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 49
        },
        {
            "id": "ec6d6678-6df1-4d14-8f64-bff26e12193e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 50
        },
        {
            "id": "00890c09-4898-4542-87b7-d193240e073e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 51
        },
        {
            "id": "5a005dbb-df05-4e90-9bc7-a0edf19526b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 52
        },
        {
            "id": "3c684aa7-825b-41e6-9749-fde99c50447a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 53
        },
        {
            "id": "f68041a3-cea5-478c-b8fd-1f4f0d85aa01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 54
        },
        {
            "id": "d0258be6-fd2f-403e-bec6-1d7892c3d9de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 55
        },
        {
            "id": "f0b07e2c-f599-4982-862c-72d761d74bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 56
        },
        {
            "id": "4583920f-5ae5-4a73-a830-4ca72472ad81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 57
        },
        {
            "id": "7b04566a-9c6e-4881-9da5-3aec80f207d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 65
        },
        {
            "id": "b0c09b53-769b-49a5-92b8-87871f3795a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 66
        },
        {
            "id": "d7ca2e22-da56-49b1-a897-ea3b7593c3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 67
        },
        {
            "id": "1a359eea-7e94-4901-8a08-0342a398ce1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 68
        },
        {
            "id": "04ae165f-4b01-41df-ba6e-7e410d65de94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 69
        },
        {
            "id": "4daf35cd-9b42-4de3-86f3-9ea140ec9b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 70
        },
        {
            "id": "5c100d9c-20ce-419f-a560-af4a49a9c85e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 71
        },
        {
            "id": "a08a2023-056d-4da4-9c18-6abe5b595881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 72
        },
        {
            "id": "1c8a9ab5-e8c7-4379-8ee8-7d56d13fb38f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 73
        },
        {
            "id": "93250bea-b40a-4c0e-8ec5-10f5c67a9382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 74
        },
        {
            "id": "a79f48d6-32ed-4258-aecc-25849b7168f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 75
        },
        {
            "id": "3b0d0507-9fb0-4607-9318-d60bb6ea4ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 76
        },
        {
            "id": "d9da1337-385a-44b6-950a-f7ac9a411356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 77
        },
        {
            "id": "fb04b335-fb17-4b9e-9936-210c5ac32e52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 78
        },
        {
            "id": "83938710-6073-4106-be55-c840a7e4c79d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 79
        },
        {
            "id": "d4ac9d17-0331-4909-9e6c-50612e4d2366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 80
        },
        {
            "id": "3deb8472-c97d-4778-a6a5-6c0239616cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 81
        },
        {
            "id": "9c55ff77-c945-4e1a-9456-b725e24d8e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 82
        },
        {
            "id": "4708d513-f5e7-4d98-8485-41c34d67c852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 83
        },
        {
            "id": "042082dc-ba95-424a-b5b7-36bc1ec78db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 84
        },
        {
            "id": "20601a3a-09ac-4374-966a-1a4827be6f46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 85
        },
        {
            "id": "3416c02d-8c2a-4340-8f3d-f4fa478fb7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 86
        },
        {
            "id": "8dc2692b-fa66-4771-9e3a-6584e815ec90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 87
        },
        {
            "id": "9ecbe0df-9048-4e32-aea4-f49bd62ac422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 88
        },
        {
            "id": "56d03868-661e-4466-9c84-2c6d846d9779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 89
        },
        {
            "id": "2ccd2644-b122-4bae-8c59-b077119b944b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 90
        },
        {
            "id": "8b40324c-11e2-4331-adea-cf7c65b15278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 48
        },
        {
            "id": "69bc2aaf-8767-4d35-abdb-95d6ea3bd5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 71,
            "second": 49
        },
        {
            "id": "68e1514e-c38b-43ad-8cbc-95dfe47363c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 50
        },
        {
            "id": "e16984fe-2502-403e-8553-7c59e3409bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 51
        },
        {
            "id": "3d097a58-f176-4b24-9c11-2e869384a0f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 52
        },
        {
            "id": "9c8db1c9-6b60-4acc-940d-e0566cbe3565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 53
        },
        {
            "id": "e2966a29-46ec-42aa-bd58-7e741a93f307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 54
        },
        {
            "id": "a4c5f903-98aa-4cf4-ab19-13d421b21d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 55
        },
        {
            "id": "8e495735-cd54-4200-a524-919c43fbdcf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 56
        },
        {
            "id": "5bf8b34a-5d85-4576-ac44-ac6a9204d960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 57
        },
        {
            "id": "71e69c1e-f118-45bd-833e-47065af3385d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 65
        },
        {
            "id": "fa8be468-a810-40b1-ad6a-4c1b48081c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 66
        },
        {
            "id": "e4478dc5-d4e8-4fa8-b492-9d764f663fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 67
        },
        {
            "id": "d6f8e749-f1f9-439f-ac10-58ec8a1042e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 68
        },
        {
            "id": "e527bd3a-f0c9-4a9b-a2bc-887d15b1328f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 69
        },
        {
            "id": "2a81c192-90ad-4fa6-a542-debb63f626f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 70
        },
        {
            "id": "c83ebfd1-6f1a-4018-bd7f-066f4816914e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 71
        },
        {
            "id": "b5cb179e-246d-4a0b-94c3-1609104bd760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 72
        },
        {
            "id": "64425023-2d0e-43f8-969f-8376d90e273c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 73
        },
        {
            "id": "2b2244cc-2e21-42c2-963d-3158f5ccb4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 74
        },
        {
            "id": "ceb64c7f-4bd4-45e7-8e75-802c75f753ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 75
        },
        {
            "id": "5f45fa6c-ddfb-4045-a3ac-e557e18dfc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 76
        },
        {
            "id": "dab9e0a8-10dd-4309-aff9-664010ecf7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 77
        },
        {
            "id": "93a335e3-4b2e-4d71-86ee-d035ff495023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 78
        },
        {
            "id": "0c609c39-f91f-4e82-ac59-9bdb967c87e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 79
        },
        {
            "id": "9804005d-80c2-49b8-abba-1d96fcd89dee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 80
        },
        {
            "id": "dd807c86-931e-4ead-aea0-7d3522087453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 81
        },
        {
            "id": "d8ba69c0-91ff-4ed1-b6de-c27900506d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 82
        },
        {
            "id": "9bb1aeaa-3997-416a-a8b3-91162f013568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 83
        },
        {
            "id": "372b4f6a-9c03-4fd3-9378-66f4835008df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 84
        },
        {
            "id": "09a06b88-6731-4d5d-aba4-ee1ff1e6fbe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 85
        },
        {
            "id": "92685cd4-7185-406c-abcf-a9732d2e9fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 86
        },
        {
            "id": "fae47dc3-483e-455e-8863-1793a6542f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 87
        },
        {
            "id": "5322c5b8-7530-48a7-8195-5987aa15873c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 88
        },
        {
            "id": "103330a6-7f31-4945-b18c-0fada80d5377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 89
        },
        {
            "id": "006536d0-7f51-4e55-a2c6-f860e31c9bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 90
        },
        {
            "id": "400eeb2f-01b2-4c3a-8b0d-81940117118c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 48
        },
        {
            "id": "1f60a47c-4d69-464d-92e2-a63a952730e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 49
        },
        {
            "id": "e64c7ece-6fd3-4405-b8cb-70882358767d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 50
        },
        {
            "id": "a4ccf01f-629a-4d6d-bb07-c5e37999e180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 51
        },
        {
            "id": "c73bb407-fbd4-45b8-866c-c00593e77fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 52
        },
        {
            "id": "514b5430-b7f7-41fd-b89c-7cfb7872f6dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 53
        },
        {
            "id": "0c9ae7c7-779d-4858-bc6e-9e0b8d97057f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 54
        },
        {
            "id": "a853c463-0841-42dc-92ae-346bc2baabdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 55
        },
        {
            "id": "df633828-ca55-42a0-ba46-0c1bfb4053a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 56
        },
        {
            "id": "1c2d96be-6496-43db-91df-ce1dedee3464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 57
        },
        {
            "id": "25879894-7877-4960-9b2b-d6eabb65e66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 65
        },
        {
            "id": "c77b9ac4-d4d8-45fb-91ce-e11b79a4fab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 66
        },
        {
            "id": "a6a832d9-ed03-4670-94e2-9b3ad9348a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 67
        },
        {
            "id": "89d73488-2aa3-4b96-8d1d-e9793b00c042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 68
        },
        {
            "id": "48c24664-940b-4de9-beb4-1ed73fcd59c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 69
        },
        {
            "id": "65e3d9d5-669a-4921-b170-df4076827ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 70
        },
        {
            "id": "bd68566d-3dee-4d92-bb04-3f4bc7903f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 71
        },
        {
            "id": "b25b3b9d-7b66-415d-95a4-fd98b141d1b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 72
        },
        {
            "id": "9a078965-dc01-4109-b8c7-9b90ddeca5fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 73
        },
        {
            "id": "94ba76fa-8d50-49b6-befd-5992916ba888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 74
        },
        {
            "id": "283d9e8a-7f9d-4333-b43a-7b6ab70424c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 75
        },
        {
            "id": "ff684b9d-1dbe-4b65-9ec5-27f2d4be6f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 76
        },
        {
            "id": "eb28c207-78cb-49b1-a1cc-683b0e08c983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 77
        },
        {
            "id": "29358d14-a93b-46cc-8109-ed83a8dadbb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 78
        },
        {
            "id": "44666dd4-be48-4b11-a2fe-be4c92e0e256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 79
        },
        {
            "id": "e1c2ce02-ee0a-4170-a1c4-efc511f3b923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 80
        },
        {
            "id": "3501eb8c-6425-4aaa-97b3-7db7618354f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 81
        },
        {
            "id": "39e03e44-55e5-441d-9ace-2724bf2eaf7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 82
        },
        {
            "id": "beba469a-b783-45d3-b888-e3d707398ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 83
        },
        {
            "id": "e9d30550-35ca-4328-be3a-44193c7e46f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 84
        },
        {
            "id": "c749b6b1-3647-45ca-a1fd-754368bf1697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 85
        },
        {
            "id": "b1febb47-fe0b-4e08-a2b2-cb578e69eee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 86
        },
        {
            "id": "b75361db-d002-4599-b55c-a01f660217a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 87
        },
        {
            "id": "ebc0cb15-db88-46e6-ad71-c40a3ec7097f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 88
        },
        {
            "id": "7fd25368-ce75-4123-8204-381c8da3cd5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 89
        },
        {
            "id": "b76fb63a-bbdf-44f7-8064-d8cc95eadb9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 90
        },
        {
            "id": "118ef5b6-07e0-4852-a75d-8c13687a2e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 48
        },
        {
            "id": "a088ffea-1c1e-49ef-a719-3bc30fb87a1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 73,
            "second": 49
        },
        {
            "id": "69fab6ed-ece7-46d7-83c0-641a62cd8381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 50
        },
        {
            "id": "723a43d8-9a17-4009-a130-d85a12be64ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 51
        },
        {
            "id": "014bf7b1-7d2d-4eb5-9b39-269ed5e02fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 73,
            "second": 52
        },
        {
            "id": "99d83e64-7c2b-4170-a680-b7fa556cc874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 53
        },
        {
            "id": "d40ef7da-7873-4077-ad09-35ceaba637ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 54
        },
        {
            "id": "73c47271-5036-4e10-ba70-a5c6632484c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 73,
            "second": 55
        },
        {
            "id": "3d1f4948-5a51-4af8-8fae-7e1da14936d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 56
        },
        {
            "id": "5b378256-5f7c-4a0c-8731-3782215e6f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 57
        },
        {
            "id": "21a96ceb-f346-49ea-aac5-dc5d40f44911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 65
        },
        {
            "id": "44a88d98-bf33-497b-8665-961f780305b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 73,
            "second": 66
        },
        {
            "id": "35bd0515-b2dd-4401-bdfb-6e276b8106f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 73,
            "second": 67
        },
        {
            "id": "c1378264-ed17-49e9-ab82-73440ef36d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 68
        },
        {
            "id": "2ba9e759-d3be-41a1-b882-afc35147519d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 69
        },
        {
            "id": "eb74e6f8-41a7-4032-ac98-0209fcc17fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 70
        },
        {
            "id": "46f5066e-6a77-4406-8a8c-4a1e8050604b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 71
        },
        {
            "id": "60d900a9-db19-4776-af11-b6a164f2b3ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 72
        },
        {
            "id": "f82fe749-8e6f-42b2-9231-65cc7265212b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 73
        },
        {
            "id": "1c294349-8c9e-4ee2-ae23-77f3098e1262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 74
        },
        {
            "id": "e8bef8a3-e51f-4681-8676-2866a92c4f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 75
        },
        {
            "id": "55f6e613-d217-4588-b1a3-db91c47cacfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 76
        },
        {
            "id": "7ee28c3a-e06e-4de5-a41e-acd3a507ea93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 77
        },
        {
            "id": "e1607cf7-303f-4f9a-8a12-ab30db3b1dc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 78
        },
        {
            "id": "ffb1fd28-8634-4ec4-9473-1ba8359e2e1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 79
        },
        {
            "id": "5749b2ce-69ac-4dcc-b78f-5818cbf5247c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 80
        },
        {
            "id": "af4cc434-6a37-4bfa-b7aa-b523ecaf2b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 81
        },
        {
            "id": "e39e4836-d821-4b8f-b454-d3b0343fab85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 82
        },
        {
            "id": "d6531341-2747-4fc7-85d3-929a38da930f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 83
        },
        {
            "id": "479ad5e0-dfe1-45a8-9dad-275f735777f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 84
        },
        {
            "id": "39ce5f4a-fcc3-4bcf-8c30-3d622e1f3521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 85
        },
        {
            "id": "2263a7fe-57d0-44c0-a983-5ad50c880619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 86
        },
        {
            "id": "89d72d35-29d7-4bb3-b228-f7230de09ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 87
        },
        {
            "id": "b2a47939-9270-4ab6-a885-775f2451fd49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 73,
            "second": 88
        },
        {
            "id": "4cbd9cc4-378c-4d34-b107-03c2f45399d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 73,
            "second": 89
        },
        {
            "id": "cbb3f533-8853-4f71-bb3e-6d2282cb67f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 73,
            "second": 90
        },
        {
            "id": "48122d4d-27c6-463b-9061-97824ae673dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 48
        },
        {
            "id": "0f8642b8-a68d-4784-9386-3d9d4e1d1783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 74,
            "second": 49
        },
        {
            "id": "ad4449c1-f953-467f-b85b-7e0e8d884e18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 50
        },
        {
            "id": "c8d18352-8fd6-4554-804e-ba3249f4d86c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 51
        },
        {
            "id": "e66be5eb-7a4c-43e9-967a-bd7cb27d2fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 74,
            "second": 52
        },
        {
            "id": "3cfeeae3-5f77-4330-9884-7477bcadf30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 53
        },
        {
            "id": "cce59c7d-0655-4716-821a-7a913955b5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 54
        },
        {
            "id": "e51b9b89-05f1-4192-a71b-9dad9b54fce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 74,
            "second": 55
        },
        {
            "id": "d9cb773d-31a6-4f42-8294-232087def84c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 56
        },
        {
            "id": "377f6da7-dea1-4602-82c2-81ce5f3dc90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 57
        },
        {
            "id": "d1f2c3a7-faf0-4f1b-a981-aa30f5585858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 74,
            "second": 65
        },
        {
            "id": "67d9395e-3a4a-4562-b17c-ae2d315e7423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 74,
            "second": 66
        },
        {
            "id": "74e58255-3d8c-4ace-aa76-1d11da60b7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 74,
            "second": 67
        },
        {
            "id": "fd6e4496-e834-4766-b8a3-859fa0f05028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 74,
            "second": 68
        },
        {
            "id": "af83e0cd-f208-40d8-80e3-39e3ec0a1547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 74,
            "second": 69
        },
        {
            "id": "86d07c89-d337-498d-9933-f652a559bac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 74,
            "second": 70
        },
        {
            "id": "05a3c425-3157-4b9a-bd30-d7f746125e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 74,
            "second": 71
        },
        {
            "id": "16e15c31-3228-4622-a863-682c83f96758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 74,
            "second": 72
        },
        {
            "id": "32443a68-d2c7-49e1-a381-3a899ffc1653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 73
        },
        {
            "id": "ce086b49-579c-4ec6-b550-fb9e29974264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 74,
            "second": 74
        },
        {
            "id": "ccd91333-39bd-42d9-8e7b-f4389dae768f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 75
        },
        {
            "id": "fb4af0ae-3503-420a-aa2a-0711c03f4697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 76
        },
        {
            "id": "ad38044c-f9da-4339-b073-894081cb7312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 77
        },
        {
            "id": "d2cd2ca3-e7c4-413b-b7d6-4ebf51a213a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 78
        },
        {
            "id": "71ec76ef-f433-4ab8-8371-fd88fbcbefdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 79
        },
        {
            "id": "aa642809-fc9d-43f2-8f4e-1a8d2e627d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 80
        },
        {
            "id": "acbcd3a7-c005-4f96-a24f-80d697887c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 74,
            "second": 81
        },
        {
            "id": "297f0775-cbd7-47cb-af92-f1d11b7382af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 82
        },
        {
            "id": "58b946e9-37f4-4b7f-a67d-4753de2992cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 83
        },
        {
            "id": "074855be-d5d5-4e7f-a8b7-bf3f28dbbb34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 84
        },
        {
            "id": "2522f48c-0718-49a5-9793-a6bade9d2057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 74,
            "second": 85
        },
        {
            "id": "52a9b1c6-2a1d-4700-acef-0e77aa862373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 74,
            "second": 86
        },
        {
            "id": "be75a22f-ee7c-4e0c-9a3a-54eceb6d8ee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 74,
            "second": 87
        },
        {
            "id": "0bfcb367-ef9e-46a4-9a9e-762de0471a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 74,
            "second": 88
        },
        {
            "id": "02ca3e53-1e7c-44f5-9490-aad5b8bb501d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 74,
            "second": 89
        },
        {
            "id": "1dbd79de-27ca-4f7c-9ea0-cf8c65744170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 74,
            "second": 90
        },
        {
            "id": "c4ea5b3c-7868-49ab-92f4-f2696e8d894b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 48
        },
        {
            "id": "6094315b-d66d-4ba1-9550-fb76bd6964e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 49
        },
        {
            "id": "8b8fa301-2667-4038-8ec3-5073131d35ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 50
        },
        {
            "id": "97b508f3-55f1-40bb-9f2c-e5ce14c8977b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 51
        },
        {
            "id": "84d26ef9-f88c-48ad-b0c1-cd0482346e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 52
        },
        {
            "id": "a676093a-82dd-4762-86cf-94034220c78a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 53
        },
        {
            "id": "9664117a-f26e-4a4f-bddd-0804e01f4e5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 54
        },
        {
            "id": "31f59f09-ea89-435b-8578-6c85efc2b146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 55
        },
        {
            "id": "de7ffe40-dfaa-47af-a7c3-c3bf5475471a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 56
        },
        {
            "id": "d99e00f7-9422-4e61-a29b-bf0c845ea1c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 57
        },
        {
            "id": "5a66f5df-78e9-4b71-b5a4-80690fab39be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 65
        },
        {
            "id": "4dd49931-0c6c-4ab5-988a-ff7ac93931bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 66
        },
        {
            "id": "c86eb174-5b8c-4cd4-aca2-baded145677d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 67
        },
        {
            "id": "909785f8-fa46-49f6-b39a-cddd05a8ea71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 68
        },
        {
            "id": "626bb5ac-0543-4ec7-b8e6-1b42ef62c4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 69
        },
        {
            "id": "21af90cb-f628-4942-8d7d-21af9a23b271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 70
        },
        {
            "id": "da132ff2-b150-419e-b006-03a7c12eb5e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 71
        },
        {
            "id": "2a25887e-2d98-483c-aa5b-e97f6284cb3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 72
        },
        {
            "id": "4ab308b3-870d-4d61-be01-4cabcb73c1d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 73
        },
        {
            "id": "821b7891-d181-4c5d-ad95-b4b7a5f8d9b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 74
        },
        {
            "id": "ad229c59-e0de-4f77-9bdf-d7d1df070320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 75
        },
        {
            "id": "61371c56-c027-44fd-93a6-14d98511de29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 76
        },
        {
            "id": "bd1bfe4d-f90a-4de6-a879-340684222c7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 77
        },
        {
            "id": "2f46b14b-77ad-42fb-a01f-b1c641a0c890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 78
        },
        {
            "id": "eccb0c71-6906-4c97-9221-6ba8ffb0796b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 79
        },
        {
            "id": "3b16b2a5-00be-48b0-b7ff-05a0f9c95b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 80
        },
        {
            "id": "85d57491-bf02-464b-830d-2b716416ef2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 81
        },
        {
            "id": "b769c478-2230-42d6-9687-cc306a054a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 82
        },
        {
            "id": "ffa40659-830d-44d7-96c5-0d7356504f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 83
        },
        {
            "id": "d3ae1ecb-bd93-4619-b08b-74f0924957cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 84
        },
        {
            "id": "6512aa21-b44d-4f38-a096-18b8780330ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 85
        },
        {
            "id": "930e965d-df77-403c-bdf1-b89e2f3172cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 86
        },
        {
            "id": "70fa8bee-e078-49a2-8091-76ed62dc7aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 87
        },
        {
            "id": "154726ed-2ffe-4d16-8b99-5fc0de07e6a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 88
        },
        {
            "id": "1e9fe48d-2e1a-48e2-8ba2-7ce8ae788b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 89
        },
        {
            "id": "9eada190-da68-4e89-a80c-0e2013367cbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 90
        },
        {
            "id": "630d385f-2131-4961-b984-b54e85bb72af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 48
        },
        {
            "id": "f2b7a1e3-4204-4b7a-a722-196973e9edf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 49
        },
        {
            "id": "95537c65-8615-4bd4-b68a-d69971696585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 50
        },
        {
            "id": "ba97ab25-612b-4fb7-af7f-637f7cddac16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 51
        },
        {
            "id": "67b7898e-a9f0-4768-9401-bf95a6730844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 52
        },
        {
            "id": "a076dd35-b2e0-4736-a0fb-58bd60ab33fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 53
        },
        {
            "id": "4ed7a809-a70e-41dd-b9ed-a771b0268c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 54
        },
        {
            "id": "e044125c-5086-47d9-9770-4b9f7e355c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 55
        },
        {
            "id": "ec87900c-14f9-434c-817f-0ebbb59f17fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 56
        },
        {
            "id": "2a0c1901-d9b6-4989-b39a-cbe503206dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "1da086d4-506f-4da5-b2d6-c58cdae8ac13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 65
        },
        {
            "id": "54263bc9-0669-40b8-be41-0ba6329749bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 66
        },
        {
            "id": "7f346e27-54c4-4fad-9fca-2f467591d38f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 67
        },
        {
            "id": "175b644b-a9a9-490e-a72e-ddebcb94a820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 68
        },
        {
            "id": "d49b88bb-912a-448d-9803-d8c9548be4d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 69
        },
        {
            "id": "0bba14c0-06c8-418c-aa4e-3745901c96f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 70
        },
        {
            "id": "3c8e30e7-cf36-415f-a123-d436ffebabef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "759cae02-4cab-41bd-a4e3-5586ec7f34e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 72
        },
        {
            "id": "c51fe9b4-47a5-481e-b53d-3f9a75e868d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 73
        },
        {
            "id": "36f2f058-84d0-4534-aac0-ebd314ac04f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 74
        },
        {
            "id": "e446b78a-8b65-4195-a189-d9a57bbfa7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 75
        },
        {
            "id": "a7e57ac9-ad03-4fba-ac1e-aec5eb211ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 76
        },
        {
            "id": "36347d07-7ddb-4a19-940f-3f1cb20c38ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 77
        },
        {
            "id": "c7dfe7e4-91a3-472e-badb-dbdaf0a061d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 78
        },
        {
            "id": "ef3c4bb8-f261-4bde-83e3-229bea3cbd76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "b6ad5612-f5a3-4521-be24-af6371d08cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 80
        },
        {
            "id": "f726d4c3-88c9-4efe-b08a-77f10fdfc65a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "7e51f08d-f767-454b-a4e1-7f4eff1559fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 82
        },
        {
            "id": "b104f3ea-fb7a-41af-88e8-731f388cbd76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 83
        },
        {
            "id": "e1c61ef7-aae1-4415-b981-f230a93d89a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "b7467b9f-4c46-4cf9-87d1-0b3fcca12858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "673f692a-d66d-49ab-9860-f0d5ca062b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "e8419392-472b-4cb3-882d-f184786fe9c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "341f9041-e0a3-4158-bb03-a004947177ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 88
        },
        {
            "id": "76dd4012-5f1e-41b1-a69b-35492e2d9604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "2d9c48ae-4432-442f-94dd-7df4e4e7917d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 90
        },
        {
            "id": "3a17bc0a-47fc-4ec9-8cd3-68b4245998ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 48
        },
        {
            "id": "976d9304-4252-4e17-8f2b-62133204f280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 49
        },
        {
            "id": "d680b405-f5bb-459b-9d02-4070ca343b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 50
        },
        {
            "id": "5ab082f2-3988-46d8-a034-717ab0851e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 51
        },
        {
            "id": "0d20fbe2-8cde-447c-aecc-f09c48d31ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 77,
            "second": 52
        },
        {
            "id": "244da803-eede-4586-adb6-912ff33541ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 53
        },
        {
            "id": "af24a7ea-b9bc-4992-82d8-c8062e047eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 54
        },
        {
            "id": "94e0296c-3fe8-498f-8185-ab88784567e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 77,
            "second": 55
        },
        {
            "id": "814a8fda-17ed-4f43-b09c-ca3b04587bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 77,
            "second": 56
        },
        {
            "id": "a03c5e09-6120-495a-b3ae-ab093e8b1607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 57
        },
        {
            "id": "184af5a4-3802-43fe-ac04-409933a77f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 65
        },
        {
            "id": "b2c49573-94cf-49b4-aafc-52536d97e824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 66
        },
        {
            "id": "a884c6e7-eb28-4f2f-88a0-bcf7bbeac4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 67
        },
        {
            "id": "b585deae-3521-4647-a94c-19358bbd5300",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 68
        },
        {
            "id": "47f139e9-4606-446f-ad8f-30ad5816cd1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 69
        },
        {
            "id": "b4725abc-e27d-43ef-b9a3-92e5cda5ddda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 77,
            "second": 70
        },
        {
            "id": "4bd57e72-42d1-407c-9374-a3b549aa42d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 71
        },
        {
            "id": "47208d04-e155-4fed-872b-e0d794d02a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 72
        },
        {
            "id": "a55467a5-f93a-4d9b-89c2-2f8aa3c42ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 73
        },
        {
            "id": "8d0444fd-0716-4be0-b55b-0ab30aacffc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 74
        },
        {
            "id": "c5204e17-f853-4a1d-982a-0b7d2895bdb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 75
        },
        {
            "id": "d46e5962-27c6-44fa-b7bc-c99e7e844734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 76
        },
        {
            "id": "e28a7be2-e629-4173-829a-ac80b669a06b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 77
        },
        {
            "id": "9bc733d6-79a0-417e-aa70-cff14e689467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 78
        },
        {
            "id": "39e7ffea-4c9b-474a-9127-4e5b6aa7b91d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 79
        },
        {
            "id": "a56decd4-2d37-4177-aef4-56fbaa5fe5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 80
        },
        {
            "id": "9347057f-0c45-4703-80dc-8a3bce946407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 81
        },
        {
            "id": "f9e6e87d-874a-4af3-8d34-a3b685e735e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 82
        },
        {
            "id": "7fe8bf78-c52f-465f-8402-2f64b80c6d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 83
        },
        {
            "id": "9e95291a-7891-40c9-83a1-8dfab2771120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 84
        },
        {
            "id": "b7589a5f-391c-4a61-a4b2-96763a5bdb24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 85
        },
        {
            "id": "33d474d8-c266-42c9-952a-07a143b44737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 86
        },
        {
            "id": "1e7e4dac-78f4-4352-89bc-5e8c403d702d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 87
        },
        {
            "id": "97478c81-a003-4220-9fd1-3c1a1302bc07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 88
        },
        {
            "id": "f7f8a44a-3e03-4e6f-ab2b-b3a8ad3d6b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 89
        },
        {
            "id": "e003bf47-4d77-43fb-ad92-8b4678bad9fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 77,
            "second": 90
        },
        {
            "id": "03c9ea58-77d7-479a-898c-bd15dc8111c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 48
        },
        {
            "id": "57dff237-16ba-45d5-b1f8-0595a8209a4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 49
        },
        {
            "id": "b31bfae2-1e4b-413b-aa8a-91d170dd361c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 50
        },
        {
            "id": "c1b388c3-53de-4974-a56d-ee1951b8f92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 51
        },
        {
            "id": "4278a033-8acd-45e0-9e3b-0407a98b74f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 52
        },
        {
            "id": "5aacfc0d-5f76-490c-939f-186944f296ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 53
        },
        {
            "id": "b2b7f0d3-734d-4662-a00c-8a20e07f246f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 54
        },
        {
            "id": "b69d4c61-f007-4745-a59a-d87963c880f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 55
        },
        {
            "id": "217adeeb-f9ea-413a-9334-e2b70f92113b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 56
        },
        {
            "id": "bde5de3e-cdef-4a88-9141-4eaa3e691975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 57
        },
        {
            "id": "f6cc3ef9-f24a-4272-8d55-83b2623c3a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 65
        },
        {
            "id": "ee9176ff-64e4-42af-b0f0-586ce5c52d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 78,
            "second": 66
        },
        {
            "id": "aec96126-f7f3-414f-b339-d616c76f3895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 67
        },
        {
            "id": "1df609e0-c166-487b-ba10-b8d5836fb55e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 68
        },
        {
            "id": "9dc0e765-73b5-4d29-bd1a-bb104a0ede68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 78,
            "second": 69
        },
        {
            "id": "1c5b3f2b-1c86-44ef-8db1-56fcedffe584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 70
        },
        {
            "id": "082a175d-c71b-444a-93ee-fb6965d343c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 71
        },
        {
            "id": "a5c78021-f3ef-4e31-8e56-a25ba0a281f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 72
        },
        {
            "id": "2f714902-2fd6-40ab-8810-59f8ce1f74a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 73
        },
        {
            "id": "4e615e8c-7235-4643-b2ee-0b2eb0135536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 74
        },
        {
            "id": "96e1108d-7e82-4928-8cb8-7732cec45a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 75
        },
        {
            "id": "5c3d74eb-3e2a-4f35-a323-47a5d3debacf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 76
        },
        {
            "id": "d55a3340-c6e9-4b55-851b-3118b444a018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 77
        },
        {
            "id": "f020c2df-c705-4eba-aeb8-b9a64412a72d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 78
        },
        {
            "id": "119e3cdb-4ec8-4798-96bd-2ff597a26971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 79
        },
        {
            "id": "4467ec99-77db-4c6a-8e29-1dc67de832db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 80
        },
        {
            "id": "493f278b-6249-4610-863c-2c32f6ea1d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 78,
            "second": 81
        },
        {
            "id": "d9d81068-2574-4aee-b017-5365d0a76fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 82
        },
        {
            "id": "a6aac375-3c74-4339-9412-32ecfdceb9bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 83
        },
        {
            "id": "06253952-7bb9-4e25-9f47-fed83050d7b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 84
        },
        {
            "id": "c66b7dde-610a-48e7-97d4-c0abb4d7cb42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 78,
            "second": 85
        },
        {
            "id": "039b9db3-b75e-413a-8438-ae0d2d75d908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 86
        },
        {
            "id": "651ddf62-1ffe-4326-9f8f-14a36ce7d977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 78,
            "second": 87
        },
        {
            "id": "b5890c0f-fc95-4100-9fe3-bdee46ea2b6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 78,
            "second": 88
        },
        {
            "id": "9046a972-c9f2-4fa8-b6dd-a02b050f0731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 78,
            "second": 89
        },
        {
            "id": "d31cf75f-3e5d-4f0e-9ae8-209680db27cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 78,
            "second": 90
        },
        {
            "id": "32b98f80-935a-4036-a7ce-66fee62b9101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 48
        },
        {
            "id": "72008207-c66c-4822-a093-525cc9dc5d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 49
        },
        {
            "id": "0dc62e5d-da6a-4164-9420-5257d7145799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 50
        },
        {
            "id": "b9a8341f-bca1-4a97-ad31-4942c26930ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 51
        },
        {
            "id": "0e153ee2-e582-4d0c-9162-829e8888e12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 52
        },
        {
            "id": "f8191aa8-1587-487a-827b-f7c040cd3792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 53
        },
        {
            "id": "babc93c2-6f29-4bc3-9b6f-bcb85fe76c4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 54
        },
        {
            "id": "15e838ef-9e5c-44f3-982b-68a1cd7e229a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 55
        },
        {
            "id": "ffed1824-337b-4f8f-bf47-4ee543dba26a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 56
        },
        {
            "id": "b430fb7f-d647-4879-971f-8ad7c782f7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 57
        },
        {
            "id": "97e6fec3-2fc6-4dc0-a0e4-90a77c882978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 65
        },
        {
            "id": "3cf2ba7e-3d1f-4c0c-8177-adb3fceacb78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 66
        },
        {
            "id": "8d4df1dc-355c-484b-90d8-0a4b9fb8d487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 67
        },
        {
            "id": "56bbf5f7-0e1b-4d20-8a58-2aaeb74dfe9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 68
        },
        {
            "id": "ecdb0f42-fb9a-4aa1-8002-67d496031ed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 69
        },
        {
            "id": "878e68ec-ca69-42bd-bff0-e40012df6e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 70
        },
        {
            "id": "c4440327-c4a1-4411-a53e-5d69a4fc3624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 71
        },
        {
            "id": "6466913e-c2b6-4464-a8ca-84fa0689cb9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 72
        },
        {
            "id": "c77c9d89-d648-4ece-b398-719f0bb9061a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 73
        },
        {
            "id": "51a85412-1009-49df-a3bd-ec792f878472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 74
        },
        {
            "id": "36c41f19-2b61-4c97-92e5-87d54a0f24af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 75
        },
        {
            "id": "f1e447db-dde5-4b38-a44f-781714b000d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 76
        },
        {
            "id": "41f9c98b-d214-4118-a851-8b0207973fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 77
        },
        {
            "id": "4d115c74-9636-4fa7-b578-ee54ef2f5e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 78
        },
        {
            "id": "0ee295e4-025e-4b34-8f49-c1cb504b2f7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 79
        },
        {
            "id": "7a8bad6d-901f-41cc-a3c0-edfc2c0ddca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 80
        },
        {
            "id": "00e0908c-0d4b-487a-ae28-aed6cec32576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 81
        },
        {
            "id": "f9fa4835-ceca-4235-9d5e-100f22012de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 82
        },
        {
            "id": "81fddbfb-ba7a-45ba-a635-3b34da3c8fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 83
        },
        {
            "id": "44880a08-d5b8-479f-a93a-bc734a944fed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 84
        },
        {
            "id": "5177b88c-8e56-4e7c-a995-8e86735854ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 85
        },
        {
            "id": "2159e4ff-56cd-4ebf-b925-9a5b86e38dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 86
        },
        {
            "id": "0601780d-bffd-4fd4-8d89-ebe3a0059408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 87
        },
        {
            "id": "b3ab5cab-13c3-4e03-9fb4-8ce33f0802f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 88
        },
        {
            "id": "8d8198b5-eb99-4bab-b6bc-60c73c841116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 89
        },
        {
            "id": "21fae827-4bc2-4e1e-b20d-963e46c5a487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 90
        },
        {
            "id": "ca06fa81-dc1e-4110-a7d2-5695af97a3d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 48
        },
        {
            "id": "7a87a8dd-9b3f-43a5-8811-76ec174b52d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 49
        },
        {
            "id": "9c611604-81af-4e51-8313-a18d58ce6da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 50
        },
        {
            "id": "c39bfec9-93df-452f-ad5b-a381e690bfa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 51
        },
        {
            "id": "4bafda03-044c-44a3-90cb-cbf5d9afb9ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 52
        },
        {
            "id": "0059b4e7-71a1-48f9-92a5-1ab11bd7a35e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 53
        },
        {
            "id": "77140f2e-2b67-4d90-be3b-9a6ba17890eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 54
        },
        {
            "id": "b42a3199-0385-4be4-acc4-b88977597c59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 55
        },
        {
            "id": "8240abe0-aca9-4c5f-a85f-c7adc49f4887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 56
        },
        {
            "id": "e05d86f7-c4a5-4631-b404-0aaf33b70dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 57
        },
        {
            "id": "6789f05a-cbf4-459e-9a98-f7aa97c3ebe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "b16eace1-f35c-455f-a8be-490a39ebd9f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 66
        },
        {
            "id": "34aa7935-8661-419f-8e83-c6e75bb33605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 67
        },
        {
            "id": "09985c10-8fda-4fb2-88f3-6a83931f914f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 68
        },
        {
            "id": "90acf62f-f8d2-4885-b507-00f240f8fb35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 69
        },
        {
            "id": "235386f8-f592-4671-9503-2be9a39a1fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 70
        },
        {
            "id": "ce3483ea-1d2f-4c97-8d0a-5f9eac9817e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 71
        },
        {
            "id": "65aa1560-b2a0-48c1-9477-efcd72873af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 72
        },
        {
            "id": "23a58335-9863-496c-8783-92c73bc3230c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 73
        },
        {
            "id": "17d4b6e0-5b24-49e2-82cb-ab7cb71ff0e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 74
        },
        {
            "id": "92002da3-97b0-4ad6-8262-49969da13056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 75
        },
        {
            "id": "2fe96496-1faf-4530-b44e-e65d11a5bcf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 76
        },
        {
            "id": "9c5c42a0-a334-4fca-b2f4-d1d0a2652363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 77
        },
        {
            "id": "2cb6f54e-5718-4308-bb3f-69d633e1f8b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 78
        },
        {
            "id": "8509d740-043f-4246-a176-99d2d8225c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 79
        },
        {
            "id": "b411ffdc-5880-4af5-b7f5-3a6413303802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 80
        },
        {
            "id": "d8c52f53-2911-4fd1-9019-4b12318adc94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 81
        },
        {
            "id": "f5485559-d06d-4206-bcfa-713c0aed2b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 82
        },
        {
            "id": "0fe379c9-7fea-4597-9f4c-2869b36d7d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 83
        },
        {
            "id": "ff2a9dde-0244-4d33-a89b-6f83c2ca57d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 84
        },
        {
            "id": "904f0fb6-f6d7-4639-805c-ab158d141196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 85
        },
        {
            "id": "fc93c5c8-51f2-47da-906c-2e140c6f76d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 86
        },
        {
            "id": "0ff119b0-9c37-41d2-848e-067fafe61ece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 87
        },
        {
            "id": "e83d462c-71f3-4d8b-a8a8-02a495537f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 88
        },
        {
            "id": "0e32d8df-d73a-4fd5-94ea-7adce7245cb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 89
        },
        {
            "id": "114e3e1b-1bd0-484a-a371-51b1d4096b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 90
        },
        {
            "id": "3859204e-98f6-4e83-ad68-dc40fcb0dc47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 48
        },
        {
            "id": "8f561ffe-ee3d-4a19-a4ed-ce97416c3822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 49
        },
        {
            "id": "53fd4ab8-dfb0-4ca6-a49d-6abc0b7f71c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 50
        },
        {
            "id": "2a7ecd1f-8368-40f4-93bb-0b37b911b33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 51
        },
        {
            "id": "bc877815-9074-4259-9bcc-07f8afe4dbec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 52
        },
        {
            "id": "6d35b8c0-d1b8-4b82-93a1-337b369568e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 53
        },
        {
            "id": "7e726bf4-33d8-4b3d-b39b-a7264116ef84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 54
        },
        {
            "id": "7396f18e-729b-4c41-8219-ab7770869289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 55
        },
        {
            "id": "2d4a09c3-cebb-4efa-ba65-5aa2b3447bf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 56
        },
        {
            "id": "92604d1f-6cd7-446b-a5c1-a19c8c90c41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 57
        },
        {
            "id": "33d0e822-7ee1-498d-85f4-07d7cd64ed11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 65
        },
        {
            "id": "b7ae0640-b090-47f1-80ee-fbe05e38d251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 66
        },
        {
            "id": "e7529d3e-2425-425e-9255-8d38574c558c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 67
        },
        {
            "id": "9cae9eec-554a-4e30-bf00-e48ec08e9774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 68
        },
        {
            "id": "87a62fff-cc63-4815-a122-52207e03d187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 69
        },
        {
            "id": "27de2847-88b2-483f-9950-00bd13a179d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 70
        },
        {
            "id": "6c3666fb-6bfa-4e16-976c-cebfa4736a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 71
        },
        {
            "id": "5ad217c5-ee0e-431b-b952-5ce287499034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 72
        },
        {
            "id": "e4fbae25-34ef-4ad3-a9f4-d4d0f900f592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 73
        },
        {
            "id": "a108135a-9402-45ec-9b02-e7d44b3068e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 74
        },
        {
            "id": "45df9ccf-b4c0-4f22-850a-bcb273420971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 75
        },
        {
            "id": "713f0f83-200d-4aaf-b7d3-68a85e0ad023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 76
        },
        {
            "id": "8b07bca9-6f2d-4381-beae-63a048d48346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 77
        },
        {
            "id": "8abfdb84-6fab-4e9c-a05d-2873b07d9ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 78
        },
        {
            "id": "4fb70446-69a5-4038-92c8-908b865a9ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 79
        },
        {
            "id": "65f4c33b-4c63-4ab5-8483-ee3eb000f115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 80
        },
        {
            "id": "0b1afafa-941e-4f41-a72e-012645005c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 81
        },
        {
            "id": "8e901397-5349-4e30-a864-dbe2f1fe48ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 82
        },
        {
            "id": "b795bf4e-634f-4cd5-9eaa-6516be85f2bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 83
        },
        {
            "id": "60ecad4c-ba24-4c41-88b5-9c3cd11d301e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 84
        },
        {
            "id": "e1481ba9-aa85-482e-ba11-e8a45ada699d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 85
        },
        {
            "id": "81a2338c-00f7-4b6b-93f8-089be7385435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 86
        },
        {
            "id": "09815833-ffc9-49f3-9c53-442d986548fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 87
        },
        {
            "id": "049e2513-6b6d-457b-89be-4fb08e1b8bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 88
        },
        {
            "id": "6633b470-401b-460e-b15c-be05e4314da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 89
        },
        {
            "id": "befb5351-e882-4f0f-bd7f-b2c4a13924ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 90
        },
        {
            "id": "c13e4e82-c61e-4035-8187-b90c274e8e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 48
        },
        {
            "id": "7d22295f-4490-4af1-8646-e3e7516a386e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 49
        },
        {
            "id": "e61e1576-e4df-4c3f-9d27-cba0ff255918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 50
        },
        {
            "id": "210050d1-6216-4a98-8d4f-dd4dbf387ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 51
        },
        {
            "id": "66777ed0-3002-4b54-a127-2c8187af6dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 52
        },
        {
            "id": "c7bd0066-78ce-4db2-b518-c14e0a962fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 53
        },
        {
            "id": "1e9d1790-4cdf-4a98-b592-296eb124aaa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 54
        },
        {
            "id": "151de6f8-bdda-4190-9c36-e513587c012c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 55
        },
        {
            "id": "3c44cc23-9287-4e64-855f-76d558fdc76a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 56
        },
        {
            "id": "37b8c417-3fe7-4468-bb67-73110388656d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 57
        },
        {
            "id": "64b4c907-db99-41da-94ec-e6707db9b4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 65
        },
        {
            "id": "df509512-5b6e-4c7f-89c7-ac250c96d725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 66
        },
        {
            "id": "0d31be51-15d4-4c40-b4fd-a06b269e7233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 67
        },
        {
            "id": "110c225d-18c2-4cd2-a871-9ad6ca12cc9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 68
        },
        {
            "id": "ca8fa9b0-f5a0-41d4-8124-6b9fed987598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 69
        },
        {
            "id": "e66144ca-047a-4cd5-acc1-05bcb88deeec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 70
        },
        {
            "id": "bf231a8f-388c-434f-b3db-fdad4ea2e731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 71
        },
        {
            "id": "8d46b330-8ad2-4094-9d96-ca73ed14141a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 72
        },
        {
            "id": "cd0253bb-b6c4-4806-93ea-3749a6042a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 73
        },
        {
            "id": "9a03e5d6-9879-4bd7-9ef2-c9982305faa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 74
        },
        {
            "id": "e7ac01b8-72d4-471e-aea2-bbf2d23c38ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 75
        },
        {
            "id": "595e7aa2-e9c5-4672-b88e-7acf2dbe0d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 76
        },
        {
            "id": "ca1e4a35-00f8-4f57-bedd-80b5ecfd62d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 77
        },
        {
            "id": "bcf2135b-7597-4da3-a804-d0eb754239cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 78
        },
        {
            "id": "5ac41e9e-d13e-4dd9-9fdd-e4f6c3c8874c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 79
        },
        {
            "id": "a907d1db-9cdd-4f87-a27d-be979ba1b23c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 80
        },
        {
            "id": "553ef172-5361-49fc-b4e2-4c175f6d9eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 81
        },
        {
            "id": "5653f44d-c3cc-4302-bafc-df744fb2a180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 82
        },
        {
            "id": "9cf75c72-8d06-413a-bd81-6b8bccda0009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 83
        },
        {
            "id": "92aada2f-7346-4418-a7ba-c480b102bde1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "46ad050b-dc4b-4bd5-aad4-5d4aebb6c880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 85
        },
        {
            "id": "8eb5b5be-7581-46e6-bb16-a2591c02f94b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "58f4d495-ef74-4d76-a1e4-8f595d0de910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 87
        },
        {
            "id": "b140077b-86ea-473b-9154-e4c9047be2ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 88
        },
        {
            "id": "007f631b-aa7d-43e1-a2c1-2c2f0ef6c7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "791a0650-aa81-46ba-8ee4-63bdb1906a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 90
        },
        {
            "id": "4d07daad-ef86-4257-8590-60b5ea732970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 48
        },
        {
            "id": "d9a7caad-f538-4faa-babe-1934e0b16ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 83,
            "second": 49
        },
        {
            "id": "bb436990-8b43-4f24-a9d4-883828691a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 83,
            "second": 50
        },
        {
            "id": "a5250659-ae4c-495e-b41b-8df1063c807c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 83,
            "second": 51
        },
        {
            "id": "63903149-4350-4d3c-9f81-f03a48a16db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 83,
            "second": 52
        },
        {
            "id": "a5a48419-78d9-4ca1-a917-c4d700051c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 53
        },
        {
            "id": "ebc10385-a769-4484-b057-a3ec40b2fccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 54
        },
        {
            "id": "61a36649-40f0-453a-b506-194a2c51b245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 83,
            "second": 55
        },
        {
            "id": "8ae62059-7fce-4f3e-b1f2-4866fa564a3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 56
        },
        {
            "id": "e6a17f83-13ce-4e6d-abb4-7eb25f26cc82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 57
        },
        {
            "id": "7d7d657c-19c0-4572-b48a-37a1c04ef93a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 65
        },
        {
            "id": "91cbad41-5db0-4c49-96d1-36a6f33012f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 66
        },
        {
            "id": "187dcf54-a26f-47ef-9f2e-e501ab3cfe86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 83,
            "second": 67
        },
        {
            "id": "ca3011cd-c4b8-4aa2-ae5c-36c4ea4cec61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 83,
            "second": 68
        },
        {
            "id": "937a8b91-67bc-4cef-b998-484e475a4868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 83,
            "second": 69
        },
        {
            "id": "51be63d7-c924-41b5-a36f-7fc0dbafe639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 70
        },
        {
            "id": "8d0fca86-a6b0-4d52-a685-ca65cb61d92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 83,
            "second": 71
        },
        {
            "id": "55dd6e77-16cd-4444-b02c-8c971de06a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 83,
            "second": 72
        },
        {
            "id": "c51ae9d9-4988-4009-b207-1db934c02b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 83,
            "second": 73
        },
        {
            "id": "aafc250e-23a4-479a-ab03-53ed4bdda3bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 83,
            "second": 74
        },
        {
            "id": "7a2b50e8-225f-4ad4-9bbf-af3e9ad89c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 75
        },
        {
            "id": "fc6fce09-63f8-4510-a217-84ce63e871f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 83,
            "second": 76
        },
        {
            "id": "8fe5ff79-919c-4078-8b6c-34a3e65c4fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 83,
            "second": 77
        },
        {
            "id": "e413faaf-a189-400f-bfd7-2a379063b141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 83,
            "second": 78
        },
        {
            "id": "27ce0d4d-1865-4290-9d3a-4fb3ebbd453a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 79
        },
        {
            "id": "760abaf1-c933-4c8c-9f1c-107354eb7636",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 83,
            "second": 80
        },
        {
            "id": "14e096eb-7038-4556-8a54-e73659cdca7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 81
        },
        {
            "id": "28a05f96-653c-4bd8-81c0-2e2c960149e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 83,
            "second": 82
        },
        {
            "id": "a92ae448-156f-4d6a-882f-fd5104e7cfa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 83,
            "second": 83
        },
        {
            "id": "2adf4369-1c09-4fbe-956c-0577e824cb50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        },
        {
            "id": "3c94c81b-b585-45fc-9c4b-f3eb35ddc4e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 83,
            "second": 85
        },
        {
            "id": "59884f32-2154-4be4-8f70-cdfd734d89d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 83,
            "second": 86
        },
        {
            "id": "a574864d-21af-4919-96f2-ea2c573a620b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 83,
            "second": 87
        },
        {
            "id": "a1c7f564-7b1e-45c6-ae8a-996266031bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 83,
            "second": 88
        },
        {
            "id": "79fc91c5-40f2-46f8-95f4-c491046df42c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 83,
            "second": 89
        },
        {
            "id": "0187c116-7caa-46fa-bc7e-fc7329280ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 83,
            "second": 90
        },
        {
            "id": "1cf9380a-9f2a-4217-9868-3d35502b3d28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 48
        },
        {
            "id": "c43b43e8-1835-4f0d-91ef-a55ea7200c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 49
        },
        {
            "id": "182c6e3f-3ce1-474a-8650-79fdbd126205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 50
        },
        {
            "id": "46caa021-c4a9-4e8e-8fc3-d50af34f95d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 51
        },
        {
            "id": "ecf30d45-6bf5-4719-948e-9833000a469c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 52
        },
        {
            "id": "d9033bd8-b23e-4a4a-9df7-e05972927f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 53
        },
        {
            "id": "06ba94e7-be85-4d15-8e5c-a9e475f17f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 54
        },
        {
            "id": "67abcdb7-da77-4026-aa3b-7887c23d13f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 55
        },
        {
            "id": "c6080c83-b256-4d07-834c-bd99496edf5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 56
        },
        {
            "id": "b853081d-7c16-40cc-8bf9-d4fb05f425b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 57
        },
        {
            "id": "dcf77686-b7af-4abd-947a-d6791b05abb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 65
        },
        {
            "id": "4f7be93d-de94-4384-a619-5d533398555d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 66
        },
        {
            "id": "22bfbc94-8f41-4286-b7d5-2c98c88489bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 67
        },
        {
            "id": "7952de9e-50b1-42fb-aee3-6801c04ba805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 68
        },
        {
            "id": "5cd0abd4-919f-4843-9657-422e340eef33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 69
        },
        {
            "id": "5901e96e-02f0-49bb-bc07-b4de7cafcc92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 70
        },
        {
            "id": "acf1ad96-8757-40e2-93cb-90d85885ba9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 71
        },
        {
            "id": "cefbd257-98ac-472a-91b0-ec7a2f233cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 72
        },
        {
            "id": "a32cbd05-4ab8-4844-a902-1ed539d0d5e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 73
        },
        {
            "id": "4cdf3126-2d67-4dd0-bdbc-24a1b08ca56d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 84,
            "second": 74
        },
        {
            "id": "42cc06ec-bbe0-438c-9543-a3b9054a2dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 75
        },
        {
            "id": "dfcbb0b0-98e4-439d-9702-ba48a2558d8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 76
        },
        {
            "id": "57979419-91e7-4c93-91f3-b3e7690eadfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 77
        },
        {
            "id": "69ead62d-a35c-428b-a2de-3776503394fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 78
        },
        {
            "id": "6be79e53-7f0d-4e99-a140-25c1e1218fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 79
        },
        {
            "id": "06e7dfad-3327-4d6b-b480-3edff60f055a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 80
        },
        {
            "id": "bf55ff6c-1eb8-4485-99a9-96f0ff334165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 81
        },
        {
            "id": "72e16dac-b18c-4a3a-9fc4-ea0b019d8f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 82
        },
        {
            "id": "12df3b0e-70f5-4672-a100-9405fb3ee23b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 83
        },
        {
            "id": "88d5c229-71a2-4032-a6d5-063071e98d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 84
        },
        {
            "id": "e2dc350a-83c7-469c-a123-6f8b36489333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 85
        },
        {
            "id": "882cc91d-a257-4da9-8fa6-6c37cc0a42d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 86
        },
        {
            "id": "03394e05-d4f4-480d-b6a3-9859b014bd4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 87
        },
        {
            "id": "99d577b8-0868-4f60-a350-3c7dfae5b19c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 88
        },
        {
            "id": "7df91be9-9b65-454d-a55b-b71ae0f34301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 89
        },
        {
            "id": "97926098-9dce-4590-85f5-a90f739fdc52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 90
        },
        {
            "id": "58e0d92d-f393-4611-a56d-33a817e55b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 48
        },
        {
            "id": "bf05b9cd-8bd2-4f85-819b-f6e690b9fd7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 85,
            "second": 49
        },
        {
            "id": "e6d51ef7-81d4-4de6-a0ed-ca6fc81334ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 50
        },
        {
            "id": "85300871-a4cf-48ee-bac1-4053fbdd1464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 51
        },
        {
            "id": "ed35a742-2e99-45a3-b2b4-e17ffa32f27f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 85,
            "second": 52
        },
        {
            "id": "4814878f-acc8-4dbd-a38c-5582bc449a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 53
        },
        {
            "id": "b3c67149-80cc-4cdd-b93c-1f6bc9ac2c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 54
        },
        {
            "id": "1bf08345-a5e0-4b0c-8abc-175e27ef37a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 85,
            "second": 55
        },
        {
            "id": "02111cce-f46c-41c1-80c9-f55fa332a624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 56
        },
        {
            "id": "18c80ae8-5ed1-45cc-ad98-ba300248ba48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 57
        },
        {
            "id": "c4c1beb8-aaec-49b2-b230-206657c26133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 65
        },
        {
            "id": "dc1ef880-2a63-4148-a67b-2a63d9d0e1cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 85,
            "second": 66
        },
        {
            "id": "848ccd23-30fd-4818-b2e9-090c289fd5f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 85,
            "second": 67
        },
        {
            "id": "bf6eef4b-03f0-49c4-94e2-a50c9bcc8e90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 85,
            "second": 68
        },
        {
            "id": "a0f04447-c371-4372-848f-2f296852920e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 85,
            "second": 69
        },
        {
            "id": "a8fd585f-93a1-44f1-992d-500885e6e919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 70
        },
        {
            "id": "d279dc49-fd23-4eaf-b1d9-c8145fe72927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 85,
            "second": 71
        },
        {
            "id": "64a347a0-98ec-4b71-9d54-de015dea179f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 85,
            "second": 72
        },
        {
            "id": "c58359dd-9807-49ea-8639-76a86af3e71c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 73
        },
        {
            "id": "7d55b772-5834-4017-932a-f2e4432f516f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 85,
            "second": 74
        },
        {
            "id": "056fdda8-734f-4987-ae1a-532770937bfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 75
        },
        {
            "id": "e57f0e4b-dba1-4200-bb5e-0121b32b8f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 76
        },
        {
            "id": "cb830da3-35d8-42e3-9f4b-40e127f2c05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 77
        },
        {
            "id": "c3d10899-5a27-4a21-a9f3-d56adc9c8f48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 78
        },
        {
            "id": "e3eee69a-d096-4994-b7bb-7c9769e587ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 79
        },
        {
            "id": "7f77ae13-960e-4055-a6cf-733ac48a6446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 80
        },
        {
            "id": "e79be722-bcdd-4fbe-9363-5de28692521c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 85,
            "second": 81
        },
        {
            "id": "ef885218-4cd5-41e0-b56e-66217f3fcc64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 82
        },
        {
            "id": "c2601769-3c69-467b-a266-4110ffc9eff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 83
        },
        {
            "id": "4cc9253b-9d7d-417e-86b2-b49de5b658d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 84
        },
        {
            "id": "5eb274e8-b1de-4921-bf12-f360144b784a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 85,
            "second": 85
        },
        {
            "id": "2e4dc3e8-4d4c-43ad-8b3d-a3fff06e0739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 85,
            "second": 86
        },
        {
            "id": "52ba156e-7ceb-407f-9f75-353f8f2e6fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 85,
            "second": 87
        },
        {
            "id": "138bd32a-865e-47b2-b475-c92bb2e449d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 85,
            "second": 88
        },
        {
            "id": "1e2b56bb-c549-4286-a9fa-c7ff530b69e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 85,
            "second": 89
        },
        {
            "id": "f16f6f13-aea4-4165-920a-5dee5bdcefae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 85,
            "second": 90
        },
        {
            "id": "6cb6c569-9532-4d76-a707-1daa0f23c4c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 48
        },
        {
            "id": "c13d6135-8706-435a-a5c8-d43282a74bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 49
        },
        {
            "id": "1343b514-d834-4c96-a0aa-c4cb5a2606f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 50
        },
        {
            "id": "2d03144c-2615-42c4-a80e-7a693e8f6374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 51
        },
        {
            "id": "ca5ddadd-e4d0-496c-8f44-e4c7acd7130d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 52
        },
        {
            "id": "fa2fa0da-6ccd-4052-9120-e49a48e5c759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 53
        },
        {
            "id": "6b8486ce-7163-47f2-bf18-5cada85f9466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 54
        },
        {
            "id": "89082056-2920-47b9-85a3-60882ac28ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 55
        },
        {
            "id": "ad69153c-f989-40da-a956-454f6371071a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 56
        },
        {
            "id": "d4633969-cc13-445d-b681-3c765dc39a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 57
        },
        {
            "id": "72fb4515-a375-428a-b2e4-befb83079681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 65
        },
        {
            "id": "22f0fc40-b598-4097-8c90-12177b8ad146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 66
        },
        {
            "id": "7b51fc44-d4af-4751-b0ca-0b8beacd170d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 67
        },
        {
            "id": "5881a2e2-d0f6-4f91-92d1-2c57226da70f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 68
        },
        {
            "id": "0c946b67-bb82-4be1-88ae-a9e3fceaad95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 69
        },
        {
            "id": "998654ab-f91a-48d0-8b17-1583efd6fd2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 70
        },
        {
            "id": "fcf8fefc-6222-4c83-8360-d6a16a3eb699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 71
        },
        {
            "id": "da98763f-04f1-4505-9b96-2ee4bc8fc85a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 72
        },
        {
            "id": "e0ef041d-6798-481d-8fb7-f7ffa17a51ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 73
        },
        {
            "id": "55b15ad7-e7a3-443a-afd1-073a6c2d8fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 74
        },
        {
            "id": "164820c8-4a33-4f95-9a5e-0a9ba2be7f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 75
        },
        {
            "id": "fc4fb255-80f8-4555-b955-9447b0df49a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 76
        },
        {
            "id": "8b587cb9-4552-46b4-846a-1aa0b8a42709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 77
        },
        {
            "id": "caeff58e-e577-40c7-907f-424baeb68ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 78
        },
        {
            "id": "bf063c57-4a7b-4054-a36b-7e19688baf84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 79
        },
        {
            "id": "59ef7244-42fa-419e-a471-9e144aa5818d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 80
        },
        {
            "id": "8bb17bea-888d-4db8-ad53-bd4c871bf017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 81
        },
        {
            "id": "af3f63d8-1dd0-4c51-9904-f8fbe2e14bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 82
        },
        {
            "id": "f5a59316-921d-406b-981e-04080db65e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 83
        },
        {
            "id": "a0675562-4d45-4deb-af6e-267fef3994a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 84
        },
        {
            "id": "0c6105dc-0af3-42dd-8c6f-c7e0b49529c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 85
        },
        {
            "id": "094d809c-12b1-4505-af40-1e44780e0d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 86
        },
        {
            "id": "88f58baf-ceb7-4944-8340-4a604b506435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 87
        },
        {
            "id": "8d98ad11-9751-4200-9da3-e25207b6a183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 88
        },
        {
            "id": "1cf9c89a-b00c-490c-ba55-dabda1f7b6ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 89
        },
        {
            "id": "7ba4a443-7008-4a68-a2eb-32e497dfc461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 90
        },
        {
            "id": "dde1b6c3-08e0-47e5-8aef-a6587353dfef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 48
        },
        {
            "id": "bb346876-a1db-4e36-a270-5ea1a4edab37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 49
        },
        {
            "id": "fd877649-f779-42ae-85a1-e22742970b0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 50
        },
        {
            "id": "46a3d6dc-9ec2-48a9-854c-e88e38ac7ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 51
        },
        {
            "id": "1759c388-0158-476f-b338-173d218d359b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 52
        },
        {
            "id": "28ccb679-79b2-4575-9b0f-b3c297fff0e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 53
        },
        {
            "id": "f31f32de-71b5-43cb-9759-dc8f220a1227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 54
        },
        {
            "id": "7026c220-6ed2-443d-af8d-feb9ace3acd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 55
        },
        {
            "id": "f71b1764-8a05-4881-9918-9ab2b904346f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 56
        },
        {
            "id": "a4bc8bc1-083e-42c1-a11d-5ea354f93698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 57
        },
        {
            "id": "228c7fe2-e273-4ff3-8617-ae4206579854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 65
        },
        {
            "id": "d1b855c6-49ae-42cb-9351-087e011def9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 66
        },
        {
            "id": "d42dddb7-9fa1-44d5-98af-3ca613e07eed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 67
        },
        {
            "id": "2495d6b8-f5c9-4b42-86d0-7ba63eda56c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 68
        },
        {
            "id": "cde6cbf7-82a9-41c7-9089-eccdec08855c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 69
        },
        {
            "id": "019222aa-16fa-4e7d-962f-daedea27772b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 70
        },
        {
            "id": "1ac361ce-a4c7-42a6-9253-1a4a07c3dd09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 71
        },
        {
            "id": "2156fad4-0576-4d0f-a8a4-dc9c5a2cbb14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 72
        },
        {
            "id": "e0989e40-5267-449b-8671-7e68e417da37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 73
        },
        {
            "id": "6ac9e548-f02d-4c50-9f65-3a043e95f3ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 74
        },
        {
            "id": "af2c3919-676f-4dfe-8baa-ac3c1bc30f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 75
        },
        {
            "id": "d4ea2c47-43e6-4db7-bf12-08aa8d8529e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 76
        },
        {
            "id": "1eb45a2d-4a80-40d2-92d7-806a04597d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 77
        },
        {
            "id": "255f736a-793c-4015-81ee-f91364cc18de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 78
        },
        {
            "id": "09e5c6af-c8e8-4f1b-b225-c6a7e9ece98b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 79
        },
        {
            "id": "34e3ebce-5904-49af-819c-1685275b6668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 80
        },
        {
            "id": "2a915374-9983-41ad-a090-9956244aabaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 81
        },
        {
            "id": "79e932b0-02f9-4809-8bc0-1864c590f02f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 82
        },
        {
            "id": "30ad17b0-b5a6-492e-8f13-b840ebb6cb3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 83
        },
        {
            "id": "3495dc3b-c143-4e3a-beaa-319e81a8ff4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 84
        },
        {
            "id": "67c74cd4-1478-43f5-8fe2-ce46cc9b4a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 85
        },
        {
            "id": "d23981d2-b197-464d-b6a3-655e831b815b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 86
        },
        {
            "id": "5dc4d9d2-d4fe-4723-9bae-0089ff48622f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 87
        },
        {
            "id": "9babe984-de92-401b-b9eb-c2be83022b77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 88
        },
        {
            "id": "3f719b9b-1137-473d-8dcc-d00000b84477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 89
        },
        {
            "id": "6df4badd-f8ed-4fce-8903-661338ce325a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 87,
            "second": 90
        },
        {
            "id": "60dc9354-9322-4e92-9ab1-8538571ab5cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 48
        },
        {
            "id": "a026004f-66ca-44d1-a390-6f4c658f083e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 49
        },
        {
            "id": "35c74162-98b9-4021-8899-1ac7bcf683f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 50
        },
        {
            "id": "c660e26b-1aa4-4c78-bbdd-702def886b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 51
        },
        {
            "id": "13d1cc13-eeb1-430a-8110-41d1aa9555d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 52
        },
        {
            "id": "90bbf00f-1c53-49f3-8135-6f55fff0d0d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 53
        },
        {
            "id": "95dbe6d6-32a3-4d35-b7af-bae5107c712d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 54
        },
        {
            "id": "1cbf4fa8-c11b-4b5d-90b5-827c99aa54c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 55
        },
        {
            "id": "d4bafc06-6093-4441-b4b4-1432c27ca807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 56
        },
        {
            "id": "ceba149c-9019-4cfd-8fd1-6bdf65364cbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 57
        },
        {
            "id": "def43508-747f-48dc-8564-f42d888b5308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 65
        },
        {
            "id": "b5999af6-6506-4d14-8a30-65bc6c7046d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 66
        },
        {
            "id": "a2069ff3-858c-4d9b-9ae8-a13bad19f09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 67
        },
        {
            "id": "b41a9424-b01d-4023-bdc2-5c2659ae2a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 68
        },
        {
            "id": "1c30f4a3-30cc-47fe-a432-b82aba9dd427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 69
        },
        {
            "id": "7ffb8c0e-5236-42e9-9177-8d3f6ab50aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 70
        },
        {
            "id": "8e592fee-adf5-4e1c-ad68-5076b08b599a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 71
        },
        {
            "id": "1a5506a5-f1c9-4bf4-a40b-7e162768c47d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 72
        },
        {
            "id": "a1361700-7a30-4f36-8aa7-3b868f550088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 73
        },
        {
            "id": "1b0a1806-62d0-4d97-8a4e-841065f84555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 74
        },
        {
            "id": "0bd72142-db6c-492f-b526-919371d9586c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 75
        },
        {
            "id": "cd4a0681-b663-48fa-8a07-bb16c152f255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 76
        },
        {
            "id": "01f79c19-c9dd-4337-aaea-6747e22e8368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 77
        },
        {
            "id": "1cd12c1d-3750-4e31-9363-58e5400f8336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 78
        },
        {
            "id": "71ef512a-b5ee-4a2b-b85d-e04841b03a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 79
        },
        {
            "id": "ae45aa62-2e85-4f04-9d7a-1ce016e1ba68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 80
        },
        {
            "id": "ba470b19-feae-4f56-a453-8d9be1b8297e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 81
        },
        {
            "id": "af447d39-df52-4008-92d4-bb54fafff48b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 82
        },
        {
            "id": "9ddec052-af7b-41df-8f64-7d35b71507aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 83
        },
        {
            "id": "8badd991-a294-4a48-8aaf-71ba0d9303df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 84
        },
        {
            "id": "06029a79-dfc9-4568-8f3d-8a538306f89f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 85
        },
        {
            "id": "c64f41bb-8957-4b36-aa33-243ad920499b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 86
        },
        {
            "id": "a837d7e2-e2d5-401e-803a-36040860b8ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 87
        },
        {
            "id": "ea25092a-33c5-4496-b7bc-4bea2b8b2591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 88
        },
        {
            "id": "ef28da67-2e00-42a1-8976-4071cac5775b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 89
        },
        {
            "id": "ccd8a99f-bd42-44e8-89d7-19f6cc71074f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 90
        },
        {
            "id": "737dafc8-337e-4adf-8be4-1b75bbb931ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 48
        },
        {
            "id": "ddbab921-4bf3-46b2-9934-68d893732093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 49
        },
        {
            "id": "94d983a6-1c64-4b6a-b049-929f5b758478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 50
        },
        {
            "id": "d3e03052-2cf3-4ef2-85eb-df654c2476c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 51
        },
        {
            "id": "5e84e4d8-3c40-4111-bb53-993faf86e8a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 52
        },
        {
            "id": "a26cd249-d328-42c2-928f-307d3a9eda9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 53
        },
        {
            "id": "61ba064e-5362-43e2-beec-731337b5bc03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 54
        },
        {
            "id": "b41c9bf6-a3ab-4b36-90da-3b8d0494a0bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 55
        },
        {
            "id": "88eee486-94a3-41e6-b865-c9ef5de95153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 56
        },
        {
            "id": "0bcaefdd-f0ea-4e4a-9c81-c2cd3f216e33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 57
        },
        {
            "id": "0bd5cb6d-4038-400c-9b89-34005e8cceda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 65
        },
        {
            "id": "678f3a5f-99c0-4fd0-b37c-7f8193d577d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 66
        },
        {
            "id": "5f7b4d3a-266d-4777-abd6-1442c3ec7921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 67
        },
        {
            "id": "54ec8672-404a-47a2-a3ab-2bd4569d1c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 68
        },
        {
            "id": "66323809-3207-44ce-a579-e84ae2c0514b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 69
        },
        {
            "id": "26581557-4c16-4829-a9b8-31cce176c221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 70
        },
        {
            "id": "4ba3d718-6cd9-4569-ac71-12f816973140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 71
        },
        {
            "id": "ab1aed36-7461-4df1-a4a1-f2b835bec310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 72
        },
        {
            "id": "44958b4b-f178-4812-8521-1901930e4bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 73
        },
        {
            "id": "c5d88438-496a-46e8-b89a-667c1a84af6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 74
        },
        {
            "id": "b0367b61-4e8e-494d-963a-bb61e22c8752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 75
        },
        {
            "id": "b0d99224-028c-4538-a0f0-93085f9d1047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 76
        },
        {
            "id": "9520d153-0365-4259-a4f1-9a77eba199fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 77
        },
        {
            "id": "f119114e-411c-437b-bd3c-19ea55578758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 78
        },
        {
            "id": "8f6b50ea-ac04-45dc-9c46-e1b35d66507e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 79
        },
        {
            "id": "4f096121-bc61-4796-b492-25c3d1f14032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 80
        },
        {
            "id": "302d90d4-6daa-4255-b25c-e9e4bdbddeab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 81
        },
        {
            "id": "173ec807-13a1-43db-9732-cd1e84685a60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 82
        },
        {
            "id": "c73970e9-9a80-49ad-9a5d-10a55ba5308d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 83
        },
        {
            "id": "0a52df5e-773a-4f39-85a0-89445f77951d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 84
        },
        {
            "id": "a9ff6f00-eafc-4daa-b148-2910948d9230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 85
        },
        {
            "id": "b21765ea-e6ce-4cdd-b87c-620137fd7d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 86
        },
        {
            "id": "87515003-442b-4b31-a4aa-442ce7836bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 87
        },
        {
            "id": "936505eb-cbf7-4dec-a1f0-fcf4da1b44c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 88
        },
        {
            "id": "9777efce-6549-4b8c-9328-b8e04f256494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 89
        },
        {
            "id": "83c22cb4-4e76-4ccf-88ed-771e4a9ee90e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 90
        },
        {
            "id": "1af1137c-4b17-4c3c-8592-3a51b5cc814d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 48
        },
        {
            "id": "f402f501-8885-4f85-8c3a-961ac9ec3d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 49
        },
        {
            "id": "32a96c7a-1aff-4e1d-a4a2-e6b34aa1192e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 50
        },
        {
            "id": "4ea27404-9251-4db5-91c5-54e8c7274f4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 51
        },
        {
            "id": "25f67b97-69c7-48e3-99e6-9f794bbfc1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 52
        },
        {
            "id": "290635c0-f263-42d0-bf24-8d77dae64200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 53
        },
        {
            "id": "dc2f9d4b-667d-4165-b426-2874e769cade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 54
        },
        {
            "id": "0eaf4696-82d8-4eba-84a0-1fa27fe6d1b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 55
        },
        {
            "id": "5ae78f91-c91b-40f1-b57e-27d8e0549bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 56
        },
        {
            "id": "9543c566-c253-4df9-9f20-c7481ffcbd67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 57
        },
        {
            "id": "6b143855-698c-4116-afb0-4157d992d4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 65
        },
        {
            "id": "ccf1f0c7-b100-4a6a-b95d-48fcffd768f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 66
        },
        {
            "id": "7b1052bf-3547-451a-9027-fd5105683a27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 67
        },
        {
            "id": "5362841c-3fa6-4438-8e24-f3efb1079578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 68
        },
        {
            "id": "302b781a-8cc5-48d1-8c55-680d734f39ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 69
        },
        {
            "id": "ee675ecc-54d7-4a2a-9506-cea02547d1e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 70
        },
        {
            "id": "f91ef689-27e6-4196-a61b-ff965a52f700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 71
        },
        {
            "id": "5af3ec72-9483-4e28-a092-6f141e58ddad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 72
        },
        {
            "id": "65f49c5a-3ea1-4368-a9fc-6d75479d504c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 73
        },
        {
            "id": "491cafb9-9f88-4dc1-87fb-16f65fbb05e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 74
        },
        {
            "id": "b433e9f8-f3b2-46f1-a1c6-1b88f4feeefb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 75
        },
        {
            "id": "b6533512-e9c3-4a12-a15a-6c9c444f0c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 76
        },
        {
            "id": "540855ae-2dc2-41ce-b662-56b0aed8879f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 77
        },
        {
            "id": "8d2f4ca5-b0d2-4fb2-94e0-7af70b770d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 78
        },
        {
            "id": "7092dc5e-bed8-4aa3-a6dd-1ca220b2b1b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 79
        },
        {
            "id": "6393fded-096a-445f-81e0-7bc63ac8b6f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 80
        },
        {
            "id": "4991f517-845f-465e-9060-021aacda4998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 81
        },
        {
            "id": "2d166028-2bbe-4291-bdb4-a01f723e1698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 82
        },
        {
            "id": "ab4db1d5-65b1-468e-984a-a7c4a22f0c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 90,
            "second": 83
        },
        {
            "id": "4642b4b3-f58f-4974-aeaf-757473e6513e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 84
        },
        {
            "id": "b5fbf639-341e-4108-bdf7-e01dd4569a71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 85
        },
        {
            "id": "aa7b9368-03c2-4a77-ae3d-d8935b7fbb44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 86
        },
        {
            "id": "3669ddbf-5813-471b-93a1-5be39d08e339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 90,
            "second": 87
        },
        {
            "id": "ebb103fe-e731-4b9d-90d5-23e860c007b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 88
        },
        {
            "id": "a4d9e2aa-4885-4f8b-8b46-f177b6eff03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 89
        },
        {
            "id": "2e80b973-ab76-4a8a-932c-b44593f0fe3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 90
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 15,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}