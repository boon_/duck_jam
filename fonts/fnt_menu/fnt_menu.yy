{
    "id": "738f4331-9b31-487f-ac9c-90169e9e230d",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Soft Marshmallow",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6182c5c0-6ae7-4330-8324-2e69d8a22c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 58,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 377,
                "y": 182
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "06fdc595-82df-407a-952a-6dfe2012ad52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 58,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 486,
                "y": 182
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "50f89f7b-3728-42f4-9de4-17572f0fbd8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 58,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 306,
                "y": 182
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "87d8c2b5-28e6-4ce1-a316-d2e516ee0f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 58,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 400,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "903a65aa-a9c9-4b9d-8660-6cd0707636dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 2,
                "y": 182
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4a0c886c-c3cc-4d98-9b5a-c787f6bd5ed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 58,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "596c750a-7316-404b-b8d9-3c828e607a6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 58,
                "offset": 2,
                "shift": 31,
                "w": 30,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d348aae1-8aa1-45b8-8513-94e2cab8067c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 58,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 2,
                "y": 242
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6a611ada-c1e3-43aa-88d3-8378d67ed74f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 58,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 364,
                "y": 182
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f71c8e8a-ff83-4e54-bfed-27edecd7f7f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 58,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 416,
                "y": 182
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e51718c3-d4bf-4396-9aa3-c7e84047833c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 58,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 140,
                "y": 122
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d8fa7709-dbd8-40d7-a28d-5cf85378bffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 278,
                "y": 122
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0d39700a-f8cf-4c7b-888c-f18db7feb991",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 58,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 461,
                "y": 182
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "cf66f995-b665-43cf-a13a-9049991b875b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 336,
                "y": 182
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0b1a54cf-1f52-47de-b52e-9ccbae4dbdab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 58,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 478,
                "y": 182
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6b86f93d-a445-45ce-99f3-7f986aff595b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 58,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 222,
                "y": 182
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "424a0611-e096-467f-b08d-e19a867d94d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 94,
                "y": 122
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9d23c3de-f7ea-453d-874b-7ea250d9cde8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 58,
                "offset": 3,
                "shift": 25,
                "w": 13,
                "x": 321,
                "y": 182
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d3812dfb-ff30-42af-bcbf-c3edf38a9538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 117,
                "y": 122
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "96485e31-798a-4592-b962-e0f4a3ca1d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 58,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 306,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e5118e7f-a6a5-4237-9e33-c6777728f367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 58,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 56,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a5c8b252-93ac-4233-969d-a3011bc9f8ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 209,
                "y": 122
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "32d78007-37f3-416c-aea9-5fb790936a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 232,
                "y": 122
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e0de1cc9-ace3-4125-977b-904f854f96f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 58,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 354,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "30fce19f-4fd7-4470-9d3f-0dee887cc75d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 301,
                "y": 122
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "cb01e723-e3d8-4634-a0ca-a298198e4f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 324,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "eb3e482b-259f-418d-81dc-a8219a9b6de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 58,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 470,
                "y": 182
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ebf3d594-bf29-4bac-86a5-fd06ac3850c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 58,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 451,
                "y": 182
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0a4fac8f-767d-46d0-91d5-fb61469b609a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 255,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0ed1f0c8-1272-4124-9b93-925e74b76ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 186,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1343a25a-1fec-45b8-8e08-094255950e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 163,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d917bf7d-dbb1-4e9a-8adb-1d0a1afa264c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 58,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 146,
                "y": 182
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fdf418ae-0df5-48cd-bd43-1be39280c561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 58,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4d2965d7-9356-47c9-a300-d63c0dbdcccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 58,
                "offset": 0,
                "shift": 27,
                "w": 28,
                "x": 313,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "67a9857a-8892-4796-b282-53e6646b64fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 58,
                "offset": 4,
                "shift": 28,
                "w": 22,
                "x": 258,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "bc1d06ff-c012-4ee3-af12-9880f9d86b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 58,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 108,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b90d75f8-8d4b-451f-9771-81aea7ea0273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 58,
                "offset": 4,
                "shift": 31,
                "w": 25,
                "x": 455,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "abd7326b-6011-4466-a7e5-cd6f3665d5d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 58,
                "offset": 4,
                "shift": 24,
                "w": 18,
                "x": 86,
                "y": 182
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0fbb59c4-7715-4281-a03f-e4350a550791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 58,
                "offset": 4,
                "shift": 22,
                "w": 18,
                "x": 66,
                "y": 182
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "038e8fb8-075e-4c45-bc4d-76c1c2d047f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 58,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 343,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "060a031d-a52f-49ae-b68f-5737f47c1e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 58,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 82,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0ec0ea45-c5d5-41ae-b40b-1e047a117fad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 58,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 9,
                "y": 242
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2a7d93dd-6386-4189-bfb6-ab7751a480ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 58,
                "offset": -4,
                "shift": 11,
                "w": 12,
                "x": 350,
                "y": 182
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5b1394f3-3a42-4ac3-af72-aeb4870b9e4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 58,
                "offset": 4,
                "shift": 26,
                "w": 23,
                "x": 160,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0173e9a5-c9cc-4fba-a62c-3f64c6eb91c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 58,
                "offset": 4,
                "shift": 22,
                "w": 18,
                "x": 126,
                "y": 182
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c54e2d18-d179-4426-b9e3-81a4b1ea2c88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 58,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9877d381-6721-4631-afb0-65a656cb3d09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 58,
                "offset": 4,
                "shift": 32,
                "w": 25,
                "x": 482,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ce44f1dc-ba5d-41e0-b999-099bcdd6849b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 58,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ae867ead-dc29-4c58-ae78-13b9eb2ff845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 58,
                "offset": 4,
                "shift": 26,
                "w": 20,
                "x": 435,
                "y": 122
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d019b1d8-790f-4f69-933d-cb4027aabe63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 58,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "15103d88-1892-468a-8abf-83d8870ae08f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 58,
                "offset": 4,
                "shift": 27,
                "w": 22,
                "x": 426,
                "y": 62
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fe3e3c93-824a-4cc0-99a7-656dd90952b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 58,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 391,
                "y": 122
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "06a78cf6-7800-424f-9542-c08e849abc62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 58,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "82d6ca82-3b64-4e6f-b9b4-8a9970853d12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 58,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 428,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "87da6ba0-cafc-477f-b8be-7f4050e24a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 58,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 372,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "fe40b851-f7ea-4714-a588-ece093a665a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 58,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5a6c1a3d-45c9-4054-808e-58c22fcd823e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 58,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d851c431-143f-43b3-8274-d33e2f4033ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 58,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d64f6038-1470-4028-988e-26e836c4fce9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 58,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 378,
                "y": 62
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4b74a744-3510-4316-9d37-0413a4caf9db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 58,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 403,
                "y": 182
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8a68de0b-7cc3-439e-a3bc-cb21f57f2060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 58,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 204,
                "y": 182
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "52a1927f-c529-457d-a1b2-376fcbec54c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 428,
                "y": 182
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0ee74d79-a38f-4e6a-92ab-56b17b504975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 402,
                "y": 62
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "077d0036-2a83-421a-9a11-d18bac10e8c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 58,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 473,
                "y": 62
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ad7d3b71-3403-4ef1-abf5-7d847d8d37a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 58,
                "offset": 8,
                "shift": 25,
                "w": 9,
                "x": 440,
                "y": 182
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "023be2a2-fcbe-42fb-8806-6e1beae002e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 58,
                "offset": 1,
                "shift": 24,
                "w": 20,
                "x": 413,
                "y": 122
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "20de82bf-c43a-43eb-ab81-63391be34317",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 58,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d0bb2ebf-76bd-4b58-972c-c3fbed230d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 106,
                "y": 182
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ea9151c4-51fb-4ab2-8c04-c1d0bd7198ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 58,
                "offset": 2,
                "shift": 26,
                "w": 21,
                "x": 25,
                "y": 122
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6f968318-e53e-4f45-bf9b-151ecc0b2e16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 58,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 347,
                "y": 122
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6de8fbbe-9b41-478f-bd42-f4d2d4a68493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 58,
                "offset": 0,
                "shift": 15,
                "w": 17,
                "x": 166,
                "y": 182
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1cedb609-e5f9-4518-ab88-0241e0ee0444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 58,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 185,
                "y": 62
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "203ec477-9367-4269-af5c-22202e1f19a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 58,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 369,
                "y": 122
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0e3d24cd-cc6f-4baa-add3-c1076695c9d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 58,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 501,
                "y": 182
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "763f919c-ff28-4ce3-8787-3ef4429ff800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 58,
                "offset": -3,
                "shift": 11,
                "w": 11,
                "x": 390,
                "y": 182
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7aaa0086-ea84-4885-b5b3-075d173eca64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 58,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 24,
                "y": 182
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e2105ce3-b01d-4ab7-8887-2359dc23e053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 58,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 494,
                "y": 182
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3c8b2a68-cd30-4558-a857-1cd9059a1c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 58,
                "offset": 3,
                "shift": 40,
                "w": 34,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e912f8de-df9b-470a-9881-6889e4065756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 58,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 457,
                "y": 122
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b0e699c3-6241-4036-be6c-88d46602b76b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 58,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 330,
                "y": 62
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "34d2f896-0a26-49ed-b626-358557723235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 58,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 48,
                "y": 122
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3f15f6b0-b33c-4794-8885-755407225a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 58,
                "offset": 2,
                "shift": 26,
                "w": 21,
                "x": 450,
                "y": 62
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6f9288ad-d465-46f9-b836-b4c6febaf016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 58,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 290,
                "y": 182
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "eef2b622-1983-4798-8831-313c8725d963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 185,
                "y": 182
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "55d543ef-c7b6-42ea-b752-18ba4f1e8ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 58,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 240,
                "y": 182
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c3af4a60-2db6-41c6-b0f5-d09e19e42c9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 58,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 479,
                "y": 122
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "64a54323-3606-4418-850a-baf874a92666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 58,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 282,
                "y": 62
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b13d4a8f-cf86-4340-88ac-b5617d559fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 58,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b89043d8-ded0-4d5b-8fff-3ee8a7097725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 58,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 210,
                "y": 62
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "50d69e7f-d412-489e-8e81-c09e72187b4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 58,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 234,
                "y": 62
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1245764e-a2df-4115-be7f-cbf4297d8a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 58,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 46,
                "y": 182
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "abee7729-b657-43ca-b7a3-bc901026bc7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 58,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 274,
                "y": 182
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3181fdcf-6d84-42f5-bf57-8400c28952fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 58,
                "offset": 10,
                "shift": 24,
                "w": 4,
                "x": 15,
                "y": 242
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "12dceb8d-64b5-4247-a3c8-3dc1969d7bd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 58,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 257,
                "y": 182
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3c70fb2f-736d-478c-8ff0-36563d74ce63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 71,
                "y": 122
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "8c65ba93-c4fb-4362-8460-40bf4baff262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 65
        },
        {
            "id": "7f44ca3a-0a39-4879-8a38-83b995d507c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 84
        },
        {
            "id": "55492351-2aeb-4e15-b36c-9e2923227b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 86
        },
        {
            "id": "4dbd8adb-e48c-4363-a374-c8bbb882dc74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 87
        },
        {
            "id": "dd6a91ca-1b37-4ee0-94c6-aebc52334539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 97
        },
        {
            "id": "b655521e-48af-4c6e-90e5-95e7e123a027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 99
        },
        {
            "id": "dfe2b7fb-e27a-4228-aa03-739092057087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 100
        },
        {
            "id": "e6c77849-e718-434d-8d9f-efa71c2ff3ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 101
        },
        {
            "id": "1e5dfa80-871e-41e3-917f-37a72e16e47d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 103
        },
        {
            "id": "0b24834d-d4a9-421e-b2a3-ecff65d2e8e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 109
        },
        {
            "id": "8f653df0-9c02-44b2-8fb7-aaf17ca1d859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 110
        },
        {
            "id": "62a634cf-631f-441c-aa39-a9cc1176c5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 111
        },
        {
            "id": "6c1d4302-d723-4dcc-922b-e1a52ad95886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 112
        },
        {
            "id": "0ed2e908-b0b4-447a-b626-f34413f41b07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 113
        },
        {
            "id": "4746f073-8c30-48fd-8ead-4441e1b517fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 114
        },
        {
            "id": "69f7a2e7-074c-457a-8db0-611aaea9de5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "46500ac7-e0f6-4f7a-b2f6-cdb63a53e6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 117
        },
        {
            "id": "74a99dfd-8251-4a21-abb1-e727ec26a490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 192
        },
        {
            "id": "7f6f867c-bcb1-4d9f-8087-fe7b9f679211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 193
        },
        {
            "id": "6333717e-4a4e-4a7a-b21f-efbf3d9bdde4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 194
        },
        {
            "id": "a9d77794-5380-43dd-921f-99060e9f0050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 195
        },
        {
            "id": "82e497b6-b726-4f92-9d75-e7621718bc35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 196
        },
        {
            "id": "609cd689-c9f7-49fd-b156-2ced5afacaaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 197
        },
        {
            "id": "fa64af96-33ff-40d2-9aa9-4578df0391e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 224
        },
        {
            "id": "4a8e0e7a-c0fb-4721-a8ef-c3b94aba9033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 225
        },
        {
            "id": "2bba9a1f-e549-41f7-9a14-f86673e362a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 226
        },
        {
            "id": "1d6f95eb-85c4-4951-89a5-ab10e48d1bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 227
        },
        {
            "id": "cd86bd13-33b7-4df3-bc4d-4aff81bfe7fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 228
        },
        {
            "id": "4e1890b0-31c4-475a-8613-0792b4758b8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 229
        },
        {
            "id": "17b536c6-e647-404e-933e-26bd3c357ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 230
        },
        {
            "id": "c0dec754-fbcd-4cb0-8400-fc2ddc8f10bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 231
        },
        {
            "id": "7168c146-c64c-4969-baf7-d62d7adae5f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 232
        },
        {
            "id": "23be7ddc-e625-4988-82e5-f00cd78d1214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 233
        },
        {
            "id": "f6532960-1717-43df-8923-1bff00f7b8c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 234
        },
        {
            "id": "a731ab37-8321-4cc9-ba89-dc6d53e2b916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 235
        },
        {
            "id": "5aa4657d-7bfd-44db-aafe-7e526b5ae109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 242
        },
        {
            "id": "c02e437c-db41-44e3-926b-77a0010add28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 243
        },
        {
            "id": "bd1f3ac6-5eee-4b03-beda-a6e3a60933fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 244
        },
        {
            "id": "1e921abc-0348-4088-b51f-473b75f0a339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 245
        },
        {
            "id": "58729c79-ca22-4ebc-a765-7d101631066c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 246
        },
        {
            "id": "e66fb5c2-1046-4712-aac4-50bd9aa322d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 248
        },
        {
            "id": "a0a677c1-8b9c-4fc3-9e94-d598bd8d4544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 249
        },
        {
            "id": "90bca21a-75f1-4e15-9f1b-9cad9d598320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 250
        },
        {
            "id": "ed1cd41b-4940-4374-b391-82638365ef4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 251
        },
        {
            "id": "738566f0-0864-4347-8704-3acfe4043123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 252
        },
        {
            "id": "800685c0-e120-4e44-b419-88344157f7fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 256
        },
        {
            "id": "2e7da9bc-6c38-4860-8811-738766a4dc63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 257
        },
        {
            "id": "fd7277e1-1eba-4e57-9d16-1a0b9cff9661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 258
        },
        {
            "id": "6149aedb-99ff-46af-a804-3576bfe1d1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 259
        },
        {
            "id": "dd3d86a6-64a5-4880-aa1a-8236204074e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 260
        },
        {
            "id": "d05235e1-5e02-49a6-98c4-813541c2b61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 261
        },
        {
            "id": "1ed09e37-e608-4fcb-8ce0-15f167fce867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 263
        },
        {
            "id": "41e56177-4b42-4c4c-86c4-24abe7e30541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 265
        },
        {
            "id": "66697565-d188-41fc-8cbf-d7192d41e0c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 267
        },
        {
            "id": "9f9893b6-3182-419c-8eff-582684500171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 269
        },
        {
            "id": "1b52abbc-cffc-4d74-8786-1653c707e89f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 271
        },
        {
            "id": "c51a65db-e3d5-4af8-9104-36a992497faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 273
        },
        {
            "id": "6c021b35-509b-4106-a03e-e6473e78ef91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 275
        },
        {
            "id": "3fe660a3-9a07-4eae-b7cd-62bac9158e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 277
        },
        {
            "id": "06daeaa9-f4cf-49aa-a504-03f90a969c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 279
        },
        {
            "id": "cd0c4ebd-f4ab-478e-bba7-d17211d7d192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 281
        },
        {
            "id": "eaefe77d-b29a-4640-86a7-25508bd35659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 283
        },
        {
            "id": "738266d7-bf9b-4d63-9947-fc7cc683d210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 285
        },
        {
            "id": "852d617b-196b-4c6d-8807-9b70b9ce0782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 287
        },
        {
            "id": "41c2ac18-b041-4146-9058-504779441f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 289
        },
        {
            "id": "f6a59d2b-e5d7-4c72-af5d-8ed7ed976a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 291
        },
        {
            "id": "efb30b0c-324b-4cd9-8fbd-6e2909f07b90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 312
        },
        {
            "id": "a8c9b0e1-7c79-47a8-9e58-81fb9012bcec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 324
        },
        {
            "id": "b4c646b4-421f-402a-9d5a-885bf994dde8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 326
        },
        {
            "id": "6105a671-ff94-467a-aab4-effc2463167c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 331
        },
        {
            "id": "b0d5b567-6269-4935-a89a-d97b70d12a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 333
        },
        {
            "id": "01173dd7-df17-4fa1-9ff1-40f1ea32dc4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 335
        },
        {
            "id": "34f31d1d-d6f5-4773-ba8c-590d3c9156e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 337
        },
        {
            "id": "7442dde7-0ce8-4348-bdae-7a641b5889f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 339
        },
        {
            "id": "52f10b28-9890-4cc5-9521-bb231cffa630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 341
        },
        {
            "id": "b7e8f16a-afcb-4c14-b57b-47d64570c8d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 343
        },
        {
            "id": "5d7f9f90-babc-45cb-b429-8bdf489cd2c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 347
        },
        {
            "id": "735bdd0e-7f8c-473a-81a6-73ff2a45b683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 351
        },
        {
            "id": "0066625f-dbc8-493c-8747-56107da9c5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 354
        },
        {
            "id": "77f2c7d0-2aa4-4570-8c7d-e21df764df6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 356
        },
        {
            "id": "b18ea28e-0e06-45a5-8351-8619466622b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 361
        },
        {
            "id": "19fb86b4-8f27-43e4-bc2f-754c1deae1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 363
        },
        {
            "id": "8b3a161d-e39b-4dba-af66-eacb3b0330cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 365
        },
        {
            "id": "f5339e28-07d8-41c2-ae08-3eeda7f154a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 367
        },
        {
            "id": "96a0312a-c451-4349-ba3c-8abbb850185f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 369
        },
        {
            "id": "4dd79361-db1e-42eb-8ab7-1312b69fede5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 371
        },
        {
            "id": "dc0dea74-2ae2-485d-b361-198871e52ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 372
        },
        {
            "id": "4a43c15a-27cd-4a98-9042-e8ad6219f6a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 417
        },
        {
            "id": "55c5b46d-3c78-41c9-9769-9727da4a365a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 432
        },
        {
            "id": "996c4931-0977-45d2-89bd-4abae6d3b0b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 506
        },
        {
            "id": "ff910752-be23-4afe-b211-10795aeb85df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 507
        },
        {
            "id": "6a5a3235-e0de-43a0-9149-ce2f81225cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 509
        },
        {
            "id": "c0c0ac7c-c132-482a-b9a9-0837466b0537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 511
        },
        {
            "id": "741f27f2-17a6-4ef9-ba9e-67071ec1417a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 537
        },
        {
            "id": "4daa7055-ad36-48d7-91a3-18f22d36cbb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 538
        },
        {
            "id": "ee240bdb-a28b-46eb-bf43-678014d8896b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 902
        },
        {
            "id": "0e97febf-4fc5-4435-8d8a-6ce7b28903f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 913
        },
        {
            "id": "369f45c3-24dd-4e4e-bdad-77706ae1b85e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 916
        },
        {
            "id": "2096599b-fa9d-46fd-83ea-8cd053b92d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 923
        },
        {
            "id": "bd63ddec-97f7-42b9-8c64-618ddc4b4a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 940
        },
        {
            "id": "beaeada7-33ab-4b74-9fc4-6ce579f722ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 941
        },
        {
            "id": "1443ef63-e805-40fb-b5f5-f58ad34b8d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 942
        },
        {
            "id": "d4709865-57cd-4d1e-909b-64d38231ca22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 945
        },
        {
            "id": "6f568bb5-5b5f-4c4b-8e49-cea67706a8cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 948
        },
        {
            "id": "4a668731-13c1-4922-9a25-a2a2d40ec7e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 949
        },
        {
            "id": "de89881d-cb1f-44a9-9be1-d4cdb7434436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 950
        },
        {
            "id": "2ab6025b-5c2d-44bc-b905-f32244fd20d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 951
        },
        {
            "id": "6d250721-8325-4b67-a0c0-1060d20245b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 954
        },
        {
            "id": "bfd75036-84e5-4272-964e-99c889dad0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 956
        },
        {
            "id": "9a2d17d6-f0f6-4ddc-a91d-5381b0cb93f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 959
        },
        {
            "id": "debeb797-4ff3-44de-9be9-57bc33a5cfcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 961
        },
        {
            "id": "46b8a73d-eefc-44e1-bea1-d8f47abb24ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 962
        },
        {
            "id": "c8c50038-480b-46ef-a0eb-9e35663fade1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 963
        },
        {
            "id": "ea78ac9e-b4ad-4645-ba94-d44c352919b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 966
        },
        {
            "id": "4acbda9b-a078-4a96-82b9-a95aaeadc0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 972
        },
        {
            "id": "9e480c66-0555-4a8f-aee6-64b00115326d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1033
        },
        {
            "id": "8da48250-d411-4f5a-b95f-d218d8ca8842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1040
        },
        {
            "id": "58e0de6a-d511-4bc5-9a4a-e308009f842b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1044
        },
        {
            "id": "4d7994aa-7bab-459e-99ea-cb8f2f4f7234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1051
        },
        {
            "id": "6ad85bfb-4003-44da-8303-768737a2e281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1072
        },
        {
            "id": "260aa219-8406-4cab-8727-1ee048230a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1076
        },
        {
            "id": "6f2710b2-37f4-432b-90b9-8bd4e999994e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1077
        },
        {
            "id": "ade856be-6b36-4b64-b9ba-0395574de99c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1083
        },
        {
            "id": "710940ad-2da2-43d6-a319-cad3b615a4e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1086
        },
        {
            "id": "48e347f1-7aaf-4841-ae91-be306b43bbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1089
        },
        {
            "id": "b36badba-768a-4024-b398-99005ef5585b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1092
        },
        {
            "id": "f0b3c4eb-d837-49d5-a5ae-a3cf4266a4fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1104
        },
        {
            "id": "c9c85efa-7904-47a7-a648-2b13ca0a729e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1105
        },
        {
            "id": "d33228ce-a98c-4ac4-a7fc-74a73009abcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1108
        },
        {
            "id": "9321a44a-4bb2-457a-8884-ed5d8e4f2237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1109
        },
        {
            "id": "e9199d2a-f3e9-4e35-89ca-e345c012f3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1113
        },
        {
            "id": "705d0d37-085f-4d4d-b919-23a0910c4277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1126
        },
        {
            "id": "90812404-5a3f-455b-a670-b9c45b111d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1127
        },
        {
            "id": "3ef34097-85ca-4e98-b5c6-299e352cd0d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1139
        },
        {
            "id": "f56736e6-3e66-4fc0-bb42-446f40fe6400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1145
        },
        {
            "id": "2f8951d4-4823-42c8-8b71-f6357f6396f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1147
        },
        {
            "id": "c72a0ab3-708d-4431-9483-900ba061b57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1149
        },
        {
            "id": "f2bdeada-33ab-448f-9758-4e1077c1f989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1153
        },
        {
            "id": "ad9f02c6-9c18-45c2-ba70-2db9114708ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1193
        },
        {
            "id": "b84f998d-47c0-4ad9-bb01-278b2490a7a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1195
        },
        {
            "id": "15f3a9b9-7842-43c7-a1eb-7d0e401e813e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1221
        },
        {
            "id": "b35425de-3dda-432c-bcda-3c9a2f3ff517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1222
        },
        {
            "id": "4ea2aafa-68b8-4dcd-84ef-8fcbb57b79cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1232
        },
        {
            "id": "0cb09abf-a879-483b-ab11-a43c565ee400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1233
        },
        {
            "id": "31553246-4d87-46bd-b096-14606c318524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1234
        },
        {
            "id": "7fa6c9c0-3cea-4e31-9629-6880e2fe3cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1235
        },
        {
            "id": "1e61243b-0a05-43e2-bd6c-8131f8a79b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1236
        },
        {
            "id": "a7826a9f-1c05-46de-8023-e4cd0e39e3f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1237
        },
        {
            "id": "f85b0167-7949-4942-8d00-8b8ebd7ec61c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1239
        },
        {
            "id": "16b8995c-46df-494e-a7a8-0094929f47fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1241
        },
        {
            "id": "2b2818b5-a538-4cd3-8e3e-f740236a4979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1243
        },
        {
            "id": "07953ac1-685e-42a2-a405-4b3a6a62ec54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1255
        },
        {
            "id": "2a6121cb-2e33-46bb-b817-4aa252cd5b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1257
        },
        {
            "id": "f516f197-8577-42bd-b85c-a5d36d165268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1259
        },
        {
            "id": "cdca8e19-1110-41db-ac39-2ee4f073f787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1280
        },
        {
            "id": "725bd7bd-4b92-45de-98f5-59ebf6cd0d47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1281
        },
        {
            "id": "b235fef1-e5e8-48ac-a8ab-bcd1c1b7c27d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1282
        },
        {
            "id": "68593bd4-ac6a-417a-b890-be1551652dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1283
        },
        {
            "id": "679b751a-14cc-4c21-8825-37c795d715e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1288
        },
        {
            "id": "4b063948-dfa2-4f66-8fda-194147d00b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1289
        },
        {
            "id": "b0aaa143-074c-4c63-9c59-43a9c29c09c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1293
        },
        {
            "id": "a673ad1c-c8dc-4405-aa2b-9a11fd74afc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1297
        },
        {
            "id": "f86a541a-c7a0-4c4d-a7d8-26682a4d1aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1298
        },
        {
            "id": "848f5748-cf54-4bcb-81bf-2a1700568a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1299
        },
        {
            "id": "49f523e6-68da-4ad6-aa59-fac9dafb9a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7680
        },
        {
            "id": "e3213222-e992-4a85-a2df-200fc3b0ab33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7681
        },
        {
            "id": "b4056cb1-1865-4f38-ad71-9a24bb46df98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7743
        },
        {
            "id": "c57e696a-702b-4328-a6fd-97b53de950ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7808
        },
        {
            "id": "e33bcce3-db13-4b17-ad15-145fbeb4f9d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7810
        },
        {
            "id": "7106988f-7617-446a-9567-ff61c196f2fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7812
        },
        {
            "id": "82adc093-6864-48e2-b511-c97b1c1f3b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7840
        },
        {
            "id": "e874383b-e768-42c2-8c4d-fa65c8595a86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7841
        },
        {
            "id": "a5f643d1-8bcd-4e33-9423-0eee9b1fd2ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7842
        },
        {
            "id": "fcfe2753-d26a-4548-b319-5a86f7b6435a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7843
        },
        {
            "id": "98b887e9-eebe-4145-aeff-b0f88a544c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7844
        },
        {
            "id": "a63307ca-341c-4880-a27d-f092632f80f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7845
        },
        {
            "id": "5d3f1a7b-2860-4252-9918-9dd0abf00b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7846
        },
        {
            "id": "0a7611e7-ff12-442f-bead-d5676006b9ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7848
        },
        {
            "id": "d6ab5381-9236-41f5-a10c-3c0b32a0dede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7849
        },
        {
            "id": "5b4ad06f-dd21-4458-bd59-c4b2bc31c8e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7850
        },
        {
            "id": "a8cad3a9-6858-4c3b-aff6-1a60b130869e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7851
        },
        {
            "id": "d4170101-16d4-4ca1-bece-4f713ca795db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7852
        },
        {
            "id": "dd52ec0a-36a9-4049-b2cc-ef63ddedd7ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7853
        },
        {
            "id": "1640a520-2098-4699-b860-2a4a7918d837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7854
        },
        {
            "id": "f2bdf24f-d6fe-44d7-8255-789ff8e0f418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7855
        },
        {
            "id": "85f6938e-0fe5-49d5-a1c1-654f7509c5e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7856
        },
        {
            "id": "1b2ff166-a775-469a-bda1-17bf1cb975bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7857
        },
        {
            "id": "78722b47-c309-499e-af06-a82a05d0bdfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7858
        },
        {
            "id": "293f84dd-f63d-4519-a8cd-df628e6c3303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7859
        },
        {
            "id": "01f68c47-5f1d-42ab-b279-4a1ae2cb5f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7860
        },
        {
            "id": "3e45783b-686d-40d0-8b94-dbf2080bd3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7861
        },
        {
            "id": "353129fc-2fa9-4e2a-8cc9-f11956fd784d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7862
        },
        {
            "id": "9253dbed-c38f-4230-b8bb-e75f53c8f437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7863
        },
        {
            "id": "a7fa54d0-dadb-4910-b9f1-4ae820605004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7865
        },
        {
            "id": "4acbc992-2347-4d58-ac60-059d47cda538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7867
        },
        {
            "id": "ed7cfba4-696d-46a8-95f5-a09d0a3cea54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7869
        },
        {
            "id": "b2813276-b5a9-4392-9ba0-c57824861a86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7871
        },
        {
            "id": "9da02c29-870f-4576-8b28-a6273c74c028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7875
        },
        {
            "id": "df1c9b78-5af4-472c-b2c6-39cf7d1065b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7877
        },
        {
            "id": "f76445b7-f107-4a1e-a35f-d33896da7bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7879
        },
        {
            "id": "59684842-c420-4616-bb28-f2f24dfb72e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7885
        },
        {
            "id": "8f4cfdb7-23c9-4f9c-87a5-bf2bfe3fe183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7887
        },
        {
            "id": "586cfdee-98c7-4e7b-bc9f-faed849917c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7889
        },
        {
            "id": "14a4bb7a-7d7c-4a19-8e86-3cdd06296122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7893
        },
        {
            "id": "87aea2c6-4aac-4492-9038-538a279c464e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7895
        },
        {
            "id": "fa5d2c42-0857-47cb-8d5e-4f6c29d97e67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7897
        },
        {
            "id": "0dfbdbff-9153-43c4-be69-efbf4eafb9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7899
        },
        {
            "id": "c1c8dfa8-3d03-4519-9ef0-ae58af884535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7901
        },
        {
            "id": "3f24871e-9071-4015-9d9c-1698b89c3eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7903
        },
        {
            "id": "17dc743a-1948-480e-973f-73064031a77e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7905
        },
        {
            "id": "90d23730-9ee6-439c-bfc5-41ef4f45731b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7907
        },
        {
            "id": "8d43469d-915b-4d25-bddb-29d4ad385ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7909
        },
        {
            "id": "8291eb44-aea3-40b7-8d8a-d7b819c1d5d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7911
        },
        {
            "id": "4aacf56d-78b2-487b-a244-a1814994cc61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7913
        },
        {
            "id": "01ed1907-92ff-4993-842d-732a19d0da96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7915
        },
        {
            "id": "f001d931-4ec1-4b1a-aca8-b3c3728f5e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7917
        },
        {
            "id": "9b00c391-0556-4237-93d0-82e4d06f59b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7919
        },
        {
            "id": "14eb4027-e720-4512-b1e7-764816311a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7921
        },
        {
            "id": "2616bd41-ce3d-4387-8d89-5df1c7c1f7dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 65
        },
        {
            "id": "9b3ffc26-28f1-4621-ab5b-527d7dcddd38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 84
        },
        {
            "id": "1b58c057-cf09-40a9-9b57-d5a7e5337843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 86
        },
        {
            "id": "c48d415e-ec67-44db-bcf5-c3e5f6d6a768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 87
        },
        {
            "id": "8a258fb0-099a-403c-9b11-99fa21083508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 97
        },
        {
            "id": "83190f40-9122-4a88-82e9-e62c63f17358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 99
        },
        {
            "id": "0c7b5b51-7a0d-488b-a7a2-583a32c4250d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 100
        },
        {
            "id": "2f67ebb6-8945-43dc-b068-04d5a0238fda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 101
        },
        {
            "id": "7dadf35f-ed21-4e4f-bc6c-4f8a03402f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 103
        },
        {
            "id": "e184a5ec-c5a9-4b40-a64b-11d7166ad830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 109
        },
        {
            "id": "27928b39-1e38-467f-80aa-04dedc8b8673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 110
        },
        {
            "id": "255bf3af-3db6-482d-a0c2-c645eeace9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 111
        },
        {
            "id": "a03980ab-0c99-4a3c-a826-c3bcb3aae2f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 112
        },
        {
            "id": "cb7c7f89-322b-4777-a096-31aeade92d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 113
        },
        {
            "id": "40e93329-eea0-4f0e-841a-530083c529be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 114
        },
        {
            "id": "2bda53f9-09d8-4243-9533-f26528efe8b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 115
        },
        {
            "id": "f42678e5-82fb-410b-bb02-8b0292b9c7e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 117
        },
        {
            "id": "54023eb7-70ee-43ab-b086-e6582c3a3649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 192
        },
        {
            "id": "0c895b7a-a807-40e9-8557-875bb40fb015",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 193
        },
        {
            "id": "1ec6a0e0-1107-47d4-9125-3768d29f7cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 194
        },
        {
            "id": "7fc10a0d-7c6e-4dd6-b594-40daaed0d708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 195
        },
        {
            "id": "8a9d99e3-c552-459b-be80-1b8870fa944b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 196
        },
        {
            "id": "7b424c4d-caed-491a-8f93-f4d06ce6c456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 197
        },
        {
            "id": "c5c76b67-86d1-4471-a417-42c44e6b3eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 224
        },
        {
            "id": "ee69a3f8-b57c-4414-8f30-d8a2c1f45f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 225
        },
        {
            "id": "c676510a-19db-4b03-9a62-875fc4355e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 226
        },
        {
            "id": "f71155fc-164d-4152-837e-a5633dbbd31f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 227
        },
        {
            "id": "c4aa717d-e7be-4488-8bfa-a1d29d18c14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 228
        },
        {
            "id": "bb18fcb4-3620-431c-9856-c7ddc854d9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 229
        },
        {
            "id": "b66f5bd0-d771-4647-9ced-9b5b7306b5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 230
        },
        {
            "id": "c9affd9f-40d6-4692-93e4-db2131ad23cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 231
        },
        {
            "id": "b2e6d098-7511-4dad-94c1-7bddc9001e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 232
        },
        {
            "id": "e49f2280-1cc0-4d09-a505-a2282c49be95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 233
        },
        {
            "id": "780ab1a4-5267-4949-bc87-915ce3fdbd8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 234
        },
        {
            "id": "be18ae3c-b4f5-4fce-b4ef-1dd96d62d980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 235
        },
        {
            "id": "d1154911-708b-4957-b0c3-836118a6c17b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 242
        },
        {
            "id": "75df1d16-4653-461f-a959-413d688ed41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 243
        },
        {
            "id": "793e1b38-e3f2-4c1a-98ef-d3c0a7126f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 244
        },
        {
            "id": "5b3d894d-9340-486a-9515-58cf1c53540a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 245
        },
        {
            "id": "0fa2289e-e70b-4e58-b38f-3dd559155abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 246
        },
        {
            "id": "78048a8a-ddb0-4029-81dd-919d7e208655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 248
        },
        {
            "id": "0940a65d-e18f-4435-87cd-df78ce0b4b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 249
        },
        {
            "id": "4742832d-bcf7-4bd6-b130-ac03fa59e360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 250
        },
        {
            "id": "e3ecf21e-5a99-4ae0-ab7d-4f39f4f71b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 251
        },
        {
            "id": "39afd79c-f4f3-4075-997a-9307e2046b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 252
        },
        {
            "id": "888b9fef-eb5f-40d6-a3b1-166c56119ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 256
        },
        {
            "id": "65f96b72-4415-46ec-83a9-8f192198ab45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 257
        },
        {
            "id": "0b7f8b4e-e07d-4abe-b972-315413118023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 258
        },
        {
            "id": "5f1311cf-5a07-4c9c-ad02-c7a5056c54d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 259
        },
        {
            "id": "f4415fe1-2a4c-4d4a-ac88-443f8f0a5f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 260
        },
        {
            "id": "bd369509-1d80-4b65-9942-93b491dcc060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 261
        },
        {
            "id": "9e0f9c6d-e591-49cf-b11e-8dc37d9ba3e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 263
        },
        {
            "id": "89e21156-b07d-4642-9b78-e297a64d0271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 265
        },
        {
            "id": "d3b83473-2104-448e-a8b7-09a9c556dfa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 267
        },
        {
            "id": "95017051-bc01-4b50-9670-ff74e958d551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 269
        },
        {
            "id": "a6916876-0dbe-4e7f-9010-a468c56a46a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 271
        },
        {
            "id": "dd75d1a6-6aa1-416e-a0e1-1b9b32ee411f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 273
        },
        {
            "id": "d7624d65-0c4b-4be3-a386-2b47f90fc93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 275
        },
        {
            "id": "f2f7353b-d084-4cc1-ad0b-94d5e488efab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 277
        },
        {
            "id": "cc3aafae-c979-4f82-9e8b-a2d0fd48b881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 279
        },
        {
            "id": "b03a47a0-4a2d-4a2b-9144-c00d43690b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 281
        },
        {
            "id": "007cd99f-b205-48a6-88cc-c63587546ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 283
        },
        {
            "id": "04b6e113-2f9b-4f8b-99c9-20da526116c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 285
        },
        {
            "id": "a7788cef-3583-418d-995e-4f7afad296e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 287
        },
        {
            "id": "4cc41881-04eb-49bc-845c-bfb63f7af85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 289
        },
        {
            "id": "62eeeee5-ac02-44ae-a583-8481424a2819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 291
        },
        {
            "id": "f253819a-d98a-4f93-88f2-fef4c9762e7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 312
        },
        {
            "id": "c34ad2e8-78af-4935-b7f7-0c5acc39b0b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 324
        },
        {
            "id": "a4005276-5b10-42cb-bee2-d1071a97e070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 326
        },
        {
            "id": "79833721-9ae9-48e3-aaa6-fb63271fbe2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 331
        },
        {
            "id": "440b0b9d-f462-491c-abb3-f6b66ab1aec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 333
        },
        {
            "id": "1c075abc-a889-466b-8760-177b39044ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 335
        },
        {
            "id": "0568d6f0-9f53-4c99-842d-54a8f0a73bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 337
        },
        {
            "id": "b75b04eb-b914-4a9e-bb10-30dded2d5deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 339
        },
        {
            "id": "eb4ddbf4-f252-43bc-b120-9c0aa7754b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 341
        },
        {
            "id": "036f80f7-f058-4d58-a7f3-91025c9c5899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 343
        },
        {
            "id": "37802831-ae34-4798-af2a-ea56795e04b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 347
        },
        {
            "id": "3daf9b7d-42c7-44f3-9453-dcd50bd755d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 351
        },
        {
            "id": "6602eeb0-06be-4cfe-a19f-2158feadb466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 354
        },
        {
            "id": "0cf9114e-c34a-4e6c-afc1-6beadecd7405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 356
        },
        {
            "id": "1728c621-640c-491b-8de3-04ddd561ddf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 361
        },
        {
            "id": "a34a05a8-dd9a-41e7-949e-411e96af4b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 363
        },
        {
            "id": "70fe949f-3dd2-44c3-bb5a-75d4e567bb4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 365
        },
        {
            "id": "d49c9595-5221-4353-899c-e51eb35f13c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 367
        },
        {
            "id": "052e338d-893e-43ae-bebd-5f778006d019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 369
        },
        {
            "id": "d703b157-dc9e-461a-bb7a-4522af16a376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 371
        },
        {
            "id": "a4634e66-c66e-46ff-8ffb-d6847747673e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 372
        },
        {
            "id": "7de2a564-6b70-45f0-ad60-b9506fe711c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 417
        },
        {
            "id": "91e8ec7c-6740-407f-bcff-e140fc7c7149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 432
        },
        {
            "id": "e2b89c4e-2ced-461d-aaf6-00f868c85849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 506
        },
        {
            "id": "1fba2688-2225-47fe-abda-86c8f2f20322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 507
        },
        {
            "id": "26f892b3-485b-4f36-8b2b-770dcf2e9072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 509
        },
        {
            "id": "a1b06fe4-c408-4ede-8b14-8b1e3b55bfc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 511
        },
        {
            "id": "e5ea6b3c-03f4-47ac-9335-b9198516a503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 537
        },
        {
            "id": "58bb8de0-f750-4045-b7f1-df25b9edecc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 538
        },
        {
            "id": "3edd98a7-a55f-4d63-afbf-94050f70a88d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 902
        },
        {
            "id": "b1fef750-aa05-4c50-903c-157312fe599a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 913
        },
        {
            "id": "d37a29c4-23bd-4708-a4da-a055fdb0027c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 916
        },
        {
            "id": "221d8b26-8458-45d9-84af-00c67b2e9b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 923
        },
        {
            "id": "5088d406-4ea7-4ebf-a675-0d5c2cfe222c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 940
        },
        {
            "id": "a4e7a7a8-ed5e-40f4-9964-b757a9662454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 941
        },
        {
            "id": "16846dd2-132f-486a-95ba-72d4c926ed05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 942
        },
        {
            "id": "5ea44d39-5c0f-4aaa-8f95-a33a67eee67b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 945
        },
        {
            "id": "cbaf78f1-0c39-4a75-9eef-aefa83a27727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 948
        },
        {
            "id": "fd8e826c-999d-4d26-9d61-726e0f441810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 949
        },
        {
            "id": "9503744f-0b05-45f8-a447-cdfb7d452ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 950
        },
        {
            "id": "d300cb46-9d3c-40b1-89e4-f9abc1deef3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 951
        },
        {
            "id": "e93e688a-3206-40c1-8a0d-09d33d837d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 954
        },
        {
            "id": "157d92a6-fddd-4623-911f-56d615537319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 956
        },
        {
            "id": "ef1dcde7-f673-4972-99d6-c6eaae0b3b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 959
        },
        {
            "id": "9d4a624e-64f3-4039-b196-1b67efa03954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 961
        },
        {
            "id": "44431867-31db-4cf3-a45d-756501d12c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 962
        },
        {
            "id": "ad1f7c56-5ef4-425b-a598-7b9c58447a16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 963
        },
        {
            "id": "f75cff9b-65eb-4169-91ec-c398256529ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 966
        },
        {
            "id": "ed04af05-d99e-4386-af25-2a0b36d75da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 972
        },
        {
            "id": "ff208c0f-dff5-4d19-ad70-e52b381b0205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1033
        },
        {
            "id": "cd42673e-7029-447b-90d9-cf9425c66d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1040
        },
        {
            "id": "3efd0436-422b-4917-b3b7-0bb22c37076a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1044
        },
        {
            "id": "d50b356c-80cd-4901-93fe-f8b7501430c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1051
        },
        {
            "id": "f3ef88a9-4748-4087-88cc-645c17fb31a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1072
        },
        {
            "id": "f6b34f7d-9395-403d-a9df-0ee8ecce8c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1076
        },
        {
            "id": "858064b5-72e0-4716-9ffe-5027f8ec4c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1077
        },
        {
            "id": "6fafd202-3b8f-4158-b152-3d6641dfff0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1083
        },
        {
            "id": "415b845b-02ed-4a67-a875-b122e315c9ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1086
        },
        {
            "id": "e37c3518-b94e-4738-bf51-d06f3a0777c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1089
        },
        {
            "id": "3764bb5a-f1d9-4a5d-9c60-ec4c0851f690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1092
        },
        {
            "id": "1cce5696-df37-4699-b504-02e47dbee51c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1104
        },
        {
            "id": "9fb7fe07-0454-4233-8b2c-a44c10cddddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1105
        },
        {
            "id": "3801bd39-bb7e-4ab2-a31d-a12361b28c9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1108
        },
        {
            "id": "7ef9f45b-cda8-409c-88de-8db91b20c2c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1109
        },
        {
            "id": "98b5e246-a1e1-4dfa-b252-2981c9203aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1113
        },
        {
            "id": "b3685828-f2c2-4b53-ab52-f41294150979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1126
        },
        {
            "id": "80de05a4-e1f6-4d9c-beaf-111c22e4e617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1127
        },
        {
            "id": "bdc29aef-b6a9-4110-ac72-9fee1a480852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1139
        },
        {
            "id": "32c88146-ace3-4cd3-bc49-27091fad5453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1145
        },
        {
            "id": "330f552d-53b6-4216-9219-e49f860f74aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1147
        },
        {
            "id": "42fee59e-13d1-4bd0-9250-d886d5bb7074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1149
        },
        {
            "id": "9a51b149-f3e8-44f1-bd82-49c429ad3e3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1153
        },
        {
            "id": "45c80839-d4b6-4b77-99c4-72f0f8be80e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1193
        },
        {
            "id": "8459e58c-77e3-4605-b553-8c2807141775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1195
        },
        {
            "id": "5d378993-5b62-4043-b6f0-1e4af567c9bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1221
        },
        {
            "id": "a04e5d6e-24c8-431a-9bf7-1322eb74f05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1222
        },
        {
            "id": "cffb903d-3dd6-4214-8362-b06c468f9ad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1232
        },
        {
            "id": "4cb024d6-910b-4b82-a838-32b53a6415ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1233
        },
        {
            "id": "6f7c6aad-32a7-4080-bf59-0dd6abf839b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1234
        },
        {
            "id": "354f713e-fa16-4c29-bcd2-4bba2982026b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1235
        },
        {
            "id": "51760bfc-1235-4cb9-bd10-5bbdbac70fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1236
        },
        {
            "id": "4ae33d4a-a1f1-43fc-aceb-47feef5e0944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1237
        },
        {
            "id": "c81eb197-cd21-4b74-8fc4-8bb791da97ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1239
        },
        {
            "id": "9cfb919f-22aa-4c03-b29b-faa70f5d615a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1241
        },
        {
            "id": "7dad650c-92fc-4249-a402-b13ac0925a93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1243
        },
        {
            "id": "86421dd5-ca20-4ea3-96a1-7189ab41c810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1255
        },
        {
            "id": "8b5cf793-e402-45b5-aca3-d69fb471139a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1257
        },
        {
            "id": "b64f2feb-bffa-4de4-815a-e5c6d6949b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1259
        },
        {
            "id": "3f38458d-cfd6-4f76-a100-d1d41cafb0d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1280
        },
        {
            "id": "2eab0d15-a142-489b-81a4-6547334bac36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1281
        },
        {
            "id": "9c9b4164-7c59-4279-903f-9d07e6a05a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1282
        },
        {
            "id": "4daef9a5-0c0f-4496-a2a1-245f9500c21c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1283
        },
        {
            "id": "cea364ca-2c8c-4a3d-a9d7-6884cafa0137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1288
        },
        {
            "id": "10392c5f-4eba-4ab8-857d-fb6f674cf4fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1289
        },
        {
            "id": "c1994c60-2505-4b06-ac6a-012b0dbb9123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1293
        },
        {
            "id": "5088181f-e7bd-4c97-b65f-3ce1b134b745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1297
        },
        {
            "id": "e74946ff-a70c-4d23-ac29-f927be83f3f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1298
        },
        {
            "id": "e2db7a67-03b1-49a3-b536-85aad929a009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1299
        },
        {
            "id": "bc66f480-7fba-45ed-8f72-d62c47992578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7680
        },
        {
            "id": "17123bfb-9fe9-4b15-9945-b64faef282ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7681
        },
        {
            "id": "d67b6f29-91c8-4a2a-b167-1f3c001d445a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7743
        },
        {
            "id": "0ec85f31-9ce9-4e97-85bf-5b686b5c0a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7808
        },
        {
            "id": "fa49ebbf-55b1-41d1-8d07-2469a0f2ff02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7810
        },
        {
            "id": "d8ab2503-0307-499c-bf48-314130729f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7812
        },
        {
            "id": "f3ef6a40-20a4-4bdd-9167-5a9e3b6d0681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7840
        },
        {
            "id": "612ef9ec-3c33-4ad9-9c5f-a33cca33be83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7841
        },
        {
            "id": "d8bb2130-b62c-45df-a86d-a947f949af6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7842
        },
        {
            "id": "e92d4b4b-5901-4654-9d40-f78d657720f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7843
        },
        {
            "id": "49f48263-571b-410b-801f-45fe2dad210f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7844
        },
        {
            "id": "b0a39411-d1f5-4ef7-98fa-0d4954b50e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7845
        },
        {
            "id": "e7599cf4-2d38-4171-b387-c98bb9b53364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7846
        },
        {
            "id": "a20f64af-4673-4cb8-abe6-94de16e9eba8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7848
        },
        {
            "id": "11ad2ceb-6ddf-46b4-822f-1cc4ef6c78aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7849
        },
        {
            "id": "aaabbfc7-d26a-492d-8eec-ff39e406f3f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7850
        },
        {
            "id": "a7999348-2dcc-4f12-936f-6207e75d9174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7851
        },
        {
            "id": "921b100d-f985-4d4b-9353-23108248775f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7852
        },
        {
            "id": "f37b0eea-0273-4681-9aef-86ef56b537d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7853
        },
        {
            "id": "13287626-183c-4ad1-af6b-e96da6c87a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7854
        },
        {
            "id": "da7408ff-46d1-45b8-b8e9-c14121c5b87b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7855
        },
        {
            "id": "39c3c5f9-30fe-4a66-8ec6-c21a9dd3d046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7856
        },
        {
            "id": "d03afc25-0c2c-4168-923b-1e79638d2dd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7857
        },
        {
            "id": "e0d1ae10-d4c5-4439-b755-1107fa973e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7858
        },
        {
            "id": "7226847d-52c0-46b8-b58a-84d0bc72c3e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7859
        },
        {
            "id": "fe343006-2c76-4f67-b709-17687b67bae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7860
        },
        {
            "id": "209ac613-af05-45b4-b26c-ea507aaede68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7861
        },
        {
            "id": "2646c5eb-c50f-4798-a087-ff7dc6ca5a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7862
        },
        {
            "id": "c2240b8b-faa1-48b1-9226-0360c2614877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7863
        },
        {
            "id": "05f96837-d31c-4259-a35f-353584eeb489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7865
        },
        {
            "id": "4b53c73d-4863-4073-88fc-ea9c996de18f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7867
        },
        {
            "id": "f88e8163-c7a1-4d58-9879-f07dc4a2cd19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7869
        },
        {
            "id": "d4e4d192-4c16-4bd3-aa1d-56d21b1c8102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7871
        },
        {
            "id": "8a5721cf-f688-4e76-a709-a0c8c3ea6552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7875
        },
        {
            "id": "3fce43f0-2f3c-4913-ad1d-e6c51ee97d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7877
        },
        {
            "id": "5a1e24cd-ff74-4634-8b0d-20fed75881ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7879
        },
        {
            "id": "07ee9fe2-d6a2-4d4e-8c49-29ba499be18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7885
        },
        {
            "id": "16febc26-8120-4516-9d47-202f41b6aee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7887
        },
        {
            "id": "a64af7c6-dbc3-47aa-9b4c-de410c178a4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7889
        },
        {
            "id": "3e611c84-fef9-4985-a652-5a779ca7845a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7893
        },
        {
            "id": "cb304e7f-48d4-4273-8b17-7b3dcbad2324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7895
        },
        {
            "id": "b36116f0-067e-4ab7-867d-a356fe64eb0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7897
        },
        {
            "id": "0be9cbb7-1853-4656-a33b-9ee1cf83b96a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7899
        },
        {
            "id": "90df55c4-421a-41de-89af-b7a179e155a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7901
        },
        {
            "id": "d9126383-79f5-4a08-a62c-866d546125ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7903
        },
        {
            "id": "e2102407-fa41-4b13-ab31-09df7cd075b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7905
        },
        {
            "id": "f422959c-5ab1-4e1b-b740-db921434035d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7907
        },
        {
            "id": "f3c9d324-490c-444d-8f63-7d7fe97e5038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7909
        },
        {
            "id": "56cfce6a-d4fb-48b0-9d3e-7c23f01bbf4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7911
        },
        {
            "id": "cfeff842-6039-4bb1-aa79-78b5bdf40b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7913
        },
        {
            "id": "87c41203-6200-4b89-a60c-8081d0b2f50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7915
        },
        {
            "id": "5eec105a-eb83-4e6d-9223-9b88394ce686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7917
        },
        {
            "id": "aea1aa92-d12f-401e-ac66-712ab67d73c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7919
        },
        {
            "id": "61e15a1f-31f3-4ccc-907f-d09912587a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7921
        },
        {
            "id": "f4b1e09a-2b1a-42ea-8a74-4b48c60dc095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 40,
            "second": 74
        },
        {
            "id": "c8740bb0-9854-4b02-a23a-76b257bfae70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 67
        },
        {
            "id": "dba781e7-ce4b-4bda-94e9-fbb35c32bd60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 71
        },
        {
            "id": "0ada841f-8283-4de9-b6d8-c66e14532507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 79
        },
        {
            "id": "222663ca-6b4f-46d9-a93f-d5d07f771f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 81
        },
        {
            "id": "6b306a45-a445-4b6b-9619-d75cb1a10760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 84
        },
        {
            "id": "2620ccc0-5d25-4ddd-acfd-598363f39035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 85
        },
        {
            "id": "3e378609-582a-48b2-ac21-583f46a50bbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 86
        },
        {
            "id": "c4ba8992-fea9-4ab2-ae22-53df1e01e983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 87
        },
        {
            "id": "e8cd2816-1034-4868-bce5-2a9492a8bb24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 89
        },
        {
            "id": "e59cc434-28b4-444c-918e-1dd480cb7feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 199
        },
        {
            "id": "5eaf7e28-b750-4e79-876b-097855ac6119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 210
        },
        {
            "id": "b88b563a-75f7-48b7-8702-6855ffad6227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 211
        },
        {
            "id": "6f916e93-807e-4a0a-a111-5230df7416e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 212
        },
        {
            "id": "a031d8c7-994f-4bfa-92de-308693ce039d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 213
        },
        {
            "id": "5231e72a-e232-4868-808c-980f0787aea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 214
        },
        {
            "id": "3d4cbe09-8c77-4645-8f06-056727513142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 216
        },
        {
            "id": "d1e0bea3-1ef1-4df4-8ef7-1d7773268b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 217
        },
        {
            "id": "a5383f8a-bd0d-4c75-847e-e18f9696d202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 218
        },
        {
            "id": "e8409597-1e40-4aa4-b82d-664d34f566ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 219
        },
        {
            "id": "38ddabaf-bff8-4a18-999a-3d5977d4c445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 220
        },
        {
            "id": "ae3fc121-068b-4cc2-a04b-48ea1135586f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 221
        },
        {
            "id": "31177c58-aa80-477e-a182-64623d9a430a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 262
        },
        {
            "id": "277f0356-8403-40a5-9a1d-6e8c9eeffb89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 264
        },
        {
            "id": "f8396617-732b-4c4d-b7c2-327744a621bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 266
        },
        {
            "id": "d965b3c2-84e5-4fed-8b05-3e174e662c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 268
        },
        {
            "id": "981ad79f-ca15-4ffe-ae6d-be6b6ed9058c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 284
        },
        {
            "id": "f4907d22-ce03-4da5-a273-08b5db61743a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 286
        },
        {
            "id": "2036c13f-75e3-4f71-bc86-91c9d22ccef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 288
        },
        {
            "id": "300fc66b-727c-4498-b678-218708a502a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 290
        },
        {
            "id": "af457bcd-daa1-471d-86e4-ea18b878a9ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 332
        },
        {
            "id": "e1215f4c-edb3-4321-94fc-789d494b75ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 334
        },
        {
            "id": "e77fbd63-9538-43de-89cd-c333699410e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 336
        },
        {
            "id": "4ae558d5-af95-4527-a8d8-28b9a35cad87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 338
        },
        {
            "id": "57081e4b-89d3-4ac2-b7dd-75707541041f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 354
        },
        {
            "id": "59ceaecd-b039-4d4d-9662-9f5919adaae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 356
        },
        {
            "id": "40824b66-1733-49df-a7ef-8e2182d50f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 360
        },
        {
            "id": "aafe9d09-a994-4444-b9f3-853b12755a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 362
        },
        {
            "id": "1877a038-7a3d-4c1a-b99a-3c6303b255db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 364
        },
        {
            "id": "f0a4d851-c9bd-4a73-afe9-2d27cc574ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 366
        },
        {
            "id": "48dbcd7c-5f13-43cc-80d2-a16ffaa9f3a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 368
        },
        {
            "id": "4e128b58-a257-46da-8d32-8a2a1809c6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 370
        },
        {
            "id": "641ec422-e0e1-4855-95c6-f18120f169ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 372
        },
        {
            "id": "733ff77b-f66c-42c9-9b45-1aea0056d91c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 374
        },
        {
            "id": "a9e62bf9-fa5f-4958-a0a4-fe201245d7df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 376
        },
        {
            "id": "a65c3793-fee9-40a9-9eb1-801935e3d75b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 416
        },
        {
            "id": "4da53d2f-1257-4dd2-a05d-c204269dd66b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 431
        },
        {
            "id": "fbfa398a-02e5-4529-a275-e4261f7d38ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 510
        },
        {
            "id": "482dc721-8a2d-4e0c-a524-81e84156c700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 538
        },
        {
            "id": "37a70308-f203-4ef3-a1db-8084b258005a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 920
        },
        {
            "id": "bab2791b-68e5-4271-80fd-10f6a8536bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 927
        },
        {
            "id": "6589e63a-5759-4009-9854-5d6652d19b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 932
        },
        {
            "id": "a3f65b09-9e04-4ee7-b154-336bc2fdbf65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 933
        },
        {
            "id": "8f1c9815-0cf2-43c8-b894-7048d1d0e53e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 934
        },
        {
            "id": "d74bb7e9-5f60-448c-9a5a-43ffb712b13e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 936
        },
        {
            "id": "9f91f134-4255-4dcd-a47c-1f30b919712d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 939
        },
        {
            "id": "192566f2-b482-48a6-8a3a-9fbf9ffee412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 952
        },
        {
            "id": "d9ca8003-141a-496f-881e-0e655fdd1a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 978
        },
        {
            "id": "f8f0d245-747f-41d4-a137-28a64e358313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1026
        },
        {
            "id": "5f5395ac-2b12-4e1a-a8aa-807091c1dd15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1028
        },
        {
            "id": "0170dba6-bea9-40b0-9f8b-5295f27901ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1035
        },
        {
            "id": "7b250bf0-3cd5-48df-92ef-3c25406b3005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1054
        },
        {
            "id": "2e73f167-e80c-489e-9af9-43f31f2d9973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1057
        },
        {
            "id": "b8131a0a-431b-42f7-818b-4fd2a34011e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1058
        },
        {
            "id": "fa408957-2ab3-44be-9df6-b8118bef156f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1060
        },
        {
            "id": "c812bf29-8e07-4df9-8f3c-06ea5e458539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1063
        },
        {
            "id": "82f40e35-dde8-44b8-893f-ebda193aaa60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1066
        },
        {
            "id": "a3287837-8030-413e-9c72-722f774370ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1090
        },
        {
            "id": "0e7583e1-b244-48ec-8c38-544480b3a5e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1095
        },
        {
            "id": "09a8e3f0-4dd3-4124-ba75-bbb073aaaa59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1098
        },
        {
            "id": "3542efab-1fc8-4be3-9f54-167e22219bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1120
        },
        {
            "id": "e79445b8-3f9e-4334-a50a-1227a5610b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1136
        },
        {
            "id": "2b92c2f6-64b8-4149-abd1-8537b91ac566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1138
        },
        {
            "id": "7d71ee5e-628d-4ce9-be33-cd78220eb858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1140
        },
        {
            "id": "3cef4a43-b193-48e6-aea5-b234c39a54fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1142
        },
        {
            "id": "f9270b88-1d29-47d7-9e8d-5630644df6c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1144
        },
        {
            "id": "9143d903-4eaf-4658-b3c9-8a5c622a2d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1146
        },
        {
            "id": "78299f63-09b1-462a-a2a2-b275093693a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1148
        },
        {
            "id": "53003e57-c5a8-4a9a-bad9-274cd879833d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1150
        },
        {
            "id": "deb411f9-7205-4345-a900-8208d6056f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1152
        },
        {
            "id": "5ad7d0e0-4f58-4811-944d-00e7169601f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1184
        },
        {
            "id": "c23ebcfa-7b37-44ed-8ee3-d94c9bcdbb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1185
        },
        {
            "id": "7705cf97-8bc5-43d4-bcf3-de9dea69e07b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1192
        },
        {
            "id": "c111ebce-c688-4449-b6bb-85cb43d6112e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1194
        },
        {
            "id": "e1ac2bad-9004-4283-87b1-12e70e97c78f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1196
        },
        {
            "id": "c063730b-221e-49dc-b12b-f0697b6eb9eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1197
        },
        {
            "id": "1a49ecfe-e416-468a-b991-2eaade5be019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1198
        },
        {
            "id": "1746e699-717c-411e-970c-afe5956e2844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1200
        },
        {
            "id": "509ff4ae-565f-4694-81f6-68f226a8104d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1204
        },
        {
            "id": "51f97d5b-359f-406d-a7d0-2ebdcc72f059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1205
        },
        {
            "id": "6cbee28b-ce3f-49d8-ac01-ce9f1f251a84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1206
        },
        {
            "id": "3e60c41d-2204-4293-85f8-d91ba18afb27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1207
        },
        {
            "id": "30b79db1-413e-40bd-866c-eee3de809c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1208
        },
        {
            "id": "bdafb294-8c64-420a-96d7-d7b89f2ef4d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1209
        },
        {
            "id": "b3e0f620-300f-45c0-9cec-212201bc152e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1212
        },
        {
            "id": "cba36b15-4f14-4623-8d73-9c81f7d56d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1214
        },
        {
            "id": "12db5638-7477-4eff-ae05-8d81695df91e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1227
        },
        {
            "id": "28adfc80-50ab-4711-875c-e8010c07ebbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1228
        },
        {
            "id": "14372f3a-a9d4-4a82-b0ba-7e99260abdeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1254
        },
        {
            "id": "b9045149-3809-48d5-b3d1-e054d19b2d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1256
        },
        {
            "id": "adc597dd-2207-4dbe-b45a-8bc4605d0664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1258
        },
        {
            "id": "e8c0b077-b200-4d6c-8d39-b5cd7aac6a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1268
        },
        {
            "id": "c318f25a-0634-46c2-b734-54046e2e4e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1269
        },
        {
            "id": "99a7f872-d527-4d9f-849a-08f7d2b9df0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1284
        },
        {
            "id": "a342534f-b3c8-42f2-b7ed-8bfd76480de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1285
        },
        {
            "id": "3273ead1-1735-44d2-90e2-7f0591ecfc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1286
        },
        {
            "id": "18ed60f5-c4da-4eb9-9674-c7b9b49972bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1287
        },
        {
            "id": "ecbab8e9-749c-47b9-b621-c1c160802c5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1292
        },
        {
            "id": "eb6e857a-dada-4942-9193-878018e45ad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1294
        },
        {
            "id": "ed02918c-c32c-47ac-9993-7853665d9de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1295
        },
        {
            "id": "3707b036-b038-49a8-bfde-28f52605df4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7808
        },
        {
            "id": "e4b0da77-e132-423f-a584-6c908afeced4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7810
        },
        {
            "id": "37cee14c-3550-47c0-b202-30fa97af4ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7812
        },
        {
            "id": "30585280-3578-435d-a834-ccec0c49a38e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7884
        },
        {
            "id": "4b05cffa-94ce-4e97-96f4-f3be03e9e572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7886
        },
        {
            "id": "41a606e2-1703-49cf-9fd2-9f45ecae48f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7888
        },
        {
            "id": "78127f7d-d7c7-40cd-8dd9-65a13931e83c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7890
        },
        {
            "id": "9c8c6839-79a9-40e3-b56a-7da561b05bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7892
        },
        {
            "id": "cab02b8c-1571-4736-9e6d-41c59ae48936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7894
        },
        {
            "id": "265c6581-adbe-4fa5-a356-20f054a1cddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7896
        },
        {
            "id": "714ecfc4-ba2e-4152-be5f-3428e024b68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7898
        },
        {
            "id": "66b9fd6b-0d16-4c3d-8d64-e79e46790493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7900
        },
        {
            "id": "bc7c35fb-4044-4ecc-9634-29b83650903b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7902
        },
        {
            "id": "e76129d1-0e66-4c3c-8652-7b9228d5a45c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7904
        },
        {
            "id": "fca53754-94fd-4281-91b9-8c6c86459b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7906
        },
        {
            "id": "a15af055-456d-433e-a2bd-f8c0c3a43834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7908
        },
        {
            "id": "92bef8ed-875e-4511-98c7-37b75d3d43b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7910
        },
        {
            "id": "f4427509-7d45-489f-b68a-32f8b8b9968b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7912
        },
        {
            "id": "41cd2d25-fbea-4974-9e4c-c995df55587f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7914
        },
        {
            "id": "ddd7f618-017a-4d76-8c3d-8fa460156957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7916
        },
        {
            "id": "7a03536a-66e8-49f7-94d8-32474fbb5a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7918
        },
        {
            "id": "f8bbe9df-7a2b-4290-8e62-9a92948791e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7920
        },
        {
            "id": "7621fae0-a4ff-4a78-9320-8a36ffd4edb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7922
        },
        {
            "id": "83baab90-4273-46c0-bfca-7844e7b51916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7924
        },
        {
            "id": "2a387131-8593-4868-a095-ec60908336ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7926
        },
        {
            "id": "588bb265-318e-4440-af73-382b96da7c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7928
        },
        {
            "id": "eebc6cce-bb43-48a2-b9e3-dd82c10e8d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 84
        },
        {
            "id": "f48b9a2b-33a4-4d68-a85f-600ecb2a28cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 354
        },
        {
            "id": "ca458a4c-232e-49f8-a7bd-a96c72878bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 356
        },
        {
            "id": "a1298abf-6050-4344-9dae-742fa58ec575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 538
        },
        {
            "id": "a6d02b8d-ad4d-4279-90bf-a46573acfbed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 932
        },
        {
            "id": "9bf4f3cf-aef9-44c0-bc51-5194a43279b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1026
        },
        {
            "id": "9c1fab90-39e1-41fb-a710-3922ca3deb98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1035
        },
        {
            "id": "3fae1cdb-bb16-4e10-99f9-86c61a387dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1058
        },
        {
            "id": "db601db1-8c52-4d45-b863-9158cdfb3afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1066
        },
        {
            "id": "719a7224-dccb-4c1b-a3a1-f52744edd834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1090
        },
        {
            "id": "7d1661d0-65e9-4c08-931d-eea99b51eade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1098
        },
        {
            "id": "0f071bc6-49f4-4b3a-98b3-3bbb75d24f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1184
        },
        {
            "id": "337cf7c2-2d5a-4ceb-9b92-12ffeff7b486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1185
        },
        {
            "id": "1ba72436-16cc-4b8b-93de-6bc908f16c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1196
        },
        {
            "id": "556b15d8-05c2-49f1-8f55-d7851f749255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1197
        },
        {
            "id": "d091588f-baac-4ce7-9e55-764091b11fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1204
        },
        {
            "id": "8da75809-1c8b-49dc-bea2-e9722c683c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1205
        },
        {
            "id": "c31b3946-02b1-403c-9118-b2f3636f7b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1294
        },
        {
            "id": "acdfa62f-c9bd-452d-b1d2-1efc4ee2dbe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1295
        },
        {
            "id": "11598318-e129-4449-aa8c-4fc576e8db56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 67
        },
        {
            "id": "f3d678ba-c29f-4400-984f-025a0376f61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 71
        },
        {
            "id": "23ae557b-fd13-4319-8acd-6b22df6220fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 79
        },
        {
            "id": "ba25b8ff-a874-42b3-ae74-b95a34d3c1e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 81
        },
        {
            "id": "ee28cbe8-2f00-4e6d-9925-b54322990ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 84
        },
        {
            "id": "21dc3cb3-39da-46bc-9c72-e399c5fe3064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 85
        },
        {
            "id": "a1ca055d-1e10-407f-a502-d613fa44ace6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 86
        },
        {
            "id": "edfcca46-afde-43a8-9a80-75ce59db3920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 87
        },
        {
            "id": "c6f39fa7-c61f-4fae-ad9f-74e44869a9b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 89
        },
        {
            "id": "d52d0e97-1987-4fb0-9887-4f6d7164de2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 199
        },
        {
            "id": "01b23401-28d1-4852-a551-0b1520cfb763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 210
        },
        {
            "id": "946edfa3-65fb-4c6a-954d-8746a859fa20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 211
        },
        {
            "id": "a6dbee50-8108-46d5-978a-5ad5dfd08430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 212
        },
        {
            "id": "9a2a0cda-3140-43b2-8151-eaa2c6a267d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 213
        },
        {
            "id": "633bb9d5-5cbd-4245-a0b8-a1cc5c8dfef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 214
        },
        {
            "id": "730a0e74-d999-4aef-909b-3f9e77179d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 216
        },
        {
            "id": "43fd4cf2-cf3e-4213-a5ef-5fbcf41971d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 217
        },
        {
            "id": "451b7936-338a-40f6-9e40-3809363510a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 218
        },
        {
            "id": "b0ab5b0e-65b0-4b47-ae48-45959b333b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 219
        },
        {
            "id": "8a5c6a85-9a42-4010-abcc-2552a41693b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 220
        },
        {
            "id": "b212660e-4dc8-4b5b-881c-b6d2db1b489c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 221
        },
        {
            "id": "be363bf5-9b74-4931-959f-9b272a575b96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 262
        },
        {
            "id": "773874dd-077e-44ac-9471-bdef6cb78ea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 264
        },
        {
            "id": "4991e3a9-0a90-4875-8ce6-0488622c72b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 266
        },
        {
            "id": "a26686fc-a282-478f-b2a7-7953bdd55def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 268
        },
        {
            "id": "a62b78a3-14f2-40fe-a0e2-4efa22311eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 284
        },
        {
            "id": "2c0fe6d4-0590-479c-99d1-a71b9890e68b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 286
        },
        {
            "id": "a449e53b-0b0d-46f4-8e06-ff0ab45f109a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 288
        },
        {
            "id": "07abab63-c8b8-4ca4-85f6-3d101e22f82c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 290
        },
        {
            "id": "4e265bb0-59ac-4695-9a94-1f06261d38c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 332
        },
        {
            "id": "5a7ecdf7-dc84-4990-91ba-f57106f8d50c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 334
        },
        {
            "id": "dcb7272e-3ba5-4aa1-81f4-09c97d6339c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 336
        },
        {
            "id": "ec2f8700-3ccb-4aa9-8423-66ce56b56111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 338
        },
        {
            "id": "8f23caa7-826b-4a6a-b938-ac5b82c02ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 354
        },
        {
            "id": "217089f2-13af-4a87-999a-fc4714ffcf8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 356
        },
        {
            "id": "e37fc692-7451-452a-9ac7-6bd293c6e448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 360
        },
        {
            "id": "15ea2865-2084-4bec-8282-13a71f0d665a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 362
        },
        {
            "id": "de412bbe-2e02-4af3-b129-8c22dcadf081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 364
        },
        {
            "id": "09550f92-d26d-4721-be14-e332b8956bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 366
        },
        {
            "id": "e03309ce-f1cc-4a3d-a3bd-2c104aeb44fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 368
        },
        {
            "id": "33954f7b-a703-4781-bc25-d1208cc188f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 370
        },
        {
            "id": "cbb5762d-029e-4a4d-b416-671930bf94f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 372
        },
        {
            "id": "dfb0c659-4530-46e5-b410-4db79c775560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 374
        },
        {
            "id": "ca88cc91-a76c-454c-b1af-fc35cfb28499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 376
        },
        {
            "id": "f9b0c824-e977-4856-be07-a179bd903ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 416
        },
        {
            "id": "8289af69-e918-4f64-95f9-1744cf80fade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 431
        },
        {
            "id": "8494fec3-03d6-47db-9818-ca9088d7a0af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 510
        },
        {
            "id": "11166854-14f8-4836-b672-0cf37915b3c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 538
        },
        {
            "id": "9e6fc449-baf3-4cf6-a51d-c0d24cc03d0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 920
        },
        {
            "id": "612f856f-0737-4d3d-b1dc-e5c2e462038e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 927
        },
        {
            "id": "aa0d6940-b0ea-464e-979e-544bfaa265bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 932
        },
        {
            "id": "9133ef34-a988-4597-a2cb-d05517c31ad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 933
        },
        {
            "id": "cafdbacf-c92c-46cf-8fc3-fb18aa57734c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 934
        },
        {
            "id": "67e0ad09-42a5-448f-8079-c1d8c574b680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 936
        },
        {
            "id": "8c009f98-4a99-4a38-b86d-8b21b00d9687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 939
        },
        {
            "id": "69795d31-b166-46dd-a688-00c0b2d98d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 952
        },
        {
            "id": "67a3c90b-7f45-4296-9f36-fc4219efdfec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 978
        },
        {
            "id": "c1f229ab-cbf7-43f6-a2d9-01137456c54a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1026
        },
        {
            "id": "bd71aae7-8705-48c8-98af-38ca4f9615a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1028
        },
        {
            "id": "6f915555-b515-4d1c-8fd7-5376f63150e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1035
        },
        {
            "id": "a400e356-b4a6-46de-bf5e-cafb7f0ee985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1054
        },
        {
            "id": "3107eba7-70a7-4bd0-a237-fc65e30f5bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1057
        },
        {
            "id": "5fe40771-d59c-4cc7-a767-3e968394f416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1058
        },
        {
            "id": "b519d369-813a-4589-b041-0a0c53fd301b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1060
        },
        {
            "id": "88e17bcc-e021-40df-b353-68f1d79d00d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1063
        },
        {
            "id": "b1e60fc4-b699-4058-bebd-88d62c2695d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1066
        },
        {
            "id": "d0ddd8e9-3a95-4cf4-a4e3-09f87b36fe19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1090
        },
        {
            "id": "1c08c0a2-3056-4116-b71f-8061a8b85714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1095
        },
        {
            "id": "6b941ae0-2d3a-4e6f-b9bb-c6a4f6be5b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1098
        },
        {
            "id": "8c0a6fda-326e-43d3-8451-53147e5edf12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1120
        },
        {
            "id": "01a37c15-d517-401a-a4e8-9ad7d9deaff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1136
        },
        {
            "id": "c9aaf175-3cea-4a51-9d96-ee0c58356117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1138
        },
        {
            "id": "6756ec99-37fc-49a3-876b-fbf91aa3eec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1140
        },
        {
            "id": "2f99f0d3-64fe-49f1-8e6b-d591e1114f7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1142
        },
        {
            "id": "f8986745-9329-4f75-b32d-0ca9ec91dbcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1144
        },
        {
            "id": "b3d41ad1-75a7-400f-9b4d-9aea196a31b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1146
        },
        {
            "id": "5cfe9c46-98a5-4834-944a-9db322b5125d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1148
        },
        {
            "id": "03192840-cd2a-4fe4-a491-813f9e773acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1150
        },
        {
            "id": "23aa919f-4b83-428a-9203-ca2cc2fb83ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1152
        },
        {
            "id": "671e036e-2bb8-40c1-909a-6591aa43aa0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1184
        },
        {
            "id": "0c7ecaa2-1dd6-4fc5-8cb7-193fe56a6c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1185
        },
        {
            "id": "3b44e37d-2862-44b2-9df4-f88b8a4182f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1192
        },
        {
            "id": "c5661104-d0c7-410f-93ca-1967b150fe0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1194
        },
        {
            "id": "01440126-408b-40b7-a39f-dbacf4f470bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1196
        },
        {
            "id": "a3e198cf-d11f-4a8a-9e4a-8df47b57d111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1197
        },
        {
            "id": "45547ed3-4eb7-4232-9456-d01bfe9f682e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1198
        },
        {
            "id": "34e1b717-194d-4683-9f1e-76638aea505f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1200
        },
        {
            "id": "05f79336-c9ce-4ff5-b79b-348cf1143667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1204
        },
        {
            "id": "5362e8f1-49a4-4063-a90a-c788050bfee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1205
        },
        {
            "id": "a40c1ce7-561a-4d3e-bdca-71cdd5ab9b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1206
        },
        {
            "id": "dd800b48-d348-4e75-aa3c-3a03fec469e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1207
        },
        {
            "id": "4c1bc887-61b2-4562-8411-62a1c731a027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1208
        },
        {
            "id": "34e013ec-88b3-4775-9e75-b13d2b8c78e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1209
        },
        {
            "id": "c404c7f5-db08-4c4b-9a1c-cdd6f75c3b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1212
        },
        {
            "id": "cc24b5e5-ce72-4462-8f19-3a02a316bf55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1214
        },
        {
            "id": "faaeeec1-606b-4edb-a4d7-797a3d3c2385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1227
        },
        {
            "id": "4d6073c8-7cf4-4b31-b26e-5f85d79ff335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1228
        },
        {
            "id": "da764ab8-a1f4-4e21-9b68-db5d0a39eb7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1254
        },
        {
            "id": "f80fe63a-584e-423f-8042-309ef1e2dccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1256
        },
        {
            "id": "5ffd03b0-8921-4120-8da3-a83643546454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1258
        },
        {
            "id": "24c2ccc1-4961-4bf2-b02e-21a182f75473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1268
        },
        {
            "id": "49759d24-2372-4372-bde3-838729396597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1269
        },
        {
            "id": "4ed4c52d-1849-4010-98d4-67fd45068c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1284
        },
        {
            "id": "9632ca9e-62cd-41af-83be-6fdf01af83ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1285
        },
        {
            "id": "ef4c4909-01f0-4119-bbac-03309b60a9d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1286
        },
        {
            "id": "06313a24-cb83-4a96-981f-87badc2a7ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1287
        },
        {
            "id": "69011457-014b-4fea-86d0-f0920b150f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1292
        },
        {
            "id": "4e781400-27f2-4479-b57b-89b7b82bc6bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1294
        },
        {
            "id": "01b772e5-c591-43fa-a976-188a58df5042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1295
        },
        {
            "id": "d9e8152a-1a13-4e30-a8f4-fae54830819e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7808
        },
        {
            "id": "27f90f4f-3c2c-40a6-84f9-0aff9be93efc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7810
        },
        {
            "id": "606d7c2c-6e0a-46c2-8b84-8ffea371737a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7812
        },
        {
            "id": "c5883159-927f-4d75-a534-1530d14e92b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7884
        },
        {
            "id": "f0390a92-a417-4ce2-8480-bc252328e70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7886
        },
        {
            "id": "669fbbf9-27d3-4dc4-99d2-69398e475fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7888
        },
        {
            "id": "c195b88b-a799-4735-ae54-345c127ef111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7890
        },
        {
            "id": "31a698ef-9e6f-46a4-b766-1d1c67b15e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7892
        },
        {
            "id": "a5c672dd-806a-40ec-8969-9f0e546271c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7894
        },
        {
            "id": "ba1c9713-1cc7-42c7-9f74-b92c3797d736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7896
        },
        {
            "id": "052f69cf-4081-4b4f-851d-e6f42505c41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7898
        },
        {
            "id": "86bbf7d6-342d-4b0b-8a11-6417c2a8f646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7900
        },
        {
            "id": "52febe57-9fc6-49c1-b371-5e893aa4e28d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7902
        },
        {
            "id": "c0577aad-d483-42b8-99ed-720fa9539d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7904
        },
        {
            "id": "4bba0608-bbe4-470e-ba89-955e94995e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7906
        },
        {
            "id": "87e8c4f9-0fd2-4a5b-92cc-a633f2b6acc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7908
        },
        {
            "id": "c49be426-e0ba-45b9-bc77-2a1e6be87228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7910
        },
        {
            "id": "3f63bd0b-0a84-474a-8da3-592406a19321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7912
        },
        {
            "id": "c6c25aec-1271-4fc1-862f-eb5b9abd1e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7914
        },
        {
            "id": "07828946-6a94-4e22-8591-8c325d48811d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7916
        },
        {
            "id": "e43dfd49-4a93-494d-83f3-98d334d26198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7918
        },
        {
            "id": "feef1513-4e6d-4c05-b2c6-89209c2790a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7920
        },
        {
            "id": "6a78363c-bb8c-4819-b6ef-07afd466767a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7922
        },
        {
            "id": "2a5aed3c-4959-4802-aa00-07deee04ea2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7924
        },
        {
            "id": "a0f527ce-eeae-46bb-83ef-05181dca07fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7926
        },
        {
            "id": "1ff32b99-29c9-4705-b055-bdc0c55b438d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7928
        },
        {
            "id": "1f6060f3-af53-4df3-9a96-e062fec4866d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 34
        },
        {
            "id": "dbd15ec0-0593-433e-b3e8-43d241e6456b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 39
        },
        {
            "id": "edf2b0d7-a0fa-4ce7-9b7f-db9ca9c93cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "c453a329-79ac-4930-89ea-7540f48eeae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "fc15981d-85ba-40df-a782-6d45b61ae069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 65,
            "second": 74
        },
        {
            "id": "d15f69d4-79f3-446e-8c4b-edf8c8c6667a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "cb9331b6-d93a-49e1-8b72-70c921b1cf0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "bdffbf8f-e9f8-4152-a009-4f47a57bff62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "ab06864c-e041-463f-afe7-c7328d21c7cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "08e53c48-9e76-47f0-9748-7cccf8b49c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "4216953e-c765-411c-b006-e7df189d34e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "83e62226-51b8-47c9-a8f0-8d2cf065042d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 199
        },
        {
            "id": "859861d7-3908-4d7a-ae3b-eb7e05d325a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 210
        },
        {
            "id": "dd9c4029-f37a-4ecc-9a19-18aaa1916cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 211
        },
        {
            "id": "e1439b1c-474a-4c3b-a65e-1e428a37bac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 212
        },
        {
            "id": "1af12692-dc92-405b-9879-e3e1f05948c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 213
        },
        {
            "id": "e166aa86-d1d6-431c-9858-182389cea1ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 214
        },
        {
            "id": "84f4599e-7987-4f1d-acc8-b1c8ce85615c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 216
        },
        {
            "id": "9e1adbe2-bfd3-4d18-97b8-58cace02e92f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 221
        },
        {
            "id": "b7fad707-2a65-4cd8-a116-cbadc64441ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 262
        },
        {
            "id": "1cb97df3-8ba9-4824-a28b-1a8e5b41b0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 264
        },
        {
            "id": "fecaef94-3948-4c31-9fab-f368fd064373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 266
        },
        {
            "id": "a0fc9003-da17-46e3-9d03-18964731963c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 268
        },
        {
            "id": "66ac03b9-7005-485d-bcb9-b4e3fbf42479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 284
        },
        {
            "id": "be4f9440-6f53-4bda-8387-daa264dff984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 286
        },
        {
            "id": "e13555c5-519d-40fe-9086-60e6169889d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 288
        },
        {
            "id": "d12e97d7-5ea6-4720-9919-4b93e9e0fdda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 290
        },
        {
            "id": "3b0d6518-98d8-4ff3-8e91-a15bdad2b8cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 332
        },
        {
            "id": "d8f297d7-bbb6-4935-b284-6dd6b4827e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 334
        },
        {
            "id": "0a57bb67-8d3f-46b7-b04a-84a70bec3b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 336
        },
        {
            "id": "a38c575b-c0e0-400e-8807-87c98a176f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 338
        },
        {
            "id": "ba8511ef-d352-4d60-9036-6adb975d75e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 354
        },
        {
            "id": "2f25312b-514a-4b61-b013-9baf8dc026da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 356
        },
        {
            "id": "34273310-2f90-48f7-8014-0cd7ca1f58f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 372
        },
        {
            "id": "11faf080-6918-4c5f-b26d-1158f5dcf8f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 374
        },
        {
            "id": "ae28fb7f-f69e-4827-a60f-6c5a035889e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 376
        },
        {
            "id": "cffc83f4-c39f-406b-ab07-9d49a167c9fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 416
        },
        {
            "id": "0d11e622-1ca6-4503-8f71-51045b068c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 510
        },
        {
            "id": "16fa4bea-73f1-442a-b34a-079753e81da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 538
        },
        {
            "id": "7d585fb6-7d60-450b-8092-625ed07a6bde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7808
        },
        {
            "id": "c2710227-fed4-4506-aec7-edf09f9b9085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7810
        },
        {
            "id": "3c9c0f1c-10c3-4cba-abdd-17a9a875e634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7812
        },
        {
            "id": "184464ef-a37f-49e7-b692-7c93875f1a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7884
        },
        {
            "id": "582c842e-4fdb-4922-a46b-6714a1c8a495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7886
        },
        {
            "id": "97a6b04a-3132-44f9-a40d-fe16d7ca6344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7888
        },
        {
            "id": "49b2ac77-ff48-46e1-9579-8fd4dc969edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7890
        },
        {
            "id": "438e7f4f-7b89-4836-bdf6-961ace3d4581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7892
        },
        {
            "id": "0f525e51-766f-4bbd-b0c9-735c0e307ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7894
        },
        {
            "id": "eff88d06-2f08-49b9-b346-cefad899c08e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7896
        },
        {
            "id": "341932fc-b883-4725-94e6-0a29811df163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7898
        },
        {
            "id": "3a9104a5-a2c9-4f3e-8879-82255f7f343e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7900
        },
        {
            "id": "d621096d-539f-4377-b823-8ac50c68a2b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7902
        },
        {
            "id": "906ad3db-9a88-41f9-a01e-8411f72584cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7904
        },
        {
            "id": "dc09fd15-bd4a-4233-bd77-c4d6c7cb9337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7906
        },
        {
            "id": "db0c5685-db21-4cc7-a776-5e4cdd576e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7922
        },
        {
            "id": "ab19e2e6-a52c-4a6f-85bc-6accaa985c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7924
        },
        {
            "id": "b90bd635-91b2-482b-92c9-37c6ba58802d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7926
        },
        {
            "id": "bcbbab91-5710-4ae9-954a-40ba621b2c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7928
        },
        {
            "id": "bb677bcb-c4dd-4167-b2f9-3a20cdc13e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "9d940d11-7986-4759-a640-b5d159f768e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "0a37f074-b251-4319-bcdc-c51cee788b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 44
        },
        {
            "id": "37df98b9-e1c1-409a-af83-aa35f82a2e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 46
        },
        {
            "id": "16e8fd71-bc48-44b6-8ada-7e09c93dbd3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 65
        },
        {
            "id": "762bf6e0-ea59-40fa-9454-6a285b5acbef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "4beb1745-d277-4d52-a996-a614f076ea8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "08a48943-f5de-4c6a-b7f8-ea8bcaacb96f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 192
        },
        {
            "id": "ed156089-2664-41e2-9543-f5658ee46335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 193
        },
        {
            "id": "25fce5a1-88fc-4595-be9f-f4478f6adc9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 194
        },
        {
            "id": "8be8a1fa-e104-4eee-a6ad-fac161728760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 195
        },
        {
            "id": "90953a96-63e5-4224-a5e0-8eded790f102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 196
        },
        {
            "id": "5462ea44-e6b2-40c0-b23f-d560543655a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 197
        },
        {
            "id": "8c26bb01-b725-499c-b0fe-ad97556dfc60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 256
        },
        {
            "id": "4e9d8100-c776-46d2-9e6b-aed69fd23e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 258
        },
        {
            "id": "db2be896-8a55-4d82-91db-2e53ebc78931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 260
        },
        {
            "id": "16ac5c8e-7590-49e7-bdd0-c185ac7d712b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 354
        },
        {
            "id": "f55f7c76-62eb-471e-a0ed-1126c060891c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 356
        },
        {
            "id": "bfdc48be-56c3-4ac6-8117-d5f15ea2d128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 506
        },
        {
            "id": "96cdefbd-da24-4002-8467-da3a36963159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 538
        },
        {
            "id": "b2fe9b6f-3b51-4708-b142-c28395da958c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7680
        },
        {
            "id": "75b897e3-e586-43da-8127-893ea664cc59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7840
        },
        {
            "id": "54b1300e-de45-4ead-8d84-ca6fdbd13a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7842
        },
        {
            "id": "fed98010-78d6-4109-b914-567489247ba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7844
        },
        {
            "id": "3625aa1e-a82e-4d7d-a045-ffa260cd1da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7846
        },
        {
            "id": "f58874f4-f495-4fb6-9c3a-fdedcfda996e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7848
        },
        {
            "id": "a7cc7334-11eb-4d8f-843f-3c2df1e0dc39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7850
        },
        {
            "id": "fc3679be-7b0f-4dc0-af69-030ac5f3dca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7852
        },
        {
            "id": "20be8d46-aa06-4591-90a9-3c7072003799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7854
        },
        {
            "id": "728eff88-b4b0-4378-a985-2b79a4367528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7856
        },
        {
            "id": "3f859fa0-271f-4946-8955-ce2185e774c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7858
        },
        {
            "id": "af7d6047-8885-4f13-b2a1-26cdf67017e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7860
        },
        {
            "id": "81985d12-d29a-4951-ba8c-7b6cc4548bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7862
        },
        {
            "id": "570a7490-9a29-4938-9516-6988e7110087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 8218
        },
        {
            "id": "7c872d8a-9810-4973-a154-57aca0ec77f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 8222
        },
        {
            "id": "562d347a-a005-4da2-b9b9-bebbb0a763a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "1ab13cf3-f4dd-4d2d-b896-17f5ac592f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "c73ae265-9311-4d36-a03d-9f026e7f39e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "f7e25748-9b83-488a-85d9-a8aae913d94e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "bea42e6d-ed0b-437c-8953-194f425ea21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 199
        },
        {
            "id": "fda526fb-2665-4cea-ba25-a573a5ed2a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 210
        },
        {
            "id": "1ff70b07-87ad-41ce-84a2-9aeaa613b285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 211
        },
        {
            "id": "09ee5f71-c3e1-4de1-891b-8a78604ae29a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 212
        },
        {
            "id": "25efbb41-e7d3-4b3e-bb05-61f6853295ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 213
        },
        {
            "id": "cc1724b9-eec5-4300-b7bb-51d76bf65cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 214
        },
        {
            "id": "c5aacb76-87dd-429f-8e5d-a22e72cd8e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 216
        },
        {
            "id": "ef5d1ea0-944c-4758-b6eb-3ddc405effde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 262
        },
        {
            "id": "88aeb7c8-9f61-4578-a342-c207192d39f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 264
        },
        {
            "id": "fee9da00-4ec0-444f-b429-8e7566cc81d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 266
        },
        {
            "id": "93c27617-ac05-4f54-b385-9634fc8650d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 268
        },
        {
            "id": "e16c4fc5-7d39-4c79-a5e7-45e938005df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 284
        },
        {
            "id": "861cf3c2-7ad6-4372-95c9-cae068100fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 286
        },
        {
            "id": "fdd95b34-a2e2-47d7-b73b-c1ef266f76e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 288
        },
        {
            "id": "bd1fef28-3294-48b2-a820-caf150906052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 290
        },
        {
            "id": "6f031e29-6144-48b4-a6ed-41033155baae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 332
        },
        {
            "id": "e44ae3cf-5fcb-467f-8178-d2ac1b1a2f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 334
        },
        {
            "id": "6cea35cf-8286-4cc8-8273-cc17eb3f6c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 336
        },
        {
            "id": "b3b2a658-3a00-42ac-9af1-34c1232c83ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 338
        },
        {
            "id": "0065952d-b4b2-4ede-b1ff-e708edc98929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 416
        },
        {
            "id": "09c1ec7d-74e3-44b8-8cad-cac8badc4041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 510
        },
        {
            "id": "0315b3ab-0af8-429d-8ba2-bde694e414e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7884
        },
        {
            "id": "42790ec7-e4b6-475b-9e22-fd4a04abd4f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7886
        },
        {
            "id": "a96c7bd1-514a-458f-b898-e2f8f425958d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7888
        },
        {
            "id": "6105c9d7-515f-4c07-b9f2-f8bb5c3b6965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7890
        },
        {
            "id": "c1fd75bc-0505-49e7-a693-9c0ca1d9b1ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7892
        },
        {
            "id": "47f6a851-037c-46ce-a07f-23cd234c939b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7894
        },
        {
            "id": "3731a7e3-966a-4edc-92cb-aeffe12f69a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7896
        },
        {
            "id": "f072390b-40ae-49f2-977e-1f451299df06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7898
        },
        {
            "id": "eddf9ee2-d6f0-4061-8b6b-70f3f8f2d4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7900
        },
        {
            "id": "f3ec6ebc-e6b6-49ba-9dca-a750ecec2bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7902
        },
        {
            "id": "a845fc12-0607-43a7-9e78-f8408fe21b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7904
        },
        {
            "id": "60781f31-1ca4-48d9-8f78-26c073cf13d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7906
        },
        {
            "id": "b3dab397-c7cb-420e-87bb-7ca6866c56b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 44
        },
        {
            "id": "48da7d7a-04b8-426a-a799-19e7fd85e4e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "45aff74c-3c7a-4128-8d70-4fc51eaa4d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "f618df8c-b355-47d4-8553-c145c811901a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "9f8566e5-6e81-40ab-901b-0ae09c3e0fca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "6d59de79-bb7d-4213-babf-2b49f5a2d6e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "d8befa26-148e-45c0-88ce-b69d4411c498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "9e5ef315-8e92-4143-9056-8f53216b64d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "099e41af-ab60-49e0-a990-5e44b2d2d8b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "27cb4f6c-b891-4909-8b3e-87c297d5de6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "f56342ac-b490-46cb-8ef4-e51574889c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "c3121e8e-ab72-4d88-bd50-346dcaa29848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "b2d981da-83a2-42ec-8d8d-6c1b4bb2f4b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "bc555353-5063-4f7f-8099-4263e6ab99a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "30d88f2d-2d73-45e4-9aa5-66dc181cb24e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 354
        },
        {
            "id": "c9c6008e-0e4e-4eea-ae6f-ca478960a0be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 356
        },
        {
            "id": "4f42d225-82e5-4f89-bb16-f16de9400974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "ca565ade-a708-4f0a-a819-bad711154ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 538
        },
        {
            "id": "9b399fec-c5ac-4f48-b98f-333e007df1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7680
        },
        {
            "id": "1c116854-c9f5-4e2a-babe-0e29c48b1273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "0106e020-077f-4f12-9867-491699248ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "01e27d69-0fe9-4efc-8af2-772c91dd8db8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "74f0f8a1-2760-48f8-a0fd-b1f527240936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "1cd92b53-c498-4a0a-8ad7-b8326fdc0eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "4b0cc94d-46c8-488f-88e5-4167c6b59914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "98255b47-7a2a-43df-94a3-270a908b5bc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "2c94b606-e96c-4768-805d-5007c712e088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "55f42bc0-0490-47d1-848c-a7f0c6145319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "5c8b18c2-5366-4f3b-b555-09ee509ac07e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "ed2570c8-942d-49a2-987f-398f1227d09a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "77a65343-4885-4467-a384-53bf405f2628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "268e2e75-a217-4ade-b05e-f25871392205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8218
        },
        {
            "id": "856e7895-32e9-4dd8-a991-cf3ce780f19d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8222
        },
        {
            "id": "ae314626-1cc5-40b0-8dfe-b1515cb71339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 69,
            "second": 74
        },
        {
            "id": "870bf007-b979-4837-b92c-329c61ae8d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "2fa0c907-e7cb-4fd6-9f8c-26ef3e767853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "6676d3a2-6047-4530-8ad3-4b0e3561f68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 63
        },
        {
            "id": "0facd61a-8f7d-41ba-9726-f49cca221081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "93a79abc-3d26-431c-92c2-768264bb1bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "0939746a-70dc-4cf5-b536-225e00ac99f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "0ed2aec5-d71c-4b0e-b2a2-a7ef93072581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "2f56869f-380d-4409-9541-e6a40d6a944b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "d5ca8204-d309-43ef-ad96-a5f3a71d044d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "e53e97d6-4e05-4d29-a56d-d04b115f3368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "a62fee98-22d0-4d3e-b2ea-efc2ad7311e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "085c978a-0192-4ddb-9482-f7df7a721f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "385bd3ab-43d2-4fb9-91f7-97ccb8fd504e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "32777724-2130-4aab-a916-b8aefa51760c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "5f165835-0506-44b0-a83f-7f7cdb451b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7680
        },
        {
            "id": "74694acd-06a1-4504-89ee-00482163747e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "af2495a1-2474-4a0d-96e6-fb69f228108b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "1316f11b-24f6-41db-80a2-a062ca862438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "30115cac-7be3-41bf-a04e-73b1de7571a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "fec2ff4f-a0d5-4ec8-8771-a1685929297e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "5e53d48f-ae0b-4deb-929d-17749aa66476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "45a4edcd-f79d-40be-bf57-21d539cfe969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "f6d9157d-b0e0-4243-87f4-286205221680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "ab1dd4e3-0d34-44f3-b493-dbe5b97bc24d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "f7cc9986-bdc9-453f-bcc4-b6b22c2682d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "1bfc3d11-255b-4d9d-9623-eeba3c111f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "e05ff7ba-4a99-4fa8-a631-e5400d2e6701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "bd33c4c1-a125-4b99-be3e-9f2a181f8985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8218
        },
        {
            "id": "2d5ae42d-b872-4344-8a2b-c11ee9ace3fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8222
        },
        {
            "id": "bc85633a-7afb-417f-87be-7d890a649a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "f08f019a-7d3e-4b7c-9a3b-f6afb7789ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "d6fb7fa9-2c3e-4e6a-966c-0cf745c77ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "48470050-a074-4655-a2bf-f9fb4d5fedc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "cda4924c-130a-46e5-9a99-7f40bedd2610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "bf3212d7-f353-4ef7-990d-f882a3762531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "148e042d-cb92-4d23-80ee-850270e121ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "97f21be1-7928-405c-b8cf-0df85836d671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "96e24d35-ad8c-4ab7-abf9-ef9e6f7383a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "38e7d0b4-0b6e-4480-b015-151858770eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "c2d3d905-3071-4da0-b3d4-1bcba1cbd337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "94e6e82d-8288-40d5-9a91-0e86cc182406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "13ecca10-cff4-40bc-b3fa-d12873229210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "57f9c749-ddcb-48b0-847b-fdbf4b58087b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "4a6ac496-6828-4365-86f7-bf1c2c93b006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "cf73353f-904a-4fc8-b41c-1bc8d89733af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "6b5b4d56-8067-4080-a034-52cc1bc28f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "cae868de-453a-46a3-909c-ed52b6e1fe69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "2c4a0e99-d9bb-4b65-93da-7adb9496f9ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "c25c5c5d-aa16-4715-9a39-58bb8ae7aae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "d970be71-0666-4460-b133-567edef6abb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "5f23d184-4eb4-4220-a845-0dc54862ad9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "89ad52fb-ad73-4d07-bbfc-cd65a5e87bdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "e1def2fb-762f-460a-8689-efc1b57c4e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 416
        },
        {
            "id": "08886485-d8c7-4f01-bd95-45531a400dfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "6b4dfd5d-16b1-4daf-8461-c254682f43de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7884
        },
        {
            "id": "a8cca08b-8c72-485a-b7bc-9e88e81832e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7886
        },
        {
            "id": "d9ceff3c-8772-4e07-b7de-d3d1a6a4e3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7888
        },
        {
            "id": "6dcacda5-a4c8-4030-aabc-608bfd0b81bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7890
        },
        {
            "id": "79760df1-aef0-4731-8760-70898871e8f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7892
        },
        {
            "id": "e1246507-f7b0-49ed-8091-bb7a598ccead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7894
        },
        {
            "id": "867a504b-50be-4720-a522-e61acdac0865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7896
        },
        {
            "id": "79e52d87-6c40-47ce-a67c-3b1b3da493db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7898
        },
        {
            "id": "ebc5bb18-90bf-4e54-a878-f17f3d8a819b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7900
        },
        {
            "id": "b2ce8307-9041-4933-a1f7-9b0b620e14a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7902
        },
        {
            "id": "293c5fe5-03a0-4858-ab4c-27a9b4f8bc95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7904
        },
        {
            "id": "8f0e4ebe-90fd-4756-a8ef-b2deb496c4fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7906
        },
        {
            "id": "98580cb0-b990-488b-a1c6-73b2d6f1dbe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 34
        },
        {
            "id": "976dccec-acad-43ee-8071-7bd1567d0403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 39
        },
        {
            "id": "85c3b6fd-4f27-46db-ad5c-6afbebef05af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "c1613518-378c-405b-aaa9-6184ba821a86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "4cb588b8-9570-40d8-9d15-349c3cc0858e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "a9f52a04-2e26-476a-a91a-2a17cc7782e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "b4feb69e-5eb5-408a-a7bf-99a7e6b2ee9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "7611719d-69b9-43d0-a65b-bfa048a3355f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "53c27d3a-570c-4df8-89ba-de0862ea59b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "fb3e89ba-3f2b-4edd-9c54-52a2f56313f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "4580369a-fd3c-4c79-b4e3-5ae36fa5b307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "c0f61cfa-a0ca-487e-b457-085613de1381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "d6426ced-2656-419d-8064-e73be5240915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "dc34f83b-100d-49ae-9cf5-ec63318e07c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "dc25f764-d2e0-4a6b-8aa4-299bc3889a4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "a2f7f133-41dc-4eb2-acf7-477e92b5b5d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "d49414de-f874-4334-9f5d-4a6cd575cfd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "846c3469-c468-4305-a318-efb2852b49a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "e1785f56-75b9-4767-a299-5e5df63026e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 262
        },
        {
            "id": "2ad10e38-b265-4805-86a9-14bb821b1bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 264
        },
        {
            "id": "5b3ee731-bf26-4e20-a160-bfbd8bdcec0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 266
        },
        {
            "id": "f522de07-e06c-466e-a3d4-d82cab4e67be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 268
        },
        {
            "id": "3a5ecdb4-5c3c-49aa-ba58-85d8c5887f46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "23676ffb-e428-4066-8dd0-3057f2c28b95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "57382a4e-19b6-4c5a-96e3-3403fe1d51e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "9beb1aa6-a1ca-49f2-a0a7-cdcade02b4cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "87d6cfac-3972-4154-ad47-faf0fb044659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "3787f691-8f09-4f05-bbe6-7e9bb1794aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "a887e4e3-21a3-4907-9e80-6676d178e9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "c7c0ca79-d251-4098-9812-7e9fecc6408e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "0f6a8eaa-da29-4599-b0ad-b9c8cab89963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 354
        },
        {
            "id": "a0b8f7fc-a27b-4735-b8a6-acf5869e6409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "6afb909f-ccf3-4d81-b1e0-a6bd106dc413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "6b5c4c49-be58-4604-8b65-8e23937068d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "08ab1d59-ad21-4640-ad06-7458ef533dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "3eacd192-a52a-4a7f-866c-8ce737211bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 416
        },
        {
            "id": "0cab3816-7d83-4266-906d-a0d9922cd12f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 510
        },
        {
            "id": "f27a3c6d-7b60-4bb3-8bd2-1af500206a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "f95f5dce-5fab-4a1d-8600-77dd96c61383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "794a8e28-11f9-4ed5-99a2-8abd525a4908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "3671754c-7456-4e51-9f8b-aa26d5a0e7b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "1afdce93-45ac-4062-b83d-951b5695d373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7884
        },
        {
            "id": "1adff6f0-c3be-44ea-8224-5bacc3827ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7886
        },
        {
            "id": "b893ae4b-ea63-4136-a861-b3c4f350dcdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7888
        },
        {
            "id": "b5dc8fe6-7a04-40db-9b8a-e4427b4c8d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7890
        },
        {
            "id": "b8cf6d45-b336-495e-bdb5-fb7c171e06fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7892
        },
        {
            "id": "1fce8683-8819-422a-94d6-e07d54f5d667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7894
        },
        {
            "id": "9044dd25-a3a1-4fc7-8c00-27c1a712a594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7896
        },
        {
            "id": "d71dcf15-1cff-450f-be19-6298fa3955a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7898
        },
        {
            "id": "7f4f9296-277d-43d1-a92c-ac5aff333554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7900
        },
        {
            "id": "46c123b6-4f5c-4e24-a314-cbf40b4b4e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7902
        },
        {
            "id": "40df3108-3d55-4f8c-98a1-8d9235cf5e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7904
        },
        {
            "id": "6a8760ff-d2a8-4e79-81c5-ac3c7e6d278b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7906
        },
        {
            "id": "5ffad347-a40c-4843-88dd-46dd299be45b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "a4b94339-050d-4d8b-8053-4000801a57ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "c62ff76b-5139-47be-963f-7e3a48656f2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "5a82b9ce-646d-4a3a-90f9-9f6ac80cdab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "f42cbae9-69e7-4431-899e-09a118ac5fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "95ca67bd-9a7c-49c6-89a9-e67cbaa3d427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8221
        },
        {
            "id": "a9c21f4d-284a-4501-a559-73ea2446e0fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 44
        },
        {
            "id": "5c65d896-1b50-4a89-a73e-4c73d7d7ad17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 46
        },
        {
            "id": "55dc822a-2bad-45c2-9943-330b096224b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "5b84b90b-b8b1-46a1-8747-70ec2a1ba79a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "75c66ba9-5d37-4a63-b516-edf251d0969a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "a581f933-715f-4048-b901-d28cea1bf177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 192
        },
        {
            "id": "b83b9384-ca68-48cc-a95c-4d9b364b4f3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 193
        },
        {
            "id": "50e60c5c-83c6-490e-b919-c4acfd4b3254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 194
        },
        {
            "id": "d60d7e7e-e2f4-4801-a8e1-6ffeee7fa900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 195
        },
        {
            "id": "e09b441e-5ff3-4d21-a373-ca046e637af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 196
        },
        {
            "id": "09ea8485-d406-4d13-80e4-eec59373c56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 197
        },
        {
            "id": "d67ad4e7-9f63-4505-84db-12d4f81d4478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 256
        },
        {
            "id": "ebed08de-9f47-43fb-83c8-c6637e8ff5c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 258
        },
        {
            "id": "8b34edb6-f637-475c-9fea-7aa483c59d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 260
        },
        {
            "id": "642b8d64-de7a-49b7-95b7-70029cab623c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 354
        },
        {
            "id": "dfb98d9f-7181-4f03-83c8-2a867266db4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "087c3252-0087-4763-8880-e6249e4d364e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 506
        },
        {
            "id": "54b9f8cb-3eca-4593-9826-4a175fc75b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 538
        },
        {
            "id": "18f41a45-e9c1-451f-9d63-a074864a289c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7680
        },
        {
            "id": "9e787c18-b147-4e85-b727-7afa86b38cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7840
        },
        {
            "id": "b8b5ab03-f6e0-49a1-b488-c028203d6030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7842
        },
        {
            "id": "7c5b57f1-eac1-4d23-8c43-2e5b1ce1c106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7844
        },
        {
            "id": "7ac3c138-e673-4134-974e-47836757a76a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7846
        },
        {
            "id": "bfe1dcdc-8bbc-433d-93c3-5dcdf6ba0570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7848
        },
        {
            "id": "44696875-e07e-43ca-b7e7-ea267e2ac833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7850
        },
        {
            "id": "62a0a42b-9e01-40e0-83c9-31d7ba7ba124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7852
        },
        {
            "id": "449ae947-e9b0-48b4-9179-323a0f2df25d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7854
        },
        {
            "id": "aa0bcd12-0d96-4dd0-b5b9-eb13ca22bded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7856
        },
        {
            "id": "ad9aaa22-9423-4a3f-aa5e-9c7c9f35a7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7858
        },
        {
            "id": "8e034177-5a5a-4a46-b901-0544fced01e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7860
        },
        {
            "id": "12bd18d4-f1d0-4d91-aaf7-c0c1010fe3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7862
        },
        {
            "id": "a4c168f8-e55e-4fac-888f-2b7e763158dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 8218
        },
        {
            "id": "6dd08773-7a5c-4fe0-b2ee-9da982bfe763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 8222
        },
        {
            "id": "c40d6e22-6ca2-48be-a7d6-d7fb0060d6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 44
        },
        {
            "id": "1261e7ec-77d4-49cd-8f3c-95d29857906b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 46
        },
        {
            "id": "9a01e658-9e7e-44d2-8b57-b183022054ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "e602ed9a-a9cd-4255-80f1-55e3b63c41fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "f070b02c-d7a0-4e14-b73d-9579ec969a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "d2d4ab99-dfa6-408e-9579-84f775933adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "8db0ec72-fde6-42da-85c8-d986a454a914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "7d8056a8-5336-411e-8baf-99c251539f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "1f59eda4-e63c-4efa-a69f-6997a9e6b666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "da1bc374-bbfc-4322-8289-70bdb5e1197e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "85a0496d-28a7-48d7-8a5f-6dbbd829d922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 256
        },
        {
            "id": "955b0298-0ddd-4af5-a8d7-e0fbdc7bc7db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 258
        },
        {
            "id": "7c144867-6d18-469c-9886-b1b96dda8e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 260
        },
        {
            "id": "5df6bd1f-63a0-4c1a-b687-0626de7464c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 506
        },
        {
            "id": "95a41205-d0a5-4e7a-a930-321dd252596f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7680
        },
        {
            "id": "b1c57915-9445-4e21-abbc-787bfcd431b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7840
        },
        {
            "id": "8ae31b2a-122a-4371-a728-335ec662bb54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7842
        },
        {
            "id": "0e7f47ef-bde7-43e4-b4b4-b66f318510ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7844
        },
        {
            "id": "244d072b-3594-4d7b-bbb4-e4f5360ee928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7846
        },
        {
            "id": "eef84a6a-db68-4090-8e88-3f26c8262cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7848
        },
        {
            "id": "3d2abc2d-e056-4b5b-871b-7f388b0f2206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7850
        },
        {
            "id": "f0b0222b-acc3-4113-a945-48535a727f80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7852
        },
        {
            "id": "dc0134e5-5d96-497e-9b38-653571e51e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7854
        },
        {
            "id": "febb563f-c470-4fc0-a7c2-021a610346c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7856
        },
        {
            "id": "78276eed-b62d-40e1-985d-e3af1c2fe38c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7858
        },
        {
            "id": "772b7b0b-35a2-4c29-9f28-d65a6ef0d09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7860
        },
        {
            "id": "2f2f94e4-cefb-4379-a328-6e22cb1c7e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7862
        },
        {
            "id": "46124ba6-3723-404f-b62a-801274b17756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 8218
        },
        {
            "id": "86b0f1f5-5679-478f-aee8-207acfef5718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 8222
        },
        {
            "id": "019330c6-c333-46ca-b873-9761ff742c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 44
        },
        {
            "id": "51f445b2-e556-452a-ae86-0b08387205df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 46
        },
        {
            "id": "57644765-84b7-42ca-9ab0-a6e8cccce1c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "84ba4d81-d457-42d9-be9f-b567979d2a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "14ce455e-9fd0-4ed3-af43-c778d30693ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "a82b4852-b05e-4545-8c43-af99ce4d914f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 192
        },
        {
            "id": "6bd4925e-3738-4156-8533-053a096bb756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 193
        },
        {
            "id": "2551ed28-d627-4e2e-816f-3accb4f8442a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 194
        },
        {
            "id": "14a595f5-ff47-4138-b3d2-ed0949ce03e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 195
        },
        {
            "id": "7a64123b-b46d-41dc-8780-f10df128e5cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 196
        },
        {
            "id": "7f856cca-3e84-4aa4-a2d0-dbd69f782ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 197
        },
        {
            "id": "d5bf6000-0cf5-4a01-8099-858388ac710c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 256
        },
        {
            "id": "bee3813d-496a-46fc-98d9-731d7fdfb2a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 258
        },
        {
            "id": "5efe8a94-aa01-497f-bd4c-36a808f50cfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 260
        },
        {
            "id": "0423c205-5360-47ce-b107-9ae762417b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 354
        },
        {
            "id": "da94ff94-cd46-4105-9c20-6cd3215afcce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 356
        },
        {
            "id": "a486709a-5ef7-44a5-a9f6-194ad766b0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 506
        },
        {
            "id": "4c29b500-17fd-4f89-937f-4661462527e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 538
        },
        {
            "id": "90d8f673-ce45-4769-ac6a-a76221e20b6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7680
        },
        {
            "id": "659e010a-7e94-46a5-82fa-882e702c9270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7840
        },
        {
            "id": "74113d08-97d6-454b-9c4b-3612dd01af88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7842
        },
        {
            "id": "d4288b2b-cf24-4986-9005-067590fbd891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7844
        },
        {
            "id": "6c8ec365-1a4f-413c-b3a3-ae737f37923f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7846
        },
        {
            "id": "6d1440f4-3268-4c20-9364-1d3d85ecd772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7848
        },
        {
            "id": "59544ebc-2bc0-44fb-bb43-6c5f73a1f9d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7850
        },
        {
            "id": "4a9e4375-025f-4f53-994e-ef63c547be2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7852
        },
        {
            "id": "2870e38b-3308-43db-93e4-a107f38b61a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7854
        },
        {
            "id": "8d971355-35f0-45bb-9e33-18439d149c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7856
        },
        {
            "id": "e74ea418-92a6-4534-b012-749cd6fadb4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7858
        },
        {
            "id": "c1ae24b7-9fa3-45be-ae81-b720f678da26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7860
        },
        {
            "id": "13316410-b7fc-4c1f-b855-09866de4c30c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7862
        },
        {
            "id": "8bc239cb-be82-46b6-a88b-106567d2e2a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8218
        },
        {
            "id": "84fe0bf1-7c9e-4b9b-82fe-8371f91a7860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8222
        },
        {
            "id": "cd693384-c6c4-4a98-a3d4-6af0baa86f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "995032a8-a049-4a45-933c-de9bdceabcf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "c64f5957-1899-4cfa-9a69-774754833b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "47040caf-828d-4bb3-ac53-37ecdb829dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 63
        },
        {
            "id": "3eb4ccd7-c7a1-4465-86a1-6c656acfa984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "5e75e210-dfbe-46e9-b702-ec07bc58af6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "ad6e2f70-1bf9-4e49-aa2a-3ec9ec2df2dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "1d8ca900-32b4-43a2-9564-bef64b0ed4a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "f8b59cde-baa1-4d65-a201-f2d746354ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "1f6bca48-9def-4680-b8b8-8d36b5c1b2f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 84
        },
        {
            "id": "f7476b8a-1a92-434b-a297-c9696d2a1fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "d2f9c9a1-82fc-430b-8db5-03958e21adc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "d4937bd7-aa26-4800-b5bb-8328f9613b83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 100
        },
        {
            "id": "56a593c0-a507-4100-bffc-670a808f7a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "43914870-5afc-463f-bf08-0e3aae5c4644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 103
        },
        {
            "id": "bb2ac807-dc6b-424b-b5a8-6996209d3394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "f8718401-fc8b-4807-99d2-29d5e41b14b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "995a68c0-bfc1-4f50-8f77-6f62a65b0e0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "804d58c2-8087-47ae-a11a-30c2255fc026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "e85f8812-8be5-4977-88ea-27869cadfe74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 113
        },
        {
            "id": "324377ba-9da5-4589-975e-472608fa1eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "3afe0f33-b0ac-48d0-835e-6d4f56416db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "31422813-45dd-4429-bcff-8486c025ce46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "672010ab-2f99-4f79-b795-dc66d1320cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "97a8fbab-36a3-4fd4-a569-31fc6215efcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "89090f99-8f14-443b-913f-2ab82f35dc0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "372c1a8e-b4ca-4699-b02b-4ce14eae1f1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "c7961cc0-b7fb-464f-b843-f548b0c1a53c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 122
        },
        {
            "id": "411249ee-cebd-4b94-afe2-1691240a7602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 192
        },
        {
            "id": "1c616f1f-e90f-47c9-ab4e-739f183a2272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 193
        },
        {
            "id": "e2713608-7079-4f98-bd71-95af3a6d9c79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 194
        },
        {
            "id": "25b74b05-70a9-4da0-9cf5-17a25ee857e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 195
        },
        {
            "id": "6c44cc0e-3f23-43da-9fbc-655f8a849398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 196
        },
        {
            "id": "bc875432-6d7e-41dd-8be4-61edcac11ff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 197
        },
        {
            "id": "0e535fa4-fcab-4deb-894f-f1c6ed5dcec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 199
        },
        {
            "id": "84981cd1-d80c-4a57-94fc-1d4b955d9212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 210
        },
        {
            "id": "ae3849a0-130e-4731-8059-4e790900f9db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 211
        },
        {
            "id": "3e600365-88b6-49bb-bb19-d87e29f50449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 212
        },
        {
            "id": "a37d8f9b-d29b-47c1-9d29-59900b96ed40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 213
        },
        {
            "id": "f160b900-821e-4ce7-a9a4-9acde5500f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 214
        },
        {
            "id": "5886b616-109f-45ef-8233-8d6469d942c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 216
        },
        {
            "id": "3c917402-b90c-4488-880f-b6f3a60725c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 224
        },
        {
            "id": "8a0789ca-7e09-462e-9690-9116e47ce6d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 225
        },
        {
            "id": "a6307298-7ed1-4a33-aa78-62f431c8058d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 226
        },
        {
            "id": "f4769a99-2408-4783-8ce0-60d7087573de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 227
        },
        {
            "id": "64e99f58-23e1-4f8b-baf6-183a2d075373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 228
        },
        {
            "id": "ae39aac1-a33d-47a5-9b93-5a21b514f97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 229
        },
        {
            "id": "b94d1fac-6813-4a5e-b9ec-f579b1d7ce15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 230
        },
        {
            "id": "6d6585ce-4e66-4fb7-b1ba-6db90624eb72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 231
        },
        {
            "id": "55be0184-3bc2-46ea-936a-dc0fea9f56b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 232
        },
        {
            "id": "a0aca10a-e810-4c9a-a4b5-305c61395f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 233
        },
        {
            "id": "f013123b-69cb-44b5-a0c8-45115b933b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 234
        },
        {
            "id": "c41bf9eb-65f1-491f-990b-075779d33f25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 235
        },
        {
            "id": "e06a7de9-4846-4ec8-832a-b8515e4d17e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 242
        },
        {
            "id": "a011cfb8-616b-4af6-81aa-6344023b86b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 243
        },
        {
            "id": "b5b72a00-7b92-4544-be47-be4c37e235a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 244
        },
        {
            "id": "71fe31c2-db34-470a-bbda-4f7ea5fef05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 245
        },
        {
            "id": "ca194c23-11d5-4cc6-b321-c3e81749c72f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 246
        },
        {
            "id": "137e284d-dbb9-4e38-9ae2-aadc12f1f90a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 248
        },
        {
            "id": "4c3e1340-eadd-450c-8a82-7de49cffbecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "4f764b7a-ff1a-4df8-8d87-e364466c21db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "cb669770-50d6-49d7-9d52-c065a1cae1f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 251
        },
        {
            "id": "a0d2a84e-3410-4f3e-8bc3-233441b0db5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "9bd09b90-bf63-43c8-8015-440680f15b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "75d3f1c1-b69c-4ef5-a603-cca9dff8bdf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 256
        },
        {
            "id": "836f9f67-0511-4836-9789-1a487af3c1e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 257
        },
        {
            "id": "bf4e74eb-231a-4cd4-a482-ea8bfd0ace49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 258
        },
        {
            "id": "f165e66e-fe20-448c-b318-ab2346839ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 259
        },
        {
            "id": "2e146e6f-8b33-41aa-a6ed-b3bf0fb6a7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 260
        },
        {
            "id": "7928c0ff-9550-4022-94e9-7796da457b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 261
        },
        {
            "id": "9626f694-bd39-42ff-be12-2a6583ec7ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 262
        },
        {
            "id": "c5c76190-7485-4830-afe3-b29fc360bc53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 263
        },
        {
            "id": "1dd4a63f-d01b-491c-b92f-fe836954bcf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 264
        },
        {
            "id": "e2779e90-16a2-4ee5-b291-1f3fdf96a3c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 265
        },
        {
            "id": "9f4bba36-fa51-49bc-a7cd-aa0bfab3be09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 266
        },
        {
            "id": "3d97f482-8b9e-47ef-be5e-d57d9455bc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 267
        },
        {
            "id": "8695f20e-6583-4583-8c9c-a2dab8e1bb07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 268
        },
        {
            "id": "285e729d-b2b0-4a85-bd91-ec35d2bb859f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 269
        },
        {
            "id": "59ea399f-c008-4e45-9ded-59b0325d7663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 271
        },
        {
            "id": "b748c055-34d3-4db0-b4d3-66e4f744c501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 273
        },
        {
            "id": "56fb1174-abdb-43a2-bbde-a3a9ca0b1185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 275
        },
        {
            "id": "d6542318-57d5-467d-bc04-5c03175791f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 277
        },
        {
            "id": "f606d80d-40ae-4046-8c45-7268a166c46e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 279
        },
        {
            "id": "48e34ad7-1d31-486f-8c63-cb3f65f3018c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 281
        },
        {
            "id": "c6843fca-2871-40cc-b4e8-a7b214915518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 283
        },
        {
            "id": "c2682ae2-9604-4d1e-a891-755b233943ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 284
        },
        {
            "id": "dd1955c0-972d-4718-9d79-f1368062cefb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 285
        },
        {
            "id": "6329fb2e-912c-4b18-8339-4df7f21571d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 286
        },
        {
            "id": "37a704af-7873-4ab2-9c5b-1f17db3eceb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 287
        },
        {
            "id": "04578c11-c378-4977-bc6f-9cfbf9c8f19a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 288
        },
        {
            "id": "2dd065a8-c396-4258-a77b-3062a43ff938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 289
        },
        {
            "id": "208e3e1e-e55f-4a0f-abfb-5665d9a73d01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 290
        },
        {
            "id": "02056dac-19a3-4299-86bb-a86a5b472f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 291
        },
        {
            "id": "7ec65799-5df5-46ad-9620-edc1a1c32ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 312
        },
        {
            "id": "6d85491e-ae2a-4b1f-9c14-88e27f605167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 324
        },
        {
            "id": "d4cbc60d-637b-46d9-ac52-9b502b2cf814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 326
        },
        {
            "id": "2da6161d-6865-4e68-a82f-4adf5b38f018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 331
        },
        {
            "id": "865a251f-4cff-42cf-9956-3f42d43df13d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 332
        },
        {
            "id": "0b1a1ba8-4ce9-4771-9b9b-bddc6c2ddd6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 333
        },
        {
            "id": "f4429590-5d71-4822-b794-a1d6045dfa4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 334
        },
        {
            "id": "e9acc58f-1c3a-4385-9248-f74f6e1ce2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 335
        },
        {
            "id": "f05a6c98-8fdf-4df0-87f9-c99ea8431ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 336
        },
        {
            "id": "c266f8ba-2d39-444b-bf1f-e0e1330832e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 337
        },
        {
            "id": "7c441850-1e7a-4201-a215-4b52a696262e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 338
        },
        {
            "id": "8b995c4c-4781-44be-9f39-deaa1c05a81d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 339
        },
        {
            "id": "bafd4c83-0ff1-4be0-a352-296366449428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 341
        },
        {
            "id": "03a8004a-1ffc-41dd-ac6c-12cc804a2584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 343
        },
        {
            "id": "6913934d-f7dd-40d9-a057-3851b741b17c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 347
        },
        {
            "id": "ee03ab9b-311e-4e67-bd11-9cc3fa95095b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 351
        },
        {
            "id": "173e1323-04e1-41a7-bf5f-26f29d7ad58a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 354
        },
        {
            "id": "03bc650a-353e-4e86-8934-787317f70c72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 356
        },
        {
            "id": "b60d65c1-2105-4a9d-805d-782d34e35b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 361
        },
        {
            "id": "feb5a274-e418-4ce6-8765-534f781e158b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 363
        },
        {
            "id": "06bdf18f-543b-40d0-a5c1-b0042976d673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 365
        },
        {
            "id": "a262015f-bdf0-4295-a9f1-cb12b505177a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 367
        },
        {
            "id": "89f99588-613a-4120-8764-705dc046ee87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 369
        },
        {
            "id": "f5ccbdd0-0fb7-4027-8e8e-8d52e5faab20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 371
        },
        {
            "id": "e282c219-e50d-4b3d-95c7-be2c7213bbee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "d23c6d64-bb80-4357-a6c6-fe0d1ade7249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 378
        },
        {
            "id": "d168fccd-b7b8-40aa-88be-c328a057d83b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 380
        },
        {
            "id": "1cac65b7-34b7-469a-a6ba-1a365e59fd65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 382
        },
        {
            "id": "9bc0627e-d1c8-4340-b036-695bd280e831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 416
        },
        {
            "id": "97a21f20-6826-4c4b-89a7-c71676aa0d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 417
        },
        {
            "id": "804b87f1-e171-45f6-833b-eb5f823f8ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 432
        },
        {
            "id": "9c7da5d7-672c-4bae-b2a5-a4185e6b217c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 506
        },
        {
            "id": "9c12d9c7-b9ae-41d6-9a56-da5079852f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 507
        },
        {
            "id": "6225c19d-5389-4509-889b-fea1609e66c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 509
        },
        {
            "id": "61cb49f2-45a8-48f3-b66f-0632a366124c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 510
        },
        {
            "id": "57e1ddd3-71df-4d86-a91d-7e99084b4e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 511
        },
        {
            "id": "b0ecf732-65ca-4076-940b-b590d15e00c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 537
        },
        {
            "id": "644078b1-f871-4a2d-a2f3-15a85b170350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 538
        },
        {
            "id": "dab4a95b-47de-48c7-9f7f-0129f3d09cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7680
        },
        {
            "id": "8e526c15-a2dd-436e-b591-162f421a8096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7681
        },
        {
            "id": "990be0fc-d41d-4c67-9cb4-e293d585464c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7743
        },
        {
            "id": "949e9354-2d36-40da-95b1-881387d3ef2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "f2a6b49b-243d-4b36-aee6-364f5d0d9546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "861d73ec-321d-4412-9352-fb255f9486ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7840
        },
        {
            "id": "e63fb802-d635-42e1-8af6-f9b4bac6c164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7841
        },
        {
            "id": "8b9a3a31-7626-4cb1-bc85-b0153f8f51b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7842
        },
        {
            "id": "bf18c34d-638d-4651-9d5f-31d56671a989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7843
        },
        {
            "id": "d6332c82-5300-4c1d-90ec-d54a4292e967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7844
        },
        {
            "id": "6faca917-3109-4ae0-b45d-37bddd88d607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7845
        },
        {
            "id": "2a7460d1-64db-4fb9-ae56-fda1acf985e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7846
        },
        {
            "id": "79164285-f85d-4060-a4f3-d6b5c3d7c378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7848
        },
        {
            "id": "fa3cdd44-2bb5-4eff-85dd-c46987946b95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7849
        },
        {
            "id": "31586e02-5ccd-40ab-9955-30351caa950b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7850
        },
        {
            "id": "8c6f9ac3-fad9-45b2-b1c0-0ab142f6791d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7851
        },
        {
            "id": "5af08c4a-5c11-4ee3-b309-30e718303655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7852
        },
        {
            "id": "207be4e3-b6fb-4033-8c5d-ccb52476be47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7853
        },
        {
            "id": "fa384c00-40ec-446f-8905-5492a9889425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7854
        },
        {
            "id": "4ee501bf-e68f-4d7e-a348-775ea240f08a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7855
        },
        {
            "id": "8e428810-9059-4a75-aa04-9949b1ea373f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7856
        },
        {
            "id": "05269714-35ec-4dd2-b743-ece036372324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7857
        },
        {
            "id": "b23c75d7-5240-4eff-87bc-7dbf223311f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7858
        },
        {
            "id": "d485069d-059a-45e3-a31e-80b921f700e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7859
        },
        {
            "id": "a6655ff1-4b11-43b9-b86c-e15e1f8b913a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7860
        },
        {
            "id": "07fdbdfc-79cf-4df0-8020-8ae291b0fb0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7861
        },
        {
            "id": "949e1900-658b-477e-a7fc-13d7a5f43ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7862
        },
        {
            "id": "0ca00eb6-6273-4f2f-adff-28979f311db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7863
        },
        {
            "id": "ac8918d4-f03e-4b31-a333-2cb77c9cb6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7865
        },
        {
            "id": "3c6924bd-b876-45e6-9c06-39f94e81970a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7867
        },
        {
            "id": "532f5126-3a01-42f1-b8ca-2e7412ceb1cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7869
        },
        {
            "id": "08246179-c282-45f0-8abd-27103de0b65a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7871
        },
        {
            "id": "abdada24-cad3-416f-a51a-c323f43a5893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7875
        },
        {
            "id": "d372adf4-f486-4d4d-afeb-2ff6cdd61ea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7877
        },
        {
            "id": "2d03bdbc-d847-4c13-9483-15161f9f4ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7879
        },
        {
            "id": "30a6a490-ed64-4269-9077-87de2079f2c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7884
        },
        {
            "id": "71e9286f-71f2-45ed-b408-05d55a51ad30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7885
        },
        {
            "id": "cc331bfd-44b6-444d-bb5e-4bec5a5d4160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7886
        },
        {
            "id": "05541a70-5640-4878-855d-1c8630f6a092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7887
        },
        {
            "id": "d4cf31cf-f80e-4daf-a285-e03ac410e92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7888
        },
        {
            "id": "ca31dde7-4031-432f-bb0b-2fc6fa78d54d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7889
        },
        {
            "id": "384fe01c-9c1d-44d6-b2b1-24861cb9efd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7890
        },
        {
            "id": "d3247519-0fb6-4cfe-8049-db67aab17d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7892
        },
        {
            "id": "187f56fa-9e1e-4b7a-a2a2-348ae85935b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7893
        },
        {
            "id": "e5e85eee-441d-4873-b846-2463636aeb1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7894
        },
        {
            "id": "c0ca4962-9581-4ee9-aa50-666e1ca19e33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7895
        },
        {
            "id": "f682564e-92b3-4a53-bda9-c4ddd32108c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7896
        },
        {
            "id": "6241f7e0-b1e5-418e-b48f-c2c9ef52d037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7897
        },
        {
            "id": "7bb66c21-12b4-44bb-8702-364800c121cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7898
        },
        {
            "id": "80c83f96-83cc-4dd4-9de8-b5337807f9b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7899
        },
        {
            "id": "94e4be9e-02fe-4c6b-b429-53ae16280201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7900
        },
        {
            "id": "22ed76e1-25bb-4ce8-96bc-0e252bcad73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7901
        },
        {
            "id": "0dfc5d53-038a-478a-be2e-77f69aeb9ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7902
        },
        {
            "id": "7f8f9bee-b86e-4aa6-bc9c-5a9822ebfc7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7903
        },
        {
            "id": "6b58df7b-4551-4acf-91e4-31ca62505f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7904
        },
        {
            "id": "2f651951-93ff-4b7c-bfd7-a8712d2b2853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7905
        },
        {
            "id": "961b45ba-2aef-4f46-bd60-3ebde81eea8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7906
        },
        {
            "id": "ab5dae03-36b7-4e28-b296-9182baff1530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7907
        },
        {
            "id": "116d2e17-c15c-4a92-a851-cdc74faa4011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7909
        },
        {
            "id": "882908f0-fd07-4b19-9736-e1543893ab55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7911
        },
        {
            "id": "0ebf914e-6e9d-4d99-9415-2e733bed9982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7913
        },
        {
            "id": "04f7d37a-c827-4de3-8faf-53d999821f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7915
        },
        {
            "id": "7c178fd2-81fa-412c-bc83-d06c4d808825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7917
        },
        {
            "id": "3446ea04-2947-48a1-bcb7-0919fb589e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7919
        },
        {
            "id": "872c017a-ef8d-4f65-9a19-ca15a4be2237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7921
        },
        {
            "id": "39fef63d-13c3-45a7-9fc4-69691e4a1740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7925
        },
        {
            "id": "a0d0663b-24ba-4d6a-903e-48bba8969036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8211
        },
        {
            "id": "d28c15a5-cd63-462d-99aa-cbde17213182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8212
        },
        {
            "id": "7b42c09f-8683-4cc8-85e5-08bc87946a4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8213
        },
        {
            "id": "c8ff1ec2-4efc-48cd-b5ad-f390547951a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8218
        },
        {
            "id": "da4f780e-bb76-4134-bbb3-77e713706130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8222
        },
        {
            "id": "e00f7bf6-10ed-4911-8e5e-ce17cc5ee5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "b8a2d294-c759-4696-9f8b-a3b1c8e13510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "50aacea1-a97b-4ff1-a0b0-eb0a40817938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8218
        },
        {
            "id": "fadc432f-0ab8-41f9-acf5-6b5874d54842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8222
        },
        {
            "id": "b17fe0dc-1bd0-4a3e-8be3-2efb56900997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "e78e057e-0c7f-4537-918a-c731f80546e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "b48fbc23-ecbe-45ea-a281-834222ec6b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 63
        },
        {
            "id": "c51d3931-f5ea-42cf-b4fa-8a981d4381db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "56387d19-41a0-4915-8e16-b263b6fd9121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "4444a333-1e70-419a-a67b-0821642594ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "46ba8b4c-3030-4ce5-b589-c9284a08c27a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "7309b202-8e6c-4389-ba6f-80c76d1e036a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "310d99b2-088c-4828-8e85-8302b5591df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "45c8177e-747a-4dba-89c1-d9578bdcd4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "91727eb0-8b9e-40e1-8b97-4cd0abb12c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "8c33faa4-3738-40df-a162-ed9299f6a6ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "06169e6b-7d6e-4e68-8d74-0bf026caa2ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "5221ba4b-b857-4a76-9c04-f4ef2bc4e918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "16d41bd3-d00f-4528-a59d-5477664ec7f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "85e3feec-bf0a-40b3-a396-8f08219274f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "ca56dece-3a61-4162-9223-4554454a5179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "8f595f03-b8a2-46aa-a86d-55ca0aa80c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 225
        },
        {
            "id": "58f87c69-dbea-46b5-ab98-06fd86762b76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 226
        },
        {
            "id": "5a93d9b5-2a12-4804-a74a-36a38ff5a2c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 227
        },
        {
            "id": "ff607af2-bb74-4a82-8acf-646b562eab1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 228
        },
        {
            "id": "75e06cfc-b431-49f5-b7af-5dd7ecd9d15f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 229
        },
        {
            "id": "c46d57e9-ab49-4889-a4ba-885b279429bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "4ffdae10-7fba-4b89-be99-7afe279cc308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "acaa5098-1f4c-4a3b-97ac-3ef5c5e0cc07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "5e504b4c-ba23-4572-b554-13ece6fe858d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "2f5c2f5d-aa81-4c21-b93d-e1f7f344963e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "912999c4-84c9-4dd5-bf91-550f3c2fff11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "e6b46db9-2ae6-49de-ae41-f5edbf67c942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "899fee62-585e-4307-a6f3-1ce52cacbfe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "f1d3f9ad-71a9-4c0c-b9e7-e80c83bc97bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "b107f845-7796-4f20-99cb-1557a6a165dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "52250eb1-40e8-483e-955d-dfccc8824537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "4ca50552-0ca7-4f18-9202-826a4d41701c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "a3301649-0cca-4007-b74d-dcebb846ebd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 256
        },
        {
            "id": "ea08b385-e5b0-4a6c-bfe2-5f9d1cb40a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 257
        },
        {
            "id": "66a341e1-da1b-4de5-b7de-ce2824b4fa97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 258
        },
        {
            "id": "1a6a1c02-046e-451f-bbb7-cfc5d19e7740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 259
        },
        {
            "id": "fa01cea7-e574-452e-a6c0-e2dac99833b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 260
        },
        {
            "id": "32fa8ad8-d780-4095-9651-e48b6410c8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 261
        },
        {
            "id": "676e940d-3a32-432d-8ff3-0951048d3486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 263
        },
        {
            "id": "8895b651-056b-4f3a-8fb1-1597f083a548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "9d26898a-2b2d-4db8-a3a8-b41960322339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 267
        },
        {
            "id": "3d1b45fd-66ed-443a-99b0-77498d18ca5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "dee7793f-58a5-4d1b-be04-8b829fb2016b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 271
        },
        {
            "id": "947fa25c-2de7-4eef-a60d-8eb62ac644f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 273
        },
        {
            "id": "42c5377b-f8dd-4ae9-8812-bf76696d16ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "7f2f4bf0-5e51-4f65-bb9c-f2ec9025b8b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "a6d4f1aa-f9ed-40e4-b0ce-d4219c42375e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 279
        },
        {
            "id": "be200983-7d93-448f-b3fb-2b14e3149713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 281
        },
        {
            "id": "6531e6e3-1389-4fbf-90e1-4c22851d6e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "edeecc2a-0138-4219-9aec-40e2b7a3c37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "8e488735-d379-4e17-85e8-bcd7677cd226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "56bf7350-f039-4216-ba10-1e820acfcd6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 337
        },
        {
            "id": "ad9e6ef8-07b1-445b-9fee-b11d36c1db2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "6ac6784a-0b5f-4364-8be4-ef16a7d3ad7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 417
        },
        {
            "id": "1c82c407-bbbd-420b-a0c9-45e9c0a259cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 506
        },
        {
            "id": "2cfbaace-2915-4092-a361-50740cd7c73f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 507
        },
        {
            "id": "2399f2df-f89b-4cab-b550-248953f2c17d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 509
        },
        {
            "id": "4ec4b3a7-8be1-43c7-be5f-c11ba4ea225b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 511
        },
        {
            "id": "e42eeec4-177d-4b5a-8bb9-0b354d821af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7680
        },
        {
            "id": "6e5cdab2-6fb8-413e-b3eb-581e4ac44cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7681
        },
        {
            "id": "cc0f8865-a87d-4ba7-a71d-d1456bb51cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7840
        },
        {
            "id": "4f90a3d6-f741-43d9-9067-c73c792a6028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7841
        },
        {
            "id": "112896aa-f445-4942-b420-d9ded5e813e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7842
        },
        {
            "id": "d8611c04-fb9c-4f16-882b-f716246c40ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7843
        },
        {
            "id": "56274c0d-1da3-4548-bb27-e8c93bc7cf86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7844
        },
        {
            "id": "45418418-6a0a-4e91-a38d-44b9cd64c668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7845
        },
        {
            "id": "6284fcf9-e577-459d-8aa9-dddb78a5172d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7846
        },
        {
            "id": "24eb9d46-9521-41b3-91df-9eec1bbfcf9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7848
        },
        {
            "id": "88b6a9ba-248e-487d-83da-529f39dfd965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7849
        },
        {
            "id": "55f5e8b1-99a1-454d-926c-ada033a516c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7850
        },
        {
            "id": "c7bd1983-e746-401a-b6d1-7bbd0f5f44be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7851
        },
        {
            "id": "82509f5b-7969-4e4f-b463-e3dc135a570f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7852
        },
        {
            "id": "04c68795-e8b5-425c-aa27-20d9d8d2e215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7853
        },
        {
            "id": "46eb6b62-84bc-4f00-8f03-9491279578fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7854
        },
        {
            "id": "d14a54d1-be1f-444a-a3d1-d20fe849c3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7855
        },
        {
            "id": "f5e7e4dc-a64f-4766-8158-a97317567084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7856
        },
        {
            "id": "113e8388-2745-4176-b568-4a5aec4060d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7857
        },
        {
            "id": "de233cee-487f-4e1c-8778-066b9f51ffce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7858
        },
        {
            "id": "690ed817-9aa2-4956-b318-69d692b7e77e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7859
        },
        {
            "id": "af31702e-f634-4105-9abb-2ef2d7be6ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7860
        },
        {
            "id": "1a2bd9d5-cd13-4d40-9fc3-20c3a69691ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7861
        },
        {
            "id": "c5f57720-1010-4b3a-96be-bf236907694e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7862
        },
        {
            "id": "b736df68-55a8-4b91-9c4e-0edd25476c5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7863
        },
        {
            "id": "9d474194-94f7-44b6-a917-325aeb550631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7865
        },
        {
            "id": "94071ed2-bf66-450a-9ec7-4cac3b090d45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7867
        },
        {
            "id": "3a309817-b8b3-428a-ad81-2cf504cd35d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "305360a9-05bd-4819-a2fe-caeec5ac123c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "0a8c803e-7d07-4ea9-9db4-9eac9e77c150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "b064fba6-d0b8-4d1c-b4e5-9de3b219dc8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "9592e85a-fd69-4756-9d1d-5619eac2b066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "3c9c439c-3b63-4d05-81f9-6858d39d135d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7885
        },
        {
            "id": "e930b13d-44d0-4b03-a30d-20b05992b45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7887
        },
        {
            "id": "9a0cde61-0886-4ecf-b159-af4dceff26e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7889
        },
        {
            "id": "ce7164a2-2bad-4e44-9d14-fbf4e98247cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7893
        },
        {
            "id": "29b07aa3-c0a5-45a7-8486-a36c15e993a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7895
        },
        {
            "id": "7e3c992b-7f89-4092-9b8d-fd81633f74cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7897
        },
        {
            "id": "6edefe77-0cb4-4c61-bb74-5f8c660f9955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7899
        },
        {
            "id": "77f08320-ff3d-491d-ac67-568208520d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7901
        },
        {
            "id": "e3e52109-7f38-4d49-b462-8b72cc399df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7903
        },
        {
            "id": "a15bad3c-bccf-4ec3-a244-3c329689bc78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7905
        },
        {
            "id": "9d066b9e-4745-40b7-92f0-41ae564eb1b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7907
        },
        {
            "id": "37733de6-0b6d-441a-a7d4-c0475fe18ea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8218
        },
        {
            "id": "7b6736df-3248-4706-b5eb-832628ab7aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8222
        },
        {
            "id": "cff57556-b633-4e31-86a8-c83c4f4ece58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "675ea5c7-f467-4084-8832-48bf806996de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "d0811a57-96a8-41d2-9b39-e6b6f4d72ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 63
        },
        {
            "id": "7b8c2af0-5584-4e43-bc18-3375f755c3e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "5b42efbb-a5a3-4ce2-9298-f897159b130d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "524ece1f-6cba-4701-89ce-411898620a32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "82d2e112-dfaf-4abd-be3f-843e66c66674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "243dd7b8-e1b5-4e97-9799-1061a64d64f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "6b0762dd-4b28-4cdb-a650-5e6d68e92f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "42310b41-1982-4ab6-a5a5-aa7c33c91e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "260019f0-4551-4b57-8dc0-ea6665dbd7fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 192
        },
        {
            "id": "7185fcb5-b6f4-4844-9b27-440a59d465bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 193
        },
        {
            "id": "2a36e535-901c-4a6c-9998-3c4e72a347ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 194
        },
        {
            "id": "d0e52b39-3696-44c2-864f-cc25647895b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 195
        },
        {
            "id": "fee2e69c-7ce9-4803-ae92-060fc8486c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 196
        },
        {
            "id": "7e16866a-1108-45e2-89c3-53e5411e59fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 197
        },
        {
            "id": "47107bb3-1f74-4fd4-906e-33d2e255c574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "94432e6a-a0cf-4420-b083-972bffa90cbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "d1f83ed9-22d1-49c9-9393-f3dca28b40e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "874e3500-5eb3-4cd6-b050-6ba6082c5654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "39b37223-82ca-41f9-9351-ba342182288a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "fc82e61e-e1fd-4a5e-a731-a3ca34d7a708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "fe05cd94-e1b6-4f6a-935a-c45c96ebb2ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "4a9fec53-aa66-4c91-9f94-39178ad8e3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "52065e3c-7195-457c-88b4-031baa8f4d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "1644a676-9535-4073-8b1b-3a2678656fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "555f328a-4add-4c86-99c2-7c1fa95990c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "3cd284a6-0bb2-48b8-a4e4-36123457d7b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "794b584e-0b99-4f43-a85d-ae2ee2ea3b96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "dbe9c29c-79e1-45fa-8e65-f2cda64e86d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "6d85f5e1-ac89-4a28-8155-ddb6e68d6b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "3cab29ea-dddc-474f-b943-7b809d30e7c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "c1889bca-4082-407a-83a8-0ad8c6461353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "cd0e6eb5-7fe9-4dc9-8052-ea09a0c11b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "cfe63750-0e0e-42ad-9541-1e307e7e1488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 256
        },
        {
            "id": "b1693014-381d-40df-b530-e16636fe16b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "d99c3276-329a-463a-b32c-50762195053d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 258
        },
        {
            "id": "279afd81-7dcc-4842-a7b2-e6bab537cb20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "0995f885-b7c1-4b9d-b391-885491ee2d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 260
        },
        {
            "id": "7c604f25-c3b8-491d-9021-03df489c126c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "e23a7d86-f15e-48d8-b54c-6b39a1d51eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "d0a601c1-10f8-4b70-856a-7b897377d13a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "f9332487-f28d-4081-b352-e9f1e514dfed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 267
        },
        {
            "id": "c7a94943-aba2-4670-870d-1ab58746127f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "a4a19d7a-f7fa-441e-9167-fdabf292fc9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 271
        },
        {
            "id": "db396b34-1f60-45f9-9f79-f26ac662bfe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 273
        },
        {
            "id": "1e254d9c-4d03-45d1-824d-56289f45ca3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "de3e96cf-8660-4586-ba0d-e3bc25769d5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "3212bc72-e7ac-402f-9b24-f815d14276b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "99b47948-5017-4e70-8d8a-f0309767955e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "211efaf5-2063-4626-9719-5a173bc1077d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "78b78767-845a-46d6-8fba-c2cfb01fbb49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "c4899b5c-e497-48dc-a0b1-48437b74cceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "979a26d4-f9c2-4f13-bf35-f0b9fc5b9817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "ed28d11a-12f6-4295-abbb-ed1854086b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "cec1c590-a423-4c7b-92c7-fa2a8f392f17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 417
        },
        {
            "id": "6351f8dc-b88c-4f7e-94d5-74936ce6f3cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 506
        },
        {
            "id": "2fecdaf8-c511-4db8-86ff-6b9b04963c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 507
        },
        {
            "id": "844b50da-a245-4464-9274-ded51f60d4ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "f95bf25a-fc4e-41db-849f-8ee56bed34b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "307ca56b-d0f6-49db-994c-91887042c326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7680
        },
        {
            "id": "63dd51be-4d27-4f50-832c-648f52ce91cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7681
        },
        {
            "id": "d7436ea3-dd75-42db-8ca0-c73889dcf945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7840
        },
        {
            "id": "f3531434-148d-4bd6-85c9-ee6ca87b5ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7841
        },
        {
            "id": "4d6e17af-8164-4433-befa-447048122669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7842
        },
        {
            "id": "f36bb2c9-b047-47ac-a7b2-b169b918146b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7843
        },
        {
            "id": "d090a473-45ea-4816-bc3b-cff8b96a9d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7844
        },
        {
            "id": "767546e0-8b1a-4d05-9e5a-c5074677e7f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7845
        },
        {
            "id": "6b547694-f575-4255-9ffc-88175a2d1acb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7846
        },
        {
            "id": "7c8a3b4b-afb9-4082-9605-01db1eb961ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7848
        },
        {
            "id": "8c731de8-4603-4951-96aa-7c80f50bd58f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7849
        },
        {
            "id": "1e63affa-d9ce-49a5-b11e-8d293a91636a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7850
        },
        {
            "id": "f34ca92a-83c4-41a6-a7d4-d8078ed8e73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7851
        },
        {
            "id": "7ce6cea7-7157-495a-b9cb-b05016ac049a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7852
        },
        {
            "id": "ef31ec4b-87ec-47ad-91e3-d123a5272802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7853
        },
        {
            "id": "4af90efa-3099-4add-8629-ccff1b8001a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7854
        },
        {
            "id": "1906947a-ac15-4162-8bd0-9cd2717cc2aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7855
        },
        {
            "id": "35930138-375b-4916-bc30-61580ad1b406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7856
        },
        {
            "id": "d82d2118-c533-4012-a8d5-0dc37ae06513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7857
        },
        {
            "id": "882c6433-71c6-46df-a109-1595df00b9ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7858
        },
        {
            "id": "fe710d80-c978-45cb-930e-6a923278f992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7859
        },
        {
            "id": "b9dd1360-f5d2-412f-8961-fde6cc279982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7860
        },
        {
            "id": "e0f44c0d-30d9-4f62-a23f-fa55517ed1b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7861
        },
        {
            "id": "bad2cc6d-fec8-431b-9730-77a2c6945daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7862
        },
        {
            "id": "1a21fbc7-f1e2-44ac-b178-94ee88e31992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7863
        },
        {
            "id": "4d746f57-8ac0-4722-b6f9-d135a0694fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7865
        },
        {
            "id": "fe121800-0a9a-438e-abac-32ff960abca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7867
        },
        {
            "id": "b25e1be0-6a99-4241-b468-473e3d92094d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "be975d53-fc32-4263-8e53-202c815d36f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "3d50567d-be21-4deb-9f70-a3da4884b0a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "71b514fe-c0db-411c-8507-18fc9f0a780f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "12f2b408-770c-46dd-bf2a-60c00a3f7314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "ecdb3a15-e76b-4669-babd-14bc0dcf7ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7885
        },
        {
            "id": "81ee5f93-2712-4ab4-961c-0bd67578b97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7887
        },
        {
            "id": "7e16b3b7-f907-425c-a85b-f6047a7f94e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7889
        },
        {
            "id": "a8d03479-0534-4916-9606-663980db463d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7893
        },
        {
            "id": "515ecdff-4bb4-48f1-9a53-791b694636f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7895
        },
        {
            "id": "40bd292f-4d80-4270-b3d8-3bbebf1d7bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7897
        },
        {
            "id": "c108b3a6-a80b-494b-bae1-897f7e7f4c1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7899
        },
        {
            "id": "266d56c7-e284-43af-ac90-8e747bcbd614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7901
        },
        {
            "id": "37be9d68-b054-4773-acb1-f0457ae2efc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7903
        },
        {
            "id": "f0c3f971-1f72-4d99-be1b-3d7adc7d1bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7905
        },
        {
            "id": "3d997414-cc28-4131-9ab5-53685627fc1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7907
        },
        {
            "id": "83c94314-e1b5-47a7-8282-c94e2e8763c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8218
        },
        {
            "id": "ef9dc59f-3bb9-4da8-971d-2d371b7c8c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8222
        },
        {
            "id": "8436ee7e-68c1-4cda-9a85-01e494169c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "ce4cbf24-6478-441d-b861-885fa5834bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "c1a51f91-084e-4298-9c85-73c5ab3674da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "b8cbd18b-8d9d-4f54-aa77-7e842baaecbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "644b0629-7353-4131-a830-7eddf850252b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 199
        },
        {
            "id": "5c394d4f-1863-4d24-8241-041e18e88e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 210
        },
        {
            "id": "258dbe09-a49f-4375-ab87-3072ff69b6b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 211
        },
        {
            "id": "0662209f-3640-4559-9cff-bb7d352a29aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 212
        },
        {
            "id": "b3b5a006-1fe8-4d26-91e1-835409d99353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 213
        },
        {
            "id": "d3d57591-3480-45df-8c5a-3b0339ae47dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "05aa5bdb-6d12-4187-80d3-9c6555f774b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 216
        },
        {
            "id": "fd248cfc-1638-4654-b27f-10aef1209430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 262
        },
        {
            "id": "bbaa8c97-57b2-4ddc-9827-e42a03d73324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 264
        },
        {
            "id": "8c58a262-fb05-4457-bbb9-49efb57c16bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 266
        },
        {
            "id": "f0d528f2-d4b0-41fd-884d-da81511c682f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 268
        },
        {
            "id": "bcfcb81d-1cdf-4d43-82a3-c9e1ded1a777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 284
        },
        {
            "id": "8fe2444d-4c4d-4595-8b49-f5747b362253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 286
        },
        {
            "id": "ea4b76c1-b7eb-464e-a406-05a5081896df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 288
        },
        {
            "id": "57aa9159-c11c-4b06-9ac7-822bc88f15fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 290
        },
        {
            "id": "249bc363-d644-4a2a-9323-aa00ac58eec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 332
        },
        {
            "id": "f44f2613-852c-4a12-bc99-bc6f7f2ccac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 334
        },
        {
            "id": "a8af8a56-1c85-4615-a828-4e22da32e8a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 336
        },
        {
            "id": "d30beaaf-62d8-428d-b75f-0aadab2e202a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 338
        },
        {
            "id": "a018ab72-d7d6-41d9-9fea-81c1c77e099a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 416
        },
        {
            "id": "7a7923d5-3c8d-4bff-9828-cb4e5e0f2ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 510
        },
        {
            "id": "652c8000-3f93-4172-8951-f4a8d1eb01a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7884
        },
        {
            "id": "597a8dcb-5f52-4d2a-829f-cae4536205fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7886
        },
        {
            "id": "273a4ef0-3afe-4ea5-b530-5ad09325902c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7888
        },
        {
            "id": "840e3d9f-c741-40fc-8be5-b2c15d366400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7890
        },
        {
            "id": "ef3a6419-187e-4942-a0e2-d73d7d186a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7892
        },
        {
            "id": "93a3a513-61b6-4178-a1d9-7da329bd9793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7894
        },
        {
            "id": "b23ef0a3-1379-42a6-bcaf-9c3c829e1c6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7896
        },
        {
            "id": "c51ec562-399a-484d-8854-03b60b04140f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7898
        },
        {
            "id": "985f286f-467f-415f-a99e-089850cda05e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7900
        },
        {
            "id": "7868ed82-9685-4eb4-89bd-e496c325fc36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7902
        },
        {
            "id": "7e95d953-6376-49df-bc5a-890bee40e063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7904
        },
        {
            "id": "ce4dd55d-e6f7-4e3f-aa5f-11c862b8834d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7906
        },
        {
            "id": "209a24a6-9449-4d3e-afa6-0bd8a3b27dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "fd99973e-e672-470a-bbe4-44a9a76383fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "3c4af6da-4428-4f24-b205-656fc756e127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 63
        },
        {
            "id": "4cce2a7f-70ba-4e74-97dc-bd58e8d1197b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "60c64008-5ec3-4826-89dc-c6cbd2aef187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "cc4fcd43-7d0d-4d43-a2ad-786cc5adca3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "ce009180-c900-4848-84f3-d2237dcef7b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "85c62555-f0c5-4a6b-b889-2a6ddba32439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "4b9871c2-a320-4283-87dd-c3b97df78a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "4d85278e-5aa1-436e-a7ee-5e5ea0683582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "b1c82814-abbc-4ba8-bccd-1faeb1a72e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 100
        },
        {
            "id": "ad5ed553-345a-4f91-a050-5afe91d29d0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "3ed7925f-13da-49ad-82f3-c4f4fdd42001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "18f75827-8027-4271-8941-f09a935f553e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "9a6678b4-9b10-4245-9f88-682b7c992344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "d64ce97e-b949-4e18-b31e-dd9a7d6c4610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "c9b36f27-948c-4d80-97b1-b545d8f21477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "0d151ef2-e075-4680-830b-062186dc3832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "e50d6f0d-40c1-4c5c-9e1b-58e0023fe9ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "09fe90fc-d974-42ca-9978-2eb157cd56bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "c7b19727-b9ef-40f2-b563-fd0aa1e089a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "3cfe7e59-3158-45f9-81f8-37b2a3a4f8a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "6482a8d8-8d9d-4531-acd7-c5f231d035f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 192
        },
        {
            "id": "4114e518-0663-4cd5-ab8b-436d93d65371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 193
        },
        {
            "id": "cf6025dc-be3d-4e04-b9f0-808c8cec27d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 194
        },
        {
            "id": "9775f438-9873-44a5-b6f8-eec5170e105b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 195
        },
        {
            "id": "0bd3d7d7-f2ff-4e04-a844-68c703864112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 196
        },
        {
            "id": "8f90faac-20c6-41f3-9716-9720864700ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 197
        },
        {
            "id": "4ee6616c-2ff9-4b54-844a-fb5397afd476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 199
        },
        {
            "id": "47532c5b-15da-4130-8818-8f4834614847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 210
        },
        {
            "id": "1f83999e-b174-40c4-b567-c31bb0a6b9d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 211
        },
        {
            "id": "a8102704-8042-489b-b4f1-a73a1bba0bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 212
        },
        {
            "id": "23ebd8a9-acf5-4b34-9068-de6adac9c149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 213
        },
        {
            "id": "9ea71057-cdd6-480a-9de8-94571f8e4ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "a17e7a86-f221-4573-a416-2703e2450d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 216
        },
        {
            "id": "8e13a27b-eca9-424e-aa14-190eaf36678b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 224
        },
        {
            "id": "b67de852-d746-4422-875e-50fcdaf65245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "ed2a819e-4db6-4404-aa14-5796f01f9c4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 226
        },
        {
            "id": "c7ad6729-5610-4d19-85bc-df95f02adcfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 227
        },
        {
            "id": "044ed460-9292-48cf-b9f9-eb737b6702c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 228
        },
        {
            "id": "47156536-ed40-4f26-95a7-db32f8830040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 229
        },
        {
            "id": "deb84e22-24ac-42f0-831a-9088c2ee3bed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 230
        },
        {
            "id": "337682a5-3508-4f4a-94ae-825f1d23b7d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 231
        },
        {
            "id": "6322178a-749b-4297-95c7-b4c129d6cca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 232
        },
        {
            "id": "968779e7-3561-42ca-8057-80ed4276eb07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 233
        },
        {
            "id": "d72b4f1b-46a2-40c3-b47c-550ee3c39f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 234
        },
        {
            "id": "1783af08-cf2e-4a1b-b861-781bdf6be524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 235
        },
        {
            "id": "3460857a-3bc5-42ad-ae48-69c37fcf8ee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 242
        },
        {
            "id": "6af94d8d-9c33-4d9d-b252-75ac1cc32669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 243
        },
        {
            "id": "d244a240-0f36-4cab-8469-05811d4bb715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 244
        },
        {
            "id": "16ec7d8d-3d78-4cfa-846c-eb51e66c4076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 245
        },
        {
            "id": "330fc9af-c5d6-444e-b23e-bace8b4ab8f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 246
        },
        {
            "id": "d04ed046-1ff4-4085-ac0d-7ce24aa27add",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 248
        },
        {
            "id": "c4e8394f-8709-4cc1-bcf8-a2ca12f99d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "63a9a429-4394-4974-a628-b68142f0d5f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "0e3baa39-b6ee-4d73-833d-d94ce6500850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 251
        },
        {
            "id": "e91b8ad9-aa81-44ed-acd1-2916f4863846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "07302292-2767-4d3d-af13-976879547fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 256
        },
        {
            "id": "2104353b-ef17-4cf5-aaef-c6e63ca449b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 257
        },
        {
            "id": "ce507328-512f-4ca4-b8d8-ea353476d827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 258
        },
        {
            "id": "c88a62da-17b8-4261-9a91-6e5980edcf26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 259
        },
        {
            "id": "bcdcf1ce-2d18-40bb-907b-9f1d61f33ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 260
        },
        {
            "id": "94c100e0-f463-4b1b-a838-fd6fec88a5ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 261
        },
        {
            "id": "04f68dba-ff70-4d94-af46-0bcf19b3bc29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 262
        },
        {
            "id": "85c1e695-9676-482b-b18a-b5cdadbe866d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 263
        },
        {
            "id": "dc6e7b48-4575-4b93-a211-c11add8978d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 264
        },
        {
            "id": "978c8f5d-6d90-4651-8765-6b5ecdb5b8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 265
        },
        {
            "id": "c637ce71-5235-48f7-b1c0-708f7ce31cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 266
        },
        {
            "id": "bc2ed9ef-45a7-489e-bec3-2a64516c650d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 267
        },
        {
            "id": "4258ffbd-6c67-46a1-a103-3ddb572ac739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 268
        },
        {
            "id": "f4c63c5d-b2e9-463d-a42b-5f4149612d96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 269
        },
        {
            "id": "b8aebde5-0c24-43b4-8b10-0d1edd9a7d96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 271
        },
        {
            "id": "29f48c42-e4a7-43ac-9f7a-6dfd4db17a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 273
        },
        {
            "id": "357d3664-42aa-4215-97b4-9a95a2497b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 275
        },
        {
            "id": "6f715144-701f-4937-8b0c-41e3b4f2e2e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 277
        },
        {
            "id": "f745afc4-2409-4c62-9823-5f5e411a4286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 279
        },
        {
            "id": "c30f08b1-5b5d-4170-b261-113fc1f3e567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 281
        },
        {
            "id": "e4dfef0c-243a-42b2-8912-2d37fcc6152a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 283
        },
        {
            "id": "289d55ea-9f5a-428e-ad9f-9c5a464aafa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 284
        },
        {
            "id": "5ad405fd-60af-4dfa-8616-df8182289564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 285
        },
        {
            "id": "d66c7d4b-0abe-436f-83de-421b09103029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 286
        },
        {
            "id": "44a73582-8e90-4d58-bccf-07a28951dc02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 287
        },
        {
            "id": "bb6e2bf5-2d6b-4e28-a9bc-0f366aa9c020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 288
        },
        {
            "id": "a007a4cf-feda-4eb9-a2b5-f560dbe41be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 289
        },
        {
            "id": "453b6adb-0454-44dd-9b57-88921de7c13e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 290
        },
        {
            "id": "479d194a-0184-493d-9d4c-9f0ad4ae7feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 291
        },
        {
            "id": "815047ef-3030-42f8-a8a8-0737c6ad9226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 312
        },
        {
            "id": "98291e09-1bf7-46ef-b9e1-4c98ddda3871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 324
        },
        {
            "id": "1a6cc1a9-bc91-4368-88b1-d32f6cf6b1ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 326
        },
        {
            "id": "ec6bf0d0-55ed-4ed1-b0af-8eca695f556c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 331
        },
        {
            "id": "16c23294-b104-45f4-9360-a4b59720f8ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 332
        },
        {
            "id": "cbb75591-a619-41c3-884e-de9f11b09367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 333
        },
        {
            "id": "515287c7-e79b-45dc-bbc0-640fd715428b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 334
        },
        {
            "id": "d5a6954a-2963-4ecc-b07f-2ae464c4195a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 335
        },
        {
            "id": "17355c68-19c2-498e-8ffa-c4490b9f1544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 336
        },
        {
            "id": "f9937345-2d4c-4ddd-b35c-b123a1f799c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 337
        },
        {
            "id": "3928e56b-d605-4e46-96f8-907122535b8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 338
        },
        {
            "id": "9acc13b9-4234-472f-9360-bbb3c60af7c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 339
        },
        {
            "id": "f8313ccb-1235-4aca-935d-174709c85565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 341
        },
        {
            "id": "25646cb5-b1ca-4999-bc56-009671c71bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 343
        },
        {
            "id": "e3001d22-cd7b-4593-8c0a-fdf51b6e3a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 347
        },
        {
            "id": "e1ea1aaa-3401-4940-831e-32a233478455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 351
        },
        {
            "id": "f13a914b-81a4-4b45-a19c-81725cce9d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 361
        },
        {
            "id": "5e38fdd0-0cb7-4f1d-892c-1451ddea8720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 363
        },
        {
            "id": "ef3994f8-318b-4b37-8876-dd9b6d4f8af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 365
        },
        {
            "id": "2295dce2-4fbe-4ee7-bdcf-971030eedefc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 367
        },
        {
            "id": "6914d915-2257-4178-88e7-5d8523f1bdfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 369
        },
        {
            "id": "b6189170-30b0-4014-83f6-f9833929eac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 371
        },
        {
            "id": "2d409aca-0edd-4b74-aacd-be22e81eb9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 378
        },
        {
            "id": "0dad9174-cc99-40fc-ab82-b713b0a48f62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 380
        },
        {
            "id": "7c005add-0b1a-44a9-a2c0-be2a40160bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 382
        },
        {
            "id": "8c82bb9b-2d49-4fef-8a75-152a7c15bcfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 416
        },
        {
            "id": "aa5013aa-1535-4496-ab57-055b5c62841f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 417
        },
        {
            "id": "ad8b0d73-a5c4-4269-972c-52865a8610e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 432
        },
        {
            "id": "8349f19b-ff04-4629-ac01-eb6a5bf3ce1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 506
        },
        {
            "id": "7be9ac51-8b7d-46fa-9f18-a01546e702eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 507
        },
        {
            "id": "87cdfe33-0b61-4704-a599-d858d20caa66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 509
        },
        {
            "id": "091fd351-ad5a-4b51-952a-5726d2b4f068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 510
        },
        {
            "id": "604a858d-d44b-47c9-ac51-36f22229f464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 511
        },
        {
            "id": "35f5f368-906d-4bea-8847-3af0150c272b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 537
        },
        {
            "id": "51168999-0d73-40da-bbc5-aa34c59e9dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7680
        },
        {
            "id": "ebc67982-3587-478d-aa6f-2db635f975e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7681
        },
        {
            "id": "18244840-64d0-42ba-9467-c30ccb1b1854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7743
        },
        {
            "id": "b915c0e6-ff03-4f77-bab7-c70284666c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7840
        },
        {
            "id": "b22e739b-d8cf-4955-bb1c-b6f68e8ccfc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7841
        },
        {
            "id": "17f8f787-5990-46bc-bdfc-4d734877d005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7842
        },
        {
            "id": "dfae4493-6be8-4119-8f56-e1247b23db70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7843
        },
        {
            "id": "de370e00-2f48-4589-af71-b55491ecaabe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7844
        },
        {
            "id": "863e1677-45d5-471d-aae0-e30b2a818438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7845
        },
        {
            "id": "5f38fa94-f20d-43a8-b650-19ed904e8593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7846
        },
        {
            "id": "fa090573-7952-460d-934f-46ff7c533037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7848
        },
        {
            "id": "fc3c58b1-fa11-47f4-bc1d-748fbc3afdcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7849
        },
        {
            "id": "cee5fe1c-ec1b-4c40-81e0-447d4ebd985d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7850
        },
        {
            "id": "65a22e31-0ff4-4bcc-9491-4c9d19ed0195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7851
        },
        {
            "id": "18c0ddd0-9243-4a98-a0ac-1f921c1bec8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7852
        },
        {
            "id": "90326fb2-7688-4217-990c-012ce45c2e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7853
        },
        {
            "id": "0bb17a9b-8be6-432e-ba4f-f0bbf161d911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7854
        },
        {
            "id": "ee4eaafc-38c4-4d93-b4b4-0e6736cb1650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7855
        },
        {
            "id": "e5119e21-21f3-4ba6-81f4-13841b675b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7856
        },
        {
            "id": "7601f285-d7f7-4911-a1e1-cbc1e596e541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7857
        },
        {
            "id": "00048637-03d3-4736-9bc5-a695dcb8236e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7858
        },
        {
            "id": "fa5a0e03-9c4f-4cc9-92f6-33799bcbb003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7859
        },
        {
            "id": "d6d34a9c-5739-4b22-8713-9ce5ff6b8c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7860
        },
        {
            "id": "2e4ff20d-9d70-4ab2-8a9d-a728859234a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7861
        },
        {
            "id": "2c72a8ee-4b6e-46ef-86d8-288c56252028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7862
        },
        {
            "id": "612891d8-d7fc-4bfc-96e6-e90fbb6ab034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7863
        },
        {
            "id": "19bacb7e-cf2f-4cb1-820a-436b0fb26ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7865
        },
        {
            "id": "2b8594a5-b647-4ae6-9de2-8867be3e1150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7867
        },
        {
            "id": "ef369fe6-8da4-4e5d-8460-8eb9449a6629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7869
        },
        {
            "id": "423be608-1814-4ff7-9258-415030e015f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7871
        },
        {
            "id": "80a6d6c4-3abd-4bcb-9153-28a308a3c8cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7875
        },
        {
            "id": "e041dfc7-6bb1-42a1-9fdb-c0c5dadc5766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7877
        },
        {
            "id": "4aa753e1-db6e-4197-84ae-3ab138dbe0d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7879
        },
        {
            "id": "1ed83767-ebf0-468a-90ed-d6c248bf8b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7884
        },
        {
            "id": "61244943-9beb-4620-87e1-707efb1bc611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7885
        },
        {
            "id": "5deddb8a-0fa5-4326-9e5a-6350887b96f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7886
        },
        {
            "id": "687ada08-74fb-46fe-8e34-e924a912da6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7887
        },
        {
            "id": "73faf438-419f-4028-9e34-f5ec4056f839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7888
        },
        {
            "id": "176c817c-285d-4148-9b12-eef978d89719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7889
        },
        {
            "id": "0dd1ca44-d940-471e-8108-97105794430e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7890
        },
        {
            "id": "4c365a46-35d0-4659-af5c-1639d43c7395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7892
        },
        {
            "id": "202d998c-8bc5-4e6f-9bde-6a6d65dae616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7893
        },
        {
            "id": "6fb8e1e5-eac0-4e22-8196-9e8e458c0b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7894
        },
        {
            "id": "91d4fc29-12c3-44a4-af11-4a6a58fba89c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7895
        },
        {
            "id": "f6e2e31d-c229-478f-a222-37eb684bda93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7896
        },
        {
            "id": "d73fcbe9-df30-42db-860a-883ddb77c29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7897
        },
        {
            "id": "8e751b59-b316-43b1-848d-8f4e436a0031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7898
        },
        {
            "id": "6f6660b7-8109-46df-b12e-f3c833fc7260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7899
        },
        {
            "id": "2082d117-0742-427d-93b0-9cbbc7080066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7900
        },
        {
            "id": "763dcf3b-dea3-4c85-9915-91824224f54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7901
        },
        {
            "id": "a1b21033-bcfb-4b7f-b2e6-1af13cd75ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7902
        },
        {
            "id": "052e9655-1372-4663-8c54-fa3317890803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7903
        },
        {
            "id": "c36dded7-0d27-4329-9a0e-d9ad5d5bdcc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7904
        },
        {
            "id": "d058a778-e705-4614-93e0-08e1da7da116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7905
        },
        {
            "id": "28a386df-c65d-41b4-8db6-9b3904a25a1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7906
        },
        {
            "id": "f338db63-7e38-436d-982c-924fc435b63e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7907
        },
        {
            "id": "302176e6-f669-4b02-8ebc-e5c8e16be069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7909
        },
        {
            "id": "b0dac56b-2d10-48e6-95a5-beb875e0be9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7911
        },
        {
            "id": "b130633e-ef72-476a-b45c-75d4ffa4bd11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7913
        },
        {
            "id": "b45a4b26-66dc-46d3-99f6-23b178892999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7915
        },
        {
            "id": "417d82bc-811a-4b27-8349-23abba179ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7917
        },
        {
            "id": "5bca4797-91d0-4665-99f5-db10c894c1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7919
        },
        {
            "id": "2389a934-a28d-40ad-a545-2f62d0a25ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7921
        },
        {
            "id": "eec697de-12fd-46d7-803d-55e4186b8c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8218
        },
        {
            "id": "c5fced8e-9c08-4595-9f03-4cf6b2a0bb96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8222
        },
        {
            "id": "f07f6d0a-ea9c-4dfa-a2f4-5a50c1609bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 91,
            "second": 74
        },
        {
            "id": "6afc4b58-33d3-4a83-a42c-35c9ed840bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "e775c4c6-e6f5-4fbf-9a02-8a0c54dd0fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "4fed2d56-d17e-4e14-8256-3deb222cf828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "509a40ec-0bf5-4e22-89d3-a740f648ae66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 121
        },
        {
            "id": "b26211e5-091a-484b-b819-b649a10f0709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 253
        },
        {
            "id": "1facadc8-76b4-423e-9f13-26c5d34f84a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 373
        },
        {
            "id": "16870425-94aa-432e-af60-dca5f24eccc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7809
        },
        {
            "id": "1288f914-e9cf-4996-93af-7ce0dca96817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7811
        },
        {
            "id": "42cd680b-b16c-4c3a-aac8-5cd99c16fb19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7925
        },
        {
            "id": "e943ad67-cace-4f59-924c-ec7f5f015524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 34
        },
        {
            "id": "8357696c-2a6d-44d9-877b-e73ac3ebcfa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 39
        },
        {
            "id": "bb83ef40-0cab-4442-ac9d-7449c1728771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 8217
        },
        {
            "id": "6003b761-4f32-47fb-a288-fe0738d36bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 8221
        },
        {
            "id": "8b8002aa-2404-4a62-a7dd-10a35336e528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "87508234-3840-41bb-a6d5-b8d6205c55a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "c475cc15-e927-4f6b-b39d-a4b436a48613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "e4a11752-7de7-497c-828b-0b4eeb2b40fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "cf43ad3b-ba06-4ce8-bb83-ed7647dbff8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 253
        },
        {
            "id": "7425dee2-4c3f-4eca-afb5-d35a2b0b7053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 373
        },
        {
            "id": "0ad3722d-171d-4937-a824-86c6429efe5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7809
        },
        {
            "id": "6c3a7098-8bee-464a-b336-6decf693c377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7811
        },
        {
            "id": "d01069ab-717b-4278-aa38-bc88722ef0c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7925
        },
        {
            "id": "36ab5e69-a911-43ee-ac13-c0cd572fdab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 34
        },
        {
            "id": "0cdb0e77-1c2b-4e25-9fa2-005f806f5cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 39
        },
        {
            "id": "4ef221d1-eb50-448d-b84c-0b525f4996c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 8217
        },
        {
            "id": "ce4550d6-5f51-4f50-998d-300b2e5eb624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 8221
        },
        {
            "id": "bbe0dc6f-c850-4805-8bae-a54aa78b71e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 99
        },
        {
            "id": "97fcb7d9-15c3-4cad-b115-d2ec511c4e97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "5feba94b-cf82-44c1-95bd-ca1300a11a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "b010bb56-a28b-47ae-a7ab-4c5d77742139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "012f4270-d707-4f81-bed6-82c7be3099ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "e6cd55ab-5b43-40cf-8831-ab4778b8653d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 224
        },
        {
            "id": "2abe7893-5cfd-4ec4-b264-c146ebc38f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 231
        },
        {
            "id": "5bbad50b-012a-4111-97e7-4c745e3aaeb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 232
        },
        {
            "id": "1a10ee3b-486c-4642-9023-c2b2f45b8a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 233
        },
        {
            "id": "485f0bab-5aa4-4d9a-ad26-e436d0c59a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 234
        },
        {
            "id": "95a41003-5f95-4d6f-af9d-ce4bffd3f41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 235
        },
        {
            "id": "701442fb-5ccd-47c8-8f3b-52a8aa563be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 242
        },
        {
            "id": "87a73d3d-639d-4e90-b891-da369aaa1d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 243
        },
        {
            "id": "5210e3bc-a58b-4620-a0a3-652c9ba8a83b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 244
        },
        {
            "id": "9c94900c-2329-4507-a080-b5fe18615068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 245
        },
        {
            "id": "14c2e191-8b8a-4347-b920-43a28d1bbdd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 246
        },
        {
            "id": "d433fc1a-92b6-47b0-af8f-2033c770ecf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 248
        },
        {
            "id": "f2fd0667-53f6-44cb-9d2c-9ba7c21ee231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 263
        },
        {
            "id": "0b410388-fdf9-406b-82cc-c4c9b0841ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 265
        },
        {
            "id": "267ba88d-1f43-4557-a70c-a29e2484782a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 267
        },
        {
            "id": "745f4ff5-20bc-400b-830e-d5ab51d0ba12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 269
        },
        {
            "id": "33d1f736-06f9-4629-8a8c-fe9283fc1e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 271
        },
        {
            "id": "d7aa9464-118e-40e6-99c1-d4cdedb3a56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 273
        },
        {
            "id": "a010f8d7-797c-4446-95fc-7b6c006a5e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 275
        },
        {
            "id": "c1288d7f-4a2d-41aa-a8e2-a32e0e0a2887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 277
        },
        {
            "id": "4c36ef5f-bf5a-48ed-9fde-c4bab4a042d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 279
        },
        {
            "id": "879cc164-a984-449d-887b-b701103b00af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 281
        },
        {
            "id": "01822738-6d61-4ce6-af35-17a22dad0f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 283
        },
        {
            "id": "8a6108cd-4f02-424a-aa1f-383c8a349a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 333
        },
        {
            "id": "5382ae18-1fc7-417a-8f58-853f6e8331a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 335
        },
        {
            "id": "a7fa4052-93fd-4b9f-8460-80f484f7a7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 337
        },
        {
            "id": "0ac1328a-a738-4d45-9f60-54df2617c2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 339
        },
        {
            "id": "ef8b88c7-c4fc-46e8-9941-7c953fd63f7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 417
        },
        {
            "id": "791d4588-e944-4d49-b319-2184341b4245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 511
        },
        {
            "id": "3f4b360e-f2f0-419e-a7b5-ae063e614aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7865
        },
        {
            "id": "b68bec79-0053-46f4-adc8-2cf8bfdd7fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7867
        },
        {
            "id": "6112b4f0-ce96-4a9e-8332-e367a1df5728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7869
        },
        {
            "id": "7aa785ec-99c6-490b-8883-200e81a898db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7871
        },
        {
            "id": "fe98b668-4693-4c91-aa04-b51953387c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7875
        },
        {
            "id": "b68434ee-7156-44c9-b769-a3bb5dc90db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7877
        },
        {
            "id": "1348067c-5cbd-44b8-ae2b-106f65570ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7879
        },
        {
            "id": "2188f0da-22df-4a5c-9772-942c566685a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7885
        },
        {
            "id": "d82fa16b-aa77-4f0b-8da5-8bb43349acb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7887
        },
        {
            "id": "cfaa2e30-984f-4839-8628-3f00a1e0f808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7889
        },
        {
            "id": "dc16f462-6868-46ab-b432-4653f22f8554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7893
        },
        {
            "id": "12e7ab1f-c8d7-41dd-9444-75971625e1af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7895
        },
        {
            "id": "d8818560-945c-45ea-86a4-9a32a41e8da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7897
        },
        {
            "id": "76dbf38b-617d-444d-a8be-0b1e0bb14803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7899
        },
        {
            "id": "fa98cfcd-2849-4290-8d04-4452e3affd18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7901
        },
        {
            "id": "992866b7-64fe-4509-9940-b245108e98b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7903
        },
        {
            "id": "49ef60c9-e3c3-4ec1-a231-cb2c5268720b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7905
        },
        {
            "id": "ca0333a8-b75d-4554-a253-12fcd1bf4acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7907
        },
        {
            "id": "d57e08e5-f66d-4bf9-bffa-21b1b1aa464b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 118
        },
        {
            "id": "44192e8b-2d9a-4430-9889-c76ea59ddd69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "f213ca45-5ab4-47f3-908e-ef2925fb0368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "dddeaa5d-cd69-41d5-9185-c8d0a8d630bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "f1f99c76-f0ac-4ed8-a859-6172de43d892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 253
        },
        {
            "id": "218d0032-653c-4e9c-af59-91fa090c4633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 373
        },
        {
            "id": "e8c6dd0d-6ce7-4fc2-ab1a-0691aa7049aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7809
        },
        {
            "id": "344d8936-74e7-4537-868f-30fff335541a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7811
        },
        {
            "id": "f2a2568f-0033-4691-8abb-5691766b4f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7925
        },
        {
            "id": "7cd2f0da-1482-4eb5-99e0-90d4512b414d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "31574832-a55b-44b4-ae1c-b9645fd60312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "42a0b9d9-966a-498a-a189-b72eafeaac66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "606168ee-d1dc-4791-9aeb-ef518acfb64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "6c75afef-ed79-49f5-9603-57a2a9246539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 253
        },
        {
            "id": "7bfc3108-d717-4017-8232-e4512fff8483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 373
        },
        {
            "id": "63f012a1-f93d-44d2-8438-bba79ee7a555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7809
        },
        {
            "id": "4791ef15-d579-4399-bb20-e000db105bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7811
        },
        {
            "id": "4578b68a-4077-49ee-9cbc-73475a6ae08d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7925
        },
        {
            "id": "6efa635c-03d5-4e9c-8e78-5087052727f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 34
        },
        {
            "id": "648f6122-d96c-4bc9-8cc2-e02835b1ccbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 39
        },
        {
            "id": "dca0ca77-1168-4254-a31e-9b6e8072000f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "42956603-d0cc-4480-8a39-77c96e21cc9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "2baec1f5-1001-4c3d-a7ad-3aa7ffbdbb8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 100
        },
        {
            "id": "22a43081-95b8-4d6b-8cff-83ea586fa2eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 101
        },
        {
            "id": "79e4967d-eb79-42e5-8ee5-e33c9ba70600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "4b028dce-c782-4be0-98e0-0029a5dbb68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "eb3fd055-61a2-4eb3-9587-d028559a08d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 224
        },
        {
            "id": "d6728d4a-a2e8-4ede-ad68-d3f1a745e6b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 225
        },
        {
            "id": "9289703f-5ec1-4f9a-9800-77d5859d7d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 226
        },
        {
            "id": "95a6dcfe-601b-40da-b54b-c5c02731bfbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 227
        },
        {
            "id": "72e789f9-4be8-44c9-9a26-abb7a7613791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 228
        },
        {
            "id": "8930af5f-2af9-4b9e-a174-617349b0892e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 229
        },
        {
            "id": "69e14a26-94ca-4fff-927e-3c1d944beb30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 230
        },
        {
            "id": "b3880597-2f52-4477-8d29-63644baff40d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 231
        },
        {
            "id": "cbc24eb8-cc99-4360-933f-8333f8bfb180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 232
        },
        {
            "id": "bcb951ca-e053-4978-875a-b3c0872c40a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 233
        },
        {
            "id": "46290118-a909-4fb7-a1c6-d8f6c9163692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 234
        },
        {
            "id": "8724bc4e-6ba5-401a-93ea-9375c136d468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 235
        },
        {
            "id": "d3056cda-8015-4e7e-94dd-b707ce5fa711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 242
        },
        {
            "id": "9cdcb452-45e6-481a-b7ad-4ef60becf483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 243
        },
        {
            "id": "f45cce60-1a45-4be9-b452-587c2399d313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 244
        },
        {
            "id": "86a3e00d-6559-465e-88c8-37d211585bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 245
        },
        {
            "id": "743984de-cf0b-4104-bd32-929c786dec35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 246
        },
        {
            "id": "cda3f08a-08dc-40f1-994f-dc9c1e5b6f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 248
        },
        {
            "id": "60cc5b4f-f0ab-474a-ac1b-bc164325ad70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 257
        },
        {
            "id": "2984693a-34a8-4ba6-9d39-1a4db8024327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 259
        },
        {
            "id": "a33b522a-2d63-45d7-a1db-3a7c196e4b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 261
        },
        {
            "id": "b47cc2a0-606d-4181-88e0-1ca97970186e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 263
        },
        {
            "id": "9443d956-2e5c-47a2-bb1b-01c87356ca35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 265
        },
        {
            "id": "25d960eb-e403-462a-96be-545fe0a97319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 267
        },
        {
            "id": "18c47f50-6a20-4040-980d-4f4205780e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 269
        },
        {
            "id": "961a9dc7-c60e-45cb-a939-e6578d218e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 271
        },
        {
            "id": "50e7d7f9-359d-486b-baa8-f3432b9a1a4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 273
        },
        {
            "id": "d3a92dd4-e09f-451e-9074-f680c1403826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 275
        },
        {
            "id": "049a2bbe-f1f5-46da-ae95-488e8c3a8f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 277
        },
        {
            "id": "0d46b1a0-7ac5-45e1-9c84-2b2a0b1e1da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 279
        },
        {
            "id": "0741bcf7-4554-4164-91d4-b0090e8abf77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 281
        },
        {
            "id": "1e991a0a-0d8b-48e2-8504-02aa9555ab43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 283
        },
        {
            "id": "1d1b8b1e-826f-4e0e-81d7-d5f22cdd0c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 333
        },
        {
            "id": "67d0ebcf-7f27-4db1-9b2d-184c4bf9ee48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 335
        },
        {
            "id": "d63331fa-f7d2-4dbd-b142-21a2a571c31b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 337
        },
        {
            "id": "83afd80d-daed-45b6-ba7e-56fc0458b79c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 339
        },
        {
            "id": "b6b9305e-ae48-4137-ac5c-a4bf737dd7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 417
        },
        {
            "id": "a0c9feed-9b32-4c9a-92f5-2554e3f833c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 507
        },
        {
            "id": "b58bc433-2165-48b4-81c1-03eda5942931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 509
        },
        {
            "id": "a43b8c72-7232-4514-bf55-05ac61a35110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 511
        },
        {
            "id": "7d5516a3-2a9e-470a-afae-520b57e76531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7681
        },
        {
            "id": "f47d13c3-f982-488c-b14d-743699af0ed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7841
        },
        {
            "id": "22251010-0440-4a7f-b5d9-8a6a6bfbc9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7843
        },
        {
            "id": "bb710b4d-bae0-4739-bff5-dbefde1b890b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7845
        },
        {
            "id": "846ec004-abe0-421a-b435-cebe1dfd50cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7849
        },
        {
            "id": "f043849e-17b1-4a77-a55f-eeb62fcd6215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7851
        },
        {
            "id": "ea3e6db5-a2e2-4a31-87eb-4a640aecb22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7853
        },
        {
            "id": "7c8984f4-0315-4036-b2e2-d10174cf5347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7855
        },
        {
            "id": "8d1abe5c-03f4-480a-9390-e442391dd777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7857
        },
        {
            "id": "20873ed4-aa0a-406a-8dcb-38c19f1ec845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7859
        },
        {
            "id": "6ba3e840-fd95-4048-bba8-8d5a5cf2a5bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7861
        },
        {
            "id": "e947e1f3-5511-4c3b-a6ac-09d225c88ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7863
        },
        {
            "id": "36d7b261-eb0a-4265-be1b-76b5ac1ff753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7865
        },
        {
            "id": "d3541da9-3acb-4409-84f0-ac3330671975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7867
        },
        {
            "id": "982f2471-d2a7-4bd2-b5a3-5562f228a275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7869
        },
        {
            "id": "c6d27e1b-75d4-469c-8bb1-9cad6ea21193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7871
        },
        {
            "id": "d593748c-f254-4958-b23c-3513a3a59107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7875
        },
        {
            "id": "b4d366d0-a154-4722-82c3-6356d610ad04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7877
        },
        {
            "id": "514ad06b-68b6-4c38-84f5-c3524fa548b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7879
        },
        {
            "id": "5b6ea8d3-fbb8-4689-ab30-f3fc21a6a0c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7885
        },
        {
            "id": "68cd729b-c286-40cb-8815-b0a3505bc1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7887
        },
        {
            "id": "a9d48444-66ed-48fb-a2ab-06e686dfd67c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7889
        },
        {
            "id": "9ce727c8-165a-4d78-97f9-61ed217fa7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7893
        },
        {
            "id": "0c567a90-5519-4d3c-9855-c9d9d6b9e854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7895
        },
        {
            "id": "28133bb5-e177-415f-92f7-b66a0a253a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7897
        },
        {
            "id": "0991eb05-2d45-4586-993a-f9ce86da3688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7899
        },
        {
            "id": "a2faf7ca-9140-452f-9d86-4d9fc43936ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7901
        },
        {
            "id": "bcbb1281-948f-4ee5-a400-a0344405c186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7903
        },
        {
            "id": "8769df2b-134a-4906-8c52-9accbcdbac9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7905
        },
        {
            "id": "81560e3e-7349-4d5e-bc16-f0d2aa53b158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7907
        },
        {
            "id": "3e267930-c562-466b-80d1-c306b3971269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "36323f48-e540-4c71-b473-93831559d46d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8221
        },
        {
            "id": "a6837e62-6172-4d26-8238-3d248a905d56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 34
        },
        {
            "id": "42f8eecf-31b5-4271-9940-5e3923050b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 39
        },
        {
            "id": "9b37a577-cd14-480c-991b-d1cd17dce3e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8217
        },
        {
            "id": "6fdb4504-a731-439e-981a-0a525a3b085c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8221
        },
        {
            "id": "62c7fdc3-abad-4b14-bbc1-426809fd05d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 34
        },
        {
            "id": "b6331682-73a1-427f-a888-c52c57089373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 39
        },
        {
            "id": "f165851d-cfc0-44fe-8f6a-d0b0e12cf64d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "557fe054-8194-44ef-b0ea-318935abe7d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "8d24e503-8809-4f56-926a-c576d2163c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 63
        },
        {
            "id": "6bc68f83-4bad-41ca-8c44-0fdf58cb3276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 8217
        },
        {
            "id": "fb2efa45-3b33-48ea-9f72-cef3d2718695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8218
        },
        {
            "id": "9c4a210e-c128-4e61-a687-0f1e482c39b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 8221
        },
        {
            "id": "89172465-5c02-49e0-9a5c-f2d28deeb334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8222
        },
        {
            "id": "285d032f-60eb-4951-917f-e75eb04e90a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 34
        },
        {
            "id": "40e7f033-bf4a-4b36-b5a8-5c933255479f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 39
        },
        {
            "id": "d37a316d-d52b-4bf4-88f0-333aaab8577b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "1ba847c3-dd44-443c-a531-1fa46249fd2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "3758c6f9-7dbb-42c1-8ebe-5b6f85534f4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 63
        },
        {
            "id": "9e76b531-77b8-4f2a-9381-258b121597b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 8217
        },
        {
            "id": "ba81fa8e-f017-499a-b93b-4193c1d5e011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 8218
        },
        {
            "id": "177efbc8-ed8c-40d2-961e-4750ae31838b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 8221
        },
        {
            "id": "e5963aaa-e889-40a1-a0e6-332cfaa94389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 8222
        },
        {
            "id": "450bc9f9-e6e0-40f9-88bf-b6ac80ecf538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "e67dd33a-f9ac-4703-b540-c33db31ff0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "05bf8260-c3cd-44a1-b3a6-9ae56954a7c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "7f7b9872-60d1-4697-ae26-8e508da1ecb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "8d903136-a72c-4687-829b-4c594798cbf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "08e79b2b-a689-46a1-afd9-daf14d059d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 224
        },
        {
            "id": "c4a540a5-8159-4f1e-ac2f-eaac973e6e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 231
        },
        {
            "id": "97145192-fd1b-40f8-be9d-4b33a7addc27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 232
        },
        {
            "id": "2de45ac4-b589-4130-a576-e13d2426f0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 233
        },
        {
            "id": "c1c6c6f4-ec89-4ffe-b6ca-2a507bd5a371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 234
        },
        {
            "id": "105f7fd8-8af7-4b19-91ca-d45ca01686f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 235
        },
        {
            "id": "0fb77c41-e762-4540-b847-7526e4cb1254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 242
        },
        {
            "id": "ba92c972-50f1-461a-847a-234b837b6828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 243
        },
        {
            "id": "0a10b25e-1ac3-4ba8-8921-d9e8d6bcc94e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 244
        },
        {
            "id": "3893de14-c84b-4c94-932c-54c53ee2075a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 245
        },
        {
            "id": "9eb24611-9fbc-4e21-9f94-ca813958e26d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "6567c4ee-ab2a-452b-8cfd-caea64627ceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 248
        },
        {
            "id": "30dd9248-7f0d-4c68-aab1-782a5c65e1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 263
        },
        {
            "id": "1b3e77b8-507b-4814-a2ad-fa9ff54952d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 265
        },
        {
            "id": "5023c1f3-e087-4168-bfb5-c2cfa93a5a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 267
        },
        {
            "id": "a99ae710-c051-49ae-aec0-94f50cd8267b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 269
        },
        {
            "id": "c2caca18-2874-4f88-86fe-5b01963b1cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 271
        },
        {
            "id": "64cfed6c-7af1-4e87-94f6-e6f328cce565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 273
        },
        {
            "id": "c329c1c7-0b12-41a3-87a0-b3eaad3e65cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 275
        },
        {
            "id": "ddbca189-c78d-40e1-8ea5-5d9a5fe9813f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 277
        },
        {
            "id": "dd597c43-c91c-4c10-836f-11016b772458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 279
        },
        {
            "id": "fd9184b6-3625-4b9b-88c4-df99c6e7be04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 281
        },
        {
            "id": "fa65b9d3-9f5b-4c72-b6b9-3fda1ef917a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 283
        },
        {
            "id": "c5aa5e75-61ac-4d87-9fc2-fbc59fe71711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 333
        },
        {
            "id": "acca6e4b-ffdd-4a52-a549-4f2b69a431a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 335
        },
        {
            "id": "e3a996ff-460e-4883-abc4-f6b272fee27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 337
        },
        {
            "id": "d07137c5-315f-410c-a040-cc6954eb70ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 339
        },
        {
            "id": "4d405013-c275-4708-a9c2-715dfa1cc830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 417
        },
        {
            "id": "7f8503b8-8ac0-4915-8af2-c328ec5537db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 511
        },
        {
            "id": "5da50665-56b5-44c8-ba92-ac1c19a1935f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7865
        },
        {
            "id": "36f463d4-f6a2-4ac5-8718-ebc58b84f615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7867
        },
        {
            "id": "1af70c76-e2b1-46e8-96de-deea31b37c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7869
        },
        {
            "id": "8cedcf11-a554-4c2d-9809-a7658a96f019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7871
        },
        {
            "id": "e7cb88d4-9bcf-41da-b449-3b40b4f95b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7875
        },
        {
            "id": "d966102f-bcdf-4e82-b434-b9ca2b79eeda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7877
        },
        {
            "id": "134ed635-0528-40d6-9764-d2f82e90e767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7879
        },
        {
            "id": "b4145e2d-5fbd-4be4-a3b0-b664028dabf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7885
        },
        {
            "id": "485e5063-2838-4c8f-887d-0ee1ac0bbad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7887
        },
        {
            "id": "346a73b3-04af-4599-bda9-9c3774f94fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7889
        },
        {
            "id": "c3fe5f1f-31d7-4f80-87de-7c4ea97bac05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7893
        },
        {
            "id": "55d16dce-8a02-4cbd-b80f-39b842e86ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7895
        },
        {
            "id": "cc16ea15-7c34-4f10-a55e-5ee1aa967e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7897
        },
        {
            "id": "114beed1-62ec-4ccf-968f-1c4bc437338e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7899
        },
        {
            "id": "021e2cf8-a365-42f2-beca-ff282539e805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7901
        },
        {
            "id": "229f4599-24cd-460e-9160-cc432f3720bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7903
        },
        {
            "id": "703a8fb7-1dcc-46d9-a090-5573482d2e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7905
        },
        {
            "id": "934cae14-df7c-49bf-a1d7-3e4187bd0087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7907
        },
        {
            "id": "3bccbf61-85d8-4996-8e40-c311955b90f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 34
        },
        {
            "id": "e8ce09fb-24ef-4b13-89c4-4254e10e9535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 39
        },
        {
            "id": "860ef13a-5bb9-423e-8843-e249596b4f32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "dfa5e46c-519b-4961-92d2-09a9040b99a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "06f7c551-b5e2-416d-a63f-8b15197f954e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 63
        },
        {
            "id": "52f9128c-e911-4401-a8e1-c530e580243f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 8217
        },
        {
            "id": "eca7841b-b096-4569-bf2d-9380ec4fda53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8218
        },
        {
            "id": "2894226a-743c-4648-9125-b57be44384c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 8221
        },
        {
            "id": "12ed1cca-01d0-4c86-a095-92f581fb9ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8222
        },
        {
            "id": "cb8dc0ff-d847-4f5f-a899-fe83008a57ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 123,
            "second": 74
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 32,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}